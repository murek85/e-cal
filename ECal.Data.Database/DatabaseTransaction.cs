﻿using System.Data.Common;
using ECal.Data.Common;

namespace ECal.Data.Database
{
	/// <summary>
	/// Implementacja transakcji dla źródła danych opartego na bazie danych
	/// </summary>
	public class DatabaseTransaction : IDatabaseTransaction
	{
		private DbTransaction dbtransaction;
		private DbConnection connection;

		private DatabaseTransaction()
		{
		}

		/// <summary>
		/// Rozpoczyna transakcję
		/// </summary>
		/// <param name="Database">Obiekt dostępu do bazy danych</param>
		/// <returns>Transakcja źródła danych</returns>
		public static IDatabaseTransaction BeginTransaction(DatabaseContainer databaseContainer)
		{
			DatabaseTransaction databaseTransaction = new DatabaseTransaction
			{
				connection = databaseContainer.Database.CreateConnection()
			};

			databaseTransaction.connection.Open();
			databaseTransaction.dbtransaction = databaseTransaction.connection.BeginTransaction();

			return databaseTransaction;
		}

		/// <summary>
		/// Zatwierdza transakcję
		/// </summary>
		public void Commit()
		{
			dbtransaction.Commit();
			connection.Close();
			connection.Dispose();
			connection = null;
		}

		/// <summary>
		/// Wycofuje transakcję
		/// </summary>
		public void Rollback()
		{
			if (connection != null)
			{
				dbtransaction.Rollback();
				connection.Close();
				connection.Dispose();
				connection = null;
			}
		}

		/// <summary>
		/// Obiekt transakcji bazodanowej
		/// </summary>
		internal DbTransaction Dbtransaction
		{
			get { return dbtransaction; }
			set { dbtransaction = value; }
		}

		/// <summary>
		/// Zwalnia obiekt transakcji
		/// </summary>
		public void Dispose()
		{
			Rollback();
		}
	}
}