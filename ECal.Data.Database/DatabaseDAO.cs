﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ECal.Common.Database;
using ECal.Data.Common;

namespace ECal.Data.Database
{
	/// <summary>
	/// Podstawowa klasa DAO. Przechowuje obiekt typu Database (EntLib).
	/// </summary>
	public abstract class DatabaseDAO : IDatabaseDAO
	{
		private DatabaseContainer DatabaseContainer { get; set; }

		public DatabaseDAO(DatabaseContainer databaseContainer)
		{
			DatabaseContainer = databaseContainer;
		}

		/// <summary>
		/// Tworzy i rozpoczyna nową transakcję bazodanową.
		/// </summary>
		/// <returns>Utworzona transakcja bazodanowa.</returns>
		public IDatabaseTransaction BeginTransaction()
		{
			return DatabaseTransaction.BeginTransaction(DatabaseContainer);
		}

		/// <summary>
		/// Ustawia wartość dla parametru
		/// </summary>
		/// <param name="command">Obiekt typu DbCommand</param>
		/// <param name="name">Nazwa parametru</param>
		/// <param name="type">Typ parametru</param>
		/// <param name="value">Wartość parametru</param>
		protected virtual void SetParameterTypeAndValue(DbCommand command, string name, DbType type, object value)
		{
			if (value is sbyte || (value == null && type == DbType.SByte))
			{
				AddInParameter(command, name, DbType.Byte, (byte)(sbyte)value);
			}
			else if (value is ushort || (value == null && type == DbType.UInt16))
			{
				AddInParameter(command, name, DbType.Int16, (short)(ushort)value);
			}
			else if (value is uint || (value == null && type == DbType.UInt32))
			{
				AddInParameter(command, name, DbType.Int32, (int)(uint)value);
			}
			else if (value is ulong || (value == null && type == DbType.UInt64))
			{
				AddInParameter(command, name, DbType.Int64, (long)(ulong)value);
			}
			else
			{
				AddInParameter(command, name, type, value);
			}
		}

		protected void AddInParameter(DbCommand command, string name, DbType type, object value)
		{
			DatabaseContainer.Database.AddInParameter(command, name, type, value);
		}

		protected void AddOutParameter(DbCommand command, string name, DbType type, int size)
		{
			DatabaseContainer.Database.AddOutParameter(command, name, type, size);
		}

		protected void AddReturnParameter(DbCommand command, string name, DbType type)
		{
			DatabaseContainer.Database.AddParameter(command, name, type, ParameterDirection.ReturnValue, null, DataRowVersion.Default, null);
		}

		protected object GetParameterValue(DbCommand command, string name)
		{
			return DatabaseContainer.Database.GetParameterValue(command, name);
		}

		protected DbCommand GetSqlStringCommand(string query)
		{
			return DatabaseContainer.Database.GetSqlStringCommand(query);
		}

		protected DbCommand GetStoredProcCommand(string storedProcedureName)
		{
			return DatabaseContainer.Database.GetStoredProcCommand(storedProcedureName);
		}

		/// <summary>
		/// Wykonanie zapytania nie zwracającego wyników
		/// </summary>
		/// <param name="transaction">Obiekt transakcji bazodanowej</param>
		/// <param name="command">Obiekt typu DbCommand</param>
		/// <returns>Ilość dodanych/zmodyfikowanych wierszy</returns>
		protected int ExecuteNonQuery(IDatabaseTransaction transaction, DbCommand command)
		{
			if (transaction != null && transaction is DatabaseTransaction)
				return DatabaseContainer.Database.ExecuteNonQuery(command, (transaction as DatabaseTransaction).Dbtransaction);
			else
				return DatabaseContainer.Database.ExecuteNonQuery(command);
		}

		/// <summary>
		/// Wykonanie zapytania i zwrócenie obiektu odczytującego dane
		/// </summary>
		/// <param name="transaction">Obiekt transakcji bazodanowej</param>
		/// <param name="command">Obiekt typu DbCommand</param>
		/// <returns>Obiekt odczytujący dane</returns>
		protected IDataReader ExecuteReader(IDatabaseTransaction transaction, DbCommand command)
		{
			if (transaction != null && transaction is DatabaseTransaction)
				return DatabaseContainer.Database.ExecuteReader(command, (transaction as DatabaseTransaction).Dbtransaction);
			else
				return DatabaseContainer.Database.ExecuteReader(command);
		}

		/// <summary>
		/// Wykonanie zapytania zwracającego tylko jedną wartość
		/// </summary>
		/// <typeparam name="T">Zwracany typ</typeparam>
		/// <param name="transaction">Obiekt transakcji bazodanowej</param>
		/// <param name="command">Obiekt typu DbCommand</param>
		/// <returns>Odczytana wartość</returns>
		protected object ExecuteScalar(IDatabaseTransaction transaction, DbCommand command)
		{
			if (transaction != null && transaction is DatabaseTransaction)
				return DatabaseContainer.Database.ExecuteScalar(command, (transaction as DatabaseTransaction).Dbtransaction);
			else
				return DatabaseContainer.Database.ExecuteScalar(command);
		}

		#region Metody abstrakcyjne, do zaimplementowania w klasach DAO pod konkretną bazę danych

		/// <summary>
		/// Dodaje obsługę stronicowania do zapytania
		/// </summary>
		/// <param name="dbQuery">Główne zapytanie (z ORDER BY)</param>
		/// <param name="paging"></param>
		/// <param name="sortAdditional"></param>
		/// <returns>Zapytanie opakowane w stronicowanie</returns>
		protected abstract string AddPagingWithOrderBy(string dbQuery, Paging paging, string sortAdditional = "order by Id");

		/// <summary>
		/// Odczytuje wartość kolumny, zawierającą informację o ilości wszystkich wierszy - dla zapytań utworzonych z pomocą metody AddPaging
		/// </summary>
		/// <param name="reader">Obiekt odczytu</param>
		/// <returns>Ilość wszystkich wierszy</returns>
		protected abstract long ReadPagingTotalRows(IDataReader reader);

		#endregion Metody abstrakcyjne, do zaimplementowania w klasach DAO pod konkretną bazę danych
	}
}