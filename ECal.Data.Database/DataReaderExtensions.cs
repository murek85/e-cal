﻿using System;
using System.Data;
using ECal.Common.Database;

namespace ECal.Data.Database
{
	/// <summary>
	/// Klasa rozszerzająca dla odczytu wartości z bazy danych
	/// </summary>
	public static class DataReaderExtensions
	{
		#region byte[]

		public static byte[] GetAllBytes(this IDataReader dr, int i)
		{
			if (!dr.IsDBNull(i))
			{
				long size = dr.GetBytes(i, 0, null, 0, 0);
				byte[] values = new byte[size];

				const int bufferSize = 1024;
				long bytesRead = 0;
				int curenntPos = 0;

				while (bytesRead < size)
				{
					bytesRead += dr.GetBytes(i, curenntPos, values, curenntPos, bufferSize);
					curenntPos += bufferSize;
				}

				return values;
			}

			return null;
		}

		public static byte[] GetAllBytes(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetAllBytes(i);
		}

		#endregion byte[]

		#region bool

		public static bool GetBoolean(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetBoolean(i);
		}

		public static bool? GetNullableBoolean(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<bool>(dr.GetBoolean(i));
		}

		public static bool? GetNullableBoolean(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableBoolean(i);
		}

		#endregion bool

		#region byte

		public static byte GetByte(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetByte(i);
		}

		public static byte? GetNullableByte(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<byte>(dr.GetByte(i));
		}

		public static byte? GetNullableByte(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableByte(i);
		}

		#endregion byte

		#region sbyte

		public static sbyte GetSByte(this IDataReader dr, int i)
		{
			return (sbyte)dr.GetByte(i);
		}

		public static sbyte GetSByte(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetSByte(i);
		}

		public static sbyte? GetNullableSByte(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<sbyte>(dr.GetSByte(i));
		}

		public static sbyte? GetNullableSByte(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableSByte(i);
		}

		#endregion sbyte

		#region char

		public static char GetChar(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetChar(i);
		}

		public static char? GetNullableChar(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<char>(dr.GetChar(i));
		}

		public static char? GetNullableChar(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableChar(i);
		}

		#endregion char

		#region DateTime

		public static DateTime GetDateTime(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetDateTime(i);
		}

		public static DateTime? GetNullableDateTime(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<DateTime>(dr.GetDateTime(i));
		}

		public static DateTime? GetNullableDateTime(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableDateTime(i);
		}

		#endregion DateTime

		#region decimal

		public static decimal GetDecimal(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetDecimal(i);
		}

		public static decimal? GetNullableDecimal(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<decimal>(dr.GetDecimal(i));
		}

		public static decimal? GetNullableDecimal(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableDecimal(i);
		}

		#endregion decimal

		#region double

		public static double GetDouble(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetDouble(i);
		}

		public static double? GetNullableDouble(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<double>(dr.GetDouble(i));
		}

		public static double? GetNullableDouble(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableDouble(i);
		}

		#endregion double

		#region float

		public static float GetFloat(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetFloat(i);
		}

		public static float? GetNullableFloat(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<float>(dr.GetFloat(i));
		}

		public static float? GetNullableFloat(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableFloat(i);
		}

		#endregion float

		#region Guid

		public static Guid GetGuid(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetGuid(i);
		}

		public static Guid? GetNullableGuid(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<Guid>(dr.GetGuid(i));
		}

		public static Guid? GetNullableGuid(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableGuid(i);
		}

		public static Guid? GetNullableGuidFromString(this IDataReader dr, int i)
		{
			if (dr.IsDBNull(i))
			{
				return null;
			}
			else
			{
				Guid temp = Guid.Empty;
				if (Guid.TryParse(dr.GetString(i), out temp))
				{
					return temp;
				}
				else
				{
					return null;
				}
			}
		}

		public static Guid? GetNullableGuidFromString(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableGuidFromString(i);
		}

		#endregion Guid

		#region short

		public static short GetInt16(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetInt16(i);
		}

		public static short? GetNullableInt16(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<short>(dr.GetInt16(i));
		}

		public static short? GetNullableInt16(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableInt16(i);
		}

		#endregion short

		#region ushort

		public static ushort GetUInt16(this IDataReader dr, int i)
		{
			return (ushort)dr.GetInt16(i);
		}

		public static ushort GetUInt16(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetUInt16(i);
		}

		public static ushort? GetNullableUInt16(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<ushort>(dr.GetUInt16(i));
		}

		public static ushort? GetNullableUInt16(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableUInt16(i);
		}

		#endregion ushort

		#region int

		public static int GetInt32(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetInt32(i);
		}

		public static int? GetNullableInt32(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<int>(dr.GetInt32(i));
		}

		public static int? GetNullableInt32(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableInt32(i);
		}

		#endregion int

		#region uint

		public static uint GetUInt32(this IDataReader dr, int i)
		{
			return (uint)dr.GetInt32(i);
		}

		public static uint GetUInt32(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetUInt32(i);
		}

		public static uint? GetNullableUInt32(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<uint>(dr.GetUInt32(i));
		}

		public static uint? GetNullableUInt32(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableUInt32(i);
		}

		#endregion uint

		#region long

		public static long GetInt64(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetInt64(i);
		}

		public static long? GetNullableInt64(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<long>(dr.GetInt64(i));
		}

		public static long? GetNullableInt64(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableInt64(i);
		}

		#endregion long

		#region ulong

		public static ulong GetUInt64(this IDataReader dr, int i)
		{
			return (ulong)dr.GetInt64(i);
		}

		public static ulong GetUInt64(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetUInt64(i);
		}

		public static ulong? GetNullableUInt64(this IDataReader dr, int i)
		{
			return (dr.IsDBNull(i)) ? null : new Nullable<ulong>(dr.GetUInt64(i));
		}

		public static ulong? GetNullableUInt64(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.GetNullableUInt64(i);
		}

		#endregion ulong

		#region string

		public static string GetString(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return (dr.IsDBNull(i)) ? null : dr.GetString(i);
		}

		#endregion string

		#region object

		public static object GetValue(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return (dr.IsDBNull(i)) ? null : dr.GetValue(i);
		}

		#endregion object

		#region dbnull

		public static bool IsDBNull(this IDataReader dr, string name)
		{
			int i = dr.GetOrdinal(name);
			return dr.IsDBNull(i);
		}

		#endregion dbnull

		#region generic

		public static Nullable<T> GetNullableEnum<T>(this IDataReader reader, string columnName) where T : struct, IConvertible
		{
			if (!typeof(T).IsEnum)
				throw new ArgumentException("T must be an enumerated type");

			string value = reader.GetString(columnName);
			T eValue;
			if (!string.IsNullOrWhiteSpace(value) && Enum.TryParse<T>(value, out eValue))
			{
				return eValue;
			}
			else
			{
				return null;
			}
		}

		#endregion generic
	}
}