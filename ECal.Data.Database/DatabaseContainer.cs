﻿namespace ECal.Data.Database
{
	/// <summary>
	/// Klasa opakowująca obiekt bazy danych
	/// </summary>
	public abstract class DatabaseContainer
	{
		/// <summary>
		/// Obiekt dostępu do bazy danych
		/// </summary>
		public Microsoft.Practices.EnterpriseLibrary.Data.Database Database;
	}
}