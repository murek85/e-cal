﻿using ECal.Data.Common.Ldap;
using ECal.Model.Ldap;
using Microsoft.Extensions.Options;
using Novell.Directory.Ldap;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;

namespace ECal.Data.Ldap.LdapUsers
{
	/// <summary>
	/// Zarządzanie użytkownikami z Ldap'a
	/// </summary>
	public class LdapDAO : ILdapDAO
	{
		public LdapDAO() { }

		private readonly string _searchBase;
		private readonly LdapConfig _ldapConfig;
		private readonly string[] _attributes =
		{
			"objectSid", "objectGUID", "objectCategory", "objectClass", "memberOf", "name", "cn", "distinguishedName",
			"sAMAccountName", "sAMAccountName", "userPrincipalName", "displayName", "givenName", "sn", "description",
			"telephoneNumber", "mail", "streetAddress", "postalCode", "l", "st", "co", "c"
		};

		public LdapDAO(
			IOptions<LdapConfig> piLdapConfig)
		{
			_ldapConfig = piLdapConfig.Value;
			_searchBase = _ldapConfig.SearchBase;
		}

		/// <summary>
		/// Nawiązanie połączenia z Ldap
		/// </summary>
		/// <returns></returns>
		private ILdapConnection GetLdapConn()
		{
			var ldapConn = new LdapConnection()
			{
				SecureSocketLayer = _ldapConfig.UseSsl
			};

			ldapConn.Connect(_ldapConfig.ServerName, _ldapConfig.ServerPort);
			ldapConn.Bind(_ldapConfig.Credentials.UserName, _ldapConfig.Credentials.Password);

			return ldapConn;
		}

		/// <summary>
		/// Pobranie listy grup w domenie
		/// </summary>
		/// <param name="piGroupName"></param>
		/// <param name="piChildGroups"></param>
		/// <returns></returns>
		public IList<Model.Ldap.LdapEntry> GetGroups(
			string piGroupName, bool piChildGroups = false)
		{
			var groups = new List<Model.Ldap.LdapEntry>();
			var filter = $"(&(objectClass=group)(cn={piGroupName}))";

			using (var ldapConnection = GetLdapConn())
			{
				var search = ldapConnection.Search(
					_searchBase, LdapConnection.SCOPE_SUB, filter, this._attributes, false, null, null);

				LdapMessage message;
				while ((message = search.getResponse()) != null)
				{
					if (!(message is LdapSearchResult searchResultMessage))
						continue;

					var entry = searchResultMessage.Entry;
					groups.Add(CreateEntryFromAttributes(entry.DN, entry.getAttributeSet()));

					if (!piChildGroups)
						continue;

					foreach (var child in GetChildren<Model.Ldap.LdapEntry>(string.Empty, entry.DN))
					{
						groups.Add(child);
					}
				}
			}

			return groups;
		}

		/// <summary>
		/// Pobranie wszystkich użytkowników z katalogu Ldap'a
		/// </summary>
		/// <returns></returns>
		public IList<LdapUser> GetAllUsers()
		{
			return GetUsersInGroups(null);
		}

		public IList<LdapUser> GetUsersInGroup(
			string group)
		{
			return GetUsersInGroups(GetGroups(group));
		}

		public IList<LdapUser> GetUsersInGroups(
			IList<Model.Ldap.LdapEntry> groups)
		{
			var users = new List<LdapUser>();
			if (groups == null || !groups.Any())
			{
				users.AddRange(GetChildren<LdapUser>(_searchBase));
			}
			else
			{
				foreach (var group in groups)
				{
					users.AddRange(GetChildren<LdapUser>(_searchBase, @group.DistinguishedName));
				}
			}

			return users;
		}

		/// <summary>
		/// Pobranie użytkowników na podstawie filtrowania po adresie e-mail
		/// </summary>
		/// <param name="emailAddress">Adres e-mail</param>
		/// <returns></returns>
		public IList<LdapUser> GetUsersByEmailAddress(
			string emailAddress)
		{
			var users = new List<LdapUser>();
			var filter = $"(&(objectClass=user)(mail={emailAddress}))";

			using (var ldapConnection = GetLdapConn())
			{
				var search = ldapConnection.Search(
					_searchBase, LdapConnection.SCOPE_SUB, filter, _attributes, false, null, null);

				LdapMessage message;
				while ((message = search.getResponse()) != null)
				{
					if (!(message is LdapSearchResult searchResultMessage))
						continue;

					users.Add(CreateUserFromAttributes(
						_searchBase, searchResultMessage.Entry.getAttributeSet()));
				}
			}

			return users;
		}

		/// <summary>
		/// Pobranie użytkowników z katalogu Ldap na podstawie odpowiednich filtrów
		/// </summary>
		/// <param name="filterName">Filtry rozdzielone przecinkami: SAMAccountName, DisplayName</param>
		/// <param name="query">Szukana fraza</param>
		/// <returns></returns>
		public IList<LdapUser> GetUsersByFilterName(
			string[] filterName, string query)
		{
			var users = new List<LdapUser>();

			var filter = $"(&(objectClass=user)(|";
			foreach (var name in filterName)
				filter += $"({name}=*{query}*)";

			filter += $"))";
			using (var ldapConnection = GetLdapConn())
			{
				var search = ldapConnection.Search(
					_searchBase, LdapConnection.SCOPE_SUB, filter, _attributes, false, null, null);

				LdapMessage message;
				while ((message = search.getResponse()) != null)
				{
					if (!(message is LdapSearchResult searchResultMessage))
						continue;

					users.Add(CreateUserFromAttributes(
						_searchBase, searchResultMessage.Entry.getAttributeSet()));
				}
			}

			return users;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="userPrincipalName"></param>
		/// <returns></returns>
		public LdapUser GetUserByUserPrincipalName(string userPrincipalName)
		{
			LdapUser user = null;
			var filter = $"(&(objectClass=user)(UserPrincipalName={userPrincipalName}))";
			using (var ldapConnection = GetLdapConn())
			{
				var search = ldapConnection.Search(
					_searchBase, LdapConnection.SCOPE_SUB, filter, _attributes, false, null, null);

				LdapMessage message;
				while ((message = search.getResponse()) != null)
				{
					if (!(message is LdapSearchResult searchResultMessage))
						continue;

					user = CreateUserFromAttributes(
						_searchBase, searchResultMessage.Entry.getAttributeSet());
				}
			}

			return user;
		}

		/// <summary>
		/// Pobranie użytkownika z katalogu Ldap filtrowane po nazwie
		/// </summary>
		/// <param name="samAccountName">Nazwa użytkownika</param>
		/// <returns></returns>
		public LdapUser GetUserBySAMAccountName(
			string samAccountName)
		{
			LdapUser user = null;
			var filter = $"(&(objectClass=user)(SAMAccountName={samAccountName}))";
			using (var ldapConnection = GetLdapConn())
			{
				var search = ldapConnection.Search(
					_searchBase, LdapConnection.SCOPE_SUB, filter, _attributes, false, null, null);

				LdapMessage message;
				while ((message = search.getResponse()) != null)
				{
					if (!(message is LdapSearchResult searchResultMessage))
						continue;

					user = CreateUserFromAttributes(
						_searchBase, searchResultMessage.Entry.getAttributeSet());
				}
			}

			return user;
		}

		/// <summary>
		/// Pobranie użytkownika z katalogu Ldap filtrowane po identyfikatorze z Ldap'a
		/// </summary>
		/// <param name="objectGuid">Identyfikator z Ldap</param>
		/// <returns></returns>
		public LdapUser GetUserByObjectGuid(
			string objectGuid)
		{
			LdapUser user = null;
			var filter = $"(&(objectClass=user)(objectGUID={ConvertGuidToOctetString(objectGuid)}))";
			using (var ldapConnection = GetLdapConn())
			{
				var search = ldapConnection.Search(
					_searchBase, LdapConnection.SCOPE_SUB, filter, _attributes, false, null, null);

				LdapMessage message;
				while ((message = search.getResponse()) != null)
				{
					if (!(message is LdapSearchResult searchResultMessage))
						continue;

					user = CreateUserFromAttributes(
						_searchBase, searchResultMessage.Entry.getAttributeSet());
				}
			}

			return user;
		}

		/// <summary>
		/// Pobranie administratora Ldap
		/// </summary>
		/// <returns></returns>
		public LdapUser GetLdapAdministrator()
		{
			var name = _ldapConfig.Credentials.UserName.Substring(
				_ldapConfig.Credentials.UserName.IndexOf("\\", StringComparison.Ordinal) != -1
					? _ldapConfig.Credentials.UserName.IndexOf("\\", StringComparison.Ordinal) + 1
					: 0);

			return GetUserBySAMAccountName(name);
		}

		/// <summary>
		/// Uwierzytelnienie użytkownika z katalogu Ldap
		/// </summary>
		/// <param name="distinguishedName">Nazwa konta/użytkownika</param>
		/// <param name="password">Hasło do konta</param>
		/// <returns></returns>
		public bool Authenticate(
			string distinguishedName, string password)
		{
			using (var ldapConnection = new LdapConnection() { SecureSocketLayer = _ldapConfig.UseSsl })
			{
				ldapConnection.Connect(
					_ldapConfig.ServerName, _ldapConfig.ServerPort);

				try
				{
					ldapConnection.Bind(distinguishedName, password);
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public bool AddUserToGroup(
			string distinguishedName)
		{
			using (var ldapConnection = GetLdapConn())
			{
				// modyfikacja atrybutów ldap
				ArrayList modList = new ArrayList();

				LdapAttribute member = new LdapAttribute("member", distinguishedName);
				modList.Add(new LdapModification(LdapModification.ADD, member));

				LdapModification[] mods = new LdapModification[modList.Count];
				Type mtype = Type.GetType("Novell.Directory.LdapModification");
				mods = (LdapModification[])modList.ToArray(typeof(LdapModification));

				try
				{
					ldapConnection.Modify(_ldapConfig.GetacGroup, mods);
				}
				catch
				{
					return false;
				}
			}

			return true;
		}

		public bool RemoveUserFromGroup(
			string distinguishedName)
		{
			using (var ldapConnection = GetLdapConn())
			{
				// modyfikacja atrybutów ldap
				ArrayList modList = new ArrayList();

				LdapAttribute member = new LdapAttribute("member", distinguishedName);
				modList.Add(new LdapModification(LdapModification.DELETE, member));

				LdapModification[] mods = new LdapModification[modList.Count];
				Type mtype = Type.GetType("Novell.Directory.LdapModification");
				mods = (LdapModification[])modList.ToArray(typeof(LdapModification));

				try
				{
					ldapConnection.Modify(_ldapConfig.GetacGroup, mods);
				}
				catch
				{
					return false;
				}
			}

			return true;
		}

		public bool AddGroupToUser(
			string userId)
		{
			LdapUser user = null;
			var filter = $"(&(objectClass=user)(objectGUID={ConvertGuidToOctetString(userId)}))";
			using (var ldapConnection = GetLdapConn())
			{
				var search = ldapConnection.Search(
					_searchBase, LdapConnection.SCOPE_SUB, filter, _attributes, false, null, null);

				LdapMessage message;
				while ((message = search.getResponse()) != null)
				{
					if (!(message is LdapSearchResult searchResultMessage))
						continue;

					user = CreateUserFromAttributes(
						_searchBase, searchResultMessage.Entry.getAttributeSet());
				}
			}

			return user != null;
		}

		private IList<T> GetChildren<T>(
			string searchBase, string groupDistinguishedName = null) where T : ILdapEntry, new()
		{
			var entries = new List<T>();
			var objectCategory = "*";
			var objectClass = "*";

			if (typeof(T) == typeof(Model.Ldap.LdapEntry))
			{
				objectClass = "group";
				objectCategory = "group";

				entries = GetChildren(_searchBase, groupDistinguishedName, objectCategory, objectClass)
					.Cast<T>().ToList();
			}

			if (typeof(T) == typeof(LdapUser))
			{
				objectCategory = "person";
				objectClass = "user";

				entries = GetChildren(_searchBase, null, objectCategory, objectClass).Cast<T>()
					.ToList();

			}

			return entries;
		}

		private IList<ILdapEntry> GetChildren(
			string searchBase, string groupDistinguishedName = null, string objectCategory = "*", string objectClass = "*")
		{
			var allChildren = new List<ILdapEntry>();
			var filter = string.IsNullOrEmpty(groupDistinguishedName)
				? $"(&(objectCategory={objectCategory})(objectClass={objectClass}))"
				: $"(&(objectCategory={objectCategory})(objectClass={objectClass})(memberOf={groupDistinguishedName}))";

			using (var ldapConnection = GetLdapConn())
			{
				var search = ldapConnection.Search(
					searchBase, LdapConnection.SCOPE_SUB, filter, this._attributes, false, null, null);

				LdapMessage message;
				while ((message = search.getResponse()) != null)
				{
					if (!(message is LdapSearchResult searchResultMessage))
						continue;

					var entry = searchResultMessage.Entry;
					if (objectClass == "group")
					{
						allChildren.Add(CreateEntryFromAttributes(entry.DN, entry.getAttributeSet()));

						foreach (var child in this.GetChildren(string.Empty, entry.DN, objectCategory, objectClass))
							allChildren.Add(child);
					}

					if (objectClass == "user")
						allChildren.Add(CreateUserFromAttributes(entry.DN, entry.getAttributeSet()));
				}
			}

			return allChildren;
		}

		/// <summary>
		/// Utworzenie odpowiedniego obiektu użytkownika pobranego z katalogu Ldap'a
		/// </summary>
		/// <param name="distinguishedName">Nazwa konta/użytkownika</param>
		/// <param name="attributeSet">Atrybut Ldap</param>
		/// <returns></returns>
		private LdapUser CreateUserFromAttributes(
			string distinguishedName, LdapAttributeSet attributeSet)
		{
			var ldapUser = new LdapUser
			{
				ObjectSid = ConvertByteToStringSid((byte[])(Array)attributeSet.getAttribute("objectSid")?.ByteValue),
				ObjectGuid = ConvertByteToStringGuid((byte[])(Array)attributeSet.getAttribute("objectGUID")?.ByteValue),
				ObjectCategory = attributeSet.getAttribute("objectCategory")?.StringValue,
				ObjectClass = attributeSet.getAttribute("objectClass")?.StringValue,
				IsDomainAdmin = attributeSet.getAttribute("memberOf") != null && attributeSet.getAttribute("memberOf").StringValueArray.Contains("CN=Domain Admins," + _ldapConfig.SearchBase),
				MemberOf = attributeSet.getAttribute("memberOf")?.StringValueArray,
				CommonName = attributeSet.getAttribute("cn")?.StringValue,
				UserName = attributeSet.getAttribute("sAMAccountName")?.StringValue,
				SamAccountName = attributeSet.getAttribute("sAMAccountName")?.StringValue,
				UserPrincipalName = attributeSet.getAttribute("userPrincipalName")?.StringValue,
				Name = attributeSet.getAttribute("sAMAccountName")?.StringValue,
				DistinguishedName = attributeSet.getAttribute("distinguishedName")?.StringValue ?? distinguishedName,
				DisplayName = attributeSet.getAttribute("displayName")?.StringValue,
				FirstName = attributeSet.getAttribute("givenName")?.StringValue,
				LastName = attributeSet.getAttribute("sn")?.StringValue,
				Description = attributeSet.getAttribute("description")?.StringValue,
				Phone = attributeSet.getAttribute("telephoneNumber")?.StringValue,
				EmailAddress = attributeSet.getAttribute("mail")?.StringValue,
				Address = new LdapAddress
				{
					Street = attributeSet.getAttribute("streetAddress")?.StringValue,
					City = attributeSet.getAttribute("l")?.StringValue,
					PostalCode = attributeSet.getAttribute("postalCode")?.StringValue,
					StateName = attributeSet.getAttribute("st")?.StringValue,
					CountryName = attributeSet.getAttribute("co")?.StringValue,
					CountryCode = attributeSet.getAttribute("c")?.StringValue
				},

				SamAccountType = int.Parse(attributeSet.getAttribute("sAMAccountType")?.StringValue ?? "0"),
			};

			return ldapUser;
		}

		private Model.Ldap.LdapEntry CreateEntryFromAttributes(
			string distinguishedName, LdapAttributeSet attributeSet)
		{
			return new Model.Ldap.LdapEntry
			{
				ObjectSid = attributeSet.getAttribute("objectSid")?.StringValue,
				ObjectGuid = attributeSet.getAttribute("objectGUID")?.StringValue,
				ObjectCategory = attributeSet.getAttribute("objectCategory")?.StringValue,
				ObjectClass = attributeSet.getAttribute("objectClass")?.StringValue,
				CommonName = attributeSet.getAttribute("cn")?.StringValue,
				Name = attributeSet.getAttribute("name")?.StringValue,
				DistinguishedName = attributeSet.getAttribute("distinguishedName")?.StringValue ?? distinguishedName,
				SamAccountName = attributeSet.getAttribute("sAMAccountName")?.StringValue,
				SamAccountType = int.Parse(attributeSet.getAttribute("sAMAccountType")?.StringValue ?? "0"),
			};
		}

		/// <summary>
		/// Pobranie domenowego identyfikatora SID
		/// </summary>
		/// <returns></returns>
		private SecurityIdentifier GetDomainSid()
		{
			var administratorAcount = new NTAccount(_ldapConfig.DomainName, "administrator");
			var administratorSId = (SecurityIdentifier)administratorAcount.Translate(typeof(SecurityIdentifier));
			return administratorSId.AccountDomainSid;
		}

		/// <summary>
		/// Konwersja GUID do łańcucha znaków
		/// </summary>
		/// <param name="objectGuid"></param>
		/// <returns></returns>
		private string ConvertGuidToOctetString(string objectGuid)
		{
			Guid guid = new Guid(objectGuid);
			byte[] byteGuid = guid.ToByteArray();

			string queryGuid = "";

			foreach (byte b in byteGuid)
			{
				queryGuid += @"\" + b.ToString("x2");
			}

			return queryGuid;
		}

		/// <summary>
		/// Konwersja tablicy bajtów do typu string - format Guid
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		private string ConvertByteToStringGuid(Byte[] bytes)
		{
			Guid guid = new Guid(bytes);

			return guid.ToString();
		}

		/// <summary>
		/// Konwersja tablicy bajtów do typu string - format SID
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		private string ConvertByteToStringSid(Byte[] bytes)
		{
			StringBuilder strSid = new StringBuilder();
			strSid.Append("S-");
			try
			{
				// Add SID revision.
				strSid.Append(bytes[0].ToString());
				// Next six bytes are SID authority value.
				if (bytes[6] != 0 || bytes[5] != 0)
				{
					string strAuth = String.Format("0x{0:2x}{1:2x}{2:2x}{3:2x}{4:2x}{5:2x}",
						(Int16)bytes[1], (Int16)bytes[2], (Int16)bytes[3],
						(Int16)bytes[4], (Int16)bytes[5], (Int16)bytes[6]);
					strSid.Append("-");
					strSid.Append(strAuth);
				}
				else
				{
					Int64 iVal = (Int32)(bytes[1]) +
						(Int32)(bytes[2] << 8) +
						(Int32)(bytes[3] << 16) +
						(Int32)(bytes[4] << 24);
					strSid.Append("-");
					strSid.Append(iVal.ToString());
				}

				// Get sub authority count...
				int iSubCount = Convert.ToInt32(bytes[7]);
				int idxAuth = 0;
				for (int i = 0; i < iSubCount; i++)
				{
					idxAuth = 8 + i * 4;
					UInt32 iSubAuth = BitConverter.ToUInt32(bytes, idxAuth);
					strSid.Append("-");
					strSid.Append(iSubAuth.ToString());
				}
			}
			catch (Exception ex)
			{
				return "";
			}
			return strSid.ToString();
		}
	}
}
