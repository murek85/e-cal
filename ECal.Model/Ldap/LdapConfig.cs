﻿namespace ECal.Model.Ldap
{
	public class LdapConfig
	{
		public string ServerName { get; set; }
		public int ServerPort { get; set; }
		public bool UseSsl { get; set; }
		public string SearchBase { get; set; }
		public string SearchFilter { get; set; }
		public string DomainName { get; set; }
		public LdapCredentials Credentials { get; set; }
		public string GetacGroup { get; set; }
	}
}
