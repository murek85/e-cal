﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace ECal.Model.Ldap
{
	public class LdapUser : IdentityUser, ILdapEntry
	{
		[NotMapped]
		public string ObjectSid { get; set; }
		[NotMapped]
		public string ObjectGuid { get; set; }
		[NotMapped]
		public string ObjectCategory { get; set; }
		[NotMapped]
		public string ObjectClass { get; set; }
		[NotMapped]
		public string Password { get; set; }
		[NotMapped]
		public string Name { get; set; }
		[NotMapped]
		public string CommonName { get; set; }
		[NotMapped]
		public string DistinguishedName { get; set; }
		[NotMapped]
		public string SamAccountName { get; set; }
		[NotMapped]
		public int SamAccountType { get; set; }
		[NotMapped]
		public string[] MemberOf { get; set; }
		[NotMapped]
		public bool IsDomainAdmin { get; set; }
		[NotMapped]
		public string UserPrincipalName { get; set; }
		[NotMapped]
		public string DisplayName { get; set; }
		[NotMapped]
		public string FirstName { get; set; }
		[NotMapped]
		public string LastName { get; set; }
		[NotMapped]
		public string FullName => $"{this.FirstName} {this.LastName}";
		[NotMapped]
		public string EmailAddress { get; set; }
		[NotMapped]
		public string Description { get; set; }
		[NotMapped]
		public string Phone { get; set; }
		[NotMapped]
		public LdapAddress Address { get; set; }

		[NotMapped]
		public string AspId { get; set; }
		[NotMapped]
		public bool Domain { get => true; }

		public string UserName
		{
			get => this.Name;
			set => this.Name = value;
		}
	}
}
