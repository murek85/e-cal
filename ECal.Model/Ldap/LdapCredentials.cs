﻿namespace ECal.Model.Ldap
{
	public class LdapCredentials
	{
		public string UserName { get; set; }
		public string Password { get; set; }
	}
}
