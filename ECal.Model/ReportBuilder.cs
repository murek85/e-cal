﻿namespace ECal.Model
{
	public class ReportBuilder
	{
		public string title { get; set; }
		public string format { get; set; }
		public string labels { get; set; }
		public string aliases { get; set; }
		public string locale { get; set; }
	}
}
