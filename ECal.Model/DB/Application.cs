﻿using ECal.Common.Database;

namespace ECal.Model.DB
{
   public class Application : DatabaseEntityWithLongId
   {
      public string Name { get; set; }
      public string Description { get; set; }
      public string Symbol { get; set; }
      public string ApiUrl { get; set; }
      public string AppUrl { get; set; }
   }
}
