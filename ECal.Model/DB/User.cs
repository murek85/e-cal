﻿using ECal.Common.Database;
using System;
using System.Collections.Generic;

namespace ECal.Model.DB
{
	public class User : DatabaseEntityWithGuidId
	{
		public string EmployeeNumber { get; set; }
		public string UserName { get; set; }
		public string Email { get; set; }
		public bool? EmailConfirmed { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string PhoneNumber { get; set; }
		public string Photo { get; set; }
		public bool? IsActive { get; set; }
		public bool? IsDomain { get; set; }
		public DateTime? LockoutEnd { get; set; }
  		public DateTime? LastValidLogin { get; set; }
  		public DateTime? LastIncorrectLogin { get;set; }
		public IList<string> Logins { get; set; }
	}
}
