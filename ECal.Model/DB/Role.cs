﻿using ECal.Common.Database;

namespace ECal.Model.DB
{
   public class Role : DatabaseEntityWithLongId
   {
      public string Name { get; set; }
      public string Description { get; set; }
      public string Symbol { get; set; }
   }
}
