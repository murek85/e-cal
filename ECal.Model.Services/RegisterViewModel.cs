﻿using System.ComponentModel.DataAnnotations;

namespace ECal.Model.Services
{
	public class RegisterViewModel
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		[EmailAddress(ErrorMessage = "register.error.email_address_incorrect")]
		public string Email { get; set; }
		[Required(ErrorMessage = "register.error.username_is_required")]
		public string UserName { get; set; }
		[Required(ErrorMessage = "register.error.new_password_is_required")]
		[StringLength(100, ErrorMessage = "register.error.password_minimum_length_6", MinimumLength = 6)]
		[DataType(DataType.Password)]
		public string NewPassword { get; set; }
		[Required(ErrorMessage = "register.error.repeat_password_is_required")]
		[Compare("NewPassword", ErrorMessage = "register.error.password_not_match")]
		public string RepeatPassword { get; set; }
	}
}
