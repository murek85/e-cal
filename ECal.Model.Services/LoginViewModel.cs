﻿using System.ComponentModel.DataAnnotations;

namespace ECal.Model.Services
{
	public class LoginViewModel
	{
		[Required(ErrorMessage = "login.error.email_is_required")]
		[EmailAddress(ErrorMessage = "login.error.email_address_incorrect")]
		public string Email { get; set; }
		[Required(ErrorMessage = "login.error.password_is_required")]
		[DataType(DataType.Password)]
		public string Password { get; set; }
		public string Scope { get; set; }
		public bool Persistent { get; set; }
	}
}
