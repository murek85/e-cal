﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ECal.Model.Services
{
   public class GroupResourceViewModel
   {
      public int? Id { get; set; }
      [Required(ErrorMessage = "GROUP.RESOURCE.NAME_IS_REQUIRED")]
      public string Name { get; set; }
      public string Description { get; set; }
      public string Symbol { get; set; }
   }
}
