﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ECal.Model.Services
{
	public class UserViewModel
	{
		public Guid? Id { get; set; }
		[Required(ErrorMessage = "ACCOUNT.REGISTER.USERNAME_IS_REQUIRED")]
		public string UserName { get; set; }
		public string Password { get; set; }
		[Required(ErrorMessage = "ACCOUNT.REGISTER.FIRSTNAME_IS_REQUIRED")]
		public string FirstName { get; set; }
		[Required(ErrorMessage = "ACCOUNT.REGISTER.LASTNAME_IS_REQUIRED")]
		public string LastName { get; set; }
		public string PhoneNumber { get; set; }
		public string Photo { get; set; }
		public string FullName
		{
			get
			{
				if (!String.IsNullOrEmpty(FirstName) && !String.IsNullOrEmpty(LastName))
					return $"{FirstName} {LastName}";

				return String.Empty;
			}
		}
		[EmailAddress(ErrorMessage = "ACCOUNT.REGISTER.EMAIL_ADDRESS_INCORRECT")]
		public string Email { get; set; }
		public bool? EmailConfirmed { get; set; }
		public bool? IsActive { get; set; }
		public bool? IsDomain { get; set; }
		public DateTime? LockoutEnd { get; set; }
		public bool? IsLockoutEnd
		{
			get { return LockoutEnd.HasValue && LockoutEnd.Value > DateTime.Now; }
		}
  		public DateTime? LastValidLogin { get; set; }
  		public DateTime? LastIncorrectLogin { get; set; }
		public IList<string> Logins { get; set; }
	}
}
