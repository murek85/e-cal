﻿using System.ComponentModel.DataAnnotations;

namespace ECal.Model.Services
{
   public class RoleViewModel
   {
      public int? Id { get; set; }
      [Required(ErrorMessage = "ROLE.NAME_IS_REQUIRED")]
      public string Name { get; set; }
      public string Description { get; set; }
      public string Symbol { get; set; }
   }
}
