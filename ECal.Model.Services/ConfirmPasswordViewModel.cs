﻿using System.ComponentModel.DataAnnotations;

namespace ECal.Model.Services
{
	public class ConfirmPasswordViewModel
	{
		[Required(ErrorMessage = "password.confirm.error.new_password_is_required")] // Pole wymagane
		[StringLength(100, ErrorMessage = "password.confirm.error.password_minimum_length_6", MinimumLength = 6)] // The {0} must be at least {2} and at max {1} characters long.
		[DataType(DataType.Password)]
		public string NewPassword { get; set; }
		[Required(ErrorMessage = "password.confirm.error.repeat_password_is_required")] // Pole wymagane
		[StringLength(100, ErrorMessage = "password.confirm.error.password_minimum_length_6", MinimumLength = 6)] // The {0} must be at least {2} and at max {1} characters long.
		[Compare("NewPassword", ErrorMessage = "password.confirm.error.password_not_match")] // The password and confirmation password do not match.
		[DataType(DataType.Password)]
		public string RepeatPassword { get; set; }
	}
}
