﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ECal.Model.Services
{
   public class ApplicationViewModel
   {
      public int? Id { get; set; }
      [Required(ErrorMessage = "APPLICATION.NAME_IS_REQUIRED")]
      public string Name { get; set; }
      public string Description { get; set; }
      public string Symbol { get; set; }
      [Url]
      public string ApiUrl { get; set; }
      [Url]
      public string AppUrl { get; set; }
   }
}
