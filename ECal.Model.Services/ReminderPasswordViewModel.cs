﻿using System.ComponentModel.DataAnnotations;

namespace ECal.Model.Services
{
	public class ReminderPasswordViewModel
	{
		[Required(ErrorMessage = "password.reminder.error.email_is_required")]
		[EmailAddress]
		public string Email { get; set; }
	}
}
