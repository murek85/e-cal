﻿namespace ECal.Model.Services
{
	/// <summary>
	/// Użytkownika z Ldap
	/// </summary>
	public class LdapUserViewModel
	{
		/// <summary>
		/// Identyfikator SID
		/// </summary>
		public string ObjectSid { get; set; }
		/// <summary>
		/// Identyfikator GUID
		/// </summary>
		public string ObjectGuid { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string ObjectCategory { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string ObjectClass { get; set; }
		/// <summary>
		/// Hasło ???
		/// </summary>
		public string Password { get; set; }
		/// <summary>
		/// Nazwa
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string CommonName { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string DistinguishedName { get; set; }
		/// <summary>
		/// Nazwa konta SAM
		/// </summary>
		public string SamAccountName { get; set; }
		/// <summary>
		/// Typ konta SAM
		/// </summary>
		public int SamAccountType { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string[] MemberOf { get; set; }
		/// <summary>
		/// Administrator domenowy?
		/// </summary>
		public bool IsDomainAdmin { get; set; }
		/// <summary>
		/// Nazwa ???
		/// </summary>
		public string UserPrincipalName { get; set; }
		/// <summary>
		/// Nazwa do wyświetlenia
		/// </summary>
		public string DisplayName { get; set; }
		/// <summary>
		/// Imię
		/// </summary>
		public string FirstName { get; set; }
		/// <summary>
		/// Nazwisko
		/// </summary>
		public string LastName { get; set; }
		/// <summary>
		/// Pełna nazwa
		/// </summary>
		public string FullName { get; set; }
		/// <summary>
		/// Adres e-mail
		/// </summary>
		public string EmailAddress { get; set; }
		/// <summary>
		/// Krótki opis
		/// </summary>
		public string Description { get; set; }
		/// <summary>
		/// Numer telefonu
		/// </summary>
		public string Phone { get; set; }
		/// <summary>
		/// Nazwa konta
		/// </summary>
		public string UserName { get; set; }
	}
}
