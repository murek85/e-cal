const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const mf = require('@angular-architects/module-federation/webpack');
const path = require('path');
const share = mf.share;
const shareAll = mf.shareAll;

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(path.join(__dirname, 'tsconfig.json'), [
	/* mapped paths to share */
]);

module.exports = {
	output: {
		uniqueName: 'ecal',
		publicPath: 'auto'
	},
	optimization: {
		runtimeChunk: false
	},
	resolve: {
		alias: {
			...sharedMappings.getAliases()
		}
	},
	module: {
		rules: [
			{
				test: /\.(png|jpg|gif)$/,
				use: ['url-loader']
			},
			{
				test: /\.(png|woff|woff2|eot|ttf|svg)(\?|$)/,
				use: 'url-loader'
			}
		]
	},
	plugins: [
		new ModuleFederationPlugin({
			// For remotes (please adjust)
			name: 'ecal',
			filename: 'remoteEntry.js',
			exposes: {
				'./web-components': './src/bootstrap.ts'
			},

			// For hosts (please adjust)
			// remotes: {
			//     "mfe1": "mfe1@http://localhost:3000/remoteEntry.js",

			// },

			// shared: share({
			// 	'@angular/core': { singleton: true, strictVersion: true, requiredVersion: 'auto' },
			// 	'@angular/common': { singleton: true, strictVersion: true, requiredVersion: 'auto' },
			// 	'@angular/common/http': { singleton: true, strictVersion: true, requiredVersion: 'auto' },
			// 	'@angular/router': { singleton: true, strictVersion: true, requiredVersion: 'auto' },

			// 	...sharedMappings.getDescriptors()
			// })

			shared: {
				...shareAll({
					singleton: true,
					strictVersion: false,
					requiredVersion: 'auto'
				}),
				...sharedMappings.getDescriptors()
			}
		}),
		sharedMappings.getPlugin()
	]
};
