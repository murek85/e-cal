const { app, BrowserWindow } = require('electron');
const url = require('url');
const path = require('path');

let win;

function createWindow() {
	win = new BrowserWindow({
		width: 1280,
		height: 900,
		icon: path.join(__dirname, `/dist/favicon.ico`),
		backgroundColor: '#ffffff',
		webPreferences: {
			nodeIntegration: true
		}
	});

	win.loadURL(
		url.format({
			pathname: path.join(__dirname, `/dist/index.html`),
			protocol: 'file:',
			slashes: true
		})
	);

	win.setMenuBarVisibility(false);

	win.on('closed', function () {
		win = null;
	});
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', function () {
	if (win === null) {
		createWindow();
	}
});
