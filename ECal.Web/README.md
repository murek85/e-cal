# ** # NET.Core 5 / Angular11 / Fomantic UI / Covalent UI / ElectronJS **

1. NET.Core SDK
2. VSCODE

### Screenshots

![Login](./images/2021_11_10_15_13_39_ecal.png)
![Dashboard 1](./images/2021_11_10_15_15_43_ecal.png)
![Dashboard 2](./images/2021_11_10_15_15_28_ecal.png)
![Dashboard 3](./images/2021_11_10_15_15_23_ecal.png)
![Dashboard 4](./images/)(./images/2021_11_10_15_19_00_ecal.png)
![Table](./images/2021_11_10_15_15_58_ecal.png)
![Add form](./images/2021_11_10_15_16_02_ecal.png)
![Edit form](./images/2021_11_10_15_16_07_ecal.png)
![Settings component](./images/2021_11_10_15_34_10_ecal.png)
![Settings menu](./images/2021_11_10_15_33_04_ecal.png)
![Lock](./images/2021_11_10_15_22_12_ecal.png)
![Login dark](./images/2021_11_10_15_23_58_ecal.png)
![Dashboard dark 1](./images/2021_11_10_15_24_33_ecal.png)
![Dashboard dark 2](./images/2021_11_10_15_17_21_ecal.png)
![Dashboard dark 3](./images/2021_11_10_15_24_27_ecal.png)

### Components

-   https://material.angular.io/components
-   https://teradata.github.io/covalent/
-   https://material.angular.io/components/
-   https://materialdesignicons.com/
-   https://www.materialui.co/colors/
-   https://fomantic-ui.com/
