interface Object {
	deepCopy<T>(): T;
}

interface String {
	first(): string;

	last(): string;
}

interface Array<T> {
	remove(callbackfn: (value: T, index: number) => boolean): Array<T>;
	isEmpty(): boolean;
	first(): T;
	last(): T;
	min(callbackfn: (value: T) => number): number;
	max(callbackfn: (value: T) => number): number;
	equals(array: Array<T>);
	replace(value: T, callbackfn: (value: T) => boolean);
	replaceAll(array: Array<T>): Array<T>;
	merge(array: Array<T>, equals: (value: T, other: T) => boolean): Array<T>;
	clear();
}
