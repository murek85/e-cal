Object.defineProperty(Object.prototype, 'deepCopy', {
	value: function () {
		return JSON.parse(JSON.stringify(this));
	}
});

Object.defineProperty(String.prototype, 'first', {
	value: function () {
		return this.length ? this[0] : null;
	}
});

Object.defineProperty(String.prototype, 'last', {
	value: function () {
		return this.length ? this[this.length - 1] : null;
	}
});

Object.defineProperty(Array.prototype, 'remove', {
	value: function (callbackfn: (value: any, index: number) => boolean) {
		const removed = [];

		let i = this.length;
		while (i--) {
			if (callbackfn(this[i], i)) {
				removed.push(...this.splice(i, 1));
			}
		}

		return removed;
	}
});

Object.defineProperty(Array.prototype, 'isEmpty', {
	value: function () {
		return !this.length;
	}
});

Object.defineProperty(Array.prototype, 'first', {
	value: function () {
		return this.length ? this[0] : null;
	}
});

Object.defineProperty(Array.prototype, 'last', {
	value: function () {
		return this.length ? this[this.length - 1] : null;
	}
});

Object.defineProperty(Array.prototype, 'min', {
	value: function (callbackfn: (value: any) => number) {
		const reduced = this.length ? this.reduce((left, right) => (callbackfn(left) < callbackfn(right) ? left : right)) : null;
		return reduced ? callbackfn(reduced) : null;
	}
});

Object.defineProperty(Array.prototype, 'max', {
	value: function (callbackfn: (value: any) => number) {
		const reduced = this.length ? this.reduce((left, right) => (callbackfn(left) > callbackfn(right) ? left : right)) : null;
		return reduced ? callbackfn(reduced) : null;
	}
});

Object.defineProperty(Array.prototype, 'equals', {
	value: function (array: Array<any>) {
		return JSON.stringify(this) === JSON.stringify(array);
	}
});

Object.defineProperty(Array.prototype, 'replace', {
	value: function (value: any, callbackfn: (value: any) => boolean) {
		const index = this.findIndex((element) => callbackfn(element));
		if (index >= 0) {
			this[index] = value;
		}
	}
});

Object.defineProperty(Array.prototype, 'replaceAll', {
	value: function (array: Array<any>) {
		this.clear();
		this.push(...array);
		return this;
	}
});

Object.defineProperty(Array.prototype, 'merge', {
	value: function (array: Array<any>, equals: (value: any, other: any) => boolean) {
		this.forEach((value) => {
			const found = array.find((find) => equals(find, value));
			if (found) {
				Object.assign(value, found);
			} else {
				this.push(found);
			}
		});
		return this;
	}
});

Object.defineProperty(Array.prototype, 'clear', {
	value: function () {
		this.length = 0;
	}
});
