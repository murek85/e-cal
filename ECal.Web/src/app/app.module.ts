import { NgModule, APP_INITIALIZER, Injectable, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { createCustomElement } from '@angular/elements';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { AppConfig } from './app.config';
import { rxStompConfig } from './stomp.config';

import { AppComponent } from './app.component';
import { routing } from './app-routing.module';

import { AuthComponent } from './components/account/auth/auth.component';

import { NgxMAuthOidcModule } from './core/services/oidc/oidc.module';
import { NgxMAuthOidcStorage } from './core/services/oidc/oidc-models';
import { NgxMAuthOidcService } from './core/services/oidc/oidc.service';

import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

import { DIALOG_MODAL_DATA } from './core/services/tokens/form-data.token';

import { AccountService } from './core/services/account.service';
import { AppService } from './core/services/app.service';
import { BroadcastService } from './core/services/broadcast.service';
import { InterceptorService } from './core/services/interceptor.service';
import { DashboardsService } from './core/services/dashboards.service';
import { DialogService } from './core/services/dialog.service';
import { WebSocketService } from './core/services/websocket.service';
import { WidgetsService } from './core/services/widgets.service';
import { ToastService } from './core/services/toast.service';
import { PairingService } from './core/services/pairing.service';

import { dashboardsFakeBackendProvider } from './core/fake/dashboards-fake-interceptor.service';
import { widgetsFakeBackendProvider } from './core/fake/widgets-fake-interceptor.service';
import { remoteDesktopsFakeBackendProvider } from './core/fake/remote-desktops-fake-interceptor.service';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];
const APP_COMPONENTS = [AppComponent, AuthComponent];

export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}

export function initializeApp(appConfig: AppConfig) {
	return () => appConfig.load();
}

@NgModule({
	declarations: [APP_COMPONENTS],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient]
			}
		}),
		NgxMAuthOidcModule.forRoot(),

		routing,

		APP_MODULES
	],
	providers: [
		AppConfig,
		{
			provide: APP_INITIALIZER,
			useFactory: initializeApp,
			deps: [AppConfig],
			multi: true
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: InterceptorService,
			multi: true
		},
		{
			provide: NgxMAuthOidcStorage,
			useValue: sessionStorage
		},

		AccountService,
		AppService,
		BroadcastService,
		DialogService,
		{
			provide: DIALOG_MODAL_DATA,
			useValue: {}
		},
		WebSocketService,
		ToastService,

		DashboardsService,
		PairingService,
		WidgetsService,

		dashboardsFakeBackendProvider,
		widgetsFakeBackendProvider,
		remoteDesktopsFakeBackendProvider
	],
	bootstrap: []
})
export class AppModule {
	constructor(private _injector: Injector) {}

	ngDoBootstrap() {
		const ce = createCustomElement(AppComponent, {
			injector: this._injector
		});
		customElements.define('ecal-element', ce);
	}
}
