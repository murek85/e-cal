import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { DashboardsHttpService } from './services/http/dashboards-http.service';
import { WidgetsHttpService } from './services/http/widgets-http.service';
import { ApplicationsHttpService } from './services/http/applications-http.service';
import { UsersHttpService } from './services/http/users-http.service';
import { PermissionsHttpService } from './services/http/permissions-http.service';
import { RolesHttpService } from './services/http/roles-http.service';
import { GroupsResourcesHttpService } from './services/http/groups-resources-http.service';
import { GroupsUsersHttpService } from './services/http/groups-users-http.service';
import { HealthMetricsHttpService } from './services/http/health-metrics-http.service';
import { RemoteDesktopsHttpService } from './services/http/remote-desktops-http.service';
import { NotificationsHttpService } from './services/http/notifications-http.service';

@NgModule({
	imports: [CommonModule]
})
export class CoreModule {
	// forRoot allows to override providers
	// https://angular.io/docs/ts/latest/guide/ngmodule.html#!#core-for-root
	public static forRoot(): ModuleWithProviders<CoreModule> {
		return {
			ngModule: CoreModule,
			providers: [
				DashboardsHttpService,
				WidgetsHttpService,
				ApplicationsHttpService,
				UsersHttpService,
				RolesHttpService,
				PermissionsHttpService,
				GroupsResourcesHttpService,
				GroupsUsersHttpService,
				HealthMetricsHttpService,
				RemoteDesktopsHttpService,
				NotificationsHttpService
			]
		};
	}
}
