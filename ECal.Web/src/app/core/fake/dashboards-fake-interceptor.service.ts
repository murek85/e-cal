import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import uuid from 'uuidv4';

@Injectable()
export class DashboardsFakeBackendHttpInterceptor implements HttpInterceptor {
	private _dashboardsJsonPath = 'data/dashboards.json';

	constructor(private _http: HttpClient) {}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return this.handleRequests(req, next);
	}

	handleRequests(req: HttpRequest<any>, next: HttpHandler): any {
		const { url, method } = req;
		if (url.endsWith('/dashboards') && method === 'GET') {
			req = req.clone({
				url: this._dashboardsJsonPath
			});
			return next.handle(req).pipe(delay(500));
		}
		if (url.endsWith('/dashboards') && method === 'POST') {
			const { body } = req.clone();
			body.id = uuid();
			return of(new HttpResponse({ status: 200, body })).pipe(delay(500));
		}
		if (url.match(/\/dashboards\/.*/) && method === 'PUT') {
			const { body } = req.clone();
			const dashboardId = this.getDashboardId(url);
			return of(new HttpResponse({ status: 200, body })).pipe(delay(500));
		}
		if (url.match(/\/dashboards\/.*/) && method === 'DELETE') {
			const dashboardId = this.getDashboardId(url);
			return of(new HttpResponse({ status: 200, body: dashboardId })).pipe(delay(500));
		}
		return next.handle(req);
	}

	getDashboardId(url: any) {
		const urlValues = url.split('/');
		return urlValues[urlValues.length - 1];
	}
}

export let dashboardsFakeBackendProvider = {
	provide: HTTP_INTERCEPTORS,
	useClass: DashboardsFakeBackendHttpInterceptor,
	multi: true
};
