import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import uuid from 'uuidv4';

@Injectable()
export class RemoteDesktopsFakeBackendHttpInterceptor implements HttpInterceptor {
	private _remoteDesktopsJsonPath = 'data/remotedesktops.json';

	constructor(private _http: HttpClient) {}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return this.handleRequests(req, next);
	}

	handleRequests(req: HttpRequest<any>, next: HttpHandler): any {
		const { url, method } = req;
		if (url.endsWith('/remotedesktops') && method === 'GET') {
			req = req.clone({
				url: this._remoteDesktopsJsonPath
			});
			return next.handle(req).pipe(delay(500));
		}
		if (url.endsWith('/remotedesktops') && method === 'POST') {
			const { body } = req.clone();
			body.id = uuid();
			return of(new HttpResponse({ status: 200, body })).pipe(delay(500));
		}
		if (url.match(/\/remotedesktops\/.*/) && method === 'PUT') {
			const { body } = req.clone();
			const remoteDesktopId = this.getRemoteDesktopId(url);
			return of(new HttpResponse({ status: 200, body })).pipe(delay(500));
		}
		if (url.match(/\/remotedesktops\/.*/) && method === 'DELETE') {
			const remoteDesktopId = this.getRemoteDesktopId(url);
			return of(new HttpResponse({ status: 200, body: remoteDesktopId })).pipe(delay(500));
		}
		return next.handle(req);
	}

	getRemoteDesktopId(url: any) {
		const urlValues = url.split('/');
		return urlValues[urlValues.length - 1];
	}
}

export let remoteDesktopsFakeBackendProvider = {
	provide: HTTP_INTERCEPTORS,
	useClass: RemoteDesktopsFakeBackendHttpInterceptor,
	multi: true
};
