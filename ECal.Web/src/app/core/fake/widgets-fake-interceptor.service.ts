import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import uuid from 'uuidv4';

@Injectable()
export class WidgetsFakeBackendHttpInterceptor implements HttpInterceptor {
	private _widgetsJsonPath = 'data/widgets.json';

	constructor(private _http: HttpClient) {}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return this.handleRequests(req, next);
	}

	handleRequests(req: HttpRequest<any>, next: HttpHandler): any {
		const { url, method } = req;
		if (url.endsWith('/widgets') && method === 'GET') {
			req = req.clone({
				url: this._widgetsJsonPath
			});
			return next.handle(req).pipe(delay(500));
		}
		if (url.endsWith('/widgets') && method === 'POST') {
			const { body } = req.clone();
			body.id = uuid();
			return of(new HttpResponse({ status: 200, body })).pipe(delay(500));
		}
		if (url.match(/\/widgets\/.*/) && method === 'PUT') {
			const { body } = req.clone();
			const widgetId = this.getWidgetId(url);
			return of(new HttpResponse({ status: 200, body })).pipe(delay(500));
		}
		if (url.match(/\/widgets\/.*/) && method === 'DELETE') {
			const widgetId = this.getWidgetId(url);
			return of(new HttpResponse({ status: 200, body: widgetId })).pipe(delay(500));
		}
		return next.handle(req);
	}

	getWidgetId(url: any) {
		const urlValues = url.split('/');
		return urlValues[urlValues.length - 1];
	}
}

export let widgetsFakeBackendProvider = {
	provide: HTTP_INTERCEPTORS,
	useClass: WidgetsFakeBackendHttpInterceptor,
	multi: true
};
