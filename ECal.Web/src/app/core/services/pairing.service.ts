import { takeUntil, tap } from 'rxjs/operators';
import { BroadcastService, MessageType } from './broadcast.service';
import { Subject } from 'rxjs';
import { Injectable, OnDestroy, OnInit } from '@angular/core';

@Injectable()
export class PairingService implements OnInit, OnDestroy {
	private readonly _destroyed = new Subject<void>();

	constructor(private _broadcastService: BroadcastService) {}

	ngOnInit(): void {
		// this._broadcastService
		// 	.observe(MessageType.WIDGET_PAIR_OUTSIDE_FILTER_UPDATE)
		// 	.pipe(
		// 		takeUntil(this._destroyed),
		// 		tap((message) => {})
		// 	)
		// 	.subscribe(console.log);
	}

	ngOnDestroy(): void {
		this._destroyed.next();
		this._destroyed.complete();
	}

	sendMessages(object: {} | {}[]): void {
		this.broadcastMessage(object);
	}

	private broadcastMessage(message) {
		this._broadcastService.broadcast({
			type: MessageType.WIDGET_PAIR_OUTSIDE_FILTER_UPDATE,
			payload: message
		});
	}
}
