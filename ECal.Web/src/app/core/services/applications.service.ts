import { Injectable } from '@angular/core';

import { ApplicationsHttpService } from 'src/app/core/services/http/applications-http.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { Application } from 'src/app/shared/models/application.model';
import { RsqlBuilder } from './rsql/rsql-builder';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';

@Injectable()
export class ApplicationsService {
	constructor(private _applicationsHttpService: ApplicationsHttpService) {}

	query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._applicationsHttpService.query(pagingSettings, rsql);
	}

	report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder) {
		return this._applicationsHttpService.report(pagingSettings, rsql, report);
	}

	create(model: Application) {
		return this._applicationsHttpService.create(model);
	}

	get(id) {
		return this._applicationsHttpService.get(id);
	}

	update(model: Application) {
		return this._applicationsHttpService.update(model);
	}

	delete(id) {
		return this._applicationsHttpService.delete(id);
	}
}
