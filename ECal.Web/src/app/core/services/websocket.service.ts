import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';
import * as signalR from '@microsoft/signalr';

import { AppConfig } from 'src/app/app.config';

import { BroadcastService, Message, MessageType } from './broadcast.service';

export interface WebsocketEvent {
	privilege: string;
	idUser: string;
}

export interface WebsocketEventObject {
	operation: OperationType;
	idObject: any;
}

interface Topic {
	symbol: string;
	type: ResourceType;
}

export enum OperationType {
	ADD = 'add',
	EDIT = 'edit',
	REMOVE = 'remove',
	REFRESH = 'refresh'
}

export class ResourceType {
	static APPLICATIONS = 'resource.type.applications';
	static USERS = 'resource.type.users';
	static ROLES = 'resource.type.roles';
	static PERMISSIONS = 'resource.type.permissions';
	static GROUPS_USERS = 'resource.type.groups-users';
	static GROUPS_RESOURCES = 'resource.type.groups-resources';
	static WIDGETS = 'resource.type.widgets';
	static DASHBOARDS = 'resource.type.dashboards';
	static NOTIFICATIONS = 'resource.type.notifications';
	static REMOTE_DESKTOPS = 'resource.type.remote-desktops';
	static ACCOUNT = 'resource.type.account';
}

const topics: Array<Topic> = [
	{ symbol: 'applications', type: ResourceType.APPLICATIONS },
	{ symbol: 'users', type: ResourceType.USERS },
	{ symbol: 'roles', type: ResourceType.ROLES },
	{ symbol: 'permissions', type: ResourceType.PERMISSIONS },
	{ symbol: 'groups-users', type: ResourceType.GROUPS_USERS },
	{ symbol: 'groups-resources', type: ResourceType.GROUPS_RESOURCES },
	{ symbol: 'widgets', type: ResourceType.WIDGETS },
	{ symbol: 'dashboards', type: ResourceType.DASHBOARDS },
	{ symbol: 'notifications', type: ResourceType.NOTIFICATIONS },
	{ symbol: 'remote-desktops', type: ResourceType.REMOTE_DESKTOPS },
	{ symbol: 'account', type: ResourceType.ACCOUNT }
];

@Injectable()
export class WebSocketService {
	private _hubConnection: signalR.HubConnection;

	constructor(private _broadcastService: BroadcastService) {
		this._hubConnection = new signalR.HubConnectionBuilder()
			.withUrl(`${AppConfig.settings.system.apiUrl}/api/hubs/broadcast`, { logMessageContent: true })
			.withAutomaticReconnect()
			.build();
	}

	connect(): void {
		const transfer = (topic: Topic) =>
			this._hubConnection.on(topic.symbol, (message: Message) =>
				this._broadcastService.broadcast({ type: MessageType.RESOURCES_CHANGED, payload: { type: topic.type, message } })
			);

		if (this._hubConnection.state === signalR.HubConnectionState.Disconnected) {
			this._hubConnection
				.start()
				.then(() => topics.forEach((topic: Topic) => transfer(topic)))
				.catch((err) => console.log('hub error', err));
		}
	}

	disconnect(): void {
		if (this._hubConnection.state === signalR.HubConnectionState.Connected) {
			this._hubConnection.stop().then(() => console.log('hub disconnected'));
		}
	}
}
