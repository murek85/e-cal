import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { tap } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { WidgetsHttpService } from 'src/app/core/services/http/widgets-http.service';

import { Result } from 'src/app/shared/models/result.model';
import { Widget } from 'src/app/shared/models/widget.model';

@Injectable()
export class WidgetsService {
	private _widgetsSource = new BehaviorSubject<Widget[]>([]);

	widgets = this._widgetsSource.asObservable();

	changeWidgets(data: Widget[]): void {
		this._widgetsSource.next(data);
	}

	constructor(private _tdLoadingService: TdLoadingService, private _widgetsHttpService: WidgetsHttpService) {}

	query() {
		return this._widgetsHttpService.query<Result<Widget[]>>().pipe(tap((result) => this.changeWidgets(result.data)));
	}

	create(model: Widget) {
		return this._widgetsHttpService.create(model);
	}

	get(id) {
		return this._widgetsHttpService.get(id);
	}

	update(model: Widget) {
		return this._widgetsHttpService.update(model);
	}

	delete(id) {
		return this._widgetsHttpService.delete(id);
	}
}
