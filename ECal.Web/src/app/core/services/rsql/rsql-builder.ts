import { RsqlExpression } from './rsql-expression';
import { Operator } from './rsql-operator';

export class RsqlBuilder {
	private readonly _expressions: Array<RsqlExpression> = [];

	create(field: string, operator: Operator, value: any): RsqlBuilder {
		this._expressions.push(RsqlExpression.create(field, operator, value));
		return this;
	}

	and() {
		if (!this._expressions.last().isSeparator()) {
			this._expressions.push(RsqlExpression.and());
		}
		return this;
	}

	or() {
		if (!this._expressions.last().isSeparator()) {
			this._expressions.push(RsqlExpression.or());
		}
		return this;
	}

	isEmpty() {
		return this._expressions.isEmpty();
	}

	build() {
		if (this.isEmpty()) {
			return '';
		}

		if (this._expressions.first().isSeparator()) {
			this._expressions.shift();
		}

		if (this._expressions.last().isSeparator()) {
			this._expressions.pop();
		}

		return this._expressions.map((expression) => expression.build()).join('');
	}
}
