import { Operator } from './rsql-operator';

const SEPARATOR_AND = ';';
const SEPARATOR_OR = ',';

export class RsqlExpression {
	private _separator: string;
	private _field: string;
	private _operator: Operator;
	private _value: any;

	private get operator(): string {
		switch (this._operator) {
			case Operator.EQUALS:
				return '=eq=';
			case Operator.LIKE:
				return '=like=';
			case Operator.IN:
				return '=in=';
			case Operator.NOT_EQUALS:
				return '!=';
			case Operator.GREATER_THAN:
				return '=gt=';
			case Operator.GREATER_OR_EQUALS:
				return '=ge=';
			case Operator.LESS_THAN:
				return '=lt=';
			case Operator.LESS_OR_EQUALS:
				return '=le=';

			default:
				// Operator.EQUALS
				return '=eq=';
		}
	}

	private get value(): string {
		switch (this._operator) {
			case Operator.IN:
				return `(${this._value.join(',')})`;
			default:
				return this._value.toString();
		}
	}

	static create(field: string, operator: Operator, value: any) {
		const expression = new RsqlExpression();
		expression._field = field;
		expression._operator = operator;
		expression._value = value;
		return expression;
	}

	static and(): RsqlExpression {
		const expression = new RsqlExpression();
		expression._separator = SEPARATOR_AND;
		return expression;
	}

	static or(): RsqlExpression {
		const expression = new RsqlExpression();
		expression._separator = SEPARATOR_OR;
		return expression;
	}

	isSeparator(): boolean {
		return !!this._separator;
	}

	build() {
		return this._separator || `${this._field}${this.operator}${this.value}`;
	}
}
