export enum Operator {
	LIKE = 'like',
	EQUALS = 'equals',
	NOT_EQUALS = 'not_equals',
	GREATER_THAN = 'greater_than',
	GREATER_OR_EQUALS = 'greater_or_equals',
	LESS_THAN = 'less_than',
	LESS_OR_EQUALS = 'less_or_equals',
	IN = 'in'
}
