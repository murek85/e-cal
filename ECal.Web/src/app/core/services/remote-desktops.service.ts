import { RemoteDesktop } from 'src/app/shared/models/remote-desktops.model';
import { Injectable } from '@angular/core';

import { RemoteDesktopsHttpService } from 'src/app/core/services/http/remote-desktops-http.service';

import { Result } from 'src/app/shared/models/result.model';

@Injectable()
export class RemoteDesktopsService {
	constructor(private _remoteDesktopsHttpService: RemoteDesktopsHttpService) {}

	query() {
		return this._remoteDesktopsHttpService.query<Result<RemoteDesktop[]>>();
	}
}
