import { Injectable } from '@angular/core';

import { UsersHttpService } from 'src/app/core/services/http/users-http.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';
import { User } from 'src/app/shared/models/user.model';
import { RsqlBuilder } from './rsql/rsql-builder';

@Injectable()
export class UsersService {
	constructor(private _usersHttpService: UsersHttpService) {}

	query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._usersHttpService.query(pagingSettings, rsql);
	}

	report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder) {
		return this._usersHttpService.report(pagingSettings, rsql, report);
	}

	create(model: User) {
		return this._usersHttpService.create(model);
	}

	update(model: User) {
		return this._usersHttpService.update(model);
	}

	delete(id) {
		return this._usersHttpService.delete(id);
	}

	current() {
		return this._usersHttpService.current();
	}
}
