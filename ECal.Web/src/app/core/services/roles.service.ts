import { Injectable } from '@angular/core';

import { RolesHttpService } from 'src/app/core/services/http/roles-http.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { Role } from 'src/app/shared/models/role.model';
import { RsqlBuilder } from './rsql/rsql-builder';

@Injectable()
export class RolesService {
	constructor(private _rolesHttpService: RolesHttpService) {}

	query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._rolesHttpService.query(pagingSettings, rsql);
	}

	create(model: Role) {
		return this._rolesHttpService.create(model);
	}

	get(id) {
		return this._rolesHttpService.get(id);
	}

	update(model: Role) {
		return this._rolesHttpService.update(model);
	}

	delete(id) {
		return this._rolesHttpService.delete(id);
	}
}
