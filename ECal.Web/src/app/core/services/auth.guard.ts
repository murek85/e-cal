import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { isNullOrUndefined } from '../tools';

import { AccountService } from './account.service';

import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
	constructor(private _router: Router, private _accountService: AccountService) {}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		if (this._accountService.getLock()) {
			this._router.navigate(['/lock']);
		}

		if (this._accountService.getLoggedIn()) {
			const routePermissions = route.data.permissions;
			return this._accountService.getPermissionsLogged().pipe(
				map((permissions: PermissionsTypes[]) => {
					if (!isNullOrUndefined(permissions)) {
						if (!isNullOrUndefined(routePermissions)) {
							const permissionsIntersection = routePermissions.only.filter((only) => permissions.map((permission) => permission).includes(only));

							if (permissionsIntersection && permissionsIntersection.length > 0) {
								return true;
							}
						} else {
							return true;
						}
					}

					// access denied
					this._router.navigate(['/accessdenied'], { queryParams: { returnUrl: this._router.url } });
					return false;
				})
			);
		}

		this._router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
		return of(false);
	}
}
