import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';

import { AppConfig } from 'src/app/app.config';

import { BaseHttpService, Method } from './base-http.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { RsqlBuilder } from '../rsql/rsql-builder';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';

@Injectable()
export class PermissionsHttpService extends BaseHttpService {
	constructor(protected _http: HttpClient) {
		super(_http);
	}

	query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this.request({
			method: Method.GET,
			url: `${AppConfig.settings.system.apiUrl}/api/permissions/list`,
			params: pagingSettings,
			rsql: rsql
		});
	}

	report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder) {
		return this.request<HttpResponse<Blob>>({
			method: Method.GET,
			url: `${AppConfig.settings.system.apiUrl}/api/permissions/report`,
			params: pagingSettings,
			rsql: rsql,
			report: report,
			observe: 'response',
			responseType: 'blob'
		});
	}

	get(id) {
		return this._http.get(`${AppConfig.settings.system.apiUrl}/api/permissions/${id}`);
	}

	create(model: Permission) {
		return this._http.post(`${AppConfig.settings.system.apiUrl}/api/permissions`, model);
	}

	update(model: Permission) {
		return this._http.put(`${AppConfig.settings.system.apiUrl}/api/permissions`, model);
	}

	delete(id) {
		return this._http.delete(`${AppConfig.settings.system.apiUrl}/api/permissions/${id}`);
	}
}
