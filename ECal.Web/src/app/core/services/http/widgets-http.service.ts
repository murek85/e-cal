import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppConfig } from 'src/app/app.config';

import { BaseHttpService } from './base-http.service';

import { Widget } from 'src/app/shared/models/widget.model';

@Injectable()
export class WidgetsHttpService extends BaseHttpService {
	private _apiUrl = '/widgets';

	constructor(protected _http: HttpClient) {
		super(_http);
	}

	query<T>() {
		return this._http.get<T>(this._apiUrl);
	}

	get(id) {
		return this._http.get(`${AppConfig.settings.system.apiUrl}/api/widgets/${id}`);
	}

	create(model: Widget) {
		return this._http.post(`${AppConfig.settings.system.apiUrl}/api/widgets`, model);
	}

	update(model: Widget) {
		return this._http.put(`${AppConfig.settings.system.apiUrl}/api/widgets`, model);
	}

	delete(id) {
		return this._http.delete(`${AppConfig.settings.system.apiUrl}/api/widgets/${id}`);
	}
}
