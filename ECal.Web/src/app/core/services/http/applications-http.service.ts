import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';

import { AppConfig } from 'src/app/app.config';

import { BaseHttpService, Method } from './base-http.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { Application } from 'src/app/shared/models/application.model';
import { RsqlBuilder } from '../rsql/rsql-builder';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';

@Injectable()
export class ApplicationsHttpService extends BaseHttpService {
	constructor(protected _http: HttpClient) {
		super(_http);
	}

	query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this.request({
			method: Method.GET,
			url: `${AppConfig.settings.system.apiUrl}/api/applications/list`,
			params: pagingSettings,
			rsql: rsql
		});
	}

	report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder) {
		return this.request<HttpResponse<Blob>>({
			method: Method.GET,
			url: `${AppConfig.settings.system.apiUrl}/api/applications/report`,
			params: pagingSettings,
			rsql: rsql,
			report: report,
			observe: 'response',
			responseType: 'blob'
		});
	}

	get(id) {
		return this._http.get(`${AppConfig.settings.system.apiUrl}/api/applications/${id}`);
	}

	create(model: Application) {
		return this._http.post(`${AppConfig.settings.system.apiUrl}/api/applications`, model);
	}

	update(model: Application) {
		return this._http.put(`${AppConfig.settings.system.apiUrl}/api/applications`, model);
	}

	delete(id) {
		return this._http.delete(`${AppConfig.settings.system.apiUrl}/api/applications/${id}`);
	}
}
