import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppConfig } from 'src/app/app.config';

import { BaseHttpService } from './base-http.service';

@Injectable()
export class HealthMetricsHttpService extends BaseHttpService {
	constructor(protected _http: HttpClient) {
		super(_http);
	}

	getHealthMetrics(): Observable<{ entries }> {
		return this._http.get<{ entries }>(`${AppConfig.settings.system.apiUrl}/health-metrics`);
	}
}
