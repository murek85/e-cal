import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';

import { AppConfig } from 'src/app/app.config';

import { BaseHttpService, Method } from './base-http.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { User } from 'src/app/shared/models/user.model';
import { RsqlBuilder } from '../rsql/rsql-builder';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';

@Injectable()
export class UsersHttpService extends BaseHttpService {
	constructor(protected _http: HttpClient) {
		super(_http);
	}

	query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this.request({
			method: Method.GET,
			url: `${AppConfig.settings.system.apiUrl}/api/users/list`,
			params: pagingSettings,
			rsql: rsql
		});
	}

	report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder) {
		return this.request<HttpResponse<Blob>>({
			method: Method.GET,
			url: `${AppConfig.settings.system.apiUrl}/api/users/report`,
			params: pagingSettings,
			rsql: rsql,
			report: report,
			observe: 'response',
			responseType: 'blob'
		});
	}

	get(id) {
		return this._http.get(`${AppConfig.settings.system.apiUrl}/api/users/${id}`);
	}

	create(model: User) {
		return this._http.post(`${AppConfig.settings.system.apiUrl}/api/users`, model);
	}

	update(model: User) {
		return this._http.put(`${AppConfig.settings.system.apiUrl}/api/users`, model);
	}

	delete(id) {
		return this._http.delete(`${AppConfig.settings.system.apiUrl}/api/users/${id}`);
	}

	current() {
		return this._http.get(`${AppConfig.settings.system.apiUrl}/api/account/user`);
	}

	registerAccount(model: any) {
		return new Promise((resolve, reject) => {
			const params = new HttpParams();

			this._http.post(`${AppConfig.settings.system.apiUrl}/api/account/register`, model).subscribe(
				(result) => resolve(result),
				(err) => reject(err.error)
			);
		});
	}

	reminderPassword(model: any) {
		return new Promise((resolve, reject) => {
			const params = new HttpParams();

			this._http.post(`${AppConfig.settings.system.apiUrl}/api/account/password/reminder`, model, { params }).subscribe(
				(result) => resolve(result),
				(err) => reject(err.error)
			);
		});
	}

	confirmPassword(model: any, userId: string, token: string) {
		return new Promise((resolve, reject) => {
			const headers = new HttpHeaders().set('Content-Type', 'application/json').set('userId', userId).set('token', token);

			this._http.post(`${AppConfig.settings.system.apiUrl}/api/account/password/confirm`, model, { headers }).subscribe(
				(result) => resolve(result),
				(err) => reject(err.error)
			);
		});
	}

	confirmEmail(userId: string, token: string) {
		return new Promise((resolve, reject) => {
			const headers = new HttpHeaders().set('Content-Type', 'application/json').set('userId', userId).set('token', token);

			this._http.post(`${AppConfig.settings.system.apiUrl}/api/account/email/confirm`, {}, { headers }).subscribe(
				(result) => resolve(result),
				(err) => reject(err.error)
			);
		});
	}

	activateLogin2step(model: any) {
		return new Promise((resolve, reject) => {
			const params = new HttpParams().set('email', model.email);

			this._http.post(`${AppConfig.settings.system.apiUrl}/api/account/login2step/activate`, {}, { params }).subscribe(
				(result) => resolve(result),
				(err) => reject(err.error)
			);
		});
	}

	confirmLogin2step(userId: string, token: string) {
		return new Promise((resolve, reject) => {
			const headers = new HttpHeaders().set('Content-Type', 'application/json').set('userId', userId).set('token', token);

			this._http.post(`${AppConfig.settings.system.apiUrl}/api/account/login2step/confirm`, {}, { headers }).subscribe(
				(result) => resolve(result),
				(err) => reject(err.error)
			);
		});
	}
}
