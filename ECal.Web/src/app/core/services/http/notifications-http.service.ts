import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { AppConfig } from 'src/app/app.config';

import { BaseHttpService, Method } from './base-http.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { Application } from 'src/app/shared/models/application.model';
import { RsqlBuilder } from '../rsql/rsql-builder';

@Injectable()
export class NotificationsHttpService extends BaseHttpService {
	constructor(protected _http: HttpClient) {
		super(_http);
	}

	query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this.request({
			method: Method.GET,
			url: `${AppConfig.settings.system.apiUrl}/api/notifications/list`,
			params: pagingSettings,
			rsql: rsql
		});
	}

	get(id) {
		return this._http.get(`${AppConfig.settings.system.apiUrl}/api/notifications/${id}`);
	}

	create(model: Application) {
		return this._http.post(`${AppConfig.settings.system.apiUrl}/api/notifications`, model);
	}

	update(model: Application) {
		return this._http.put(`${AppConfig.settings.system.apiUrl}/api/notifications`, model);
	}

	delete(id) {
		return this._http.delete(`${AppConfig.settings.system.apiUrl}/api/notifications/${id}`);
	}
}
