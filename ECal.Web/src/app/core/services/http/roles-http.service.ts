import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { AppConfig } from 'src/app/app.config';

import { BaseHttpService, Method } from './base-http.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { Role } from 'src/app/shared/models/role.model';
import { RsqlBuilder } from '../rsql/rsql-builder';

@Injectable()
export class RolesHttpService extends BaseHttpService {
	constructor(protected _http: HttpClient) {
		super(_http);
	}

	query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this.request({
			method: Method.GET,
			url: `${AppConfig.settings.system.apiUrl}/api/roles/list`,
			params: pagingSettings,
			rsql: rsql
		});
	}

	get(id) {
		return this._http.get(`${AppConfig.settings.system.apiUrl}/api/roles/${id}`);
	}

	create(model: Role) {
		return this._http.post(`${AppConfig.settings.system.apiUrl}/api/roles`, model);
	}

	update(model: Role) {
		return this._http.put(`${AppConfig.settings.system.apiUrl}/api/roles`, model);
	}

	delete(id) {
		return this._http.delete(`${AppConfig.settings.system.apiUrl}/api/roles/${id}`);
	}
}
