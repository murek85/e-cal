import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParameterCodec, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RsqlBuilder } from '../rsql/rsql-builder';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';

export enum Method {
	GET = 'GET',
	POST = 'POST',
	PUT = 'PUT',
	PATCH = 'PATCH',
	DELETE = 'DELETE',
	HEAD = 'HEAD'
}

export class Resource {
	url: string;
	method?: Method;
	data?: object;
	form?: object;
	params?: object;
	encode?: boolean;
	headers?: object;
	rsql?: RsqlBuilder;
	report?: ReportBuilder;
	responseType?: 'arraybuffer' | 'blob' | 'text' | 'json' | 'image/png';
	observe?: 'body' | 'events' | 'response';
	credentials?: boolean;
}

class HttpValueEncoder implements HttpParameterCodec {
	decodeKey(key: string): string {
		return key;
	}

	decodeValue(value: string): string {
		return decodeURIComponent(value);
	}

	encodeKey(key: string): string {
		return key;
	}

	encodeValue(value: string): string {
		return encodeURIComponent(value);
	}
}

@Injectable()
export class BaseHttpService {
	protected _headers = new HttpHeaders();

	constructor(protected _http: HttpClient) {}

	private url(resource: Resource): string {
		if (resource.url.startsWith('http') || resource.url.startsWith('www')) {
			return resource.url;
		}
	}

	private params(data: object, rsql: RsqlBuilder, report: ReportBuilder): HttpParams {
		let params = new HttpParams({
			encoder: new HttpValueEncoder()
		});
		if (data) {
			Object.keys(data).forEach((key) => (params = params.set(key, data[key])));
		}
		if (rsql && !rsql.isEmpty()) {
			params = params.set('query', rsql.build());
		}
		if (report) {
			Object.keys(report).forEach((key) => (params = params.set(key, report[key])));
		}
		return params;
	}

	private headers(data: object): HttpHeaders {
		let headers = new HttpHeaders();
		if (data) {
			Object.keys(data).forEach((key) => (headers = headers.set(key, data[key])));
		}
		return headers;
	}

	private encode(data: any): string {
		const params = new URLSearchParams();
		if (data) {
			Object.keys(data).forEach((key) => params.set(key, data[key]));
		}
		return params.toString();
	}

	private form(data: object): FormData {
		const form = new FormData();
		if (data) {
			Object.keys(data).forEach((key) => form.append(key, data[key]));
		}
		return form;
	}

	private data(resource: Resource): any {
		if (resource.form) {
			return this.form(resource.form);
		}
		return resource.encode ? this.encode(resource.data) : resource.data;
	}

	private options(resource: Resource): object {
		const options = {};

		if (resource.headers) {
			options['headers'] = this.headers(resource.headers);
		}
		if (resource.params || resource.rsql || resource.report) {
			options['params'] = this.params(resource.params, resource.rsql, resource.report);
		}
		if (resource.responseType) {
			options['responseType'] = resource.responseType;
		}
		if (resource.observe) {
			options['observe'] = resource.observe;
		}
		if (resource.credentials) {
			options['withCredentials'] = resource.credentials;
		}

		return options;
	}

	private _get<T>(resource: Resource): Observable<T> {
		return this._http.get<T>(this.url(resource), this.options(resource));
	}

	private _post<T>(resource: Resource): Observable<T> {
		return this._http.post<T>(this.url(resource), this.data(resource), this.options(resource));
	}

	private _put<T>(resource: Resource): Observable<T> {
		return this._http.put<T>(this.url(resource), this.data(resource), this.options(resource));
	}

	private _patch<T>(resource: Resource): Observable<T> {
		return this._http.patch<T>(this.url(resource), this.data(resource), this.options(resource));
	}

	private _delete<T>(resource: Resource): Observable<T> {
		return this._http.delete<T>(this.url(resource), this.options(resource));
	}

	private _head<T>(resource: Resource): Observable<T> {
		return this._http.head<T>(this.url(resource), this.options(resource));
	}

	protected request<T>(resource: Resource | string): Observable<T> {
		if (typeof resource === 'string') {
			return this._http.get<T>(resource);
		} else {
			switch (resource.method || Method.GET) {
				case Method.POST:
					return this._post(resource);
				case Method.PUT:
					return this._put(resource);
				case Method.PATCH:
					return this._patch(resource);
				case Method.DELETE:
					return this._delete(resource);
				case Method.HEAD:
					return this._head(resource);
				default:
					return this._get(resource);
			}
		}
	}
}
