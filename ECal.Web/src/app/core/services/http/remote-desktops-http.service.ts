import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseHttpService } from './base-http.service';

@Injectable()
export class RemoteDesktopsHttpService extends BaseHttpService {
	private _apiUrl = '/remotedesktops';

	constructor(protected _http: HttpClient) {
		super(_http);
	}

	query<T>() {
		return this._http.get<T>(this._apiUrl);
	}
}
