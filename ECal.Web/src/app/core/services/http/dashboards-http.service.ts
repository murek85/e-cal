import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppConfig } from 'src/app/app.config';

import { BaseHttpService } from './base-http.service';

import { Dashboard } from 'src/app/shared/models/dashboard.model';

@Injectable()
export class DashboardsHttpService extends BaseHttpService {
	private _apiUrl = '/dashboards';

	constructor(protected _http: HttpClient) {
		super(_http);
	}

	query<T>() {
		return this._http.get<T>(this._apiUrl);
	}

	get(id) {
		return this._http.get(`${AppConfig.settings.system.apiUrl}/api/dashboards/${id}`);
	}

	create(model: Dashboard) {
		return this._http.post(`${AppConfig.settings.system.apiUrl}/api/dashboards`, model);
	}

	update(model: Dashboard) {
		return this._http.put(`${AppConfig.settings.system.apiUrl}/api/dashboards`, model);
	}

	delete(id) {
		return this._http.delete(`${AppConfig.settings.system.apiUrl}/api/dashboards/${id}`);
	}
}
