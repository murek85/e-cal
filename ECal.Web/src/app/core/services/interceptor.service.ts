import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { isNullOrUndefined } from '../../core/tools';

import { AppConfig } from 'src/app/app.config';

import { NgxMAuthOidcService } from './oidc/oidc.service';

@Injectable()
export class InterceptorService implements HttpInterceptor {
	constructor(private _oidcService: NgxMAuthOidcService) {}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		if (!isNullOrUndefined(AppConfig.settings)) {
			const authorizationHeader = this._oidcService.authorizationHeader();
			if (!isNullOrUndefined(authorizationHeader)) {
				request = request.clone({
					setHeaders: {
						Authorization: `${authorizationHeader}`
					}
				});
			}
		}

		return next.handle(request).pipe(
			map((event: HttpEvent<any>) => {
				return event;
			}),
			catchError((err: HttpErrorResponse) => {
				return throwError(err);
			})
		);
	}
}
