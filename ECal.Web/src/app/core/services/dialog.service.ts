import { Overlay, OverlayConfig, OverlayRef, PositionStrategy } from '@angular/cdk/overlay';
import { ComponentPortal, ComponentType, PortalInjector } from '@angular/cdk/portal';
import { ComponentRef, Injectable, Injector } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DIALOG_MODAL_DATA } from './tokens/form-data.token';

interface DialogConfig {
	panelClass?: string;
	hasBackdrop?: boolean;
	backdropClass?: string;
	data?: any;
}

const DEFAULT_CONFIG: DialogConfig = {
	hasBackdrop: false,
	backdropClass: 'mrk-dialog-modal-backdrop',
	panelClass: 'mrk-modal-panel'
};

export class DialogOverlayRef {
	public events = new BehaviorSubject<{ type: 'accept' | 'deny' | 'close' }>(null);

	constructor(private _overlayRef: OverlayRef) {}

	close(): void {
		this.events.next({ type: 'close' });
		this._overlayRef.dispose();
	}
}

@Injectable()
export class DialogService {
	public dialogRef: DialogOverlayRef;

	constructor(private _injector: Injector, private _overlay: Overlay) {}

	open<T>(component: ComponentType<T>, config: DialogConfig = {}): DialogOverlayRef {
		const modalConfig = { ...DEFAULT_CONFIG, ...config };
		const overlayRef = this.createOverlay(modalConfig);
		const dialogRef = new DialogOverlayRef(overlayRef);
		const overlayComponent = this.attachModalContainer<T>(overlayRef, modalConfig, dialogRef, component);

		this.dialogRef = dialogRef;

		return dialogRef;
	}

	private attachModalContainer<T>(overlayRef: OverlayRef, config: DialogConfig, dialogRef: DialogOverlayRef, component: ComponentType<T>): T {
		const injector = this.createInjector(config, dialogRef);

		const containerPortal = new ComponentPortal(component, null, injector);
		const containerRef: ComponentRef<T> = overlayRef.attach(containerPortal);
		return containerRef.instance;
	}

	private createInjector(config: DialogConfig, dialogRef: DialogOverlayRef): PortalInjector {
		const injectionTokens = new WeakMap();

		injectionTokens.set(DialogOverlayRef, dialogRef);
		injectionTokens.set(DIALOG_MODAL_DATA, config.data);

		return new PortalInjector(this._injector, injectionTokens);
	}

	private getOverlayConfig(config: DialogConfig): OverlayConfig {
		const positionStrategy = this._overlay.position().global().centerHorizontally().centerVertically();

		const overlayConfig = new OverlayConfig({
			hasBackdrop: config.hasBackdrop,
			backdropClass: config.backdropClass,
			panelClass: config.panelClass,
			scrollStrategy: this._overlay.scrollStrategies.block(),
			positionStrategy
		});

		return overlayConfig;
	}

	private createOverlay(config: DialogConfig): OverlayRef {
		const overlayConfig = this.getOverlayConfig(config);
		return this._overlay.create(overlayConfig);
	}
}
