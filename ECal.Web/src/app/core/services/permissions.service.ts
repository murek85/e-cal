import { Injectable } from '@angular/core';

import { PermissionsHttpService } from 'src/app/core/services/http/permissions-http.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';
import { RsqlBuilder } from './rsql/rsql-builder';

@Injectable()
export class PermissionsService {
	constructor(private _permissionsHttpService: PermissionsHttpService) {}

	query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._permissionsHttpService.query(pagingSettings, rsql);
	}

	report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder) {
		return this._permissionsHttpService.report(pagingSettings, rsql, report);
	}

	create(model: Permission) {
		return this._permissionsHttpService.create(model);
	}

	get(id) {
		return this._permissionsHttpService.get(id);
	}

	update(model: Permission) {
		return this._permissionsHttpService.update(model);
	}

	delete(id) {
		return this._permissionsHttpService.delete(id);
	}
}
