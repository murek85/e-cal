import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter, map } from 'rxjs/operators';

declare var $: any;

export class ToastConfig {
	position?: string;
	class?: string;
	showIcon?: any;
	displayTime?: string;
	showProgress?: string;
	classProgress?: string;
}

const DEFAULT_CONFIG: ToastConfig = {
	position: 'top center',
	class: 'neutral',
	showIcon: false,
	showProgress: 'top'
};

@Injectable()
export class ToastService {
	open(title, message, config: ToastConfig = {}): void {
		const toastConfig = { ...DEFAULT_CONFIG, ...config };
		$('body').toast({
			position: toastConfig.position,
			class: toastConfig.class,
			showIcon: toastConfig.showIcon,
			showProgress: toastConfig.showProgress,
			// displayTime: 0,
			newestOnTop: true,
			className: {
				box: 'toast-box'
			},
			title,
			message
		});
	}
}
