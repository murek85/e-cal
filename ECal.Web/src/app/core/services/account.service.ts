import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { BehaviorSubject, Observable, forkJoin, from } from 'rxjs';

import { NgxMAuthOidcService } from './oidc/oidc.service';

import { UsersHttpService } from './http/users-http.service';

import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

import { User } from 'src/app/shared/models/user.model';
import { WebSocketService } from './websocket.service';
import { finalize, tap } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class AccountService {
	loginSocialProviders: { name: string; key: string; icon: string; class: string }[] = [
		{ name: 'Google', key: 'Google', icon: 'google', class: 'google plus' },
		{ name: 'Facebook', key: 'Facebook', icon: 'facebook f', class: 'facebook' },
		{ name: 'Twitter', key: 'Twitter', icon: 'twitter', class: 'twitter' }
	];
	loginDomainProviders: { name: string; key: string; icon: string }[] = [{ name: 'Windows', key: 'Windows', icon: 'microsoft' }];

	private _userLoggedSource = new BehaviorSubject<User>(null);

	userLogged = this._userLoggedSource.asObservable();

	changeUserLogged(userLogged: User): void {
		this._userLoggedSource.next(userLogged);
	}

	constructor(
		private _router: Router,
		private _usersHttpService: UsersHttpService,
		private _permissionsService: NgxPermissionsService,
		private _oidcService: NgxMAuthOidcService,
		private _webSocketService: WebSocketService
	) {}

	getUserLogged(): Promise<User> {
		return new Promise((resolve, reject) => {
			forkJoin(this._oidcService.loadUserProfile(), this.loadUserLogged()).subscribe(
				([loadUserProfile, loadUserLogged]: [any, User]) => {
					const user: User = {
						id: loadUserProfile.sub,
						email: loadUserProfile.email,
						userName: loadUserProfile.email,
						firstName: 'Marcin', //loadUserLogged?.firstName,
						lastName: 'Murawski', //loadUserLogged?.lastName,
						fullName: 'Marcin Murawski', //loadUserLogged?.fullName,
						phoneNumber: loadUserLogged?.phoneNumber,
						lastValidLogin: loadUserLogged?.lastValidLogin,
						lastIncorrectLogin: loadUserLogged?.lastIncorrectLogin,
						logins: loadUserLogged.logins,
						photo: 'assets/images/profile/Udx14YD4goxp.jpg',
						roles: [],
						permissions: []
					};

					this._permissionsService.loadPermissions([]);
					this._webSocketService.connect();

					this.changeUserLogged(user);
					resolve(user);
				},
				(err) => reject(err)
			);
		});
	}

	getPermissionsLogged(): Observable<PermissionsTypes[]> {
		return new Observable((observer) => observer.next([]));
	}

	getLoggedIn(): boolean {
		return this._oidcService.hasValidAccessToken();
	}

	getLock(): boolean {
		return JSON.parse(sessionStorage.getItem('lock'));
	}

	loadUserLogged() {
		return this._usersHttpService.current();
	}

	updateUserLogged(model: User) {
		return this._usersHttpService.update(model);
	}

	loginPasswordFlow(email: string, password: string): Promise<object> {
		return new Promise((resolve, reject) => {
			this._oidcService
				.fetchTokenUsingPasswordFlow(email, password)
				.then((token) => this.getUserLogged().then((_) => resolve(token)))
				.catch((err) => reject(err.error));
		});
	}

	loginDomainFlow(): Promise<object> {
		return new Promise((resolve, reject) => {
			this._oidcService
				.fetchTokenUsingDomainFlow()
				.then((token) => this.getUserLogged().then((_) => resolve(token)))
				.catch((err) => reject(err.error));
		});
	}

	loginAuthorizationCode(providerKey): void {
		const params = { provider: providerKey };
		this._oidcService.initAuthorizationCode(params);
	}

	registerAccount(model: any) {
		return this._usersHttpService.registerAccount(model);
	}

	reminderPassword(model: any) {
		return this._usersHttpService.reminderPassword(model);
	}

	confirmPassword(model: any, userId: string, token: string) {
		return this._usersHttpService.confirmPassword(model, userId, token);
	}

	confirmEmail(userId: string, token: string) {
		return this._usersHttpService.confirmEmail(userId, token);
	}

	activateLogin2step(model: any) {
		return this._usersHttpService.activateLogin2step(model);
	}

	confirmLogin2step(userId: string, token: string) {
		return this._usersHttpService.confirmLogin2step(userId, token);
	}

	logout(): void {
		from(this._router.navigate(['/']))
			.pipe(
				tap(() => this.changeUserLogged(null)),
				tap(() => this._webSocketService.disconnect()),
				tap(() => sessionStorage.setItem('lock', JSON.stringify(false))),
				finalize(() => this._oidcService.logout(true))
			)
			.subscribe();
	}

	lock(returnUrl) {
		this._router.navigate(['/lock'], { queryParams: { returnUrl } });
		sessionStorage.setItem('lock', JSON.stringify(true));
	}

	unlock(returnUrl) {
		this._router.navigateByUrl(returnUrl);
		sessionStorage.setItem('lock', JSON.stringify(false));
	}
}
