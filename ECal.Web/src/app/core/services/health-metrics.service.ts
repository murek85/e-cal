import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { HealthMetricsHttpService } from './http/health-metrics-http.service';

@Injectable()
export class HealthMetricsService {
	private _healthMetricsSource = new BehaviorSubject<{ entries }>(null);

	healthMetrics = this._healthMetricsSource.asObservable();

	changeHealthMetrics(data: { entries }): void {
		this._healthMetricsSource.next(data);
	}

	constructor(private _healthMetricsHttpService: HealthMetricsHttpService) {}

	getHealthMetrics() {
		return this._healthMetricsHttpService
			.getHealthMetrics()
			.pipe(tap((healthz) => this.changeHealthMetrics(healthz)))
			.subscribe();
	}
}
