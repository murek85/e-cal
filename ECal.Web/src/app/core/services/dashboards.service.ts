import { Injectable } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { BehaviorSubject } from 'rxjs';
import { filter, map, tap, toArray, mergeMap, take, delay, finalize } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { DashboardsHttpService } from 'src/app/core/services/http/dashboards-http.service';

import { HeaderConfig } from 'src/app/shared/models/header-config.model';
import { Result } from 'src/app/shared/models/result.model';
import { Dashboard } from 'src/app/shared/models/dashboard.model';

@Injectable()
export class DashboardsService {
	selectedDashboard: BehaviorSubject<Dashboard> = new BehaviorSubject(null);

	gridFull: BehaviorSubject<boolean> = new BehaviorSubject(false);
	editable: BehaviorSubject<boolean> = new BehaviorSubject(false);

	header: BehaviorSubject<HeaderConfig> = new BehaviorSubject(null);

	themes: string[] = AppConfig.settings.themes.available;
	darkTheme = AppConfig.settings.themes.dark;
	defaultTheme = AppConfig.settings.themes.default;

	theme: BehaviorSubject<string> = new BehaviorSubject('style-default');

	layouts: string[] = ['minimal', 'compact', 'full'];
	layout: BehaviorSubject<string> = new BehaviorSubject('full');

	toolbars: string[] = ['left', 'top'];
	toolbar: BehaviorSubject<string> = new BehaviorSubject('top');

	loading: BehaviorSubject<boolean> = new BehaviorSubject(true);
	unlockSidenav: BehaviorSubject<boolean> = new BehaviorSubject(false);
	sizeSidenav: BehaviorSubject<boolean> = new BehaviorSubject(false);

	private _dashboardsSource = new BehaviorSubject<Dashboard[]>([]);
	private _dashboardSource = new BehaviorSubject<Dashboard>(null);

	dashboards = this._dashboardsSource.asObservable();
	dashboard = this._dashboardSource.asObservable();

	changeDashboards(data: Dashboard[]): void {
		this._dashboardsSource.next(data);
	}

	changeDashboard(data: Dashboard) {
		this._dashboardSource.next(data);
	}

	constructor(private _tdLoadingService: TdLoadingService, private _dashboardsHttpService: DashboardsHttpService) {}

	query() {
		return this._dashboardsHttpService.query<Result<Dashboard[]>>().pipe(
			map((result) => result.data),
			map((dashboards) => dashboards.filter((dashboard) => dashboard.active)),
			tap((dashboards) => this.changeDashboards(dashboards))
		);
	}

	create(model: Dashboard) {
		return this._dashboardsHttpService.create(model);
	}

	get(id) {
		this._tdLoadingService.register('modalSyntax');
		this._dashboardsHttpService
			.get(id)
			.pipe(
				delay(500),
				tap((dashboard) => this.changeDashboard(dashboard)),
				finalize(() => this._tdLoadingService.resolve('modalSyntax'))
			)
			.subscribe();
	}

	update(model: Dashboard) {
		return this._dashboardsHttpService.update(model);
	}

	delete(id) {
		return this._dashboardsHttpService.delete(id);
	}
}
