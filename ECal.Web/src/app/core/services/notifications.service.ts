import { Injectable } from '@angular/core';

import { NotificationsHttpService } from 'src/app/core/services/http/notifications-http.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { Application } from 'src/app/shared/models/application.model';
import { RsqlBuilder } from './rsql/rsql-builder';

@Injectable()
export class NotificationsService {
	constructor(private _notificationsHttpService: NotificationsHttpService) {}

	query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._notificationsHttpService.query(pagingSettings, rsql);
	}

	create(model: Application) {
		return this._notificationsHttpService.create(model);
	}

	get(id) {
		return this._notificationsHttpService.get(id);
	}

	update(model: Application) {
		return this._notificationsHttpService.update(model);
	}

	delete(id) {
		return this._notificationsHttpService.delete(id);
	}
}
