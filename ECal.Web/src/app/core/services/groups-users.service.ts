import { Injectable } from '@angular/core';

import { GroupsUsersHttpService } from 'src/app/core/services/http/groups-users-http.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { GroupUser } from 'src/app/shared/models/group-user.model';
import { RsqlBuilder } from './rsql/rsql-builder';

@Injectable()
export class GroupsUsersService {
	constructor(private _groupsUsersHttpService: GroupsUsersHttpService) {}

	query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._groupsUsersHttpService.query(pagingSettings, rsql);
	}

	create(model: GroupUser) {
		return this._groupsUsersHttpService.create(model);
	}

	get(id) {
		return this._groupsUsersHttpService.get(id);
	}

	update(model: GroupUser) {
		return this._groupsUsersHttpService.update(model);
	}

	delete(id) {
		return this._groupsUsersHttpService.delete(id);
	}
}
