import { InjectionToken } from '@angular/core';

export const DIALOG_MODAL_DATA = new InjectionToken<any>('DIALOG_MODAL_DATA');
