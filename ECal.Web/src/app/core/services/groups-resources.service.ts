import { Injectable } from '@angular/core';

import { GroupsResourcesHttpService } from 'src/app/core/services/http/groups-resources-http.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { GroupResource } from 'src/app/shared/models/group-resource.model';
import { RsqlBuilder } from './rsql/rsql-builder';

@Injectable()
export class GroupsResourcesService {
	constructor(private _groupsResourcesHttpService: GroupsResourcesHttpService) {}

	query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._groupsResourcesHttpService.query(pagingSettings, rsql);
	}

	create(model: GroupResource) {
		return this._groupsResourcesHttpService.create(model);
	}

	get(id) {
		return this._groupsResourcesHttpService.get(id);
	}

	update(model: GroupResource) {
		return this._groupsResourcesHttpService.update(model);
	}

	delete(id) {
		return this._groupsResourcesHttpService.delete(id);
	}
}
