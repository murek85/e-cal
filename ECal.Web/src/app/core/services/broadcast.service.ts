import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter, map } from 'rxjs/operators';

export class Message {
	type: MessageType;
	payload?: { type; title?; message?; config?: { class?: 'success' | 'warning' | 'error'; position?; showIcon? } };
}

export class MessageType {
	static DASHBOARD_CHANGED = 'broadcast.type.dashboard.changed';
	static WIDGET_CHANGED = 'broadcast.type.widget.changed';
	static RESOURCES_CHANGED = 'broadcast.type.resources.changed';

	static WIDGET_HELP = 'broadcast.type.widget.help';
	static WIDGET_PAIR = 'broadcast.type.widget.pair';
	static WIDGET_MOVE = 'broadcast.type.widget.move';
	static WIDGET_EDIT = 'broadcast.type.widget.edit';
	static WIDGET_DELETE = 'broadcast.type.widget.delete';
	static WIDGET_PAIRING = 'broadcast.type.widget.pairing';

	static WIDGET_PAIR_OUTSIDE_FILTER_UPDATE = 'broadcast.type.widget.pair.outside.filter.update';
}

@Injectable()
export class BroadcastService {
	private _handler = new Subject<Message>();

	broadcast(message: Message): void {
		this._handler.next(message);
	}

	observe(type: string): Observable<any> {
		return this._handler.pipe(
			filter((message) => message.type === type),
			map((message) => message.payload)
		);
	}
}
