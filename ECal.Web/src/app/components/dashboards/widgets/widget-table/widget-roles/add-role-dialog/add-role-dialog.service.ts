import { Injectable } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';

import { RolesService } from 'src/app/core/services/roles.service';
import { Role } from 'src/app/shared/models/role.model';

@Injectable()
export class AddRoleDialogService {
	constructor(private _tdLoadingService: TdLoadingService, private _rolesService: RolesService) {}

	public create(model: Role) {
		return this._rolesService.create(model);
	}
}
