import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { RolesService } from 'src/app/core/services/roles.service';

import { WidgetRolesComponent } from './widget-roles.component';
import { AddRoleDialogComponent } from './add-role-dialog/add-role-dialog.component';
import { ManageRoleDialogComponent } from './manage-role-dialog/manage-role-dialog.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetRolesComponent, AddRoleDialogComponent, ManageRoleDialogComponent],
	imports: [APP_MODULES],
	exports: [WidgetRolesComponent, AddRoleDialogComponent, ManageRoleDialogComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: [RolesService]
})
export class WidgetRolesModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetRolesComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetRolesComponent);
	}

	static import() {
		return import('../widget-roles/widget-roles.module');
	}
}
