import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TdLoadingService } from '@covalent/core/loading';
import { delay, finalize, tap } from 'rxjs/operators';

import { RolesService } from 'src/app/core/services/roles.service';
import { Role } from 'src/app/shared/models/role.model';

@Injectable()
export class ManageRoleDialogService {
	private _roleSource = new BehaviorSubject<Role>(null);

	role = this._roleSource.asObservable();

	changeRole(data: Role) {
		this._roleSource.next(data);
	}

	constructor(private _tdLoadingService: TdLoadingService, private _rolesService: RolesService) {}

	public get(id) {
		this._tdLoadingService.register('modalSyntax');
		this._rolesService
			.get(id)
			.pipe(
				delay(500),
				tap((role) => this.changeRole(role)),
				finalize(() => this._tdLoadingService.resolve('modalSyntax'))
			)
			.subscribe();
	}

	public update(model: Role) {
		return this._rolesService.update(model);
	}
}
