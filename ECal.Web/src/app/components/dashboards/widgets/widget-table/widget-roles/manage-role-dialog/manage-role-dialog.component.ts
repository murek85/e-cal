import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { from, iif, of, Subject } from 'rxjs';
import { catchError, delay, filter, finalize, map, takeUntil, tap } from 'rxjs/operators';

import { NgxMAuthOidcService } from 'src/app/core/services/oidc/oidc.service';

import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { ManageRoleDialogService } from './manage-role-dialog.service';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';

import { FormField, Segment, Step } from 'src/app/shared/models/step.model';
import { User } from 'src/app/shared/models/user.model';
import { Role } from 'src/app/shared/models/role.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { RolesService } from 'src/app/core/services/roles.service';

declare var $: any;

const MODALNAME = '.modal-manage-role';

@Component({
	selector: 'app-manage-role-dialog',
	templateUrl: './manage-role-dialog.component.html',
	providers: [RolesService, ManageRoleDialogService]
})
export class ManageRoleDialogComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _formModeType: FormModeType;
	private _steps: Array<Step>;
	private _permissionsDict: Array<Permission>;
	private _selectedIndex = 0;
	private _roleId: string;
	private _tabType: 'data' | 'permissions';
	private _role: Role;
	private _userLogged: User;
	private _loading = false;

	@ViewChild('form') form;

	constructor(
		private _roleDialogService: ManageRoleDialogService,
		private _accountService: AccountService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA) public data: { formModeType: FormModeType; roleId: string; tabType: 'data' | 'permissions' }
	) {
		this._formModeType = data.formModeType;
		this._roleId = data.roleId;
		this._tabType = data.tabType;
	}

	get formConfig() {
		return {
			symbol: 'manage-role',
			broadcast: { type: ResourceType.ROLES }
		};
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get steps(): Step[] {
		return this._steps;
	}

	get selectedIndex(): number {
		return this._selectedIndex;
	}

	get loading(): boolean {
		return this._loading;
	}

	get role(): Role {
		return this._role;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		this._roleDialogService.role
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((data) => !!data),
				tap((role) => (this._role = role)),
				tap((role) => this.form.loadForm(role))
			)
			.subscribe(() => (this._loading = false));

		$(MODALNAME)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initPermissionsDict();
					this.initSteps();
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeTab($event): void {
		this.form.changeTab($event);
	}

	onBackForm(): void {
		$(MODALNAME).modal('hide');
	}

	onSaveForm(): void {
		this.form.saveForm(
			(model) => this._roleDialogService.update({ ...model, id: this._roleId }),
			() => $(MODALNAME).modal('hide')
		);
	}

	private initSteps(): void {
		const steps: Step[] = [];
		steps.push(
			this.createStep('Rola', 'Podstawowe informacje o roli.', 'role-data', [
				{
					header: 'Dane roli',
					description: 'Uzupełnienie podstawowych informacji o roli.',
					fields: [
						{
							label: 'Nazwa',
							name: 'name',
							symbol: 'role-name',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Opis',
							name: 'description',
							symbol: 'role-description',
							type: FormFieldType.TEXTAREA
						},
						{
							label: 'Symbol',
							name: 'symbol',
							symbol: 'role-symbol',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						}
					]
				}
			])
		);
		steps.push(
			this.createStep('Uprawnienia', 'Uprawnienia umożliwiające dostęp do zasobów aplikacji.', 'role-permissions', [
				{
					header: 'Uprawnienia',
					description: 'Dostęp do uprawnień w systemie.',
					fields: [
						{
							label: 'Uprawnienia',
							name: 'permissions',
							symbol: 'role-permissions',
							type: FormFieldType.DICTIONARY_MULTI,
							items: this._permissionsDict,
							api: {
								fields: {
									name: 'name',
									value: 'symbol'
								}
							}
						}
					]
				}
			])
		);
		this._steps = steps;
	}

	private createStep(label: string, description: string, symbol: string, segments: Segment[], completed: boolean = false): Step {
		const menu: Step = { label, description, symbol, segments, completed };
		return menu;
	}

	private initPermissionsDict(): void {
		const permissions: Permission[] = [];
		permissions.push({
			name: 'Zarządzanie użytkownikami',
			symbol: 'ZARZADZANIE_UZYTKOWNIKAMI'
		});
		permissions.push({
			name: 'Zarządzanie uprawnieniami',
			symbol: 'ZARZADZANIE_UPRAWNIENIAMI'
		});
		permissions.push({
			name: 'Raporty statystyczne',
			symbol: 'RAPORTY_STATYSTYCZNE'
		});
		this._permissionsDict = permissions;
	}

	private initDialog(): void {
		switch (this._formModeType) {
			case FormModeType.EDIT: {
				this._dialogConfig = {
					title: 'Edytowanie roli',
					meta: 'edytowanie roli w aplikacji'
				};

				switch (this._tabType) {
					case 'data':
						this._selectedIndex = 0;
						break;
					case 'permissions':
						this._selectedIndex = 1;
						break;

					default:
						this._selectedIndex = 0;
						break;
				}

				// pobierz dane roli w trybie edycji
				this._roleDialogService.get(this._roleId);
				break;
			}
		}
	}
}
