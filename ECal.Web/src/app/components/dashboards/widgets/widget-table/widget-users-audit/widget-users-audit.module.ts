import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { UsersService } from 'src/app/core/services/users.service';

import { WidgetUsersAuditComponent } from './widget-users-audit.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetUsersAuditComponent],
	imports: [APP_MODULES],
	exports: [WidgetUsersAuditComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: [UsersService]
})
export class WidgetUsersAuditModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetUsersAuditComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetUsersAuditComponent);
	}

	static import() {
		return import('../widget-users-audit/widget-users-audit.module');
	}
}
