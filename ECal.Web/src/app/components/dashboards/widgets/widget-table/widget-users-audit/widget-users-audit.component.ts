import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { of } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { WidgetTableComponent } from '../widget-table.component';

import { AccountService } from 'src/app/core/services/account.service';
import { UsersService } from 'src/app/core/services/users.service';

import { User } from 'src/app/shared/models/user.model';

import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { WebsocketEvent } from 'src/app/core/services/websocket.service';

import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';
import { Control } from '../../widget-container/widget-container.component';
import { ColumnDefinition, FilterType } from 'src/app/shared/models/column.model';
import { PairingService } from 'src/app/core/services/pairing.service';
import { RsqlBuilder } from 'src/app/core/services/rsql/rsql-builder';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';

declare var $: any;

@Component({
	selector: 'app-widget-users-audit',
	templateUrl: '../widget-table.component.html',
	styleUrls: ['../widget-table.component.scss']
})
export class WidgetUsersAuditComponent extends WidgetTableComponent<User> implements OnInit, OnDestroy {
	config: any = {
		type: 'users-audit'
	};

	_userLogged: User;

	constructor(
		protected _translateService: TranslateService,
		protected _tdLoadingService: TdLoadingService,
		protected _toastService: ToastService,
		protected _dialogService: DialogService,
		protected _broadcastService: BroadcastService,
		protected _dashboardsService: DashboardsService,
		protected _pairingService: PairingService,
		private _usersService: UsersService,
		private _accountService: AccountService
	) {
		super(_translateService, _tdLoadingService, _toastService, _dialogService, _broadcastService, _dashboardsService, _pairingService);
	}

	protected query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._usersService.query(pagingSettings, rsql);
	}

	protected report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder) {
		return of(null);
	}

	protected get columnDefinitions(): Array<ColumnDefinition> {
		return [
			{
				alias: 'userName',
				filter: {
					type: FilterType.TEXT
				}
			}
		];
	}

	get controls(): Array<Control> {
		return [];
	}

	get exportConfig(): { show?: boolean; disabled?: boolean; title?: string } {
		return {
			show: true,
			title: 'Historia operacji'
		};
	}

	ngOnInit(): void {
		super.ngOnInit();

		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}
}
