import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { IPageChangeEvent, TdPagingBarComponent } from '@covalent/core/paging';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { Observable } from 'rxjs';

import { saveAs } from 'file-saver';

import { TableType } from 'src/app/shared/enums/table-type.enum';
import { PagingResults, PagingSettings } from 'src/app/shared/models/paging.model';
import { delay, filter, finalize, takeUntil, tap } from 'rxjs/operators';
import { TableSettings } from 'src/app/shared/models/table.model';
import { Result } from 'src/app/shared/models/result.model';
import { WidgetComponent } from '../widget.component';
import { Control } from '../widget-container/widget-container.component';
import { ManageWidgetDialogComponent } from '../../dashboard/manage-widget-dialog/manage-widget-dialog.component';
import { WidgetOptions } from 'src/app/shared/models/widget.model';
import { CellData, Column, ColumnDefinition, FilterType, ParamValue } from 'src/app/shared/models/column.model';

import { Operator } from 'src/app/core/services/rsql/rsql-operator';
import { RsqlBuilder } from 'src/app/core/services/rsql/rsql-builder';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';

import * as moment from 'moment';

declare var $: any;

const escapes = { '?': '\\?', '"': '\\"' };

export const DATE_FORMAT = 'YYYY-MM-DD';
export const TIME_FORMAT = 'HH:mm';
export const DATE_TIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';
const REST_DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm:ss';
const RSQL_DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm:ss.SSSZ';
const SORT_ASCENDING = 'asc';
const SORT_DESCENDING = 'desc';

export const DEFAULT_DATE_TIME_FORMATER = (datetime: string): string =>
	datetime ? moment.utc(datetime, REST_DATE_TIME_FORMAT).local().format(DATE_TIME_FORMAT) : null;

@Component({ template: '' })
export abstract class WidgetTableComponent<T> extends WidgetComponent implements OnInit, OnDestroy, AfterViewInit {
	tableType: typeof TableType = TableType;

	private _columns: Array<Column>;
	private _dataSource: MatTableDataSource<T> = new MatTableDataSource();

	private _singleSelection: SelectionModel<T> = new SelectionModel<T>(false);
	private _pageSizes = [5, 10, 25, 100];
	private _pageSize = 10;

	private _pagingResults: PagingResults = new PagingResults();
	private _tableSettings: TableSettings = new TableSettings();
	private _pagingSettings: PagingSettings = new PagingSettings();

	private _editable = false;
	private _loading = false;

	private _exportButtons: Array<Control> = [
		{
			text: 'PDF',
			click: () => this.onExport('pdf')
		},
		{
			text: 'XLSX',
			click: () => this.onExport('xlsx')
		},
		{
			text: 'CSV',
			click: () => this.onExport('csv')
		},
		{
			text: 'DOCX',
			click: () => this.onExport('docx')
		}
	];

	@ViewChild(TdPagingBarComponent, { static: false }) tdPagingBar: TdPagingBarComponent;

	private get paging(): PagingSettings {
		return this._pagingSettings;
	}

	private get rsql(): RsqlBuilder {
		const rsql = new RsqlBuilder();

		this._columns
			.filter((column) => column.column.logicValue && column.filter && column.filter.logicValue)
			.forEach((column) => {
				if (column.definition.filter) {
					switch (column.definition.filter.type) {
						case FilterType.TEXT: {
							const value = this.escape(column.filter.textValue);
							const exact = !!column.filter.numberValue;
							rsql.create(column.filter.alias, Operator.EQUALS, `${value}`); // exact ? `"${value}"` : `"%${value}%"`);
							break;
						}
						case FilterType.DATE: {
							rsql.create(
								column.filter.alias,
								Operator.GREATER_OR_EQUALS,
								moment(column.filter.dateValue, REST_DATE_TIME_FORMAT).format(RSQL_DATE_TIME_FORMAT)
							);
							break;
						}
						case FilterType.DATE_RANGE: {
							break;
						}
						case FilterType.NUMBER: {
							break;
						}
						case FilterType.LOGIC: {
							rsql.create(column.filter.alias, Operator.EQUALS, column.filter.textValue === 'true');
							break;
						}
						case FilterType.DICTIONARY_SINGLE: {
							const typeColumn = column.filter.textValue;
							rsql.create(
								column.definition.filter.alternateProperty || column.definition.filter.property || column.filter.alias,
								Operator.EQUALS,
								typeof typeColumn === 'string' ? String(typeColumn) : Number(typeColumn)
							);
							break;
						}
						case FilterType.DICTIONARY_MULTI: {
							rsql.create(
								column.definition.filter.alternateProperty || column.definition.filter.property || column.filter.alias,
								Operator.IN,
								column.filter.textValue.split(',').map((part) => (column.filter.numberValue ? Number(part) : part))
							);
							break;
						}
					}

					if (!rsql.isEmpty()) {
						rsql.and();
					}
				}
			});

		return rsql;
	}

	get exportButtons(): Array<Control> {
		return this._exportButtons;
	}

	get controls(): Array<Control> {
		return [];
	}

	get toggleControls(): Array<any> {
		return [];
	}

	get singleSelection(): SelectionModel<T> {
		return this._singleSelection;
	}

	get columns(): Array<Column> {
		return this._columns ? this._columns.filter((column) => column.column.logicValue) : [];
	}

	get headers(): Array<string> {
		return this.columns.map((column) => column.definition.alias);
	}

	get dataSource(): MatTableDataSource<T> {
		return this._dataSource;
	}

	get pageSizes(): Array<number> {
		return this._pageSizes;
	}

	get pageSize(): number {
		return this._pageSize;
	}

	set pageSize(pageSize: number) {
		this._pageSize = pageSize;
	}

	get totalRows(): number | string {
		return this._pagingResults.totalRows;
	}

	get filtersCount(): string {
		return this._columns.filter((column) => column.filter).length.toString();
	}

	get activeFilters(): Array<Column> {
		return this._columns.filter((column) => column.filter);
	}

	get loading(): boolean {
		return this._loading;
	}

	get exportConfig(): { show?: boolean; disabled?: boolean; title?: string } {
		return {
			show: false,
			disabled: true
		};
	}

	TrackByFn = (index: number, item: any) => index;

	protected abstract query(pagingSettings: PagingSettings, rsql: RsqlBuilder);

	protected abstract report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder): Observable<HttpResponse<Blob>>;

	protected abstract get columnDefinitions(): Array<ColumnDefinition>;

	protected onPairInteract(item?: T, symbol?: string): void {}

	protected onPairingMessage(): void {}

	ngOnInit(): void {
		super.ngOnInit();
		this.onWidgetSet();
		this.reload();

		this._dashboardsService.editable
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((editable) => (this._editable = editable))
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	protected onRefresh(): void {
		super.onRefresh();
		this.reload();
	}

	protected onWidgetEdit(): void {
		this._dialogService
			.open<ManageWidgetDialogComponent>(ManageWidgetDialogComponent, {
				data: {
					widget: this.widget,
					columns: this._columns
				}
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	protected onWidgetFilters(): void {
		this.onFilters({ tabType: 'filters' });
	}

	protected onWidgetSet(): void {
		const definitions = this.columnDefinitions;
		if (!definitions) {
			return;
		}

		const columns: Array<ParamValue> = [];
		definitions
			.filter((definition) => !columns.find((column) => definition.alias === column.alias))
			.map((definition, index) => ({ alias: definition.alias, logicValue: true } as ParamValue))
			.forEach((value) => columns.push(value));

		this._columns = columns.map(
			(column) =>
				({
					column,
					definition: definitions.find((definition) => definition.alias === column.alias),
					display: column.alias ? `widgets.columns.${column.alias.toLowerCase()}` : null
				} as Column)
		);
	}

	protected reload(): void {
		this._loading = true;
		this._tdLoadingService.register('listSyntax');
		this.query(this.paging, this.rsql)
			.pipe(
				filter((result: Result<Array<T>>) => !!result),
				tap((result: Result<Array<T>>) => ((this._pagingResults = result.paging), (this._dataSource.data = result.data))),
				finalize(() => ((this._loading = false), this._tdLoadingService.resolve('listSyntax'), this._singleSelection.clear(), this.rebuildTooltips()))
			)
			.subscribe();
	}

	private initSemanticConfig(): void {
		$('.dropdown-pagesize').dropdown({
			onChange: (value, text, $choice) => (this._pageSize = Number(value))
		});

		$('.dropdown-pagesize').dropdown('set selected', this._pageSize);
	}

	private resetPageIndex(): void {
		this._pagingSettings.pageNumber = 1;
		if (this.tdPagingBar) {
			this.tdPagingBar.firstPage();
		}
	}

	isColumnFilterable(column: Column): boolean {
		return !!column.definition.filter;
	}

	isColumnFilterOn(column: Column): boolean {
		return column.filter && column.filter.logicValue;
	}

	isColumnFilterOff(column: Column): boolean {
		return column.filter && !column.filter.logicValue;
	}

	isColumnFilterEmpty(column: Column): boolean {
		return !column.filter;
	}

	getColumnFilterColor(column: Column): string {
		return !this.isColumnFilterEmpty(column) ? 'accent' : 'primary';
	}

	getFilterTooltip(filter: Column): string {
		return filter.filterTooltip || '';
	}

	rebuildTooltips(): void {
		this._columns
			.filter((column) => column.filter || !!column.definition.filter)
			.forEach((column: Column) => {
				let text = '';
				switch (column.definition.filter.type) {
					case FilterType.TEXT:
						if (column.filter) {
							const exact = !!column.filter.numberValue;
							text = (exact ? '' : 'Zawiera: ') + ' ' + column.filter.textValue;
						}
						break;
				}
				column.filterTooltip = text;
			});
	}

	getSelected(): T | Array<T> {
		return this._singleSelection.selected.first();
	}

	isSelected(row: T): boolean {
		return this._singleSelection.isSelected(row);
	}

	clearSelection(): void {
		this._singleSelection.clear();
	}

	getCellData(data: T, definition: ColumnDefinition): CellData {
		definition.alias.split('.').forEach((part) => (data = data ? data[part] : null));

		if (definition.formatter) {
			const formatted = definition.formatter(data, definition.alias);
			return typeof formatted === 'string' ? { text: formatted } : formatted;
		}

		return { text: data ? String(data) : null };
	}

	onChangeTableType(type: TableType): void {
		this._tableSettings.type = type;
	}

	onPageChange(event: IPageChangeEvent): void {
		this._pagingSettings.pageNumber = event.page;
		this._pagingSettings.pageSize = event.pageSize;
		this.reload();
	}

	onChangeSort(event): void {
		const alias = event.active;
		switch (event.direction) {
			case SORT_DESCENDING:
				this._pagingSettings.sort = `-${alias}`;
				break;
			case SORT_ASCENDING:
				this._pagingSettings.sort = `+${alias}`;
				break;

			default:
				delete this._pagingSettings.sort;
				break;
		}
		this.resetPageIndex();
		this.reload();
	}

	onRowClick(row: T): void {
		this._singleSelection.toggle(row);
	}

	onRowDbClick(row: T): void {
		this._singleSelection.select(row);
	}

	onSaveFilter(filter): void {
		this._pagingSettings.pageNumber = 1;
	}

	onClearFilter(filter): void {
		this._pagingSettings.pageNumber = 1;
	}

	onRemoveFilter(filter): void {
		const column = this._columns.find((column) => column.definition.alias === filter.alias);
		if (column) {
			delete column.filter;
		}
		this.reload();
	}

	onClickFilter(filter: Column) {
		filter.filter.logicValue = !filter.filter.logicValue;
		this.reload();
	}

	onFilters(options?: WidgetOptions, column?: Column): void {
		this._dialogService
			.open<ManageWidgetDialogComponent>(ManageWidgetDialogComponent, {
				data: {
					widget: this.widget,
					columns: this._columns,
					options: options
				}
			})
			.events.pipe(
				filter((event) => !!event),
				tap((_) => (this.resetPageIndex(), this.reload()))
			)
			.subscribe();
	}

	onExport(format: string): void {
		const report: ReportBuilder = {
			title: this.exportConfig.title,
			format: format,
			labels: this._columns
				.filter((column) => column.column.logicValue && column.definition.alias !== 'icon')
				.map((column) => this._translateService.instant(column.display))
				.join(';'),
			aliases: this._columns
				.filter((column) => column.column.logicValue && column.definition.alias !== 'icon')
				.map((column) => column.definition.alias.split('.'))
				.map((parts) => parts[parts.length - 1])
				.join(';'),
			locale: moment().format('Z')
		};

		this._tdLoadingService.register('listSyntax');
		this.report(this.paging, this.rsql, report)
			.pipe(
				filter((result) => !!result),
				tap((result) => saveAs(result.body, `${moment().format('YYYY_MM_DD_HH_mm_ss')}.${format}`)),
				finalize(() => this._tdLoadingService.resolve('listSyntax'))
			)
			.subscribe();
	}

	private escape(text: String) {
		Object.keys(escapes).forEach((key) => (text = text.split(key).join(escapes[key])));
		return text;
	}

	trackByFn(index: any) {
		return index;
	}
}
