import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TdLoadingService } from '@covalent/core/loading';
import { tap } from 'rxjs/operators';

import { UsersHttpService } from 'src/app/core/services/http/users-http.service';
import { User } from 'src/app/shared/models/user.model';

@Injectable()
export class AddUserDialogService {
	constructor(private _tdLoadingService: TdLoadingService, private _usersHttpService: UsersHttpService) {}

	public create(model: User) {
		return this._usersHttpService.create(model);
	}
}
