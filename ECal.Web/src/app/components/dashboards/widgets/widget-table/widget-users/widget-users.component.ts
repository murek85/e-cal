import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { of } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { WidgetTableComponent } from '../widget-table.component';

import { AccountService } from 'src/app/core/services/account.service';
import { UsersService } from 'src/app/core/services/users.service';

import { User } from 'src/app/shared/models/user.model';

import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { WebsocketEvent } from 'src/app/core/services/websocket.service';

import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';
import { AddUserDialogComponent } from './add-user-dialog/add-user-dialog.component';
import { ManageUserDialogComponent } from './manage-user-dialog/manage-user-dialog.component';
import { Control } from '../../widget-container/widget-container.component';
import { ColumnDefinition, FilterType } from 'src/app/shared/models/column.model';
import { PairingService } from 'src/app/core/services/pairing.service';
import { RsqlBuilder } from 'src/app/core/services/rsql/rsql-builder';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';
import { ConfirmActionDialogComponent } from 'src/app/shared/components/dialogs/confirm-action-dialog/confirm-action-dialog.component';

declare var $: any;

@Component({
	selector: 'app-widget-users',
	templateUrl: '../widget-table.component.html',
	styleUrls: ['../widget-table.component.scss']
})
export class WidgetUsersComponent extends WidgetTableComponent<User> implements OnInit, OnDestroy {
	config: any = {
		type: 'users'
	};

	_userLogged: User;

	constructor(
		protected _translateService: TranslateService,
		protected _tdLoadingService: TdLoadingService,
		protected _toastService: ToastService,
		protected _dialogService: DialogService,
		protected _broadcastService: BroadcastService,
		protected _dashboardsService: DashboardsService,
		protected _pairingService: PairingService,
		private _usersService: UsersService,
		private _accountService: AccountService
	) {
		super(_translateService, _tdLoadingService, _toastService, _dialogService, _broadcastService, _dashboardsService, _pairingService);
	}

	protected query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._usersService.query(pagingSettings, rsql);
	}

	protected report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder) {
		return this._usersService.report(pagingSettings, rsql, report);
	}

	protected onPairInteract(item?: User, symbol?: string, checked?: boolean) {
		this._pairingService.sendMessages(item);
	}

	protected onToggleButtonChange(symbol: string, checked: boolean, item?: User) {
		this.onPairInteract(item, symbol, !checked);
	}

	protected get columnDefinitions(): Array<ColumnDefinition> {
		return [
			{
				alias: 'icon',
				hidden: true,
				size: 'one wide',
				formatter: (value: string) => {
					return { icon: 'user' };
				}
			},
			{
				alias: 'userName',
				filter: {
					type: FilterType.TEXT
				}
			},
			{
				alias: 'email',
				filter: {
					type: FilterType.TEXT
				}
			},
			{
				alias: 'firstName',
				filter: {
					type: FilterType.TEXT
				}
			},
			{
				alias: 'lastName',
				filter: {
					type: FilterType.TEXT
				}
			}
		];
	}

	get controls(): Array<Control> {
		return [
			{
				text: 'Utwórz',
				icon: 'add user',
				showText: true,
				click: () => this.onAddUser()
			},
			{
				text: 'Zarządzaj',
				icon: 'edit user',
				showText: true,
				disabled: this.singleSelection.isEmpty(),
				click: (row: User) => this.onEditUser(row)
			},
			{
				text: 'Zmień hasło',
				icon: 'fingerprint',
				disabled: this.singleSelection.isEmpty(),
				click: (row: User) => this.onPasswordUser(row)
			},
			{
				text: 'Uprawnienia',
				icon: 'key',
				disabled: this.singleSelection.isEmpty(),
				click: (row: User) => this.onPermissionsUser(row)
			},
			{
				text: 'Usuń',
				icon: 'trash',
				color: 'red',
				disabled: this.singleSelection.isEmpty(),
				click: (row: User) => this.onDeleteUser(row)
			}
		];
	}

	get toggleControls(): Array<any> {
		return [
			{
				icon: 'sort',
				tooltip: 'COMMON.FILTER_PAIRING',
				click: (checked: boolean, item: User) => {}
			}
		];
	}

	get exportConfig(): { show?: boolean; disabled?: boolean; title?: string } {
		return {
			show: true,
			title: 'Lista użytkowników'
		};
	}

	ngOnInit(): void {
		super.ngOnInit();

		this._broadcastService
			.observe(MessageType.RESOURCES_CHANGED)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((message: { type: string; title: string; message: WebsocketEvent; config: any }) => message.type === ResourceType.USERS),
				tap((message) => this._toastService.open(message.title, message.message, message.config))
			)
			.subscribe();

		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

	onRowClick(row: User) {
		super.onRowClick(row);
		this.onPairInteract(!this.singleSelection.selected.isEmpty() ? row : null);
	}

	private onAddUser(): void {
		this._dialogService
			.open<AddUserDialogComponent>(AddUserDialogComponent, {
				data: { formModeType: FormModeType.ADD }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	private onEditUser(row: User): void {
		this._dialogService
			.open<ManageUserDialogComponent>(ManageUserDialogComponent, {
				data: { formModeType: FormModeType.EDIT, userId: row.id }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	private onPasswordUser(row: User): void {
		this._dialogService
			.open<ManageUserDialogComponent>(ManageUserDialogComponent, {
				data: { formModeType: FormModeType.EDIT, userId: row.id, tabType: 'password' }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	private onPermissionsUser(row: User): void {
		this._dialogService
			.open<ManageUserDialogComponent>(ManageUserDialogComponent, {
				data: { formModeType: FormModeType.EDIT, userId: row.id, tabType: 'permissions' }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	private onDeleteUser(row: User): void {
		this._dialogService
			.open<ConfirmActionDialogComponent>(ConfirmActionDialogComponent, {
				data: {
					title: 'Usuwanie konta',
					description: `Czy chcesz usunąć wybrane konto użytkownika '${row.userName}'?`,
					iconClass: 'question circle outline',
					approveButtonLabel: 'Usuń',
					approveButtonColorClass: 'red',
					denyButtonLabel: 'Anuluj'
				}
			})
			.events.pipe(
				filter((event) => !!event),
				filter((event) => event.type === 'accept'),
				tap(() => alert('Usuwanie'))
			)
			.subscribe();
	}
}
