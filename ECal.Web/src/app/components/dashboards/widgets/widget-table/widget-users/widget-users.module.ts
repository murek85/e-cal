import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactory, ComponentFactoryResolver } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { UsersService } from 'src/app/core/services/users.service';

import { WidgetUsersComponent } from './widget-users.component';
import { AddUserDialogComponent } from './add-user-dialog/add-user-dialog.component';
import { ManageUserDialogComponent } from './manage-user-dialog/manage-user-dialog.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetUsersComponent, AddUserDialogComponent, ManageUserDialogComponent],
	imports: [APP_MODULES],
	exports: [WidgetUsersComponent, AddUserDialogComponent, ManageUserDialogComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: [UsersService]
})
export class WidgetUsersModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetUsersComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetUsersComponent);
	}

	static import() {
		return import('../widget-users/widget-users.module');
	}
}
