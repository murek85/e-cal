import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { from, iif, of, Subject } from 'rxjs';
import { catchError, delay, filter, finalize, map, takeUntil, tap } from 'rxjs/operators';

import { NgxMAuthOidcService } from 'src/app/core/services/oidc/oidc.service';

import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';

import { ManageUserDialogService } from './manage-user-dialog.service';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';

import { FormField, Segment, Step } from 'src/app/shared/models/step.model';
import { User } from 'src/app/shared/models/user.model';
import { Role } from 'src/app/shared/models/role.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';

import * as moment from 'moment';

declare var $: any;

const flatten = (arr) => arr.reduce((a, b) => a.concat(Array.isArray(b) ? flatten(b) : b), []);

const FORMNAME = '.form-manage-user';
const MODALNAME = '.modal-manage-user';

@Component({
	selector: 'app-manage-user-dialog',
	templateUrl: './manage-user-dialog.component.html',
	providers: [ManageUserDialogService]
})
export class ManageUserDialogComponent implements OnInit, OnDestroy, AfterViewInit {
	formFieldType: typeof FormFieldType = FormFieldType;

	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _formModeType: FormModeType;
	private _steps: Array<Step>;
	private _rolesDict: Array<Role>;
	private _permissionsDict: Array<Permission>;
	private _selectedIndex = 0;
	private _error;
	private _stats: Array<any> = [];
	private _userId: string;
	private _tabType: 'data' | 'password' | 'permissions' | 'restrictions';
	private _user: User;
	private _userLogged: User;
	private _loading = false;

	@ViewChild('tabGroup') tabGroup: MatTabGroup;

	constructor(
		private _broadcastService: BroadcastService,
		private _oidcService: NgxMAuthOidcService,
		private _userDialogService: ManageUserDialogService,
		private _accountService: AccountService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA)
		public data: {
			formModeType: FormModeType;
			userId: string;
			tabType: 'data' | 'password' | 'permissions' | 'restrictions';
		}
	) {
		this._formModeType = data.formModeType;
		this._userId = data.userId;
		this._tabType = data.tabType;
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get steps(): Step[] {
		return this._steps;
	}

	get selectedIndex(): number {
		return this._selectedIndex;
	}

	get error(): any {
		return this._error;
	}

	get user(): User {
		return this._user;
	}

	get stats(): any {
		return this._stats;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	get loading(): boolean {
		return this._loading;
	}

	private initSemanticConfig(): void {
		this.initConfig(this._steps[this._selectedIndex]);
	}

	private initSemanticForm(): void {
		this.initForm(this._steps[this._selectedIndex]);
	}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
		this.initSemanticForm();
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		this._userDialogService.user
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((data) => !!data),
				tap((user) => (this._user = user)),
				tap((user) => this.loadForm(user))
			)
			.subscribe(() => (this._loading = false));

		this._stats = [
			{
				name: 'poprawne logowanie',
				icon: 'clock outline',
				type: 'datetime',
				value: `${moment(this.user?.lastValidLogin).format('DD-MM-YYYY HH:mm:ss')}`,
				color: 'rgba(28,174,196,1)'
			},
			{
				name: 'nieudane logowanie',
				icon: 'clock outline',
				type: 'datetime',
				value: `${moment(this.user?.lastIncorrectLogin).format('DD-MM-YYYY HH:mm:ss')}`,
				color: 'rgba(239,154,154,1)'
			}
		];

		$(MODALNAME)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initRolesDict();
					this.initPermissionsDict();
					this.initSteps();
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeTab($event): void {
		const tab = $event as MatTabChangeEvent;
		this._selectedIndex = tab.index;

		if (tab.index >= this.steps.length) {
			return;
		}

		this.initForm(this._steps[tab.index]);
		this.initConfig(this._steps[tab.index]);
		this.loadForm(this.prepareForm());
	}

	onBackForm(): void {
		$(MODALNAME).modal('hide');
	}

	onSaveForm($event): void {
		this._loading = true;

		// this._steps.forEach((step) => (step.completed = $(FORMNAME).form('validate form')));
		this._steps.forEach(
			(step) => (step.completed = step.segments.every((segment) => segment.fields.filter((field) => field.rules).every((field) => field.value)))
		);

		const completed: boolean[] = flatten(this._steps.map((step) => step.completed));
		const valid = completed.every((value) => value);
		const model: User = this.prepareForm();

		const broadcast = (result) =>
			this._broadcastService.broadcast({
				type: MessageType.RESOURCES_CHANGED,
				payload: {
					type: ResourceType.USERS,
					title: result.title,
					message: result.message,
					config: result.config
				}
			});

		const success = () => ({
			title: 'Zapisano',
			message: `Dodano.`,
			config: { class: 'success', position: 'top center', showIcon: 'check circle' }
		});

		const error = () =>
			of({
				title: 'Błąd',
				message: `Wystąpił problem.`,
				config: { class: 'error', position: 'top center', showIcon: 'exclamation circle' }
			});

		const invalid = () =>
			of({
				title: 'Problem',
				message: `Nie wypełniono wymaganych pól formularza.`,
				config: { class: 'warning', position: 'top center', showIcon: 'exclamation circle' }
			});

		const close = () => $(MODALNAME).modal('hide');

		iif(() => valid, this._userDialogService.update(model).pipe(delay(500), map(success), tap(close)), invalid())
			.pipe(
				delay(500),
				catchError(error),
				finalize(() => (this._loading = false))
			)
			.subscribe(broadcast);
	}

	private initSteps(): void {
		const steps: Step[] = [];
		steps.push(
			this.createStep('Konto użytkownika', 'Podstawowe informacje o koncie użytkownika umożliwiające jego identyfikację.', 'user-data', [
				{
					header: 'Dane konta',
					description: 'Uzupełnienie podstawowych informacji o koncie użytkownika.',
					fields: [
						{
							label: 'Nazwa konta',
							name: 'userName',
							symbol: 'user-name',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Adres e-mail',
							name: 'email',
							symbol: 'user-email',
							type: FormFieldType.EMAIL,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							],
							hints: ['Adres powinien zawierać odpowiednie znaki.']
						},
						{
							label: 'Imię (imiona)',
							name: 'firstName',
							symbol: 'user-firstname',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Nazwisko',
							name: 'lastName',
							symbol: 'user-lastname',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						}
					]
				}
			])
		);
		steps.push(
			this.createStep('Hasło użytkownika', 'Wygenerowanie hasła dostępu do tworzonego konta użytkownika.', 'user-password', [
				{
					header: 'Hasło dostępu',
					description: 'Ustawienie hasła dostępu do konta użytkownika.',
					fields: [
						{
							label: 'Nowe hasło',
							name: 'password',
							symbol: 'user-password',
							type: FormFieldType.PASSWORD,
							hints: ['Hasło wymaga wprowadzenia znaków specjalnych.', 'Hasło wymaga minimum 10 znaków.']
						},
						{
							label: 'Powtórz hasło',
							name: 'repeatPassword',
							symbol: 'user-repeat-password',
							type: FormFieldType.PASSWORD
						}
					]
				}
			])
		);
		steps.push(
			this.createStep('Role i uprawnienia', 'Rola i opcjonalne uprawnienia umożliwiające dostęp do zasobów aplikacji.', 'user-permissions', [
				{
					header: 'Role',
					description: 'Przypisanie odpowiedniej roli w aplikacji dla użytkownika.',
					fields: [
						{
							label: 'Role',
							name: 'roles',
							symbol: 'user-roles',
							type: FormFieldType.DICTIONARY_MULTI,
							items: this._rolesDict,
							// rules: [
							// 	{
							// 		type: 'empty',
							// 		prompt: `Pole '{name}' nie może być puste.`
							// 	}
							// ],
							api: {
								fields: {
									name: 'name',
									value: 'symbol'
								}
							}
						}
					]
				},
				{
					header: 'Uprawnienia (opcjonalnie)',
					description: 'Opcjonalny dostęp do dodatkowych uprawnień w systemie.',
					fields: [
						{
							label: 'Uprawnienia',
							name: 'permissions',
							symbol: 'user-permissions',
							type: FormFieldType.DICTIONARY_MULTI,
							items: this._permissionsDict,
							api: {
								fields: {
									name: 'name',
									value: 'symbol'
								}
							}
						}
					]
				}
			])
		);
		steps.push(
			this.createStep('Ograniczenia konta', 'Ustawienie opcjonalnych ograniczeń na konto użytkownika w aplikacji.', 'user-restrictions', [
				{
					header: 'Ograniczenia konta',
					description: 'Pozostałe ustawienia lub inne ograniczenia konta użytkownika.',
					fields: [
						{
							meta: 'Czy potwierdzasz poprawność adresu e-mail konta użytkownika?',
							label: 'Potwierdzenie konta',
							name: 'accountEmailConfirmed',
							symbol: 'user-account-email-confirmed',
							type: FormFieldType.CHECKBOX
						},
						{
							meta: 'Czy zablokować dostęp do konta użytkownika?',
							label: 'Dostęp zablokowany',
							name: 'accountBlocked',
							symbol: 'user-account-blocked',
							type: FormFieldType.CHECKBOX
						},
						{
							meta: 'Czy konto użytkownika ma być aktywne?',
							label: 'Aktywne konto',
							name: 'accountActive',
							symbol: 'user-account-active',
							type: FormFieldType.CHECKBOX
						},
						{
							meta: 'Czy przydzielić ograniczony czasowo dostęp do konta?',
							label: 'Dostęp czasowy',
							name: 'accountAccess',
							symbol: 'user-account-access',
							type: FormFieldType.CHECKBOX
						}
					]
				}
			])
		);
		steps.push(
			this.createStep('Bezpieczeństwo', 'Uwierzytelnianie dwuskładnikowe', 'user-security', [
				{
					header: 'Bezpieczeństwo',
					description: 'Uwierzytelnianie dwuskładnikowe',
					fields: [],
					message: {
						header: 'Weryfikacja dwuetapowa',
						description:
							'Uwierzytelnianie dwuskładnikowe dodaje dodatkową warstwę bezpieczeństwa do Twojego konta. Aby się zalogować, musisz dodatkowo podać 6-cyfrowy kod.'
					}
				}
			])
		);
		steps.push(
			this.createStep('Deaktywacja konta', 'Deaktywowanie dostępu do konta użytkownika', 'user-account-deactive', [
				{
					header: 'Deaktywacja konta',
					description: 'Deaktywowanie dostępu do konta użytkownika',
					fields: [
						{
							meta: '',
							label: 'Potwierdzam deaktywację konta',
							name: 'accountDeactive',
							symbol: 'user-account-deactive',
							type: FormFieldType.CHECKBOX
						}
					],
					message: {
						header: 'Deaktywacja',
						description: 'Aby zapewnić dodatkowe bezpieczeństwo, deaktywacja konta wymaga potwierdzenia adresu e-mail lub numeru telefonu.'
					}
				}
			])
		);
		this._steps = steps;
	}

	private createStep(label: string, description: string, symbol: string, segments: Segment[], completed: boolean = false): Step {
		const menu: Step = {
			label,
			description,
			symbol,
			segments,
			completed
		};
		return menu;
	}

	private initConfig(step: Step): void {
		from(step.segments)
			.pipe(
				delay(0),
				map((segment) => segment.fields.map((field) => this.initField(field)))
			)
			.subscribe();
	}

	private initField(field): void {
		switch (field.type) {
			case FormFieldType.CHECKBOX: {
				$(`.checkbox-${field.symbol}`).checkbox({
					onChecked: () => (field.value = true),
					onUnchecked: () => (field.value = false)
				});
				break;
			}
			case FormFieldType.EMAIL:
			case FormFieldType.NUMBER:
			case FormFieldType.PHONE_NUMBER:
			case FormFieldType.PASSWORD:
			case FormFieldType.TEXT: {
				field.onChange = (model: FormField) => (model.value = $(FORMNAME).form('get value', model.name));
				break;
			}
			case FormFieldType.DICTIONARY_MULTI: {
				$(`.dropdown-${field.symbol}`).dropdown({
					values: field.items,
					placeholder: 'Wybierz',
					fields: field.api.fields,
					message: {
						addResult: 'Dodaj <b>{term}</b>',
						count: 'Zaznaczono {count}',
						maxSelections: `Maksymalnie {maxCount}`,
						noResults: 'Nie znaleziono żadnych wpisów.'
					},
					onChange: (value, text, $selectedItem) => (field.value = value)
				});
				$(`.dropdown-${field.symbol}`).dropdown('set selected', field.value ? field.value.split(',') : null);
				break;
			}
			case FormFieldType.DICTIONARY_SINGLE: {
				$(`.dropdown-${field.symbol}`).dropdown({
					values: field.items,
					placeholder: 'Wybierz',
					fields: field.api.fields,
					message: {
						addResult: 'Dodaj <b>{term}</b>',
						count: 'Zaznaczono {count}',
						maxSelections: `Maksymalnie {maxCount}`,
						noResults: 'Nie znaleziono żadnych wpisów.'
					},
					onChange: (value, text, $selectedItem) => (field.value = value)
				});
				$(`.dropdown-${field.symbol}`).dropdown('set selected', field.value);
				break;
			}
		}
	}

	private initForm(step: Step): void {
		setTimeout((_) => $(FORMNAME).form({ className: { label: 'ui basic red prompt label dock-a' }, inline: true, autoCheckRequired: true }));

		from(step.segments)
			.pipe(
				map((segment) =>
					segment.fields
						.filter((field) => !!field.rules)
						.map((field) => ({
							name: field.name,
							rules: field.rules
						}))
				),
				map((rule) =>
					rule.reduce((map, obj) => {
						map[obj.name] = {
							identifier: obj.name,
							rules: obj.rules
						};
						return map;
					}, {})
				),
				delay(0),
				map((fields) => $(FORMNAME).form('add fields', fields))
			)
			.subscribe();
	}

	private initRolesDict(): void {
		const roles: Role[] = [];
		roles.push({
			name: 'Administrator',
			symbol: 'ADMINISTRATOR'
		});
		roles.push({
			name: 'Użytkownik zewnętrzny',
			symbol: 'UZYTKOWNIK'
		});
		this._rolesDict = roles;
	}

	private initPermissionsDict(): void {
		const permissions: Permission[] = [];
		permissions.push({
			name: 'Zarządzanie użytkownikami',
			symbol: 'ZARZADZANIE_UZYTKOWNIKAMI'
		});
		permissions.push({
			name: 'Zarządzanie uprawnieniami',
			symbol: 'ZARZADZANIE_UPRAWNIENIAMI'
		});
		permissions.push({
			name: 'Raporty statystyczne',
			symbol: 'RAPORTY_STATYSTYCZNE'
		});
		this._permissionsDict = permissions;
	}

	private initDialog(): void {
		switch (this._formModeType) {
			case FormModeType.EDIT: {
				this._dialogConfig = {
					title: 'Edytowanie konta',
					meta: 'edytowanie konta użytkownika w aplikacji'
				};

				switch (this._tabType) {
					case 'data':
						this._selectedIndex = 0;
						break;
					case 'password':
						this._selectedIndex = 1;
						break;
					case 'permissions':
						this._selectedIndex = 2;
						break;
					case 'restrictions':
						this._selectedIndex = 3;
						break;

					default:
						this._selectedIndex = 0;
						break;
				}

				// pobierz dane użytkownika w trybie edycji
				this._userDialogService.get(this._userId);
				break;
			}
		}
	}

	private loadForm(model: User): void {
		this._steps.forEach((step) => {
			step.segments.forEach((segment) => {
				segment.fields.forEach((field) => (field.value = model[field.name]));
			});
		});

		const fields = flatten(this._steps.map((step) => step.segments.map((segment) => segment.fields))).map((field) => ({
			name: field.name,
			value: field.value
		}));

		fields.forEach((field) => $(FORMNAME).form('set value', field.name, field.value));
	}

	private prepareForm(): User {
		const fields = flatten(this._steps.map((step) => step.segments.map((segment) => segment.fields)))
			.filter((field) => !!field.value)
			.map((field) => ({ name: field.name, value: field.value }));

		const model: User = fields.reduce((map, obj) => {
			map[obj.name] = obj.value;
			return map;
		}, {});

		return model;
	}
}
