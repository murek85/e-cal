import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TdLoadingService } from '@covalent/core/loading';
import { delay, finalize, tap } from 'rxjs/operators';

import { UsersHttpService } from 'src/app/core/services/http/users-http.service';
import { User } from 'src/app/shared/models/user.model';

@Injectable()
export class ManageUserDialogService {
	private _userSource = new BehaviorSubject<User>(null);

	user = this._userSource.asObservable();

	changeUser(data: User) {
		this._userSource.next(data);
	}

	constructor(private _tdLoadingService: TdLoadingService, private _usersHttpService: UsersHttpService) {}

	public get(id) {
		this._tdLoadingService.register('modalSyntax');
		this._usersHttpService
			.get(id)
			.pipe(
				delay(500),
				tap((user) => this.changeUser(user)),
				finalize(() => this._tdLoadingService.resolve('modalSyntax'))
			)
			.subscribe();
	}

	public update(model: User) {
		return this._usersHttpService.update(model);
	}

	public delete(id) {
		return this._usersHttpService.delete(id);
	}
}
