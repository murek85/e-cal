import { Component, OnInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { Subject } from 'rxjs';
import { filter, takeUntil, tap, finalize } from 'rxjs/operators';

import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AccountService } from 'src/app/core/services/account.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { ManageApplicationDialogService } from './manage-application-dialog.service';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';

import { Segment, Step } from 'src/app/shared/models/step.model';
import { User } from 'src/app/shared/models/user.model';
import { Application } from 'src/app/shared/models/application.model';

import { ApplicationsService } from 'src/app/core/services/applications.service';

declare var $: any;

const MODALNAME = '.modal-manage-application';

@Component({
	selector: 'app-manage-application-dialog',
	templateUrl: './manage-application-dialog.component.html',
	providers: [ApplicationsService, ManageApplicationDialogService]
})
export class ManageApplicationDialogComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _formModeType: FormModeType;
	private _steps: Array<Step>;
	private _selectedIndex = 0;
	private _applicationId: string;
	private _tabType: 'data' | 'permissions';
	private _application: Application;
	private _userLogged: User;
	private _loading = false;

	@ViewChild('form') form;

	constructor(
		private _applicationDialogService: ManageApplicationDialogService,
		private _accountService: AccountService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA) public data: { formModeType: FormModeType; applicationId: string; tabType: 'data' | 'permissions' }
	) {
		this._formModeType = data.formModeType;
		this._applicationId = data.applicationId;
		this._tabType = data.tabType;
	}

	get formConfig() {
		return {
			symbol: 'manage-application',
			broadcast: { type: ResourceType.APPLICATIONS }
		};
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get steps(): Step[] {
		return this._steps;
	}

	get selectedIndex() {
		return this._selectedIndex;
	}

	get loading(): boolean {
		return this._loading;
	}

	get application(): Application {
		return this._application;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		this._loading = true;
		this._applicationDialogService.application
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((data) => !!data),
				tap((application) => (this._application = application)),
				tap((application) => this.form.loadForm(application))
			)
			.subscribe(() => (this._loading = false));

		$(MODALNAME)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initSteps();
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeStep($event): void {
		this.form.changeStep($event);
	}

	onBackForm(): void {
		$(MODALNAME).modal('hide');
	}

	onSaveForm(): void {
		this.form.saveForm(
			(model) => this._applicationDialogService.update({ ...model, id: this._applicationId }),
			() => $(MODALNAME).modal('hide')
		);
	}

	private initSteps(): void {
		const steps: Step[] = [];
		steps.push(
			this.createStep('Aplikacja', 'Podstawowe informacje o aplikacja.', 'application-data', [
				{
					header: 'Dane aplikacji',
					description: 'Uzupełnienie podstawowych informacji o aplikacji.',
					fields: [
						{
							label: 'Nazwa',
							name: 'name',
							symbol: 'application-name',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole '{name}' nie może być puste.`
								}
							]
						},
						{
							label: 'Opis',
							name: 'description',
							symbol: 'application-description',
							type: FormFieldType.TEXTAREA
						},
						{
							label: 'Symbol',
							name: 'symbol',
							symbol: 'application-symbol',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Adres usługi api',
							name: 'apiUrl',
							symbol: 'application-api-url',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Adres aplikacji',
							name: 'appUrl',
							symbol: 'application-app-url',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						}
					]
				}
			])
		);
		steps.push(
			this.createStep('Uprawnienia', '', 'application-permissions', [
				{
					header: 'Uprawnienia',
					description: '',
					fields: []
				}
			])
		);
		this._steps = steps;
	}

	private createStep(label: string, description: string, symbol: string, segments: Segment[], completed: boolean = false): Step {
		const menu: Step = { label, description, symbol, segments, completed };
		return menu;
	}

	private initDialog(): void {
		switch (this._formModeType) {
			case FormModeType.EDIT: {
				this._dialogConfig = {
					title: 'Edytowanie aplikacji',
					meta: 'edytowanie aplikacji'
				};

				switch (this._tabType) {
					case 'data':
						this._selectedIndex = 0;
						break;
					case 'permissions':
						this._selectedIndex = 1;
						break;

					default:
						this._selectedIndex = 0;
						break;
				}

				// pobierz dane roli w trybie edycji
				this._applicationDialogService.get(this._applicationId);
				break;
			}
		}
	}
}
