import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TdLoadingService } from '@covalent/core/loading';
import { tap } from 'rxjs/operators';

import { ApplicationsService } from 'src/app/core/services/applications.service';
import { Application } from 'src/app/shared/models/application.model';

@Injectable()
export class AddApplicationDialogService {
	constructor(private _tdLoadingService: TdLoadingService, private _applicationsService: ApplicationsService) {}

	public create(model: Application) {
		return this._applicationsService.create(model);
	}
}
