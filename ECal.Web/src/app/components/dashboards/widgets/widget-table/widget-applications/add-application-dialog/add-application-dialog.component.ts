import { Component, OnInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { of, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AccountService } from 'src/app/core/services/account.service';
import { AddApplicationDialogService } from './add-application-dialog.service';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';

import { Segment, Step } from 'src/app/shared/models/step.model';
import { User } from 'src/app/shared/models/user.model';
import { Application } from 'src/app/shared/models/application.model';
import { ApplicationsService } from 'src/app/core/services/applications.service';
import { ResourceType } from 'src/app/core/services/websocket.service';

declare var $: any;

const MODALNAME = '.modal-add-application';

@Component({
	selector: 'app-add-application-dialog',
	templateUrl: './add-application-dialog.component.html',
	providers: [ApplicationsService, AddApplicationDialogService]
})
export class AddApplicationDialogComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _formModeType: FormModeType;
	private _steps: Step[];
	private _loading = false;
	private _application: Application;
	private _userLogged: User;

	@ViewChild('form') form;

	constructor(
		private _applicationDialogService: AddApplicationDialogService,
		private _accountService: AccountService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA) public data: { formModeType: FormModeType }
	) {
		this._formModeType = data.formModeType;
	}

	get formConfig() {
		return {
			symbol: 'add-application',
			broadcast: { type: ResourceType.APPLICATIONS }
		};
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get steps(): Step[] {
		return this._steps;
	}

	get loading(): boolean {
		return this._loading;
	}

	get application(): Application {
		return this._application;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		$(MODALNAME)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initSteps();
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeStep($event): void {
		this.form.changeStep($event);
	}

	onPreviousStep($event): void {
		this.form.previousStep($event);
	}

	onNextStep($event): void {
		this.form.nextStep($event);
	}

	onBackForm(): void {
		$(MODALNAME).modal('hide');
	}

	onSaveForm(): void {
		this.form.saveForm(
			(model) => this._applicationDialogService.create(model),
			() => $(MODALNAME).modal('hide')
		);
	}

	private initSteps(): void {
		const steps: Step[] = [];
		steps.push(
			this.createStep('Aplikacja', 'Podstawowe informacje o aplikacji.', 'application-data', [
				{
					header: 'Dane aplikacji',
					description: 'Uzupełnienie podstawowych informacji o aplikacji.',
					fields: [
						{
							label: 'Nazwa',
							name: 'name',
							symbol: 'application-name',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Opis',
							name: 'description',
							symbol: 'application-description',
							type: FormFieldType.TEXTAREA
						},
						{
							label: 'Symbol',
							name: 'symbol',
							symbol: 'application-symbol',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Adres usługi api',
							name: 'apiUrl',
							symbol: 'application-api-url',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Adres aplikacji',
							name: 'appUrl',
							symbol: 'application-app-url',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						}
					]
				}
			])
		);
		this._steps = steps;
	}

	private createStep(label: string, description: string, symbol: string, segments: Segment[], completed: boolean = false): Step {
		const menu: Step = { label, description, symbol, segments, completed };
		return menu;
	}

	private initDialog(): void {
		switch (this._formModeType) {
			case FormModeType.ADD: {
				this._dialogConfig = {
					title: 'Nowa aplikacja',
					meta: 'tworzenie nowej aplikacji'
				};
				break;
			}
		}
	}
}
