import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { ApplicationsService } from 'src/app/core/services/applications.service';

import { WidgetApplicationsComponent } from './widget-applications.component';
import { AddApplicationDialogComponent } from './add-application-dialog/add-application-dialog.component';
import { ManageApplicationDialogComponent } from './manage-application-dialog/manage-application-dialog.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetApplicationsComponent, AddApplicationDialogComponent, ManageApplicationDialogComponent],
	imports: [APP_MODULES],
	exports: [WidgetApplicationsComponent, AddApplicationDialogComponent, ManageApplicationDialogComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: [ApplicationsService]
})
export class WidgetApplicationsModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetApplicationsComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetApplicationsComponent);
	}

	static import() {
		return import('../widget-applications/widget-applications.module');
	}
}
