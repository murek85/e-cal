import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TdLoadingService } from '@covalent/core/loading';
import { delay, tap, finalize } from 'rxjs/operators';

import { ApplicationsService } from 'src/app/core/services/applications.service';
import { Application } from 'src/app/shared/models/application.model';

@Injectable()
export class ManageApplicationDialogService {
	private _applicationSource = new BehaviorSubject<Application>(null);

	application = this._applicationSource.asObservable();

	changeApplication(data: Application) {
		this._applicationSource.next(data);
	}

	constructor(private _tdLoadingService: TdLoadingService, private _applicationsService: ApplicationsService) {}

	public get(id) {
		this._tdLoadingService.register('modalSyntax');
		this._applicationsService
			.get(id)
			.pipe(
				delay(500),
				tap((application) => this.changeApplication(application)),
				finalize(() => this._tdLoadingService.resolve('modalSyntax'))
			)
			.subscribe();
	}

	public update(model: Application) {
		return this._applicationsService.update(model);
	}
}
