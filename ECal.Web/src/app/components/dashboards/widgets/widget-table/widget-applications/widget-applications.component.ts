import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { of } from 'rxjs';
import { catchError, delay, filter, flatMap, map, mergeMap, takeUntil, tap } from 'rxjs/operators';

import { WidgetTableComponent } from '../widget-table.component';

import { AccountService } from 'src/app/core/services/account.service';
import { ApplicationsService } from 'src/app/core/services/applications.service';

import { User } from 'src/app/shared/models/user.model';

import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { WebsocketEvent } from 'src/app/core/services/websocket.service';

import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';
import { Control } from '../../widget-container/widget-container.component';
import { ColumnDefinition, FilterType } from 'src/app/shared/models/column.model';
import { Application } from 'src/app/shared/models/application.model';
import { AddApplicationDialogComponent } from './add-application-dialog/add-application-dialog.component';
import { ManageApplicationDialogComponent } from './manage-application-dialog/manage-application-dialog.component';
import { PairingService } from 'src/app/core/services/pairing.service';
import { RsqlBuilder } from 'src/app/core/services/rsql/rsql-builder';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';
import { ConfirmActionDialogComponent } from 'src/app/shared/components/dialogs/confirm-action-dialog/confirm-action-dialog.component';

declare var $: any;

@Component({
	selector: 'app-widget-applications',
	templateUrl: '../widget-table.component.html',
	styleUrls: ['../widget-table.component.scss']
})
export class WidgetApplicationsComponent extends WidgetTableComponent<Application> implements OnInit, OnDestroy {
	config: any = {
		type: 'roles'
	};

	_userLogged: User;

	constructor(
		protected _translateService: TranslateService,
		protected _tdLoadingService: TdLoadingService,
		protected _toastService: ToastService,
		protected _dialogService: DialogService,
		protected _broadcastService: BroadcastService,
		protected _dashboardsService: DashboardsService,
		protected _pairingService: PairingService,
		private _applicationsService: ApplicationsService,
		private _accountService: AccountService
	) {
		super(_translateService, _tdLoadingService, _toastService, _dialogService, _broadcastService, _dashboardsService, _pairingService);
	}

	protected query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._applicationsService.query(pagingSettings, rsql);
	}

	protected report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder) {
		return this._applicationsService.report(pagingSettings, rsql, report);
	}

	protected get columnDefinitions(): Array<ColumnDefinition> {
		return [
			{
				alias: 'icon',
				hidden: true,
				size: 'one wide',
				formatter: (value: string) => {
					return { icon: 'html5' };
				}
			},
			{
				alias: 'name',
				filter: {
					type: FilterType.TEXT
				}
			},
			{
				alias: 'description',
				filter: {
					type: FilterType.TEXT
				}
			},
			{
				alias: 'apiUrl',
				filter: {
					type: FilterType.TEXT
				}
			},
			{
				alias: 'appUrl',
				filter: {
					type: FilterType.TEXT
				}
			},
			{
				alias: 'symbol',
				filter: {
					type: FilterType.TEXT
				}
			}
		];
	}

	get controls(): Array<Control> {
		return [
			{
				text: 'Utwórz',
				icon: 'add',
				showText: true,
				click: () => this.onAddApplication()
			},
			{
				text: 'Zarządzaj',
				icon: 'edit',
				showText: true,
				disabled: this.singleSelection.isEmpty(),
				click: (row: Application) => this.onEditApplication(row)
			},
			{
				text: 'Uprawnienia',
				icon: 'key',
				disabled: this.singleSelection.isEmpty(),
				click: (row: Application) => this.onPermissionsApplication(row)
			},
			{
				text: 'Usuń',
				icon: 'trash',
				color: 'red',
				disabled: this.singleSelection.isEmpty(),
				click: (row: Application) => this.onDeleteApplication(row)
			}
		];
	}

	ngOnInit(): void {
		super.ngOnInit();

		this._broadcastService
			.observe(MessageType.RESOURCES_CHANGED)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((message: { type: string; title: string; message: WebsocketEvent; config: any }) => message.type === ResourceType.APPLICATIONS),
				tap((message) => this._toastService.open(message.title, message.message, message.config))
			)
			.subscribe();

		this._broadcastService
			.observe(MessageType.RESOURCES_CHANGED)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((message: { type: string; title: string; message: WebsocketEvent; config: any }) => message.type === ResourceType.APPLICATIONS),
				filter((message) => message.config.class === 'success'),
				tap((_) => this.reload())
			)
			.subscribe();

		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

	private onAddApplication(): void {
		this._dialogService
			.open<AddApplicationDialogComponent>(AddApplicationDialogComponent, {
				data: { formModeType: FormModeType.ADD }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	private onEditApplication(row: Application): void {
		this._dialogService
			.open<ManageApplicationDialogComponent>(ManageApplicationDialogComponent, {
				data: { formModeType: FormModeType.EDIT, applicationId: row.id }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	private onPermissionsApplication(row: Application): void {
		this._dialogService
			.open<ManageApplicationDialogComponent>(ManageApplicationDialogComponent, {
				data: { formModeType: FormModeType.EDIT, applicationId: row.id, tabType: 'permissions' }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	private onDeleteApplication(row: Application): void {
		const broadcast = (result) =>
			this._broadcastService.broadcast({
				type: MessageType.RESOURCES_CHANGED,
				payload: {
					type: ResourceType.APPLICATIONS,
					title: result.title,
					message: result.message,
					config: result.config
				}
			});

		const success = () => ({
			title: 'Zapisano',
			message: `Usunięto.`,
			config: { class: 'success', position: 'top center', showIcon: 'check circle' }
		});

		const error = () =>
			of({
				title: 'Błąd',
				message: `Wystąpił problem.`,
				config: { class: 'error', position: 'top center', showIcon: 'exclamation circle' }
			});

		this._dialogService
			.open<ConfirmActionDialogComponent>(ConfirmActionDialogComponent, {
				data: {
					title: 'Usuwanie aplikacji',
					description: `Czy chcesz usunąć wybraną aplikację '${row.name}'?`,
					iconClass: 'question circle outline',
					approveButtonLabel: 'Usuń',
					approveButtonColorClass: 'red',
					denyButtonLabel: 'Anuluj'
				}
			})
			.events.pipe(
				filter((event) => !!event),
				filter((event) => event.type === 'accept'),
				mergeMap(() => this._applicationsService.delete(row.id).pipe(delay(500), map(success), catchError(error)))
			)
			.subscribe(broadcast);
	}
}
