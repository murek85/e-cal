import { TdFileService } from '@covalent/core/file';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Subject, of, Observable } from 'rxjs';
import { catchError, debounceTime, filter, finalize, map, takeUntil, tap } from 'rxjs/operators';

import { BroadcastService } from 'src/app/core/services/broadcast.service';
import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';

import { DialogConfig } from 'src/app/shared/models/dialog-config.model';

import { NgxMRemoteDesktopService } from 'ngx-mremote-desktop';

declare var $: any;

const MODALNAME = '.modal-file-remote-desktop';

@Component({
	selector: 'app-file-modal',
	template: `
		<div class="ui bottom aligned mini modal modal-file-remote-desktop" style="right: 5vh;">
			<div class="ui header" style="padding: .5em .75em;">
				<div class="content">
					<span translate>{{ dialogConfig.title }}</span>
				</div>
			</div>
			<div class="content" style="    padding: .5em 1em 0em 1em;">
				<div class="ui basic segment" style="margin: 0;">
					<div class="ui width grid">
						<div class="sixteen wide row" style="padding: 0;">
							<div class="ten wide column" style="padding: 0;">
								<ng-template ngFor let-file let-last="last" [ngForOf]="fileUpload.files">
									<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">{{ file.name }}</div>
								</ng-template>

								<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">{{ fileUpload.files.name }}</div>
							</div>
							<div class="right aligned six wide column" style="padding: 0;">
								<span
									>[{{ fileUpload?.offset / (1024 * 1024) | number: '1.0-0' }}/{{
										fileUpload?.size / (1024 * 1024) | number: '1.0-0'
									}}
									MB]</span
								>
							</div>
						</div>
						<div class="sixteen wide row">
							<mat-progress-bar mode="determinate" [value]="fileUpload.progress" [min]="0" [max]="100"></mat-progress-bar>
						</div>
					</div>
				</div>
			</div>
		</div>
	`
})
export class FileRemoteDesktopDialogComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _service: NgxMRemoteDesktopService;
	private _fileUpload: { progress: number; offset: number; size: number; files };
	private _url: string;

	constructor(
		private _fileService: TdFileService,
		private _broadcastService: BroadcastService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA) public data: { files; service; url }
	) {
		this._service = data.service;
		this._fileUpload = { progress: 0, offset: 0, size: 0, files: data.files };
		this._url = data.url;
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get fileUpload(): { progress: number; offset: number; size: number; files } {
		return this._fileUpload;
	}

	set filesUpload(data) {
		this._fileUpload = data;
	}

	ngOnInit(): void {
		$(MODALNAME)
			.modal({
				context: '.modal-connect-remote-desktop',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					// this._dialogRef.events.next({ type: 'deny' });
					return true;
				},
				onApprove: () => {
					// this._dialogRef.events.next({ type: 'accept' });
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	close(): void {
		$(MODALNAME).modal('hide');
	}

	private initDialog(): void {
		this._dialogConfig = {
			title: `Przesyłanie pliku`,
			meta: ''
		};

		this.uploadFile(this._fileUpload.files, this._url)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((data) => data.type === 1),
				map(
					(data: { type; loaded; total }) =>
						(this._fileUpload = {
							...this._fileUpload,
							progress: Math.floor((data.loaded / data.total) * 100),
							offset: data.loaded,
							size: data.total
						})
				),
				filter((data) => data.offset >= data.size),
				tap(() => this.close()),
				catchError((err) => of(this.close()))
			)
			.subscribe();
	}

	private uploadFile(files: FileList | File, url: string): Observable<any> {
		const formData: any = new FormData();
		if (files instanceof FileList) {
			const arrayFiles = Array.from(files);
			arrayFiles.forEach((file) => {
				formData.append('uploads[]', file, file.name);
			});
			return this._fileService.send(`${url}/api/repositories`, 'post', formData, { params: { type: 'desktop' } });
		} else {
			formData.append('uploads[]', files, files.name);
			return this._fileService.send(`${url}/api/repositories`, 'post', formData, { params: { type: 'desktop' } });
		}
	}
}
