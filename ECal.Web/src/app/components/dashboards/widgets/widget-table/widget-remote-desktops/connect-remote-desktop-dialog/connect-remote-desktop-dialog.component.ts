import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, Inject, ElementRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CdkStepper } from '@angular/cdk/stepper';
import { Clipboard } from '@angular/cdk/clipboard';
import { TdLoadingService } from '@covalent/core/loading';
import { HTTPTunnel, WebSocketTunnel } from '@illgrenoble/guacamole-common-js';
import { Subject } from 'rxjs';
import { filter, finalize, map, takeUntil, tap } from 'rxjs/operators';

import { FileSaverService } from 'ngx-filesaver';
import { KeyboardShortcutsComponent, ShortcutInput } from 'ng-keyboard-shortcuts';

import { NgxMRemoteDesktopService } from 'ngx-mremote-desktop';
import { NgxMAuthOidcService } from 'src/app/core/services/oidc/oidc.service';

import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { DialogService, DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { BroadcastService } from 'src/app/core/services/broadcast.service';
import { ConnectRemoteDesktopDialogService } from './connect-remote-desktop-dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';

import { RemoteDesktop } from 'src/app/shared/models/remote-desktops.model';
import { User } from 'src/app/shared/models/user.model';
import { ClipboardRemoteDesktopDialogComponent } from './clipboard-remote-desktop-dialog/clipboard-remote-desktop-dialog.component';
import { FileRemoteDesktopDialogComponent } from './file-remote-desktop-dialog/file-remote-desktop-dialog.component';

import * as moment from 'moment';
import { AngularPageVisibilityStateEnum, OnPageHidden, OnPagePrerender, OnPageUnloaded, OnPageVisibilityChange, OnPageVisible } from 'angular-page-visibility';
import { TdFileService } from '@covalent/core/file';

declare var $: any;

const flatten = (arr) => arr.reduce((a, b) => a.concat(Array.isArray(b) ? flatten(b) : b), []);

const MODALNAME = '.modal-connect-remote-desktop';

@Component({
	selector: 'app-connect-remote-desktop-dialog',
	templateUrl: './connect-remote-desktop-dialog.component.html',
	styleUrls: ['./connect-remote-desktop-dialog.component.scss'],
	providers: [ConnectRemoteDesktopDialogService]
})
export class ConnectRemoteDesktopDialogComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _userLogged: User;
	private _timer: any;
	private _isManualDisconnect = false;
	private _service: NgxMRemoteDesktopService;

	@ViewChild('stepper') stepper: CdkStepper;

	@ViewChild(KeyboardShortcutsComponent) private _keyboard: KeyboardShortcutsComponent;

	_shortcuts: ShortcutInput[] = [];

	constructor(
		private _clipboard: Clipboard,
		private _tdLoadingService: TdLoadingService,
		private _broadcastService: BroadcastService,
		private _toastService: ToastService,
		private _oidcService: NgxMAuthOidcService,
		private _accountService: AccountService,
		private _dialogRef: DialogOverlayRef,
		private _dialogService: DialogService,
		private _fileSaverService: FileSaverService,
		private _snackBar: MatSnackBar,
		private _connectRemoteDesktopDialogService: ConnectRemoteDesktopDialogService,
		@Inject(DIALOG_MODAL_DATA) public data: RemoteDesktop
	) {}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	get service(): NgxMRemoteDesktopService {
		return this._service;
	}

	get isManualDisconnect(): boolean {
		return this._isManualDisconnect;
	}

	get shortcuts() {
		return this._shortcuts;
	}

	private initSemanticConfig(): void {}

	private initSemanticForm(): void {}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
		this.initSemanticForm();

		this._shortcuts.push(
			{
				key: ['f11'],
				label: 'paste',
				description: 'f11',
				command: (e) => alert('f11'),
				preventDefault: true
			},
			{
				key: ['cmd + w'],
				label: 'paste',
				description: 'Cmd + w',
				command: (e) => alert('Cmd + w'),
				preventDefault: true
			},
			{
				key: ['cmd + v'],
				label: 'paste',
				description: 'Cmd + v',
				command: (e) => alert('Cmd + v'),
				preventDefault: true
			},
			{
				key: ['cmd + c'],
				label: 'copy',
				description: 'Cmd + c',
				command: (e) => {
					navigator.clipboard
						.readText()
						.then((text) => {
							if (text) {
								this._service.sendRemoteClipboardData(text);
								this._snackBar.open('Wklejono zawartość ze schowka', null, { duration: 2000 });
							}
						})
						.catch((err) => console.error('Failed to read clipboard contents: ', err));
				},
				preventDefault: true
			}
		);
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		$(MODALNAME)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					// inicjalizacja okna dialogowego
					this.initDialog();
					// inicjalizacja modułu pulpitu zdalnego
					this.initRemoteDesktop();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();

		clearInterval(this._timer);
	}

	onClick(event): void {
		navigator.clipboard
			.readText()
			.then((text) => this._service.sendRemoteClipboardData(text))
			.catch((err) => console.error('Failed to read clipboard contents: ', err));
	}

	close(): void {
		$(MODALNAME).modal('hide');
	}

	private initDialog(): void {
		this._dialogConfig = {
			title: `[${this.data.protocol}] [${this.data.hostname}]`,
			meta: 'sesja pulpitu zdalnego'
		};

		$('.modal-connect-remote-desktop').on('keydown keypress keyup click contextmenu', false);
	}

	private initRemoteDesktop(): void {
		// Setup tunnel. The tunnel can be either: WebsocketTunnel, HTTPTunnel or ChainedTunnel
		const tunnel = new WebSocketTunnel(AppConfig.settings.system.guacamoleUrl);
		// const tunnel = new HTTPTunnel(AppConfig.settings.system.guacamoleUrl, true, {});
		/**
		 *  Create an instance of the remote desktop manager by
		 *  passing in the tunnel
		 */
		this._service = new NgxMRemoteDesktopService(tunnel);
		this._service.onRemoteClipboardData
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((text) => !!text),
				tap((text) => this._clipboard.copy(text as string))
			)
			.subscribe();

		this._service.onReconnect.pipe(tap((_) => this.connect())).subscribe();

		this._service.onStateChange
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((state) => state === NgxMRemoteDesktopService.STATE.CONNECTED || state === NgxMRemoteDesktopService.STATE.DISCONNECTED),
				tap((_) => this._tdLoadingService.resolve('remoteDesktopSyntax'))
			)
			.subscribe();

		this.connect();
	}

	fileDrop(files: File | FileList): void {
		this._dialogService
			.open<FileRemoteDesktopDialogComponent>(FileRemoteDesktopDialogComponent, {
				data: { files, service: this._service, url: `http://${this.data.machinename}:4889` }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	screenshot(): void {
		this._service.createScreenshot((blob) => {
			if (blob) {
				const fileName = `screenshot_${moment().format('DD_MM_yyyy_HH_mm_ss')}.png`;
				this._fileSaverService.save(blob, fileName);
				this._snackBar.open(`Zapisano zrzut ekranu do pliku ${fileName}.`, null, { duration: 2000 });
			}
		});
	}

	disconnect(): void {
		this._tdLoadingService.register('remoteDesktopSyntax');
		this._isManualDisconnect = true;
		this._service.getClient().disconnect();
		this._timer = setTimeout(() => this.close(), 1000);
	}

	enterFullScreen(): void {
		this._service.setFullScreen(true);
	}

	exitFullScreen(): void {
		this._service.setFullScreen(false);
	}

	enterDisplayScale(): void {
		this._service.setDisplayScale(true);
	}

	exitDisplayScale(): void {
		this._service.setDisplayScale(false);
	}

	clipboard(): void {
		this._dialogService
			.open<ClipboardRemoteDesktopDialogComponent>(ClipboardRemoteDesktopDialogComponent, {
				data: {
					service: this._service,
					clipboardCopy: ['root', 'journalctl -fu guacamole-server.service', 'systemctl status guacamole-server.service']
				}
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	connect(): void {
		const parameters = {
			hostname: this.data.hostname,
			port: this.data.port,
			dpi: this.data.dpi,
			protocol: this.data.protocol,
			image: this.data.image,
			width: window.screen.width,
			height: window.screen.height,
			'ignore-cert': this.data['ignore-cert'],
			'enable-drive': this.data['enable-drive'],
			'drive-path': this.data['drive-path'],
			'enable-sftp': this.data['enable-sftp'],
			'clipboard-encoding': this.data['clipboard-encoding']
		};

		this._tdLoadingService.register('remoteDesktopSyntax');

		/*
		 * The manager will establish a connection to:
		 * http://localhost:8011?ws?ip={address}&image=image/png&audio=audio/L16&dpi=96&width=n&height=n
		 */
		this._timer = setTimeout(() => this._service.connect(parameters), 1000);
	}
}
