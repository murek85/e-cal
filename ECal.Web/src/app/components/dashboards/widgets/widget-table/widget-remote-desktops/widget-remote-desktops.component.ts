import { TranslateService } from '@ngx-translate/core';
import { ConnectRemoteDesktopDialogComponent } from './connect-remote-desktop-dialog/connect-remote-desktop-dialog.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { of } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { WidgetTableComponent } from '../widget-table.component';

import { AccountService } from 'src/app/core/services/account.service';
import { RemoteDesktopsService } from 'src/app/core/services/remote-desktops.service';

import { User } from 'src/app/shared/models/user.model';

import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { WebsocketEvent } from 'src/app/core/services/websocket.service';

import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';
import { Control } from '../../widget-container/widget-container.component';
import { CellData, ColumnDefinition, FilterType } from 'src/app/shared/models/column.model';
import { RemoteDesktop } from 'src/app/shared/models/remote-desktops.model';
import { PairingService } from 'src/app/core/services/pairing.service';
import { RsqlBuilder } from 'src/app/core/services/rsql/rsql-builder';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';

declare var $: any;

@Component({
	selector: 'app-widget-remote-desktops',
	templateUrl: '../widget-table.component.html',
	styleUrls: ['../widget-table.component.scss']
})
export class WidgetRemoteDesktopsComponent extends WidgetTableComponent<RemoteDesktop> implements OnInit, OnDestroy {
	config: any = {
		type: 'remote-desktops'
	};

	_userLogged: User;

	constructor(
		protected _translateService: TranslateService,
		protected _tdLoadingService: TdLoadingService,
		protected _toastService: ToastService,
		protected _dialogService: DialogService,
		protected _broadcastService: BroadcastService,
		protected _dashboardsService: DashboardsService,
		protected _pairingService: PairingService,
		private _remoteDesktopsService: RemoteDesktopsService,
		private _accountService: AccountService
	) {
		super(_translateService, _tdLoadingService, _toastService, _dialogService, _broadcastService, _dashboardsService, _pairingService);
	}

	protected query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._remoteDesktopsService.query();
	}

	protected report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder) {
		return of(null);
	}

	protected get columnDefinitions(): Array<ColumnDefinition> {
		return [
			{
				alias: 'icon',
				hidden: true,
				size: 'one wide',
				formatter: (value: string) => {
					return { icon: 'desktop' };
				}
			},
			{
				alias: 'protocol',
				filter: {
					type: FilterType.TEXT
				},
				size: 'three wide',
				formatter: (value: string): string => {
					if (value === null || value === undefined) {
						return null;
					}
					return `[${value.toUpperCase()}]`;
				}
			},
			{
				alias: 'systemname',
				filter: {
					type: FilterType.TEXT
				}
			},
			{
				alias: 'hostname',
				filter: {
					type: FilterType.TEXT
				}
			},
			{
				alias: 'machinename',
				filter: {
					type: FilterType.TEXT
				}
			}
		];
	}

	get controls(): Array<Control> {
		return [
			{
				text: 'Połącz',
				icon: 'play',
				showText: true,
				disabled: this.singleSelection.isEmpty(),
				click: () => this.onConnectRemoteDesktop()
			},
			{
				text: 'Zarządzaj',
				icon: 'edit',
				showText: true,
				disabled: this.singleSelection.isEmpty(),
				click: () => this.onEditRemoteDesktop()
			}
		];
	}

	ngOnInit(): void {
		super.ngOnInit();

		this._broadcastService
			.observe(MessageType.RESOURCES_CHANGED)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((message: { type: string; title: string; message: WebsocketEvent; config: any }) => message.type === ResourceType.REMOTE_DESKTOPS),
				tap((message) => this._toastService.open(message.title, message.message, message.config))
			)
			.subscribe();

		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

	private onConnectRemoteDesktop(): void {
		this._dialogService
			.open<ConnectRemoteDesktopDialogComponent>(ConnectRemoteDesktopDialogComponent, {
				data: this.singleSelection.selected.first()
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	private onEditRemoteDesktop(): void {}
}
