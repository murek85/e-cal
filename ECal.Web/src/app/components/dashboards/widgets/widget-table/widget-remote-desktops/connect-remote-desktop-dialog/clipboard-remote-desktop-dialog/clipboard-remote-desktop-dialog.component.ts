import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { Component, OnInit, Input, Inject, OnDestroy } from '@angular/core';
import { BroadcastService } from 'src/app/core/services/broadcast.service';
import { DialogOverlayRef } from 'src/app/core/services/dialog.service';
import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';

import { DialogConfig } from 'src/app/shared/models/dialog-config.model';

import { NgxMRemoteDesktopService } from 'ngx-mremote-desktop';

declare var $: any;

const MODALNAME = '.modal-clipboard-remote-desktop';

@Component({
	selector: 'app-clipboard-modal',
	template: `
		<div class="ui bottom aligned tiny fullscreen modal modal-clipboard-remote-desktop" style="left: 5vh;">
			<i class="close icon" style="top: .25em"></i>
			<div class="ui header" style="padding: .5em .75em;">
				<div class="content">
					<span translate>{{ dialogConfig.title }}</span>
				</div>
			</div>
			<div class="content" style="padding: 1em;">
				<div class="ui basic segment" style="margin: 0; padding: 0;">
					<form class="ui form form-clipboard">
						<div class="ui width grid">
							<div class="sixteen wide row">
								<div class="column">
									<div class="ui middle aligned selection divided list" style="overflow-y: auto; height: 144px;">
										<ng-template #lists ngFor let-copy let-last="last" [ngForOf]="clipboardCopy">
											<div class="item" (click)="clipboardData = copy">
												<i class="paste icon"></i>
												<div class="content">
													<div class="header">{{ copy }}</div>
												</div>
											</div>
										</ng-template>
									</div>
								</div>
							</div>

							<div class="sixteen wide row">
								<div class="column">
									<div class="field">
										<div class="ui input">
											<textarea
												name="clipboard"
												[(ngModel)]="clipboardData"
												rows="5"
												placeholder="{{ 'Treść schowka' | translate }}"
											></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="actions">
				<button type="button" class="ui button" (click)="onClear()" style="box-shadow: 0px 0px 25px 5px rgba(0,0,0,.1);">
					<span translate>Wyczyść</span>
				</button>
				<button
					type="button"
					class="ui primary button"
					(click)="onSend()"
					[disabled]="!clipboardData?.length"
					style="box-shadow: 0px 0px 25px 5px rgba(0,0,0,.1);"
				>
					<span translate>Wyślij</span>
				</button>
			</div>
		</div>
	`
})
export class ClipboardRemoteDesktopDialogComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _clipboardData: string;
	private _clipboardCopy: string[];
	private _service: NgxMRemoteDesktopService;
	private _clipboardSubscription;

	constructor(
		private _snackBar: MatSnackBar,
		private _broadcastService: BroadcastService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA) public data: { service; clipboardCopy }
	) {
		this._service = data.service;
		this._clipboardCopy = data.clipboardCopy;
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get clipboardData(): string {
		return this._clipboardData;
	}

	set clipboardData(clipboardData: string) {
		this._clipboardData = clipboardData;
	}

	get clipboardCopy(): string[] {
		return this._clipboardCopy;
	}

	ngOnInit(): void {
		this._clipboardSubscription = this._service.onRemoteClipboardData;
		this._clipboardSubscription.subscribe((data) => (this._clipboardData = data));

		$(MODALNAME)
			.modal({
				context: '.modal-connect-remote-desktop',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this._service.setFocused(false);
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
					this._service.setFocused(true);
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	close(): void {
		$(MODALNAME).modal('hide');
	}

	onSend(): void {
		this._service.sendRemoteClipboardData(this._clipboardData);
		this._snackBar.open('Skopiowano zawartość schowka', null, { duration: 2000 });
		this.close();
	}

	onClear(): void {
		this._clipboardData = '';
	}

	private initDialog(): void {
		this._dialogConfig = {
			title: `Schowek`,
			meta: ''
		};
	}
}
