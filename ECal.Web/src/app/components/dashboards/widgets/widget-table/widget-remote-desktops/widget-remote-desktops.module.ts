import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { RemoteDesktopsService } from 'src/app/core/services/remote-desktops.service';

import { WidgetRemoteDesktopsComponent } from './widget-remote-desktops.component';
import { ConnectRemoteDesktopDialogComponent } from './connect-remote-desktop-dialog/connect-remote-desktop-dialog.component';
import { FileRemoteDesktopDialogComponent } from './connect-remote-desktop-dialog/file-remote-desktop-dialog/file-remote-desktop-dialog.component';
import { ClipboardRemoteDesktopDialogComponent } from './connect-remote-desktop-dialog/clipboard-remote-desktop-dialog/clipboard-remote-desktop-dialog.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetRemoteDesktopsComponent, ConnectRemoteDesktopDialogComponent, FileRemoteDesktopDialogComponent, ClipboardRemoteDesktopDialogComponent],
	imports: [APP_MODULES],
	exports: [WidgetRemoteDesktopsComponent, ConnectRemoteDesktopDialogComponent, FileRemoteDesktopDialogComponent, ClipboardRemoteDesktopDialogComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: [RemoteDesktopsService]
})
export class WidgetRemoteDesktopsModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetRemoteDesktopsComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetRemoteDesktopsComponent);
	}

	static import() {
		return import('../widget-remote-desktops/widget-remote-desktops.module');
	}
}
