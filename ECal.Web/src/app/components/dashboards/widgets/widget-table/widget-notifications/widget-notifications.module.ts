import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { NotificationsService } from 'src/app/core/services/notifications.service';

import { WidgetNotificationsComponent } from './widget-notifications.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetNotificationsComponent],
	imports: [APP_MODULES],
	exports: [WidgetNotificationsComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: [NotificationsService]
})
export class WidgetNotificationsModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetNotificationsComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetNotificationsComponent);
	}

	static import() {
		return import('../widget-notifications/widget-notifications.module');
	}
}
