import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { of } from 'rxjs';
import { filter, flatMap, map, mergeMap, takeUntil, tap } from 'rxjs/operators';

import { WidgetTableComponent } from '../widget-table.component';

import { AccountService } from 'src/app/core/services/account.service';
import { NotificationsService } from 'src/app/core/services/notifications.service';

import { User } from 'src/app/shared/models/user.model';

import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { WebsocketEvent } from 'src/app/core/services/websocket.service';

import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { Control } from '../../widget-container/widget-container.component';
import { ColumnDefinition, FilterType } from 'src/app/shared/models/column.model';
import { PairingService } from 'src/app/core/services/pairing.service';
import { RsqlBuilder } from 'src/app/core/services/rsql/rsql-builder';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';

import { DEFAULT_DATE_TIME_FORMATER } from './../widget-table.component';

import * as moment from 'moment';

declare var $: any;

@Component({
	selector: 'app-widget-notifications',
	templateUrl: '../widget-table.component.html',
	styleUrls: ['../widget-table.component.scss']
})
export class WidgetNotificationsComponent extends WidgetTableComponent<{}> implements OnInit, OnDestroy {
	config: any = {
		type: 'roles'
	};

	_userLogged: User;

	constructor(
		protected _translateService: TranslateService,
		protected _tdLoadingService: TdLoadingService,
		protected _toastService: ToastService,
		protected _dialogService: DialogService,
		protected _broadcastService: BroadcastService,
		protected _dashboardsService: DashboardsService,
		protected _pairingService: PairingService,
		private _notificationsService: NotificationsService,
		private _accountService: AccountService
	) {
		super(_translateService, _tdLoadingService, _toastService, _dialogService, _broadcastService, _dashboardsService, _pairingService);
	}

	protected query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._notificationsService.query(pagingSettings, rsql);
	}

	protected report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder) {
		return of(null);
	}

	protected get columnDefinitions(): Array<ColumnDefinition> {
		return [
			{
				alias: 'date',
				filter: {
					type: FilterType.DATE_RANGE
				},
				formatter: DEFAULT_DATE_TIME_FORMATER
			},
			{
				alias: 'category',
				filter: {
					type: FilterType.DICTIONARY_MULTI
				}
			},
			{
				alias: 'status',
				filter: {
					type: FilterType.DICTIONARY_MULTI
				}
			},
			{
				alias: 'description',
				filter: {
					type: FilterType.TEXT
				}
			}
		];
	}

	get controls(): Array<Control> {
		return [];
	}

	ngOnInit(): void {
		super.ngOnInit();

		this._broadcastService
			.observe(MessageType.RESOURCES_CHANGED)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((message: { type: string; title: string; message: WebsocketEvent; config: any }) => message.type === ResourceType.NOTIFICATIONS),
				tap((message) => this._toastService.open(message.title, message.message, message.config))
			)
			.subscribe();

		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}
}
