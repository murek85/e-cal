import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { GroupsResourcesService } from 'src/app/core/services/groups-resources.service';

import { WidgetGroupsResourcesComponent } from './widget-groups-resources.component';
import { AddGroupResourceDialogComponent } from './add-group-resource-dialog/add-group-resource-dialog.component';
import { ManageGroupResourceDialogComponent } from './manage-group-resource-dialog/manage-group-resource-dialog.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetGroupsResourcesComponent, AddGroupResourceDialogComponent, ManageGroupResourceDialogComponent],
	imports: [APP_MODULES],
	exports: [WidgetGroupsResourcesComponent, AddGroupResourceDialogComponent, ManageGroupResourceDialogComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: [GroupsResourcesService]
})
export class WidgetGroupsResourcesModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetGroupsResourcesComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetGroupsResourcesComponent);
	}

	static import() {
		return import('../widget-groups-resources/widget-groups-resources.module');
	}
}
