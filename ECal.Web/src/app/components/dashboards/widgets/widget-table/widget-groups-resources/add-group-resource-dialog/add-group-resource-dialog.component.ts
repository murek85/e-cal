import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { CdkStepper, StepperSelectionEvent } from '@angular/cdk/stepper';
import { iif, Subject, Observable, of, from, interval } from 'rxjs';
import { catchError, delay, filter, finalize, map, mergeMap, takeUntil, tap, timeout } from 'rxjs/operators';

import { NgxMAuthOidcService } from 'src/app/core/services/oidc/oidc.service';

import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { AddGroupResourceDialogService } from './add-group-resource-dialog.service';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';

import { FormField, Segment, Step } from 'src/app/shared/models/step.model';
import { User } from 'src/app/shared/models/user.model';
import { GroupResource } from 'src/app/shared/models/group-resource.model';
import { GroupsResourcesService } from 'src/app/core/services/groups-resources.service';

declare var $: any;

const MODALNAME = '.modal-add-group-resource';

@Component({
	selector: 'app-add-group-resource-dialog',
	templateUrl: './add-group-resource-dialog.component.html',
	providers: [GroupsResourcesService, AddGroupResourceDialogService]
})
export class AddGroupResourceDialogComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _formModeType: FormModeType;
	private _steps: Step[];
	private _loading = false;
	private _groupResource: GroupResource;
	private _userLogged: User;

	@ViewChild('form') form;

	constructor(
		private _groupResourceDialogService: AddGroupResourceDialogService,
		private _accountService: AccountService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA) public data: { formModeType: FormModeType }
	) {
		this._formModeType = data.formModeType;
	}

	get formConfig() {
		return {
			symbol: 'add-group-resource',
			broadcast: { type: ResourceType.GROUPS_RESOURCES }
		};
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get steps(): Step[] {
		return this._steps;
	}

	get loading(): boolean {
		return this._loading;
	}

	get groupuser(): GroupResource {
		return this._groupResource;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		$(MODALNAME)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initSteps();
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeStep($event): void {
		this.form.changeStep($event);
	}

	onPreviousStep($event): void {
		this.form.previousStep($event);
	}

	onNextStep($event): void {
		this.form.nextStep($event);
	}

	onBackForm(): void {
		$(MODALNAME).modal('hide');
	}

	onSaveForm(): void {
		this.form.saveForm(
			(model) => this._groupResourceDialogService.create(model),
			() => $(MODALNAME).modal('hide')
		);
	}

	private initSteps(): void {
		const steps: Step[] = [];
		steps.push(
			this.createStep('Grupa zasobów', 'Podstawowe informacje o grupie zasobów.', 'group-resource-data', [
				{
					header: 'Dane grupy zasobów',
					description: 'Uzupełnienie podstawowych informacji o grupie zasobów.',
					fields: [
						{
							label: 'Nazwa',
							name: 'name',
							symbol: 'group-resource-name',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Opis',
							name: 'description',
							symbol: 'group-resource-description',
							type: FormFieldType.TEXTAREA
						},
						{
							label: 'Symbol',
							name: 'symbol',
							symbol: 'group-user-symbol',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Nadrzędna grupa zasobów',
							name: 'resources',
							symbol: 'group-resource-resources',
							type: FormFieldType.DICTIONARY_SINGLE,
							items: [],
							api: {
								fields: {
									name: 'name',
									value: 'symbol'
								}
							}
						}
					]
				}
			])
		);
		this._steps = steps;
	}

	private createStep(label: string, description: string, symbol: string, segments: Segment[], completed: boolean = false): Step {
		const menu: Step = { label, description, symbol, segments, completed };
		return menu;
	}

	private initDialog(): void {
		switch (this._formModeType) {
			case FormModeType.ADD: {
				this._dialogConfig = {
					title: 'Nowa grupa zasobów',
					meta: 'tworzenie nowej grupy zasobów'
				};
				break;
			}
		}
	}
}
