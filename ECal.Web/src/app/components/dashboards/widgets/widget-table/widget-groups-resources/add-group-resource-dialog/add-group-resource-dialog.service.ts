import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TdLoadingService } from '@covalent/core/loading';
import { tap } from 'rxjs/operators';

import { GroupsResourcesService } from 'src/app/core/services/groups-resources.service';
import { GroupResource } from 'src/app/shared/models/group-resource.model';

@Injectable()
export class AddGroupResourceDialogService {
	constructor(private _tdLoadingService: TdLoadingService, private _groupResourcesService: GroupsResourcesService) {}

	public create(model: GroupResource) {
		return this._groupResourcesService.create(model);
	}
}
