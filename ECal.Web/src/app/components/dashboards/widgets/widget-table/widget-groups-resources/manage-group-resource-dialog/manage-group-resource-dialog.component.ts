import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { from, iif, of, Subject } from 'rxjs';
import { catchError, delay, filter, finalize, map, takeUntil, tap } from 'rxjs/operators';

import { NgxMAuthOidcService } from 'src/app/core/services/oidc/oidc.service';

import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { ManageGroupResourceDialogService } from './manage-group-resource-dialog.service';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';

import { FormField, Segment, Step } from 'src/app/shared/models/step.model';
import { User } from 'src/app/shared/models/user.model';
import { GroupResource } from 'src/app/shared/models/group-resource.model';
import { GroupsResourcesService } from 'src/app/core/services/groups-resources.service';

declare var $: any;

const MODALNAME = '.modal-manage-group-resource';

@Component({
	selector: 'app-manage-group-resource-dialog',
	templateUrl: './manage-group-resource-dialog.component.html',
	providers: [GroupsResourcesService, ManageGroupResourceDialogService]
})
export class ManageGroupResourceDialogComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _formModeType: FormModeType;
	private _steps: Array<Step>;
	private _selectedIndex = 0;
	private _groupResourceId: string;
	private _tabType: 'data' | 'permissions';
	private _groupResource: GroupResource;
	private _userLogged: User;
	private _loading = false;

	@ViewChild('form') form;

	constructor(
		private _groupResourceDialogService: ManageGroupResourceDialogService,
		private _accountService: AccountService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA) public data: { formModeType: FormModeType; groupResourceId: string; tabType: 'data' | 'permissions' }
	) {
		this._formModeType = data.formModeType;
		this._groupResourceId = data.groupResourceId;
		this._tabType = data.tabType;
	}

	get formConfig() {
		return {
			symbol: 'manage-group-resource',
			broadcast: { type: ResourceType.GROUPS_RESOURCES }
		};
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get steps(): Step[] {
		return this._steps;
	}

	get selectedIndex(): number {
		return this._selectedIndex;
	}

	get loading(): boolean {
		return this._loading;
	}

	get groupResource(): GroupResource {
		return this._groupResource;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		this._groupResourceDialogService.groupResource
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((data) => !!data),
				tap((groupResource) => (this._groupResource = groupResource)),
				tap((groupResource) => this.form.loadForm(groupResource))
			)
			.subscribe(() => (this._loading = false));

		$(MODALNAME)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initSteps();
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeTab($event): void {
		this.form.changeTab($event);
	}

	onBackForm(): void {
		$(MODALNAME).modal('hide');
	}

	onSaveForm(): void {
		this.form.saveForm(
			(model) => this._groupResourceDialogService.update({ ...model, id: this._groupResourceId }),
			() => $(MODALNAME).modal('hide')
		);
	}

	private initSteps(): void {
		const steps: Step[] = [];
		steps.push(
			this.createStep('Grupa zasobów', 'Podstawowe informacje o grupie zasobów.', 'group-resource-data', [
				{
					header: 'Dane grupy zasobów',
					description: 'Uzupełnienie podstawowych informacji o grupie zasobów.',
					fields: [
						{
							label: 'Nazwa',
							name: 'name',
							symbol: 'group-resource-name',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole '{name}' nie może być puste.`
								}
							]
						},
						{
							label: 'Opis',
							name: 'description',
							symbol: 'group-resource-description',
							type: FormFieldType.TEXTAREA
						},
						{
							label: 'Symbol',
							name: 'symbol',
							symbol: 'group-user-symbol',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Nadrzędna grupa zasobów',
							name: 'resources',
							symbol: 'group-resource-resources',
							type: FormFieldType.DICTIONARY_SINGLE,
							items: [],
							api: {
								fields: {
									name: 'name',
									value: 'symbol'
								}
							}
						}
					]
				}
			])
		);
		steps.push(
			this.createStep('Uprawnienia', '', 'group-resource-permissions', [
				{
					header: 'Uprawnienia dziedziczone',
					description: '',
					fields: []
				},
				{
					header: 'Uprawnienia indywidualne',
					description: '',
					fields: []
				}
			])
		);
		this._steps = steps;
	}

	private createStep(label: string, description: string, symbol: string, segments: Segment[], completed: boolean = false): Step {
		const menu: Step = { label, description, symbol, segments, completed };
		return menu;
	}

	private initDialog(): void {
		switch (this._formModeType) {
			case FormModeType.EDIT: {
				this._dialogConfig = {
					title: 'Edytowanie grupy zasobów',
					meta: 'edytowanie grupy zasobów'
				};

				switch (this._tabType) {
					case 'data':
						this._selectedIndex = 0;
						break;
					case 'permissions':
						this._selectedIndex = 1;
						break;

					default:
						this._selectedIndex = 0;
						break;
				}

				// pobierz dane roli w trybie edycji
				this._groupResourceDialogService.get(this._groupResourceId);
				break;
			}
		}
	}
}
