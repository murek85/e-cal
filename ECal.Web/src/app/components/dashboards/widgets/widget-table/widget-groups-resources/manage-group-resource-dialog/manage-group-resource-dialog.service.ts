import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TdLoadingService } from '@covalent/core/loading';
import { delay, finalize, tap } from 'rxjs/operators';

import { GroupsResourcesService } from 'src/app/core/services/groups-resources.service';
import { GroupResource } from 'src/app/shared/models/group-resource.model';

@Injectable()
export class ManageGroupResourceDialogService {
	private _groupResourceSource = new BehaviorSubject<GroupResource>(null);

	groupResource = this._groupResourceSource.asObservable();

	changeGroupResource(data: GroupResource) {
		this._groupResourceSource.next(data);
	}

	constructor(private _tdLoadingService: TdLoadingService, private _groupResourcesService: GroupsResourcesService) {}

	public get(id) {
		this._tdLoadingService.register('modalSyntax');
		this._groupResourcesService
			.get(id)
			.pipe(
				delay(500),
				tap((groupResource) => this.changeGroupResource(groupResource)),
				finalize(() => this._tdLoadingService.resolve('modalSyntax'))
			)
			.subscribe();
	}

	public update(model: GroupResource) {
		return this._groupResourcesService.update(model);
	}
}
