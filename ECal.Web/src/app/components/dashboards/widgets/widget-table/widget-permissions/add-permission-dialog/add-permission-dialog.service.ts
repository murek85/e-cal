import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TdLoadingService } from '@covalent/core/loading';
import { tap } from 'rxjs/operators';

import { PermissionsHttpService } from 'src/app/core/services/http/permissions-http.service';
import { Permission } from 'src/app/shared/models/permission.model';

@Injectable()
export class AddPermissionDialogService {
	constructor(private _tdLoadingService: TdLoadingService, private _permissionsHttpService: PermissionsHttpService) {}

	public create(model: Permission) {
		return this._permissionsHttpService.create(model);
	}
}
