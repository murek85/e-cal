import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { PermissionsService } from 'src/app/core/services/permissions.service';

import { WidgetPermissionsComponent } from './widget-permissions.component';
import { AddPermissionDialogComponent } from './add-permission-dialog/add-permission-dialog.component';
import { ManagePermissionDialogComponent } from './manage-permission-dialog/manage-permission-dialog.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetPermissionsComponent, AddPermissionDialogComponent, ManagePermissionDialogComponent],
	imports: [APP_MODULES],
	exports: [WidgetPermissionsComponent, AddPermissionDialogComponent, ManagePermissionDialogComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: [PermissionsService]
})
export class WidgetPermissionsModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetPermissionsComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetPermissionsComponent);
	}

	static import() {
		return import('../widget-permissions/widget-permissions.module');
	}
}
