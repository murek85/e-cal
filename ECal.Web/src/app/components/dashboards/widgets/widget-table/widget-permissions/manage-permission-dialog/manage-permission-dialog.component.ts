import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { ManagePermissionDialogService } from './manage-permission-dialog.service';
import { PermissionsService } from 'src/app/core/services/permissions.service';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';

import { Segment, Step } from 'src/app/shared/models/step.model';
import { User } from 'src/app/shared/models/user.model';
import { Permission } from 'src/app/shared/models/permission.model';

declare var $: any;

const MODALNAME = '.modal-manage-permission';

@Component({
	selector: 'app-manage-permission-dialog',
	templateUrl: './manage-permission-dialog.component.html',
	providers: [PermissionsService, ManagePermissionDialogService]
})
export class ManagePermissionDialogComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _formModeType: FormModeType;
	private _steps: Array<Step>;
	private _selectedIndex = 0;
	private _permissionId: string;
	private _tabType;
	private _permission: Permission;
	private _userLogged: User;
	private _loading = false;

	@ViewChild('form') form;

	constructor(
		private _permissionDialogService: ManagePermissionDialogService,
		private _accountService: AccountService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA) public data: { formModeType: FormModeType; permissionId: string; tabType }
	) {
		this._formModeType = data.formModeType;
		this._permissionId = data.permissionId;
		this._tabType = data.tabType;
	}

	get formConfig() {
		return {
			symbol: 'manage-permission',
			broadcast: { type: ResourceType.PERMISSIONS }
		};
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get steps(): Step[] {
		return this._steps;
	}

	get selectedIndex(): number {
		return this._selectedIndex;
	}

	get loading(): boolean {
		return this._loading;
	}

	get permission(): Permission {
		return this._permission;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		this._permissionDialogService.permission
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((data) => !!data),
				tap((permission) => (this._permission = permission)),
				tap((permission) => this.form.loadForm(permission))
			)
			.subscribe(() => (this._loading = false));

		$(MODALNAME)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initSteps();
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeTab($event): void {
		this.form.changeTab($event);
	}

	onBackForm(): void {
		$(MODALNAME).modal('hide');
	}

	onSaveForm(): void {
		this.form.saveForm(
			(model) => this._permissionDialogService.update({ ...model, id: this._permissionId }),
			() => $(MODALNAME).modal('hide')
		);
	}

	private initSteps(): void {
		const steps: Step[] = [];
		steps.push(
			this.createStep('Uprawnienie', 'Podstawowe informacje o uprawnieniu.', 'permission-data', [
				{
					header: 'Dane uprawnienia',
					description: 'Uzupełnienie podstawowych informacji o uprawnieniu.',
					fields: [
						{
							label: 'Nazwa',
							name: 'name',
							symbol: 'permission-name',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole '{name}' nie może być puste.`
								}
							]
						},
						{
							label: 'Opis',
							name: 'description',
							symbol: 'permission-description',
							type: FormFieldType.TEXTAREA
						},
						{
							label: 'Symbol',
							name: 'symbol',
							symbol: 'permission-symbol',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole '{name}' nie może być puste.`
								}
							]
						}
					]
				}
			])
		);
		this._steps = steps;
	}

	private createStep(label: string, description: string, symbol: string, segments: Segment[], completed: boolean = false): Step {
		const menu: Step = { label, description, symbol, segments, completed };
		return menu;
	}

	private initDialog(): void {
		switch (this._formModeType) {
			case FormModeType.EDIT: {
				this._dialogConfig = {
					title: 'Edytowanie uprawnienia',
					meta: 'edytowanie uprawnienia w aplikacji'
				};

				switch (this._tabType) {
					default:
						this._selectedIndex = 0;
						break;
				}

				// pobierz dane roli w trybie edycji
				this._permissionDialogService.get(this._permissionId);
				break;
			}
		}
	}
}
