import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { catchError, delay, filter, map, mergeMap, takeUntil, tap } from 'rxjs/operators';

import { WidgetTableComponent } from '../widget-table.component';

import { AccountService } from 'src/app/core/services/account.service';
import { PermissionsService } from 'src/app/core/services/permissions.service';

import { User } from 'src/app/shared/models/user.model';

import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { WebsocketEvent } from 'src/app/core/services/websocket.service';

import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';
import { Control } from '../../widget-container/widget-container.component';
import { ColumnDefinition, FilterType } from 'src/app/shared/models/column.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { AddPermissionDialogComponent } from './add-permission-dialog/add-permission-dialog.component';
import { ManagePermissionDialogComponent } from './manage-permission-dialog/manage-permission-dialog.component';
import { PairingService } from 'src/app/core/services/pairing.service';
import { RsqlBuilder } from 'src/app/core/services/rsql/rsql-builder';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';
import { ConfirmActionDialogComponent } from 'src/app/shared/components/dialogs/confirm-action-dialog/confirm-action-dialog.component';
import { of } from 'rxjs';

declare var $: any;

@Component({
	selector: 'app-widget-permissions',
	templateUrl: '../widget-table.component.html',
	styleUrls: ['../widget-table.component.scss']
})
export class WidgetPermissionsComponent extends WidgetTableComponent<Permission> implements OnInit, OnDestroy {
	config: any = {
		type: 'permissions'
	};

	_userLogged: User;

	constructor(
		protected _translateService: TranslateService,
		protected _tdLoadingService: TdLoadingService,
		protected _toastService: ToastService,
		protected _dialogService: DialogService,
		protected _broadcastService: BroadcastService,
		protected _dashboardsService: DashboardsService,
		protected _pairingService: PairingService,
		private _permissionsService: PermissionsService,
		private _accountService: AccountService
	) {
		super(_translateService, _tdLoadingService, _toastService, _dialogService, _broadcastService, _dashboardsService, _pairingService);
	}

	protected query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._permissionsService.query(pagingSettings, rsql);
	}

	protected report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder) {
		return this._permissionsService.report(pagingSettings, rsql, report);
	}

	protected get columnDefinitions(): Array<ColumnDefinition> {
		return [
			{
				alias: 'icon',
				hidden: true,
				size: 'one wide',
				formatter: (value: string) => {
					return { icon: 'key' };
				}
			},
			{
				alias: 'name',
				filter: {
					type: FilterType.TEXT
				}
			},
			{
				alias: 'description',
				filter: {
					type: FilterType.TEXT
				}
			},
			{
				alias: 'symbol',
				filter: {
					type: FilterType.TEXT
				}
			}
		];
	}

	get controls(): Array<Control> {
		return [
			{
				text: 'Utwórz',
				icon: 'add',
				showText: true,
				click: () => this.onAddPermission()
			},
			{
				text: 'Zarządzaj',
				icon: 'edit',
				showText: true,
				disabled: this.singleSelection.isEmpty(),
				click: (row: Permission) => this.onEditPermission(row)
			},
			{
				text: 'Usuń',
				icon: 'trash',
				color: 'red',
				disabled: this.singleSelection.isEmpty(),
				click: (row: Permission) => this.onDeletePermission(row)
			}
		];
	}

	ngOnInit(): void {
		super.ngOnInit();

		this._broadcastService
			.observe(MessageType.RESOURCES_CHANGED)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((message: { type: string; title: string; message: WebsocketEvent; config: any }) => message.type === ResourceType.PERMISSIONS),
				tap((message) => this._toastService.open(message.title, message.message, message.config))
			)
			.subscribe();

		this._broadcastService
			.observe(MessageType.RESOURCES_CHANGED)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((message: { type: string; title: string; message: WebsocketEvent; config: any }) => message.type === ResourceType.PERMISSIONS),
				filter((message) => message.config.class === 'success'),
				tap((_) => this.reload())
			)
			.subscribe();

		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

	private onAddPermission(): void {
		this._dialogService
			.open<AddPermissionDialogComponent>(AddPermissionDialogComponent, {
				data: { formModeType: FormModeType.ADD }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	private onEditPermission(row: Permission): void {
		this._dialogService
			.open<ManagePermissionDialogComponent>(ManagePermissionDialogComponent, {
				data: { formModeType: FormModeType.EDIT, permissionId: row.id }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	onDeletePermission(row: Permission): void {
		const broadcast = (result) =>
			this._broadcastService.broadcast({
				type: MessageType.RESOURCES_CHANGED,
				payload: {
					type: ResourceType.PERMISSIONS,
					title: result.title,
					message: result.message,
					config: result.config
				}
			});

		const success = () => ({
			title: 'Zapisano',
			message: `Usunięto.`,
			config: { class: 'success', position: 'top center', showIcon: 'check circle' }
		});

		const error = () =>
			of({
				title: 'Błąd',
				message: `Wystąpił problem.`,
				config: { class: 'error', position: 'top center', showIcon: 'exclamation circle' }
			});

		this._dialogService
			.open<ConfirmActionDialogComponent>(ConfirmActionDialogComponent, {
				data: {
					title: 'Usuwanie uprawnienia',
					description: `Czy chcesz usunąć wybrane uprawnienie '${row.name}'?`,
					iconClass: 'question circle outline',
					approveButtonLabel: 'Usuń',
					approveButtonColorClass: 'red',
					denyButtonLabel: 'Anuluj'
				}
			})
			.events.pipe(
				filter((event) => !!event),
				filter((event) => event.type === 'accept'),
				mergeMap(() => this._permissionsService.delete(row.id).pipe(delay(500), map(success), catchError(error)))
			)
			.subscribe(broadcast);
	}
}
