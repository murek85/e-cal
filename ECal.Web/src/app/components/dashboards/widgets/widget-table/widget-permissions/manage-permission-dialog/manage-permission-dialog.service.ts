import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TdLoadingService } from '@covalent/core/loading';
import { delay, finalize, tap } from 'rxjs/operators';

import { PermissionsService } from 'src/app/core/services/permissions.service';
import { Permission } from 'src/app/shared/models/permission.model';

@Injectable()
export class ManagePermissionDialogService {
	private _permissionSource = new BehaviorSubject<Permission>(null);

	permission = this._permissionSource.asObservable();

	changePermission(data: Permission) {
		this._permissionSource.next(data);
	}

	constructor(private _tdLoadingService: TdLoadingService, private _permissionsService: PermissionsService) {}

	public get(id) {
		this._tdLoadingService.register('modalSyntax');
		this._permissionsService
			.get(id)
			.pipe(
				delay(500),
				tap((permission) => this.changePermission(permission)),
				finalize(() => this._tdLoadingService.resolve('modalSyntax'))
			)
			.subscribe();
	}

	public update(model: Permission) {
		return this._permissionsService.update(model);
	}
}
