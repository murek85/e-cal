import { Component, OnInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { of, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AccountService } from 'src/app/core/services/account.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { AddPermissionDialogService } from './add-permission-dialog.service';
import { PermissionsService } from 'src/app/core/services/permissions.service';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';

import { Segment, Step } from 'src/app/shared/models/step.model';
import { User } from 'src/app/shared/models/user.model';
import { Permission } from 'src/app/shared/models/permission.model';

declare var $: any;

const MODALNAME = '.modal-add-permission';

@Component({
	selector: 'app-add-permission-dialog',
	templateUrl: './add-permission-dialog.component.html',
	providers: [PermissionsService, AddPermissionDialogService]
})
export class AddPermissionDialogComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _formModeType: FormModeType;
	private _steps: Step[];
	private _loading = false;
	private _permission: Permission;
	private _userLogged: User;

	@ViewChild('form') form;

	constructor(
		private _permissionDialogService: AddPermissionDialogService,
		private _accountService: AccountService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA) public data: { formModeType: FormModeType }
	) {
		this._formModeType = data.formModeType;
	}

	get formConfig() {
		return {
			symbol: 'add-permission',
			broadcast: { type: ResourceType.PERMISSIONS }
		};
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get steps(): Step[] {
		return this._steps;
	}

	get loading(): boolean {
		return this._loading;
	}

	get permission(): Permission {
		return this._permission;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		$(MODALNAME)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initSteps();
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeStep($event): void {
		this.form.changeStep($event);
	}

	onPreviousStep($event): void {
		this.form.previousStep($event);
	}

	onNextStep($event): void {
		this.form.nextStep($event);
	}

	onBackForm(): void {
		$(MODALNAME).modal('hide');
	}

	onSaveForm(): void {
		this.form.saveForm(
			(model) => this._permissionDialogService.create(model),
			() => $(MODALNAME).modal('hide')
		);
	}

	private initSteps(): void {
		const steps: Step[] = [];
		steps.push(
			this.createStep('Uprawnienie', 'Podstawowe informacje o uprawnieniu.', 'permission-data', [
				{
					header: 'Dane uprawnienia',
					description: 'Uzupełnienie podstawowych informacji o uprawnieniu.',
					fields: [
						{
							label: 'Identyfikator',
							name: 'id',
							symbol: 'permission-id',
							type: FormFieldType.NUMBER,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Nazwa',
							name: 'name',
							symbol: 'permission-name',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Opis',
							name: 'description',
							symbol: 'permission-description',
							type: FormFieldType.TEXTAREA
						},
						{
							label: 'Zasób',
							name: 'resource',
							symbol: 'permission-resource',
							type: FormFieldType.DICTIONARY_SINGLE,
							items: [{ name: 'Użytkownik', symbol: 'user' }],
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							],
							api: {
								fields: {
									name: 'name',
									value: 'symbol'
								}
							}
						},
						{
							label: 'Symbol',
							name: 'symbol',
							symbol: 'permission-symbol',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						}
					]
				}
			])
		);
		this._steps = steps;
	}

	private createStep(label: string, description: string, symbol: string, segments: Segment[], completed: boolean = false): Step {
		const menu: Step = { label, description, symbol, segments, completed };
		return menu;
	}

	private initDialog(): void {
		switch (this._formModeType) {
			case FormModeType.ADD: {
				this._dialogConfig = {
					title: 'Nowe uprawnienie',
					meta: 'tworzenie nowego uprawnienia w aplikacji'
				};
				break;
			}
		}
	}
}
