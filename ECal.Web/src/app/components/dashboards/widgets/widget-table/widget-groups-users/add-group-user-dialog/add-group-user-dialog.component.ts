import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { CdkStepper, StepperSelectionEvent } from '@angular/cdk/stepper';
import { iif, Subject, Observable, of, from, interval } from 'rxjs';
import { catchError, delay, filter, finalize, map, mergeMap, takeUntil, tap, timeout } from 'rxjs/operators';

import { NgxMAuthOidcService } from 'src/app/core/services/oidc/oidc.service';

import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { AddGroupUserDialogService } from './add-group-user-dialog.service';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';

import { FormField, Segment, Step } from 'src/app/shared/models/step.model';
import { User } from 'src/app/shared/models/user.model';
import { GroupUser } from 'src/app/shared/models/group-user.model';
import { GroupsUsersService } from 'src/app/core/services/groups-users.service';

declare var $: any;

const MODALNAME = '.modal-add-group-user';

@Component({
	selector: 'app-add-group-user-dialog',
	templateUrl: './add-group-user-dialog.component.html',
	providers: [GroupsUsersService, AddGroupUserDialogService]
})
export class AddGroupUserDialogComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _formModeType: FormModeType;
	private _steps: Step[];
	private _loading = false;
	private _groupUser: GroupUser;
	private _userLogged: User;

	@ViewChild('form') form;

	constructor(
		private _groupUserDialogService: AddGroupUserDialogService,
		private _accountService: AccountService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA) public data: { formModeType: FormModeType }
	) {
		this._formModeType = data.formModeType;
	}

	get formConfig() {
		return {
			symbol: 'add-group-user',
			broadcast: { type: ResourceType.GROUPS_USERS }
		};
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get steps(): Step[] {
		return this._steps;
	}

	get loading(): boolean {
		return this._loading;
	}

	get groupUser(): GroupUser {
		return this._groupUser;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		$(MODALNAME)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initSteps();
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeStep($event): void {
		this.form.changeStep($event);
	}

	onPreviousStep($event): void {
		this.form.previousStep($event);
	}

	onNextStep($event): void {
		this.form.nextStep($event);
	}

	onBackForm(): void {
		$(MODALNAME).modal('hide');
	}

	onSaveForm(): void {
		this.form.saveForm(
			(model) => this._groupUserDialogService.create(model),
			() => $(MODALNAME).modal('hide')
		);
	}

	private initSteps(): void {
		const steps: Step[] = [];
		steps.push(
			this.createStep('Grupa użytkowników', 'Podstawowe informacje o grupie użytkowników.', 'group-user-data', [
				{
					header: 'Dane grupy użytkowników',
					description: 'Uzupełnienie podstawowych informacji o grupie użytkowników.',
					fields: [
						{
							label: 'Nazwa',
							name: 'name',
							symbol: 'group-user-name',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Opis',
							name: 'description',
							symbol: 'group-user-description',
							type: FormFieldType.TEXTAREA
						},
						{
							label: 'Symbol',
							name: 'symbol',
							symbol: 'group-user-symbol',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						}
					]
				}
			])
		);
		steps.push(
			this.createStep('Przypisanie użytkowników', 'Lista dostępnych użytkowników w systemie', 'group-user-users', [
				{
					header: 'Lista użytkowników',
					description: 'Przypisanie użytkownika do nowej grupy użytkowników',
					fields: [
						{
							label: 'Dostępni użytkownicy',
							name: 'users',
							symbol: 'user-users',
							type: FormFieldType.DICTIONARY_MULTI,
							items: [],
							api: {
								fields: {
									name: 'name',
									value: 'symbol'
								}
							}
						}
					]
				}
			])
		);
		this._steps = steps;
	}

	private createStep(label: string, description: string, symbol: string, segments: Segment[], completed: boolean = false): Step {
		const menu: Step = { label, description, symbol, segments, completed };
		return menu;
	}

	private initDialog(): void {
		switch (this._formModeType) {
			case FormModeType.ADD: {
				this._dialogConfig = {
					title: 'Nowa grupa użytkowników',
					meta: 'tworzenie nowej grupy użytkowników'
				};
				break;
			}
		}
	}
}
