import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TdLoadingService } from '@covalent/core/loading';
import { tap } from 'rxjs/operators';

import { GroupsUsersService } from 'src/app/core/services/groups-users.service';
import { GroupUser } from 'src/app/shared/models/group-user.model';

@Injectable()
export class AddGroupUserDialogService {
	constructor(private _tdLoadingService: TdLoadingService, private _groupUsersService: GroupsUsersService) {}

	public create(model: GroupUser) {
		return this._groupUsersService.create(model);
	}
}
