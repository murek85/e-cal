import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { GroupsUsersService } from 'src/app/core/services/groups-users.service';

import { WidgetGroupsUsersComponent } from './widget-groups-users.component';
import { AddGroupUserDialogComponent } from './add-group-user-dialog/add-group-user-dialog.component';
import { ManageGroupUserDialogComponent } from './manage-group-user-dialog/manage-group-user-dialog.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetGroupsUsersComponent, AddGroupUserDialogComponent, ManageGroupUserDialogComponent],
	imports: [APP_MODULES],
	exports: [WidgetGroupsUsersComponent, AddGroupUserDialogComponent, ManageGroupUserDialogComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: [GroupsUsersService]
})
export class WidgetGroupsUsersModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetGroupsUsersComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetGroupsUsersComponent);
	}

	static import() {
		return import('../widget-groups-users/widget-groups-users.module');
	}
}
