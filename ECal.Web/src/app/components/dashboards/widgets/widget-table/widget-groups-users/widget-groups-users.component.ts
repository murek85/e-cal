import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { of } from 'rxjs';
import { catchError, delay, filter, map, mergeMap, takeUntil, tap } from 'rxjs/operators';

import { WidgetTableComponent } from '../widget-table.component';

import { AccountService } from 'src/app/core/services/account.service';
import { GroupsUsersService } from 'src/app/core/services/groups-users.service';

import { User } from 'src/app/shared/models/user.model';

import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { WebsocketEvent } from 'src/app/core/services/websocket.service';

import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';

import { PagingSettings } from 'src/app/shared/models/paging.model';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';
import { Control } from '../../widget-container/widget-container.component';
import { ColumnDefinition, FilterType } from 'src/app/shared/models/column.model';
import { GroupUser } from 'src/app/shared/models/group-user.model';
import { AddGroupUserDialogComponent } from './add-group-user-dialog/add-group-user-dialog.component';
import { ManageGroupUserDialogComponent } from './manage-group-user-dialog/manage-group-user-dialog.component';
import { PairingService } from 'src/app/core/services/pairing.service';
import { RsqlBuilder } from 'src/app/core/services/rsql/rsql-builder';
import { ReportBuilder } from 'src/app/shared/models/report-builder.model';
import { ConfirmActionDialogComponent } from 'src/app/shared/components/dialogs/confirm-action-dialog/confirm-action-dialog.component';

declare var $: any;

@Component({
	selector: 'app-widget-groups-users',
	templateUrl: '../widget-table.component.html',
	styleUrls: ['../widget-table.component.scss']
})
export class WidgetGroupsUsersComponent extends WidgetTableComponent<GroupUser> implements OnInit, OnDestroy {
	config: any = {
		type: 'groups-users'
	};

	_userLogged: User;

	constructor(
		protected _translateService: TranslateService,
		protected _tdLoadingService: TdLoadingService,
		protected _toastService: ToastService,
		protected _dialogService: DialogService,
		protected _broadcastService: BroadcastService,
		protected _dashboardsService: DashboardsService,
		protected _pairingService: PairingService,
		private _groupsUsersService: GroupsUsersService,
		private _accountService: AccountService
	) {
		super(_translateService, _tdLoadingService, _toastService, _dialogService, _broadcastService, _dashboardsService, _pairingService);
	}

	protected query(pagingSettings: PagingSettings, rsql: RsqlBuilder) {
		return this._groupsUsersService.query(pagingSettings, rsql);
	}

	protected report(pagingSettings: PagingSettings, rsql: RsqlBuilder, report: ReportBuilder) {
		return of(null);
	}

	protected get columnDefinitions(): Array<ColumnDefinition> {
		return [
			{
				alias: 'icon',
				hidden: true,
				size: 'one wide',
				formatter: (value: string) => {
					return { icon: 'object group' };
				}
			},
			{
				alias: 'name',
				filter: {
					type: FilterType.TEXT
				}
			},
			{
				alias: 'description',
				filter: {
					type: FilterType.TEXT
				}
			},
			{
				alias: 'symbol',
				filter: {
					type: FilterType.TEXT
				}
			}
		];
	}

	get controls(): Array<Control> {
		return [
			{
				text: 'Utwórz',
				icon: 'add',
				showText: true,
				click: () => this.onAddGroup()
			},
			{
				text: 'Zarządzaj',
				icon: 'edit',
				showText: true,
				disabled: this.singleSelection.isEmpty(),
				click: (row: GroupUser) => this.onEditGroup(row)
			},
			{
				text: 'Uprawnienia',
				icon: 'key',
				disabled: this.singleSelection.isEmpty(),
				click: (row: GroupUser) => this.onPermissionsGroup(row)
			},
			{
				text: 'Usuń',
				icon: 'trash',
				color: 'red',
				disabled: this.singleSelection.isEmpty(),
				click: (row: GroupUser) => this.onDeleteGroup(row)
			}
		];
	}

	ngOnInit(): void {
		super.ngOnInit();

		this._broadcastService
			.observe(MessageType.RESOURCES_CHANGED)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((message: { type: string; title: string; message: WebsocketEvent; config: any }) => message.type === ResourceType.GROUPS_USERS),
				tap((message) => this._toastService.open(message.title, message.message, message.config))
			)
			.subscribe();

		this._broadcastService
			.observe(MessageType.RESOURCES_CHANGED)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((message: { type: string; title: string; message: WebsocketEvent; config: any }) => message.type === ResourceType.GROUPS_USERS),
				filter((message) => message.config.class === 'success'),
				tap((_) => this.reload())
			)
			.subscribe();

		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

	private onAddGroup(): void {
		this._dialogService
			.open<AddGroupUserDialogComponent>(AddGroupUserDialogComponent, {
				data: { formModeType: FormModeType.ADD }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	private onEditGroup(row: GroupUser): void {
		this._dialogService
			.open<ManageGroupUserDialogComponent>(ManageGroupUserDialogComponent, {
				data: { formModeType: FormModeType.EDIT, groupUserId: row.id }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	private onPermissionsGroup(row: GroupUser): void {
		this._dialogService
			.open<ManageGroupUserDialogComponent>(ManageGroupUserDialogComponent, {
				data: { formModeType: FormModeType.EDIT, groupUserId: row.id, tabType: 'permissions' }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	private onDeleteGroup(row: GroupUser): void {
		const broadcast = (result) =>
			this._broadcastService.broadcast({
				type: MessageType.RESOURCES_CHANGED,
				payload: {
					type: ResourceType.GROUPS_USERS,
					title: result.title,
					message: result.message,
					config: result.config
				}
			});

		const success = () => ({
			title: 'Zapisano',
			message: `Usunięto.`,
			config: { class: 'success', position: 'top center', showIcon: 'check circle' }
		});

		const error = () =>
			of({
				title: 'Błąd',
				message: `Wystąpił problem.`,
				config: { class: 'error', position: 'top center', showIcon: 'exclamation circle' }
			});

		this._dialogService
			.open<ConfirmActionDialogComponent>(ConfirmActionDialogComponent, {
				data: {
					title: 'Usuwanie grupy uprawnień',
					description: `Czy chcesz usunąć wybraną grupę uprawnień '${row.name}'?`,
					iconClass: 'question circle outline',
					approveButtonLabel: 'Usuń',
					approveButtonColorClass: 'red',
					denyButtonLabel: 'Anuluj'
				}
			})
			.events.pipe(
				filter((event) => !!event),
				filter((event) => event.type === 'accept'),
				mergeMap(() => this._groupsUsersService.delete(row.id).pipe(delay(500), map(success), catchError(error)))
			)
			.subscribe(broadcast);
	}
}
