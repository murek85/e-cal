import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TdLoadingService } from '@covalent/core/loading';
import { delay, finalize, tap } from 'rxjs/operators';

import { GroupsUsersService } from 'src/app/core/services/groups-users.service';
import { GroupUser } from 'src/app/shared/models/group-user.model';

@Injectable()
export class ManageGroupUserDialogService {
	private _groupUserSource = new BehaviorSubject<GroupUser>(null);

	groupUser = this._groupUserSource.asObservable();

	changeGroupUser(data: GroupUser) {
		this._groupUserSource.next(data);
	}

	constructor(private _tdLoadingService: TdLoadingService, private _groupUsersService: GroupsUsersService) {}

	public get(id) {
		this._tdLoadingService.register('modalSyntax');
		this._groupUsersService
			.get(id)
			.pipe(
				delay(500),
				tap((groupUser) => this.changeGroupUser(groupUser)),
				finalize(() => this._tdLoadingService.resolve('modalSyntax'))
			)
			.subscribe();
	}

	public update(model: GroupUser) {
		return this._groupUsersService.update(model);
	}
}
