import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { WidgetMonitorDiskComponent } from './widget-monitor-disk.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetMonitorDiskComponent],
	imports: [APP_MODULES],
	exports: [WidgetMonitorDiskComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: []
})
export class WidgetMonitorDiskModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetMonitorDiskComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetMonitorDiskComponent);
	}

	static import() {
		return import('../widget-monitor-disk/widget-monitor-disk.module');
	}
}
