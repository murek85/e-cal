import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { WidgetStatisticsComponent } from './widget-statistics.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetStatisticsComponent],
	imports: [APP_MODULES],
	exports: [WidgetStatisticsComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: []
})
export class WidgetStatisticsModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetStatisticsComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetStatisticsComponent);
	}

	static import() {
		return import('../widget-statistics/widget-statistics.module');
	}
}
