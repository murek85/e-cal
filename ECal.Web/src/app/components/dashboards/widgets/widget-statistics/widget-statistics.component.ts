import { Component, OnDestroy, OnInit } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { from } from 'rxjs';
import { filter, map, takeUntil, tap } from 'rxjs/operators';

import * as echarts from 'echarts';

import { BroadcastService } from 'src/app/core/services/broadcast.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { HealthMetricsService } from 'src/app/core/services/health-metrics.service';
import { ToastService } from 'src/app/core/services/toast.service';
import { WidgetOptions } from 'src/app/shared/models/widget.model';
import { ManageWidgetDialogComponent } from '../../dashboard/manage-widget-dialog/manage-widget-dialog.component';
import { WidgetComponent } from '../widget.component';
import { ResizedEvent } from 'angular-resize-event';
import { PairingService } from 'src/app/core/services/pairing.service';
import { TranslateService } from '@ngx-translate/core';

const tooltip = {
	trigger: 'axis',
	triggerOn: 'mousemove|click',
	showDelay: 0,
	hideDelay: 100,
	axisPointer: { animation: true },
	appendToBody: true
};

const xAxis = {
	type: 'time',
	show: false,
	boundaryGap: false,
	axisLine: { show: false },
	axisTick: { show: false }
};

const yAxis = {
	type: 'value',
	show: false,
	boundaryGap: false,
	min: 0,
	max: 100,
	axisLine: { show: false },
	axisTick: { show: false }
};

const grid = {
	top: 10,
	left: 0,
	right: 0,
	bottom: 0
	// height: 90
};

const title = (text) => ({
	text,
	textStyle: { fontSize: 12 },
	padding: 0
});

const series = (name, data) => [
	{
		name,
		type: 'line',
		showSymbol: false,
		smooth: true,
		hoverAnimation: true,
		itemStyle: { color: 'rgba(97,97,97,1)' },
		areaStyle: {
			// color: 'rgba(97,97,97,.1)'
			// color: new (<any>echarts).graphic.LinearGradient(0, 0, 0, 1, [{
			//     offset: 0,
			//     color: 'rgba(97,97,97,1)'
			// }, {
			//     offset: 1,
			//     color: 'rgba(97,97,97,.1)'
			// }])
			shadowBlur: 0,
			opacity: 0.25
		},
		data
	}
];

@Component({
	selector: 'app-widget-statistics',
	templateUrl: './widget-statistics.component.html',
	styleUrls: ['./widget-statistics.component.scss'],
	providers: [HealthMetricsService]
})
export class WidgetStatisticsComponent extends WidgetComponent implements OnInit, OnDestroy {
	private _chart: { symbol; options; update?; instance? };
	private _timer: any;

	type: 'memory' | 'processor';

	constructor(
		protected _translateService: TranslateService,
		protected _tdLoadingService: TdLoadingService,
		protected _toastService: ToastService,
		protected _dialogService: DialogService,
		protected _broadcastService: BroadcastService,
		protected _dashboardsService: DashboardsService,
		protected _pairingService: PairingService,
		private _healthMetricsService: HealthMetricsService
	) {
		super(_translateService, _tdLoadingService, _toastService, _dialogService, _broadcastService, _dashboardsService, _pairingService);
	}

	get chart(): { symbol; options; update?; instance? } {
		return this._chart;
	}

	private initChart = (symbol, name, data) => ({
		symbol,
		options: {
			visualMap: [
				{
					type: 'piecewise',
					min: 0,
					max: 100,
					dimension: 1,
					seriesIndex: 0,
					pieces: [
						{ min: 0, max: 59 },
						{ min: 60, max: 89 },
						{ min: 90, max: 100 }
					],
					color: ['#ef5350', '#ffa726', '#9ccc65'],
					show: false
				}
			],
			title: title(name),
			tooltip,
			xAxis,
			yAxis,
			grid,
			series: series(name, data)
		}
	});

	private initCharts = (data) => {
		this._chart = this.initChart(this.widget.data, `0%`, data);
	};

	private reinitChart = (symbol, entry) => {
		if (this._chart.symbol === symbol) {
			this._chart.options.series
				.map((serie) => (serie.data = this.prepareUpdateData(serie.data, entry.value.percentUsed)))
				.map((data) => (this._chart.update = { series: [{ symbol, data }], title: { text: `${entry.value.percentUsed}%` } }));
		}
		// from(this._charts)
		// 	.pipe(
		// 		filter((chart) => chart.symbol === symbol),
		// 		map((chart) =>
		// 			chart.options.series
		// 				.map((serie) => (serie.data = this.prepareUpdateData(serie.data, entry.value.percentUsed)))
		// 				.map((data) => (chart.update = { series: [{ symbol, data }] }))
		// 		)
		// 	)
		// 	.subscribe();
	};

	private reinitCharts = (entries) => entries.forEach((entry) => entry.value.forEach((value) => this.reinitChart(entry.key, value)));

	private prepareInitData(value): {} {
		return [this.generateData(value)];
	}

	private prepareUpdateData(data, value): Array<{}> {
		if (data.length > 60) {
			data.shift();
		}

		return [...data, this.generateData(value)];
	}

	private generateData(value): { name; value } {
		const now = new Date();
		return {
			name: now.toString(),
			value: [[now.getTime()], Math.round(value)]
		};
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.onWidgetSet();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
		clearInterval(this._timer);
	}

	protected onWidgetEdit(): void {
		this.onEdit();
	}

	protected onWidgetSet(): void {
		this._tdLoadingService.register(`overlay_${this.widget.settings.type}_${this.widget.id}`);
		this.initCharts(this.prepareInitData(0));

		this._healthMetricsService.healthMetrics
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((metrics) => !!metrics),
				tap((metrics) => (this.reinitCharts(metrics.entries), this._tdLoadingService.resolve(`overlay_${this.widget.settings.type}_${this.widget.id}`)))
			)
			.subscribe();

		this._healthMetricsService.getHealthMetrics();
		this._timer = setInterval(() => this._healthMetricsService.getHealthMetrics(), 10000);
	}

	onEdit(options?: WidgetOptions): void {
		this._dialogService
			.open<ManageWidgetDialogComponent>(ManageWidgetDialogComponent, {
				data: {
					widget: this.widget,
					options,
					monitor: { statistic: true }
				}
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	onChartInit(chart: { symbol; options; update?; instance? }, instance: any): void {
		chart.instance = instance;
	}

	onResized(event: ResizedEvent): void {
		if (!!this._chart.instance) {
			this._chart.instance.resize();
		}
		// from(this._charts)
		// 	.pipe(
		// 		filter((chart) => !!chart.instance),
		// 		tap((chart) => chart.instance.resize())
		// 	)
		// 	.subscribe();
	}
}
