import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { WidgetImplementationComponent } from './widget-implementation.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetImplementationComponent],
	imports: [APP_MODULES],
	exports: [WidgetImplementationComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WidgetImplementationModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetImplementationComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetImplementationComponent);
	}

	static import() {
		return import('../widget-implementation/widget-implementation.module');
	}
}
