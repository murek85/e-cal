import { Component, OnDestroy, OnInit } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { filter } from 'rxjs/operators';
import { BroadcastService } from 'src/app/core/services/broadcast.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { PairingService } from 'src/app/core/services/pairing.service';
import { ToastService } from 'src/app/core/services/toast.service';
import { WidgetOptions } from 'src/app/shared/models/widget.model';
import { ManageWidgetDialogComponent } from '../../dashboard/manage-widget-dialog/manage-widget-dialog.component';
import { WidgetComponent } from '../widget.component';

@Component({
	selector: 'app-widget-implementation',
	template: `
		<div class="ui aligned grid" style="margin: 0; padding: 0; align-items: center; height: 100%; border-radius: 0 0 10px 10px">
			<div class="center aligned column row" style="padding: .5em;">
				<div class="column">
					<h3 class="ui icon header">
						<i class="tools icon"></i>
						<div class="content">
							<span translate>{{ widget.settings.header }}</span>
							<div class="sub header" translate>W trakcie implementacji...</div>
						</div>
					</h3>
					<div class="size-height-24"></div>
				</div>
			</div>
		</div>
	`,
	styles: []
})
export class WidgetImplementationComponent extends WidgetComponent implements OnInit, OnDestroy {
	constructor(
		protected _translateService: TranslateService,
		protected _tdLoadingService: TdLoadingService,
		protected _toastService: ToastService,
		protected _dialogService: DialogService,
		protected _broadcastService: BroadcastService,
		protected _dashboardsService: DashboardsService,
		protected _pairingService: PairingService
	) {
		super(_translateService, _tdLoadingService, _toastService, _dialogService, _broadcastService, _dashboardsService, _pairingService);
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.onWidgetSet();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

	protected onWidgetEdit(): void {
		this.onEdit();
	}

	protected onWidgetSet(): void {}

	onEdit(options?: WidgetOptions): void {
		this._dialogService
			.open<ManageWidgetDialogComponent>(ManageWidgetDialogComponent, {
				data: {
					widget: this.widget
				}
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}
}
