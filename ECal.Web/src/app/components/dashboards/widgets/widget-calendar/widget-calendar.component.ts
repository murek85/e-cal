import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TdLoadingService } from '@covalent/core/loading';
import { filter } from 'rxjs/operators';

import { BroadcastService } from 'src/app/core/services/broadcast.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';
import { PairingService } from 'src/app/core/services/pairing.service';

import { ManageWidgetDialogComponent } from '../../dashboard/manage-widget-dialog/manage-widget-dialog.component';
import { WidgetOptions } from 'src/app/shared/models/widget.model';
import { WidgetComponent } from '../widget.component';

import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView, DAYS_OF_WEEK, CalendarDateFormatter } from 'angular-calendar';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';

import { CustomDateFormatter } from './custom-date-formatter.provider';

import 'moment/locale/pl';

@Component({
	selector: 'app-widget-calendar',
	templateUrl: './widget-calendar.component.html',
	styleUrls: ['./widget-calendar.component.scss'],
	providers: [
		{
			provide: CalendarDateFormatter,
			useClass: CustomDateFormatter
		}
	]
})
export class WidgetCalendarComponent extends WidgetComponent implements OnInit, OnDestroy {
	date: Date = new Date();

	view: CalendarView = CalendarView.Month;
	calendarView: typeof CalendarView = CalendarView;
	viewDate: Date = new Date();
	locale = 'pl';
	actions: CalendarEventAction[] = [];
	//   refresh: Subject<any> = new Subject();
	events: CalendarEvent[] = [];
	activeDayIsOpen = false;

	constructor(
		protected _translateService: TranslateService,
		protected _tdLoadingService: TdLoadingService,
		protected _toastService: ToastService,
		protected _dialogService: DialogService,
		protected _broadcastService: BroadcastService,
		protected _dashboardsService: DashboardsService,
		protected _pairingService: PairingService
	) {
		super(_translateService, _tdLoadingService, _toastService, _dialogService, _broadcastService, _dashboardsService, _pairingService);
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.onWidgetSet();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

	protected onWidgetEdit(): void {
		this.onEdit();
	}

	protected onWidgetSet(): void {}

	onEdit(options?: WidgetOptions): void {
		this._dialogService
			.open<ManageWidgetDialogComponent>(ManageWidgetDialogComponent, {
				data: {
					widget: this.widget,
					monitor: { disk: true }
				}
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	public dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
		if (isSameMonth(date, this.viewDate)) {
			if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
				this.activeDayIsOpen = false;
			} else {
				this.activeDayIsOpen = true;
			}
			this.viewDate = date;
		}
	}

	public eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
		this.events = this.events.map((iEvent) => {
			if (iEvent === event) {
				return {
					...event,
					start: newStart,
					end: newEnd
				};
			}
			return iEvent;
		});
		this.handleEvent('Dropped or resized', event);
	}

	public handleEvent(action: string, event: CalendarEvent): void {}
}
