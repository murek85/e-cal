import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { WidgetCalendarComponent } from './widget-calendar.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetCalendarComponent],
	imports: [APP_MODULES],
	exports: [WidgetCalendarComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: []
})
export class WidgetCalendarModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetCalendarComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetCalendarComponent);
	}

	static import() {
		return import('../widget-calendar/widget-calendar.module');
	}
}
