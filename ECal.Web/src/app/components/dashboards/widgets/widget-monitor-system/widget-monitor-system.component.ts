import { Component, OnDestroy, OnInit } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { filter, finalize, takeUntil, tap } from 'rxjs/operators';

import { BroadcastService } from 'src/app/core/services/broadcast.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { HealthMetricsService } from 'src/app/core/services/health-metrics.service';
import { PairingService } from 'src/app/core/services/pairing.service';
import { ToastService } from 'src/app/core/services/toast.service';
import { Gauge } from 'src/app/shared/models/gauge.model';
import { WidgetOptions } from 'src/app/shared/models/widget.model';
import { ManageWidgetDialogComponent } from '../../dashboard/manage-widget-dialog/manage-widget-dialog.component';
import { WidgetComponent } from '../widget.component';

const initGauge = {
	min: 0,
	options: {
		type: 'full',
		cap: 'flat',
		size: 112,
		thick: 3,
		foregroundColor: 'rgba(97,97,97,1)',
		backgroundColor: 'rgba(97,97,97,.1)',
		showExtremum: false
	},
	thresholds: {
		'0': { color: '#9ccc65' },
		'60': { color: '#ffa726' },
		'80': { color: '#ef5350' }
	}
};

@Component({
	selector: 'app-widget-monitor-system',
	templateUrl: './widget-monitor-system.component.html',
	styleUrls: ['./widget-monitor-system.component.scss'],
	providers: [HealthMetricsService]
})
export class WidgetMonitorSystemComponent extends WidgetComponent implements OnInit, OnDestroy {
	private _gauges: Array<Gauge>;
	private _timer: any;

	constructor(
		protected _translateService: TranslateService,
		protected _tdLoadingService: TdLoadingService,
		protected _toastService: ToastService,
		protected _dialogService: DialogService,
		protected _broadcastService: BroadcastService,
		protected _dashboardsService: DashboardsService,
		protected _pairingService: PairingService,
		private _healthMetricsService: HealthMetricsService
	) {
		super(_translateService, _tdLoadingService, _toastService, _dialogService, _broadcastService, _dashboardsService, _pairingService);
		this._gauges = [];
	}

	get gauges(): Array<Gauge> {
		return this._gauges;
	}

	private formatValue = (value, type: 'GB' | 'MB' | '%', fixed = 1) => {
		switch (type) {
			case 'GB':
				return parseFloat((value / 1024).toFixed(fixed));
			case 'MB':
				return parseFloat(value.toFixed(fixed));
			case '%':
				return value;

			default:
				return value;
		}
	};

	private initGauge = (symbol, name, entry, type: 'GB' | 'MB' | '%'): Gauge => ({
		...initGauge,
		symbol,
		max: 100,
		value: entry.percentUsed,
		label: name,
		append: '%',
		data: [
			{ key: 'dostępna', value: this.formatValue(entry.total, type), type },
			{ key: 'wolna', value: this.formatValue(entry.free, type), type },
			{ key: 'w użyciu', value: this.formatValue(entry.used, type), type }
		]
	});

	private initGauges = (entries) => {
		this._gauges.push(this.initGauge('memory', 'RAM', entries.memory, 'GB'));
		this._gauges.push(this.initGauge('cpu', 'CPU', entries.processor, '%'));
	};

	private reinitGauge = (symbol, entry, type: 'GB' | 'MB' | '%') => {
		this._gauges
			.filter((gauge) => gauge.symbol === symbol)
			.map(
				(gauge) => (
					(gauge.name = entry.value.name),
					(gauge.value = entry.value.percentUsed),
					(gauge.data = [
						{ key: 'dostępna', value: this.formatValue(entry.value.total, type), type },
						{ key: 'wolna', value: this.formatValue(entry.value.free, type), type },
						{ key: 'w użyciu', value: this.formatValue(entry.value.used, type), type }
					])
				)
			);
	};

	private reinitGauges = (entries) => {
		entries.forEach((entry) => {
			switch (entry.key) {
				case 'memory':
					entry.value.forEach((value) => this.reinitGauge(value.key, value, 'GB'));
					break;
				case 'processor':
					entry.value.forEach((value) => this.reinitGauge(value.key, value, '%'));
					break;
			}
		});
	};

	ngOnInit(): void {
		super.ngOnInit();
		this.onWidgetSet();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
		clearInterval(this._timer);
	}

	protected onWidgetEdit(): void {
		this.onEdit();
	}

	protected onWidgetSet(): void {
		this._tdLoadingService.register(`overlay_${this.widget.settings.type}_${this.widget.id}`);
		this.initGauges({
			memory: { total: 0, free: 0, used: 0, percentUsed: 0 },
			processor: { used: 0, percentUsed: 0 }
		});

		this._healthMetricsService.healthMetrics
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((metrics) => !!metrics),
				tap((metrics) => (this.reinitGauges(metrics.entries), this._tdLoadingService.resolve(`overlay_${this.widget.settings.type}_${this.widget.id}`)))
			)
			.subscribe();

		this._healthMetricsService.getHealthMetrics();
		this._timer = setInterval(() => this._healthMetricsService.getHealthMetrics(), 10000);
	}

	onEdit(options?: WidgetOptions): void {
		this._dialogService
			.open<ManageWidgetDialogComponent>(ManageWidgetDialogComponent, {
				data: {
					widget: this.widget,
					monitor: { system: true }
				}
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}
}
