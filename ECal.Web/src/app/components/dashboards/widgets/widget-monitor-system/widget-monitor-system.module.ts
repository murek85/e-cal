import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { WidgetMonitorSystemComponent } from './widget-monitor-system.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetMonitorSystemComponent],
	imports: [APP_MODULES],
	exports: [WidgetMonitorSystemComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: []
})
export class WidgetMonitorSystemModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetMonitorSystemComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetMonitorSystemComponent);
	}

	static import() {
		return import('../widget-monitor-system/widget-monitor-system.module');
	}
}
