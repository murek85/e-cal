import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { WidgetGuacamoleComponent } from './widget-guacamole.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetGuacamoleComponent],
	imports: [APP_MODULES],
	exports: [WidgetGuacamoleComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: []
})
export class WidgetGuacamoleModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetGuacamoleComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetGuacamoleComponent);
	}

	static import() {
		return import('../widget-guacamole/widget-guacamole.module');
	}
}
