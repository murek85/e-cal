import { Widget } from './../../../../shared/models/widget.model';
import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, ElementRef, AfterContentInit, AfterViewInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TdLoadingService } from '@covalent/core/loading';
import { WebSocketTunnel } from '@illgrenoble/guacamole-common-js';
import { FileSaverService } from 'ngx-filesaver';
import { filter, tap } from 'rxjs/operators';
import { AppConfig } from 'src/app/app.config';

import { BroadcastService } from 'src/app/core/services/broadcast.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';
import { NgxMRemoteDesktopService } from 'ngx-mremote-desktop';

import { WidgetOptions } from 'src/app/shared/models/widget.model';

import { ManageWidgetDialogComponent } from '../../dashboard/manage-widget-dialog/manage-widget-dialog.component';
import { WidgetComponent } from '../widget.component';

import { MenuItem, Control } from './../widget-container/widget-container.component';

import { ConfirmActionDialogComponent } from 'src/app/shared/components/dialogs/confirm-action-dialog/confirm-action-dialog.component';
import { PairingService } from 'src/app/core/services/pairing.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-widget-guacamole',
	templateUrl: './widget-guacamole.component.html',
	styleUrls: ['./widget-guacamole.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class WidgetGuacamoleComponent extends WidgetComponent implements OnInit, AfterContentInit, AfterViewInit, OnDestroy {
	service: NgxMRemoteDesktopService;

	private _timer: any;
	private readonly _widgetMenuItems: Array<MenuItem> = [
		{
			text: 'Parowanie',
			icon: 'handshake',
			click: () => this.onPair()
		},
		{
			text: 'Zmień położenie',
			icon: 'vector square',
			click: () => this.onMove()
		},
		{
			text: 'Ustawienia',
			icon: 'wrench',
			click: () => this.onEdit()
		},
		{
			text: 'Usuń komponent',
			icon: 'trash alternate red',
			click: () => this.onDelete()
		}
	];

	get widgetMenuItems(): any {
		return this._widgetMenuItems;
	}

	get controls(): Array<Control> {
		return [
			{
				text: 'Screenshot',
				icon: 'image outline',
				disabled: !this.service.isConnected(),
				click: () => this.screenshot()
			},
			{
				text: 'Fullscreen',
				icon: 'compress',
				disabled: !this.service.isConnected(),
				click: () => this.enterFullScreen()
			},
			{
				text: 'Rozłącz',
				icon: 'close',
				disabled: !this.service.isConnected(),
				click: () => this.disconnect()
			}
		];
	}

	constructor(
		protected _translateService: TranslateService,
		protected _tdLoadingService: TdLoadingService,
		protected _toastService: ToastService,
		protected _dialogService: DialogService,
		protected _broadcastService: BroadcastService,
		protected _dashboardsService: DashboardsService,
		protected _pairingService: PairingService,
		private _fileSaverService: FileSaverService,
		private _snackBar: MatSnackBar
	) {
		super(_translateService, _tdLoadingService, _toastService, _dialogService, _broadcastService, _dashboardsService, _pairingService);
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.onWidgetSet();
	}

	ngAfterContentInit(): void {}

	ngAfterViewInit(): void {}

	ngOnDestroy(): void {
		super.ngOnDestroy();
		clearInterval(this._timer);
	}

	protected onWidgetEdit(): void {
		this.onEdit();
	}

	protected onWidgetSet(): void {
		// Setup tunnel. The tunnel can be either: WebsocketTunnel, HTTPTunnel or ChainedTunnel
		const tunnel = new WebSocketTunnel(AppConfig.settings.system.guacamoleUrl);
		this.service = new NgxMRemoteDesktopService(tunnel);
		this.service.onRemoteClipboardData.subscribe((text) => {
			const snackbar = this._snackBar.open('Received from remote clipboard', 'OPEN CLIPBOARD', {
				duration: 1500
			});
			snackbar.onAction().subscribe(this.clipboard);
		});
		this.service.onReconnect.subscribe(this.connect);

		this._timer = setTimeout(() => this.connect(), 1000);
	}

	screenshot(): void {
		this.service.createScreenshot((blob) => {
			if (blob) {
				this._fileSaverService.save(blob, `screenshot.png`);
			}
		});
	}

	disconnect(): void {
		this.service.getClient().disconnect();
	}

	enterFullScreen(): void {
		this.service.setFullScreen(true);
	}

	exitFullScreen(): void {
		this.service.setFullScreen(false);
	}

	clipboard(): void {
		// const modal = this.createModal(ClipboardModalComponent);
		// modal.result.then((text) => {
		//     this.manager.setFocused(true);
		//     if (text) {
		//         this.manager.sendRemoteClipboardData(text);
		//         this.snackBar.open('Sent to remote clipboard', 'OK', {
		//             duration: 2000,
		//         });
		//     }
		// }, () => this.manager.setFocused(true));
	}

	connect(): void {
		this.service.connect({ ...this.widget.data, width: window.screen.width, height: window.screen.height });
	}

	onPair(): void {}

	onMove(): void {}

	onEdit(options?: WidgetOptions): void {
		this._dialogService
			.open<ManageWidgetDialogComponent>(ManageWidgetDialogComponent, {
				data: {
					widget: this.widget,
					options: options
				}
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	onDelete(): void {
		this._dialogService
			.open<ConfirmActionDialogComponent>(ConfirmActionDialogComponent, {
				data: {
					title: 'Usuwanie komponentu',
					description: 'Czy chcesz usunąć wybrany komponent z panelu?',
					iconClass: 'question circle outline',
					approveButtonLabel: 'Usuń',
					approveButtonColorClass: 'red',
					denyButtonLabel: 'Anuluj'
				}
			})
			.events.pipe(
				filter((event) => !!event),
				filter((event) => event.type === 'accept'),
				tap(() => (this.widget.active = !this.widget.active))
			)
			.subscribe();
	}
}
