import { OnInit, Input, Output, OnDestroy, EventEmitter, Injectable, Component } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of, Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';
import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { PairingService } from 'src/app/core/services/pairing.service';
import { ToastService } from 'src/app/core/services/toast.service';

import { Widget, WidgetParam, WidgetParamValue } from 'src/app/shared/models/widget.model';

declare var $: any;

@Component({ template: '' })
export abstract class WidgetComponent implements OnInit, OnDestroy {
	protected readonly _unsubscribe$ = new Subject<void>();

	private _widgetRefresh: EventEmitter<Widget> = new EventEmitter();
	private _refresh: Observable<void>;
	private _widget: Widget;

	protected constructor(
		protected _translateService: TranslateService,
		protected _tdLoadingService: TdLoadingService,
		protected _toastService: ToastService,
		protected _dialogService: DialogService,
		protected _broadcastService: BroadcastService,
		protected _dashboardsService: DashboardsService,
		protected _pairingService: PairingService
	) {}

	get widget(): Widget {
		return this._widget;
	}

	@Input()
	set widget(widget: Widget) {
		this._widget = widget;
	}

	get refresh(): Observable<void> {
		return this._refresh;
	}

	@Input()
	set refresh(refresh: Observable<void>) {
		this._refresh = refresh;
		refresh.pipe(tap(() => this.onRefresh())).subscribe();
	}

	@Output()
	get widgetRefresh(): EventEmitter<Widget> {
		return this._widgetRefresh;
	}

	ngOnInit(): void {
		this._broadcastService
			.observe(MessageType.WIDGET_EDIT)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((widget: Widget) => widget.settings.type === this._widget.settings.type),
				tap(() => this.onWidgetEdit())
			)
			.subscribe();

		this._broadcastService
			.observe(MessageType.WIDGET_DELETE)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((widget: Widget) => widget.settings.type === this._widget.settings.type),
				tap(() => this.onWidgetDelete())
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	protected abstract onWidgetEdit(): void;

	protected onWidgetDelete(): void {}

	protected onWidgetFilters(): void {}

	protected onWidgetSet(): void {}

	protected onRefresh(): void {}

	protected onPairingMessage(): void {}
}
