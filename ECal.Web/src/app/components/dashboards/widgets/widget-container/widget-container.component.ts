import { Component, OnInit, Input, OnDestroy, AfterViewInit, ViewChild, ViewContainerRef, Injector, Compiler } from '@angular/core';
import { Subject } from 'rxjs';
import { filter, tap, takeUntil, map } from 'rxjs/operators';

import { DialogService } from 'src/app/core/services/dialog.service';
import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ConfirmActionDialogComponent } from 'src/app/shared/components/dialogs/confirm-action-dialog/confirm-action-dialog.component';

import { WidgetTypeEnum } from 'src/app/shared/enums/widget-type.enum';
import { Widget } from 'src/app/shared/models/widget.model';
import { Pairing } from 'src/app/shared/models/pairing.model';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { TdLoadingService } from '@covalent/core/loading';
import { WidgetComponent } from '../widget.component';
import { WidgetNotificationsModule } from '../widget-table/widget-notifications/widget-notifications.module';
import { WidgetSnakeModule } from '../widget-snake/widget-snake.module';
import { WidgetMonitorSystemModule } from '../widget-monitor-system/widget-monitor-system.module';
import { WidgetMonitorDiskModule } from '../widget-monitor-disk/widget-monitor-disk.module';
import { WidgetStatisticsModule } from '../widget-statistics/widget-statistics.module';
import { WidgetApplicationsModule } from '../widget-table/widget-applications/widget-applications.module';
import { WidgetUsersAuditModule } from '../widget-table/widget-users-audit/widget-users-audit.module';
import { WidgetUsersModule } from '../widget-table/widget-users/widget-users.module';
import { WidgetRolesModule } from '../widget-table/widget-roles/widget-roles.module';
import { WidgetPermissionsModule } from '../widget-table/widget-permissions/widget-permissions.module';
import { WidgetGroupsUsersModule } from '../widget-table/widget-groups-users/widget-groups-users.module';
import { WidgetGroupsResourcesModule } from '../widget-table/widget-groups-resources/widget-groups-resources.module';
import { WidgetGuacamoleModule } from '../widget-guacamole/widget-guacamole.module';
import { WidgetRemoteDesktopsModule } from '../widget-table/widget-remote-desktops/widget-remote-desktops.module';
import { WidgetCalendarModule } from '../widget-calendar/widget-calendar.module';
import { WidgetImplementationModule } from '../widget-implementation/widget-implementation.module';

declare var $: any;

export interface Control {
	text?: string;
	tooltip?: string;
	icon?: string;
	color?: string;
	disabled?: boolean;
	count?: number;
	showText?: boolean;
	click?(item1?: any, item2?: any): void;
}

export interface MenuItem {
	text?: string;
	tooltip?: string;
	icon?: string;
	hidden?: boolean;
	click?(data?: any): void;
}

@Component({
	selector: 'app-widget-container',
	templateUrl: './widget-container.component.html',
	styleUrls: ['./widget-container.component.scss']
})
export class WidgetContainerComponent implements OnInit, OnDestroy, AfterViewInit {
	widgetType: typeof WidgetTypeEnum = WidgetTypeEnum;

	private _unsubscribe$ = new Subject<void>();

	private _pairing: Pairing;
	private _refresh: Subject<void>;
	private _widget: Widget;

	private _message;
	private _editable = false;

	@ViewChild('container', { read: ViewContainerRef }) _container: ViewContainerRef;

	private readonly _widgetMenuItems: Array<MenuItem> = [
		{
			text: 'Parowanie',
			icon: 'handshake',
			click: () => this.onPair()
		},
		{
			text: 'Zmień położenie',
			icon: 'vector square',
			click: () => this.onMove()
		},
		{
			text: 'Ustawienia',
			icon: 'wrench',
			click: () => this.onEdit(this._widget)
		},
		{
			text: 'Usuń komponent',
			icon: 'trash alternate red',
			click: () => this.onDelete()
		}
	];

	constructor(
		private _compiler: Compiler,
		private _injector: Injector,
		private _tdLoadingService: TdLoadingService,
		private _dialogService: DialogService,
		private _dashboardsService: DashboardsService,
		private _broadcastService: BroadcastService
	) {}

	loadModules() {
		const complier = (moduleType: any) =>
			this._compiler
				.compileModuleAsync(moduleType)
				.then((moduleFactory: any) => {
					const moduleRef = moduleFactory.create(this._injector);
					const componentFactory = moduleRef.instance.resolveComponent();
					const { instance }: any = this._container.createComponent(componentFactory, null, moduleRef.injector);
					instance.widget = this.widget;
					instance.refresh = this.refresh;
				})
				.finally(() => this._tdLoadingService.resolve(`overlay_${this.widget.settings.type}_${this.widget.id}`));

		this._tdLoadingService.register(`overlay_${this.widget.settings.type}_${this.widget.id}`);
		switch (this._widget?.settings?.type) {
			case WidgetTypeEnum.SNAKE: {
				WidgetSnakeModule.import().then(({ WidgetSnakeModule }) => complier(WidgetSnakeModule));
				break;
			}
			case WidgetTypeEnum.MONITOR_SYSTEM: {
				WidgetMonitorSystemModule.import().then(({ WidgetMonitorSystemModule }) => complier(WidgetMonitorSystemModule));
				break;
			}
			case WidgetTypeEnum.MONITOR_DISK: {
				WidgetMonitorDiskModule.import().then(({ WidgetMonitorDiskModule }) => complier(WidgetMonitorDiskModule));
				break;
			}
			case WidgetTypeEnum.STATISTICS: {
				WidgetStatisticsModule.import().then(({ WidgetStatisticsModule }) => complier(WidgetStatisticsModule));
				break;
			}
			case WidgetTypeEnum.APPLICATIONS: {
				WidgetApplicationsModule.import().then(({ WidgetApplicationsModule }) => complier(WidgetApplicationsModule));
				break;
			}
			case WidgetTypeEnum.USERS_AUDIT: {
				WidgetUsersAuditModule.import().then(({ WidgetUsersAuditModule }) => complier(WidgetUsersAuditModule));
				break;
			}
			case WidgetTypeEnum.USERS: {
				WidgetUsersModule.import().then(({ WidgetUsersModule }) => complier(WidgetUsersModule));
				break;
			}
			case WidgetTypeEnum.ROLES: {
				WidgetRolesModule.import().then(({ WidgetRolesModule }) => complier(WidgetRolesModule));
				break;
			}
			case WidgetTypeEnum.PERMISSIONS: {
				WidgetPermissionsModule.import().then(({ WidgetPermissionsModule }) => complier(WidgetPermissionsModule));
				break;
			}
			case WidgetTypeEnum.GROUPS_USERS: {
				WidgetGroupsUsersModule.import().then(({ WidgetGroupsUsersModule }) => complier(WidgetGroupsUsersModule));
				break;
			}
			case WidgetTypeEnum.GROUPS_RESOURCES: {
				WidgetGroupsResourcesModule.import().then(({ WidgetGroupsResourcesModule }) => complier(WidgetGroupsResourcesModule));
				break;
			}
			case WidgetTypeEnum.GUACAMOLE: {
				WidgetGuacamoleModule.import().then(({ WidgetGuacamoleModule }) => complier(WidgetGuacamoleModule));
				break;
			}
			case WidgetTypeEnum.REMOTE_DESKTOPS: {
				WidgetRemoteDesktopsModule.import().then(({ WidgetRemoteDesktopsModule }) => complier(WidgetRemoteDesktopsModule));
				break;
			}
			case WidgetTypeEnum.NOTIFICATIONS: {
				WidgetNotificationsModule.import().then(({ WidgetNotificationsModule }) => complier(WidgetNotificationsModule));
				break;
			}
			case WidgetTypeEnum.CALENDAR: {
				WidgetCalendarModule.import().then(({ WidgetCalendarModule }) => complier(WidgetCalendarModule));
				break;
			}
			default: {
				WidgetImplementationModule.import().then(({ WidgetImplementationModule }) => complier(WidgetImplementationModule));
				break;
			}
		}
	}

	get widgetMenuItems(): any {
		return this._widgetMenuItems;
	}

	get widget(): Widget {
		return this._widget;
	}

	@Input()
	set widget(widget: Widget) {
		this._widget = widget;
	}

	get refresh(): Subject<void> {
		return this._refresh;
	}

	@Input()
	set refresh(refresh: Subject<void>) {
		this._refresh = refresh;
	}

	get type(): boolean {
		switch (this._widget.type) {
			case 'table':
			case 'profile':
				return false;

			default:
				return true;
		}
	}

	get message() {
		return this._message;
	}

	ngOnInit(): void {
		this._dashboardsService.editable
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((editable) => (this._editable = editable))
			)
			.subscribe();

		this._broadcastService
			.observe(MessageType.WIDGET_PAIR_OUTSIDE_FILTER_UPDATE)
			.pipe(
				takeUntil(this._unsubscribe$),
				// tap((message) => $(`.sidebar-pair-${this.widget.settings.type}`).sidebar('show')),
				tap((message) => (this._message = message))
			)
			.subscribe();

		this.loadModules();
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	ngAfterViewInit(): void {
		setTimeout((_) => {
			$('.dropdown-export').dropdown({ action: 'hide' });
			$('.dropdown-settings').dropdown({ action: 'hide' });
			$(`.sidebar-pair-${this.widget.settings.type}`)
				.sidebar({ context: $(`#widget-${this.widget.settings.type}`) })
				.sidebar('setting', 'transition', 'scale down');
		});
	}

	onPair(): void {
		console.log('onPair container');
		$(`.sidebar-pair-${this.widget.settings.type}`).sidebar('show');
	}

	onMove(): void {
		console.log('onMove container');
		this._dashboardsService.editable.next(!this._editable);
	}

	onEdit(widget: Widget): void {
		console.log('onEdit container');
		this._broadcastService.broadcast({ type: MessageType.WIDGET_EDIT, payload: widget });
	}

	onDelete(): void {
		console.log('onDelete container');
		this._dialogService
			.open<ConfirmActionDialogComponent>(ConfirmActionDialogComponent, {
				data: {
					title: 'Usuwanie komponentu',
					description: 'Czy chcesz usunąć wybrany komponent z panelu?',
					iconClass: 'question circle outline',
					approveButtonLabel: 'Usuń',
					approveButtonColorClass: 'red',
					denyButtonLabel: 'Anuluj'
				}
			})
			.events.pipe(
				filter((event) => !!event),
				filter((event) => event.type === 'accept'),
				tap(() => (this.widget.active = !this.widget.active))
			)
			.subscribe();
	}
}
