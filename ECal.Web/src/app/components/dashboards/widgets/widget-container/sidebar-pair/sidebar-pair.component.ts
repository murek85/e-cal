import { Component, OnInit, AfterViewInit, Input, ChangeDetectorRef, Output, OnDestroy, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService, DefaultLangChangeEvent } from '@ngx-translate/core';
import { BehaviorSubject, fromEvent, Observable, of, Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { AccountService } from 'src/app/core/services/account.service';
import { AppService } from 'src/app/core/services/app.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';

import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';

import { Dashboard } from 'src/app/shared/models/dashboard.model';
import { User } from 'src/app/shared/models/user.model';
import { Widget, WidgetParam, WidgetParamValue } from 'src/app/shared/models/widget.model';
import { Pairing, PairingAction, WidgetPair } from 'src/app/shared/models/pairing.model';

declare var $: any;

@Component({
	selector: 'app-sidebar-pair',
	templateUrl: './sidebar-pair.component.html',
	styleUrls: ['./sidebar-pair.component.scss']
})
export class SidebarPairComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	private readonly _cancel: EventEmitter<void> = new EventEmitter();

	private _message;

	constructor(private _dashboardsService: DashboardsService, private _broadcastService: BroadcastService, private _accountService: AccountService) {}

	get message(): Widget {
		return this._message;
	}

	@Input()
	set message(message) {
		this._message = message;
	}

	private initSemanticConfig(): void {}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onClose(): void {
		this._cancel.emit();
	}
}
