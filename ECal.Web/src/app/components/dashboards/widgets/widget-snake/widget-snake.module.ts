import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { WidgetSnakeComponent } from './widget-snake.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [WidgetSnakeComponent],
	imports: [APP_MODULES],
	exports: [WidgetSnakeComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: []
})
export class WidgetSnakeModule {
	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	public resolveComponent(): ComponentFactory<WidgetSnakeComponent> {
		return this._componentFactoryResolver.resolveComponentFactory(WidgetSnakeComponent);
	}

	static import() {
		return import('../widget-snake/widget-snake.module');
	}
}
