import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { DashboardsComponent } from './dashboards.component';
import { DashboardsRoutingModule } from './dashboards-routing.module';

import { DashboardModule } from './dashboard/dashboard.module';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot(), DashboardModule];
const COMPONENTS = [];
const DIALOGS = [];

@NgModule({
	declarations: [DashboardsComponent, COMPONENTS, DIALOGS],
	imports: [DashboardsRoutingModule, APP_MODULES],
	exports: [COMPONENTS, DIALOGS],
	providers: [],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardsModule {}
