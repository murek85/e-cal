import { Component, OnInit, AfterViewInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { AccountService } from 'src/app/core/services/account.service';
import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';
import { ResourceType, WebsocketEvent } from 'src/app/core/services/websocket.service';
import { ConfirmActionDialogComponent } from 'src/app/shared/components/dialogs/confirm-action-dialog/confirm-action-dialog.component';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';
import { User } from 'src/app/shared/models/user.model';

import { AddWidgetDialogComponent } from './dashboard/add-widget-dialog/add-widget-dialog.component';
import { ManageDashboardDialogComponent } from './dashboard/manage-dashboard-dialog/manage-dashboard-dialog.component';

declare var $: any;

@Component({
	selector: 'app-dashboards',
	templateUrl: './dashboards.component.html',
	styleUrls: ['./dashboards.component.scss']
})
export class DashboardsComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	private _loading = false;
	private _editable = false;
	private _sizeSidenav = true;
	private _unlockSidenav = true;

	private _userLogged: User;

	constructor(
		private _dialogService: DialogService,
		private _toastService: ToastService,
		private _broadcastService: BroadcastService,
		private _dashboardsService: DashboardsService,
		private _accountService: AccountService
	) {}

	get loading(): boolean {
		return this._loading;
	}

	get editable(): boolean {
		return this._editable;
	}

	get unlockSidenav(): boolean {
		return this._unlockSidenav;
	}

	get sizeSidenav(): boolean {
		return this._sizeSidenav;
	}

	set sizeSidenav(sizeSidenav) {
		this._sizeSidenav = sizeSidenav;
	}

	get getToolbar() {
		return JSON.parse(localStorage.getItem('toolbar')) === 'left';
	}

	get getLoggedIn(): boolean {
		return this._accountService.getLoggedIn();
	}

	get userLogged(): User {
		return this._userLogged;
	}

	private initSemanticConfig(): void {
		setTimeout((_) => {
			$('.sidebar-widgets')
				.sidebar({ context: $('#main') })
				.sidebar('setting', 'transition', 'scale down');
		});
	}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		this._dashboardsService.loading
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((loading) => (this._loading = loading))
			)
			.subscribe();

		this._dashboardsService.unlockSidenav
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((unlockSidenav) => (this._unlockSidenav = unlockSidenav))
			)
			.subscribe();

		this._dashboardsService.sizeSidenav
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((sizeSidenav) => (this._sizeSidenav = sizeSidenav))
			)
			.subscribe();

		this._dashboardsService.editable
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((editable) => (this._editable = editable))
			)
			.subscribe();

		this._broadcastService
			.observe(MessageType.RESOURCES_CHANGED)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((message: { type: string; title: string; message: WebsocketEvent; config: any }) => message.type === ResourceType.ACCOUNT),
				tap((message) => this._toastService.open(message.title, message.message, message.config))
			)
			.subscribe();

		this._broadcastService
			.observe(MessageType.RESOURCES_CHANGED)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((message: { type: string; message: WebsocketEvent }) => message.type === ResourceType.NOTIFICATIONS),
				tap((message) => this._toastService.open('Komunikat', message.message, { showIcon: 'info' }))
			)
			.subscribe();

		this._broadcastService
			.observe(MessageType.RESOURCES_CHANGED)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((message: { type: string; title: string; message: WebsocketEvent; config: any }) => message.type === ResourceType.DASHBOARDS),
				tap((message) => this._toastService.open(message.title, message.message, message.config))
			)
			.subscribe();

		this._broadcastService
			.observe(MessageType.RESOURCES_CHANGED)
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((message: { type: string; title: string; message: WebsocketEvent; config: any }) => message.type === ResourceType.WIDGETS),
				tap((message) => this._toastService.open(message.title, message.message, message.config))
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onFabMenuClick(event): void {
		if (this._editable) {
			this.onChangeEditable();
			event.preventDefault();
			event.stopPropagation();
		}
	}

	onChangeSizeSidenav(): void {
		this._sizeSidenav = !this._sizeSidenav;
		this._dashboardsService.sizeSidenav.next(this._sizeSidenav);
	}

	onSidebar(): void {
		$('.sidebar-widgets').sidebar('show');
	}

	onChangeEditable(): void {
		this._editable = !this._editable;
		this._dashboardsService.editable.next(this._editable);
	}

	onAddWidget(): void {
		this._dialogService
			.open<AddWidgetDialogComponent>(AddWidgetDialogComponent, {
				data: { formModeType: FormModeType.ADD }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	onManageDashboard(): void {
		this._dialogService
			.open<ManageDashboardDialogComponent>(ManageDashboardDialogComponent, {
				data: { formModeType: FormModeType.EDIT }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	onDeleteDashboard(): void {
		this._dialogService
			.open<ConfirmActionDialogComponent>(ConfirmActionDialogComponent, {
				data: {
					title: 'Usuwanie panelu',
					description: 'Czy chcesz usunąć wybrany panel?',
					iconClass: 'question circle outline',
					approveButtonLabel: 'Usuń',
					approveButtonColorClass: 'red',
					denyButtonLabel: 'Anuluj'
				}
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}
}
