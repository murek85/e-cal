import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { flatMap, takeUntil, tap } from 'rxjs/operators';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { WidgetsService } from 'src/app/core/services/widgets.service';

import { User } from 'src/app/shared/models/user.model';
import { Column } from 'src/app/shared/models/column.model';

declare var $: any;

@Component({
	selector: 'app-column-settings',
	templateUrl: './column-settings.component.html'
})
export class ColumnSettingsComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _userLogged: User;

	private _columns: Array<Column>;

	constructor(private _widgetsService: WidgetsService, private _accountService: AccountService, private _dialogRef: DialogOverlayRef) {}

	get columns(): Array<Column> {
		return this._columns;
	}

	@Input()
	set columns(columns: Array<Column>) {
		this._columns = columns;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onDrop(event: CdkDragDrop<Array<Column>>): void {
		moveItemInArray(this._columns, event.previousIndex, event.currentIndex);
	}

	onChangeColumn(column: Column): void {
		column.column.logicValue = !column.column.logicValue;
	}
}
