import { Component, OnInit, AfterViewInit, OnDestroy, Output, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { WidgetsService } from 'src/app/core/services/widgets.service';

import { WidgetOptions } from 'src/app/shared/models/widget.model';
import { User } from 'src/app/shared/models/user.model';
import { Column, FilterType, ParamValue } from 'src/app/shared/models/column.model';
import { MatTableDataSource } from '@angular/material/table';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EventEmitter } from 'events';

declare var $: any;

@Component({
	selector: 'app-filter-settings',
	templateUrl: './filter-settings.component.html',
	styleUrls: ['./filter-settings.component.scss']
})
export class FilterSettingsComponent implements OnInit, OnDestroy, AfterViewInit {
	filterType: typeof FilterType = FilterType;

	private _unsubscribe$ = new Subject<void>();

	private _userLogged: User;

	private _options: WidgetOptions;

	private _columns: Array<Column>;
	private _editColumn: Column;
	private _filtersFormGroup: FormGroup;
	private _selectedControl: FormControl;
	private _filterTooltip: { [alias: string]: string } = {};

	private _dictionaries: { [alias: string]: MatTableDataSource<any> } = {};

	@Output() filterChanged: any = new EventEmitter();

	constructor(private _widgetsService: WidgetsService, private _accountService: AccountService, private _dialogRef: DialogOverlayRef) {}

	private get filter(): ParamValue {
		const definition = this._selectedControl.value.definition;
		const controls = this._filtersFormGroup.controls;
		const param = {
			alias: definition.alias,
			logicValue: true
		} as ParamValue;

		switch (definition.filter.type) {
			case FilterType.TEXT: {
				param.textValue = controls[definition.alias].value;
				break;
			}
		}

		return param;
	}

	get columns(): Array<Column> {
		return this._columns.filter((column) => (this._editColumn ? !!column.filter && column.filter.alias === this._editColumn.filter.alias : !column.filter));
	}

	@Input()
	set columns(columns: Array<Column>) {
		this._columns = columns.filter((column) => !!column.definition.filter);
	}

	@Input()
	set options(options: WidgetOptions) {
		this._options = options || {};
	}

	get filters(): Array<Column> {
		return this._columns.filter((column) => !!column.filter);
	}

	get userLogged(): User {
		return this._userLogged;
	}

	get filtersFormGroup(): FormGroup {
		return this._filtersFormGroup;
	}

	get selectedControl(): FormControl {
		return this._selectedControl;
	}

	get editColumn(): Column {
		return this._editColumn;
	}

	set editColumn(editColumn: Column) {
		this._editColumn = editColumn;
		this.onSelectedControlUpdate();
	}

	private initSemanticConfig(): void {
		setTimeout((_) => {
			$(`.dropdown-columns`).dropdown({
				direction: 'downward',
				placeholder: 'Wybierz',
				message: {
					addResult: 'Dodaj <b>{term}</b>',
					count: 'Zaznaczono {count}',
					maxSelections: `Maksymalnie {maxCount}`,
					noResults: 'Nie znaleziono żadnych wpisów.'
				},
				onChange: (value, text, $selectedItem) => this._selectedControl.setValue(this._columns.find((column) => column.definition.alias === value))
			});

			if (this._options.filter) {
				$(`.dropdown-columns`).dropdown('set selected', this._options.filter.definition.alias);
			}
		});
	}

	private initSemanticForm(): void {}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
		this.initSemanticForm();
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		this.initFilters();
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	getFilterTooltip(filter: Column) {
		return this._filterTooltip[filter.column.alias] || '';
	}

	onClickFilter(column: Column) {
		column.filter.logicValue = !column.filter.logicValue;
	}

	onEditFilter(column: Column) {
		this._selectedControl.disable();
		this._editColumn = column;
		this._selectedControl.setValue(this._columns.find((filter) => filter.filter && filter.filter.alias === column.filter.alias));

		setTimeout((_) => $(`.dropdown-columns`).dropdown('set selected', column.definition.alias));

		const controls = this._filtersFormGroup.controls;
		const definition = column.definition;
		switch (definition.filter.type) {
			case FilterType.TEXT: {
				controls[definition.alias].setValue(column.filter.textValue);
				break;
			}
		}
	}

	onRemoveFilter(param: ParamValue) {
		const column = this._columns.find((column) => column.definition.alias === param.alias);
		if (column) {
			delete column.filter;
		}
	}

	onUpdateFilter() {
		const column = this._columns.find((column) => column.column.alias === this.filter.alias);
		if (column) {
			column.filter = this.filter;
		}

		this._editColumn = null;
		this.onClearFiltersForm();

		this.rebuildTooltips();
	}

	onUpdateCancel() {
		this._editColumn = null;
		this.onClearFiltersForm();
	}

	initFilters() {
		this._selectedControl = new FormControl({ value: '', disabled: this._columns.filter((column) => !column.filter).isEmpty() });
		const controls = { selectedColumn: this._selectedControl };

		if (this._options.filter) {
			this._selectedControl.setValue(this._options.filter);
		}

		this._columns
			.map((column) => column.definition)
			.forEach((definition) => {
				switch (definition.filter.type) {
					case FilterType.TEXT: {
						controls[definition.alias] = new FormControl('', [Validators.required, Validators.minLength(1)]);
						break;
					}
				}
			});
		this._filtersFormGroup = new FormGroup(controls);

		this.rebuildTooltips();

		if (this._options.column) {
			this.onEditFilter(this._options.column);
		}
	}

	rebuildTooltips() {
		this.filters.forEach((filter) => {
			let text = '';
			switch (filter.definition.filter.type) {
				case FilterType.TEXT: {
					const exact = !!filter.filter.numberValue;
					text = (exact ? '' : 'Zawiera: ') + ' ' + filter.filter.textValue;
					break;
				}
			}
			this._filterTooltip[filter.column.alias] = text;
		});
	}

	selectedValueChanged(event, alias) {
		this._filtersFormGroup.controls[alias].setValue(event);
	}

	isFormDisabled(): boolean {
		const value = this._selectedControl.value;
		const controls = this._filtersFormGroup.controls;

		return (
			!value ||
			!!Object.keys(controls)
				.filter((key) => key === value.alias)
				.filter((key) => controls[key].invalid).length
		);
	}

	getDictionary(alias: string): any[] {
		return this._dictionaries[alias] ? this._dictionaries[alias].filteredData : [];
	}

	private setDictionary(definition: any, data: any[]) {
		if (!this._dictionaries[definition.alias]) {
			this._dictionaries[definition.alias] = new MatTableDataSource();
		}

		this._dictionaries[definition.alias].data = data;
	}

	private onClearFiltersForm() {
		Object.keys(this._filtersFormGroup.controls).forEach((key) => this._filtersFormGroup.controls[key].reset());
		setTimeout((_) => $(`.dropdown-columns`).dropdown('clear'));
	}

	private onSelectedControlUpdate() {
		if (this._columns.filter((column) => !column.filter).isEmpty()) {
			this._selectedControl.disable();
		} else {
			this._selectedControl.enable();
		}
	}
}
