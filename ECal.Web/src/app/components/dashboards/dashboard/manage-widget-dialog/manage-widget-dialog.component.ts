import { Component, OnInit, AfterViewInit, OnDestroy, Inject } from '@angular/core';
import { from, iif, of, Subject } from 'rxjs';
import { catchError, delay, filter, finalize, flatMap, map, takeUntil, tap } from 'rxjs/operators';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';

import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { WidgetsService } from 'src/app/core/services/widgets.service';

import { Widget, WidgetOptions } from 'src/app/shared/models/widget.model';
import { User } from 'src/app/shared/models/user.model';
import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { FormField, Segment, Step } from 'src/app/shared/models/step.model';
import { Column, FilterType } from 'src/app/shared/models/column.model';
import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';

declare var $: any;

const flatten = (arr) => arr.reduce((a, b) => a.concat(Array.isArray(b) ? flatten(b) : b), []);

const FORMNAME = '.form-manage-widget';
const MODALNAME = '.modal-manage-widget';

interface VisibleTabs {
	columns?: boolean;
	filters?: boolean;
	monitor?: boolean;
}

@Component({
	selector: 'app-manage-widget-dialog',
	templateUrl: './manage-widget-dialog.component.html'
})
export class ManageWidgetDialogComponent implements OnInit, OnDestroy, AfterViewInit {
	formFieldType: typeof FormFieldType = FormFieldType;
	filterType: typeof FilterType = FilterType;

	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _steps: Array<Step>;
	private _widgets: Array<Widget>;
	private _selectedIndex = 0;
	private _error;
	private _userLogged: User;
	private _loading = false;

	private _visibleTabs: VisibleTabs = { columns: false, filters: false, monitor: false };

	private readonly _widget: Widget;
	private readonly _columns: Array<Column>;
	private readonly _monitor: {};
	private readonly _options: WidgetOptions;

	constructor(
		private _broadcastService: BroadcastService,
		private _widgetsService: WidgetsService,
		private _accountService: AccountService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA)
		public data: {
			widget: Widget;
			columns: Array<Column>;
			monitor: {};
			options: WidgetOptions;
			filterEdit?: Column;
		}
	) {
		this._widget = data.widget;
		this._columns = data.columns || [];
		this._monitor = data.monitor;
		this._options = data.options || {};
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get widget(): Widget {
		return this._widget;
	}

	get columns(): Array<Column> {
		return this._columns;
	}

	get monitor(): {} {
		return this._monitor;
	}

	get options(): WidgetOptions {
		return this._options;
	}

	get steps(): Step[] {
		return this._steps;
	}

	get selectedIndex(): number {
		return this._selectedIndex;
	}

	get error(): any {
		return this._error;
	}

	get widgets(): Array<Widget> {
		return this._widgets;
	}

	set widgets(widgets: Array<Widget>) {
		this._widgets = widgets;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	get loading(): boolean {
		return this._loading;
	}

	get isMonitorVisible(): boolean {
		return this._visibleTabs.monitor;
	}

	get isColumnsVisible(): boolean {
		return this._visibleTabs.columns;
	}

	get isFiltersVisible(): boolean {
		return this._visibleTabs.filters;
	}

	private initSemanticConfig(): void {
		this.initConfig(this._steps[this._selectedIndex]);
	}

	private initSemanticForm(): void {
		this.initForm(this._steps[this._selectedIndex]);
	}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
		this.initSemanticForm();
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		$(MODALNAME)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initSteps();
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeTab($event): void {
		const tab = $event as MatTabChangeEvent;
		this._selectedIndex = tab.index;

		this.initForm(this._steps[tab.index]);
		this.initConfig(this._steps[tab.index]);
		this.loadForm(this.prepareForm());
	}

	onBackForm(): void {
		$(MODALNAME).modal('hide');
	}

	onSaveForm($event): void {
		this._loading = true;

		// this._steps.forEach((step) => (step.completed = $(FORMNAME).form('validate form')));
		this._steps.forEach(
			(step) => (step.completed = step.segments.every((segment) => segment.fields.filter((field) => field.rules).every((field) => field.value)))
		);

		const completed: boolean[] = flatten(this._steps.map((step) => step.completed));
		const valid = completed.every((value) => value);
		const model: Widget = this.prepareForm();

		const broadcast = (result) =>
			this._broadcastService.broadcast({
				type: MessageType.RESOURCES_CHANGED,
				payload: {
					type: ResourceType.WIDGETS,
					title: result.title,
					message: result.message,
					config: result.config
				}
			});

		const success = () =>
			of({
				title: 'Zapisano',
				message: `Dodano.`,
				config: { class: 'success', position: 'top center', showIcon: 'check circle' }
			});

		const error = () =>
			of({
				title: 'Błąd',
				message: `Wystąpił problem.`,
				config: { class: 'error', position: 'top center', showIcon: 'exclamation circle' }
			});

		const invalid = () =>
			of({
				title: 'Problem',
				message: `Nie wypełniono wymaganych pól formularza.`,
				config: { class: 'warning', position: 'top center', showIcon: 'exclamation circle' }
			});

		const close = () => $(MODALNAME).modal('hide');

		iif(() => valid, this._widgetsService.update(model).pipe(delay(500), map(success), tap(close)), invalid())
			.pipe(
				delay(500),
				catchError(error),
				finalize(() => (this._loading = false))
			)
			.subscribe(broadcast);
	}

	private initSteps(): void {
		const steps: Step[] = [];
		steps.push(
			this.createStep('Komponent', 'Nowy komponent.', 'widget-data', [
				{
					header: 'Dane komponentu',
					description: 'Uzupełnienie podstawowych informacji o komponencie.',
					fields: [
						{
							label: 'Nazwa komponentu',
							name: 'name',
							symbol: 'name',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole '{name}' nie może być puste.`
								}
							]
						},
						{
							label: 'Opis panelu',
							name: 'desc',
							symbol: 'desc',
							type: FormFieldType.TEXTAREA
						}
					]
				}
			])
		);
		this._steps = steps;
	}

	private createStep(label: string, description: string, symbol: string, segments: Segment[], completed: boolean = false): Step {
		const menu: Step = { label, description, symbol, segments, completed };
		return menu;
	}

	private initConfig(step?: Step): void {
		from(this._steps)
			.pipe(
				delay(0),
				filter((step) => step.symbol === step.symbol),
				flatMap((step) => step.segments),
				map((segment) => segment.fields.map((field) => this.initField(field)))
			)
			.subscribe();
	}

	private initField(field): void {
		switch (field.type) {
			case FormFieldType.CHECKBOX: {
				$(`.checkbox-${field.symbol}`).checkbox({
					onChecked: () => (field.value = true),
					onUnchecked: () => (field.value = false)
				});
				break;
			}
			case FormFieldType.EMAIL:
			case FormFieldType.NUMBER:
			case FormFieldType.PHONE_NUMBER:
			case FormFieldType.PASSWORD:
			case FormFieldType.TEXT: {
				field.onChange = (model: FormField) => (model.value = $(FORMNAME).form('get value', model.name));
				break;
			}
			case FormFieldType.DICTIONARY_MULTI: {
				$(`.dropdown-${field.symbol}`).dropdown({
					values: field.items,
					placeholder: 'Wybierz',
					fields: field.api.fields,
					message: {
						addResult: 'Dodaj <b>{term}</b>',
						count: 'Zaznaczono {count}',
						maxSelections: `Maksymalnie {maxCount}`,
						noResults: 'Nie znaleziono żadnych wpisów.'
					},
					onChange: (value, text, $selectedItem) => (field.value = value)
				});
				$(`.dropdown-${field.symbol}`).dropdown('set selected', field.value ? field.value.split(',') : null);
				break;
			}
			case FormFieldType.DICTIONARY_SINGLE: {
				$(`.dropdown-${field.symbol}`).dropdown({
					values: field.items,
					placeholder: 'Wybierz',
					fields: field.api.fields,
					message: {
						addResult: 'Dodaj <b>{term}</b>',
						count: 'Zaznaczono {count}',
						maxSelections: `Maksymalnie {maxCount}`,
						noResults: 'Nie znaleziono żadnych wpisów.'
					},
					onChange: (value, text, $selectedItem) => (field.value = value)
				});
				$(`.dropdown-${field.symbol}`).dropdown('set selected', field.value);
				break;
			}
		}
	}

	private initForm(step?: Step): void {
		setTimeout((_) => $(FORMNAME).form({ className: { label: 'ui basic red prompt label dock-a' }, inline: true, autoCheckRequired: true }));

		from(this._steps)
			.pipe(
				filter((step) => step.symbol === step.symbol),
				flatMap((step) => step.segments),
				map((segment) => segment.fields.filter((field) => !!field.rules).map((field) => ({ name: field.name, rules: field.rules }))),
				map((rule) =>
					rule.reduce((map, obj) => {
						map[obj.name] = { identifier: obj.name, rules: obj.rules };
						return map;
					}, {})
				),
				delay(0),
				map((fields) => $(FORMNAME).form('add fields', fields))
			)
			.subscribe();
	}

	private initDialog(): void {
		this._dialogConfig = {
			title: 'Ustawienia komponentu',
			meta: 'skonfigurowanie parametrów komponentu'
		};

		this._visibleTabs.columns = !!this._columns.length;
		this._visibleTabs.filters = this._columns.some((column) => !!column.definition.filter);
		this._visibleTabs.monitor = !!this._monitor;

		switch (this._options.tabType) {
			case 'columns':
				this._selectedIndex = 1;
				break;
			case 'filters':
				this._selectedIndex = 2;
				break;
			case 'monitor':
				this._selectedIndex = 3;
				break;

			default:
				this._selectedIndex = 0;
				break;
		}
	}

	private loadForm(model: Widget): void {
		this._steps.forEach((step) => {
			step.segments.forEach((segment) => {
				segment.fields.forEach((field) => (field.value = model[field.name]));
			});
		});

		const fields = flatten(this._steps.map((step) => step.segments.map((segment) => segment.fields))).map((field) => ({
			name: field.name,
			value: field.value
		}));

		fields.forEach((field) => $(FORMNAME).form('set value', field.name, field.value));
	}

	private prepareForm(): Widget {
		const fields = flatten(this._steps.map((step) => step.segments.map((segment) => segment.fields)))
			.filter((field) => !!field.value)
			.map((field) => ({ name: field.name, value: field.value }));

		const model: Widget = fields.reduce((map, obj) => {
			map[obj.name] = obj.value;
			return map;
		}, {});

		return model;
	}
}
