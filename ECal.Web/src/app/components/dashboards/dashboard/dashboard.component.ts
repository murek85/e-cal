import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { isNullOrUndefined } from '../../../core/tools';

import { AccountService } from 'src/app/core/services/account.service';
import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { ToastService } from 'src/app/core/services/toast.service';

import { User } from 'src/app/shared/models/user.model';
import { Dashboard } from 'src/app/shared/models/dashboard.model';
import { Widget } from 'src/app/shared/models/widget.model';
import { ResourceType, WebsocketEvent } from 'src/app/core/services/websocket.service';

declare var $: any;

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	private _dashboard: Dashboard;
	private _grid: ElementRef;
	private _gridFull: boolean;
	private _editable = false;
	private _swapWidgets = true;
	private _showGrid = false;
	private _highlightNextPosition = false;
	private _rows = 80;
	private _cols = 120;
	private _refresh: Subject<void> = new Subject();

	constructor(private _dashboardsService: DashboardsService) {}

	get grid(): ElementRef {
		return this._grid;
	}

	@ViewChild('grid', { read: ElementRef })
	set grid(grid: ElementRef) {
		this._grid = grid;
	}

	get gridFull(): boolean {
		return this._gridFull;
	}

	get editable(): boolean {
		return this._editable;
	}

	get swapWidgets(): boolean {
		return this._swapWidgets;
	}

	get showGrid(): boolean {
		return this._showGrid;
	}

	get highlightNextPosition(): boolean {
		return this._highlightNextPosition;
	}

	get rows(): number {
		return this._rows;
	}

	get cols(): number {
		return this._cols;
	}

	get refresh(): Observable<void> {
		return this._refresh;
	}

	get dashboard(): Dashboard {
		return this._dashboard;
	}

	get getToolbar() {
		return JSON.parse(localStorage.getItem('toolbar')) === 'left';
	}

	private initSemanticConfig(): void {}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		this._dashboardsService.selectedDashboard
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((dashboard) => (this._dashboard = dashboard))
			)
			.subscribe();

		this._dashboardsService.editable
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((editable) => (this._editable = editable))
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onContextMenu(event): void {}

	onDrawn(event): void {}

	onWidgetChange(event): void {}

	onGridFull(event): void {
		this._gridFull = !!event;
		this._dashboardsService.gridFull.next(!!event);
	}

	onPositionChange(event, widget: Widget): void {
		const { top, left, width, height } = event;
		if (
			!isNullOrUndefined(widget) &&
			(widget.position.left !== top || widget.position.top !== left || widget.position.width !== width || widget.position.height !== height)
		) {
			// nowe położenie komponentu
			widget.position.left = left;
			widget.position.top = top;
			widget.position.width = width;
			widget.position.height = height;

			this._refresh.next();
		}
	}
}
