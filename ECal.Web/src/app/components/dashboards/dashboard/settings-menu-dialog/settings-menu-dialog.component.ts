import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';

import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';

import { Dashboard } from 'src/app/shared/models/dashboard.model';
import { User } from 'src/app/shared/models/user.model';

declare var $: any;

@Component({
	selector: 'app-settings-menu-dialog',
	templateUrl: './settings-menu-dialog.component.html',
	styleUrls: []
})
export class SettingsMenuDialogComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	private _dashboards: Array<Dashboard> = [];
	private _userLogged: User;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _tdLoadingService: TdLoadingService,
		private _translateService: TranslateService,
		private _dashboardsService: DashboardsService,
		private _dialogRef: DialogOverlayRef,
		private _accountService: AccountService
	) {}

	get dashboards(): Array<Dashboard> {
		return this._dashboards;
	}

	set dashboards(dashboards: Array<Dashboard>) {
		this._dashboards = dashboards;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	private initSemanticConfig(): void {}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		this._dashboardsService.dashboards
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((dashboards) => (this._dashboards = dashboards))
			)
			.subscribe();

		$('.modal-settings-menu')
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onHidden: () => {
					$('.modal-settings-menu').remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onDrop(event: CdkDragDrop<Dashboard[]>): void {
		moveItemInArray(this._dashboards, event.previousIndex, event.currentIndex);
		this._dashboards = this._dashboards.filter((item) => item);
		this._dashboardsService.changeDashboards(this._dashboards);
	}

	onChangeMenu(dashboard: Dashboard): void {
		dashboard.favourite = dashboard.favourite === 'show' ? 'hide' : 'show';
		this._dashboardsService.update(dashboard).subscribe();
		this._dashboards = this._dashboards.filter((item) => item);
		this._dashboardsService.changeDashboards(this._dashboards);
	}
}
