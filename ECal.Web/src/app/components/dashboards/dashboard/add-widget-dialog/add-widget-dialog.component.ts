import { Component, OnInit, OnDestroy, Inject, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AccountService } from 'src/app/core/services/account.service';
import { WidgetsService } from 'src/app/core/services/widgets.service';

import { Widget } from 'src/app/shared/models/widget.model';
import { User } from 'src/app/shared/models/user.model';
import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';
import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { Segment, Step } from 'src/app/shared/models/step.model';
import { ResourceType } from 'src/app/core/services/websocket.service';

declare var $: any;

const MODALNAME = '.modal-add-widget';

@Component({
	selector: 'app-add-widget-dialog',
	templateUrl: './add-widget-dialog.component.html'
})
export class AddWidgetDialogComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _formModeType: FormModeType;
	private _steps: Array<Step>;
	private _loading = false;
	private _widgets: Array<Widget>;
	private _userLogged: User;

	@ViewChild('form') form;

	constructor(
		private _widgetsService: WidgetsService,
		private _accountService: AccountService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA) public data: { formModeType: FormModeType }
	) {
		this._formModeType = data.formModeType;
	}

	get formConfig() {
		return {
			symbol: 'add-widget',
			broadcast: { type: ResourceType.WIDGETS }
		};
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get steps(): Step[] {
		return this._steps;
	}

	get loading(): boolean {
		return this._loading;
	}

	get widgets(): Array<Widget> {
		return this._widgets;
	}

	set widgets(widgets: Array<Widget>) {
		this._widgets = widgets;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		$(MODALNAME)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initSteps();
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeStep($event): void {
		this.form.changeStep($event);
	}

	onPreviousStep($event): void {
		this.form.previousStep($event);
	}

	onNextStep($event): void {
		this.form.nextStep($event);
	}

	onBackForm(): void {
		$(MODALNAME).modal('hide');
	}

	onSaveForm(): void {
		this.form.saveForm(
			(model) => this._widgetsService.create(model),
			() => $(MODALNAME).modal('hide')
		);
	}

	private initSteps(): void {
		const steps: Step[] = [];
		steps.push(
			this.createStep('Komponent', 'Nowy komponent.', 'widget-data', [
				{
					header: 'Dane komponentu',
					description: 'Uzupełnienie podstawowych informacji o komponencie.',
					fields: [
						{
							label: 'Komponent',
							name: 'widget',
							symbol: 'widget',
							type: FormFieldType.DICTIONARY_SINGLE,
							items: [],
							rules: [
								{
									type: 'empty',
									prompt: `Pole '{name}' nie może być puste.`
								}
							]
						},
						{
							label: 'Nazwa komponentu',
							name: 'name',
							symbol: 'name',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole '{name}' nie może być puste.`
								}
							]
						},
						{
							label: 'Opis komponentu',
							name: 'desc',
							symbol: 'desc',
							type: FormFieldType.TEXTAREA
						}
					]
				}
			])
		);
		this._steps = steps;
	}

	private createStep(label: string, description: string, symbol: string, segments: Segment[], completed: boolean = false): Step {
		const menu: Step = { label, description, symbol, segments, completed };
		return menu;
	}

	private initDialog(): void {
		switch (this._formModeType) {
			case FormModeType.ADD: {
				this._dialogConfig = {
					title: 'Nowy komponent',
					meta: 'dodawanie nowego komponentu do panelu'
				};
				break;
			}
		}
	}
}
