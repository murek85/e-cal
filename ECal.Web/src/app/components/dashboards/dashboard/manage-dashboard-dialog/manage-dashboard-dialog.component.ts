import { Dashboard } from 'src/app/shared/models/dashboard.model';
import { Component, OnInit, AfterViewInit, OnDestroy, Inject, ViewChild } from '@angular/core';
import { from, iif, of, Subject } from 'rxjs';
import { catchError, delay, filter, finalize, map, takeUntil, tap } from 'rxjs/operators';

import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AccountService } from 'src/app/core/services/account.service';

import { User } from 'src/app/shared/models/user.model';
import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';
import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { FormField, Segment, Step } from 'src/app/shared/models/step.model';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';

declare var $: any;

const MODALNAME = '.modal-manage-dashboard';

@Component({
	selector: 'app-manage-dashboard-dialog',
	templateUrl: './manage-dashboard-dialog.component.html'
})
export class ManageDashboardDialogComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _formModeType: FormModeType;
	private _steps: Array<Step>;
	private _selectedIndex = 0;
	private _dashboardId: string;
	private _tabType: 'data';
	private _dashboard: Dashboard;
	private _userLogged: User;
	private _loading = false;

	@ViewChild('form') form;

	constructor(
		private _dashboardsService: DashboardsService,
		private _accountService: AccountService,
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA) public data: { formModeType: FormModeType; dashboardId: string; tabType: 'data' }
	) {
		this._formModeType = data.formModeType;
		this._dashboardId = data.dashboardId;
		this._tabType = data.tabType;
	}

	get formConfig() {
		return {
			symbol: 'manage-dashboard',
			broadcast: { type: ResourceType.DASHBOARDS }
		};
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get steps(): Array<Step> {
		return this._steps;
	}

	get selectedIndex(): number {
		return this._selectedIndex;
	}

	get loading(): boolean {
		return this._loading;
	}

	get dashboard(): Dashboard {
		return this._dashboard;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		this._loading = true;
		this._dashboardsService.dashboard
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((data) => !!data),
				tap((dashboard) => (this._dashboard = dashboard)),
				tap((dashboard) => this.form.loadForm(dashboard))
			)
			.subscribe(() => (this._loading = false));

		$(MODALNAME)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initSteps();
					this.initDialog();
				},
				onHidden: () => {
					$(MODALNAME).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeTab($event): void {
		this.form.changeTab($event);
	}

	onBackForm(): void {
		$(MODALNAME).modal('hide');
	}

	onSaveForm(): void {
		this.form.saveForm(
			(model) => this._dashboardsService.update({ ...model, id: this._dashboardId }),
			() => $(MODALNAME).modal('hide')
		);
	}

	private initSteps(): void {
		const steps: Step[] = [];
		steps.push(
			this.createStep('Komponent', 'Nowy komponent.', 'widget-data', [
				{
					header: 'Dane panelu',
					description: 'Uzupełnienie podstawowych informacji o panelu.',
					fields: [
						{
							label: 'Nazwa panelu',
							name: 'name',
							symbol: 'name',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole '{name}' nie może być puste.`
								}
							]
						},
						{
							label: 'Opis panelu',
							name: 'desc',
							symbol: 'desc',
							type: FormFieldType.TEXTAREA
						}
					]
				}
			])
		);
		this._steps = steps;
	}

	private createStep(label: string, description: string, symbol: string, segments: Segment[], completed: boolean = false): Step {
		const menu: Step = { label, description, symbol, segments, completed };
		return menu;
	}

	private initDialog(): void {
		switch (this._formModeType) {
			case FormModeType.EDIT: {
				this._dialogConfig = {
					title: 'Edycja panela',
					meta: 'edytowanie panela'
				};

				switch (this._tabType) {
					case 'data':
						this._selectedIndex = 0;
						break;

					default:
						this._selectedIndex = 0;
						break;
				}

				// pobierz dane roli w trybie edycji
				this._dashboardsService.get(this._dashboardId);
				break;
			}
		}
	}
}
