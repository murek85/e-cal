import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { WidgetContainerComponent } from '../widgets/widget-container/widget-container.component';

import { SidebarPairComponent } from '../widgets/widget-container/sidebar-pair/sidebar-pair.component';

import { AddWidgetDialogComponent } from './add-widget-dialog/add-widget-dialog.component';
import { ManageWidgetDialogComponent } from './manage-widget-dialog/manage-widget-dialog.component';

import { ManageDashboardDialogComponent } from './manage-dashboard-dialog/manage-dashboard-dialog.component';
import { ManageDashboardsDialogComponent } from './manage-dashboards-dialog/manage-dashboards-dialog.component';

import { SettingsMenuDialogComponent } from './settings-menu-dialog/settings-menu-dialog.component';

import { FilterSettingsComponent } from './manage-widget-dialog/filter-settings/filter-settings.component';
import { ColumnSettingsComponent } from './manage-widget-dialog/column-settings/column-settings.component';

import { DashboardComponent } from './dashboard.component';

const APP_MODULES = [CoreModule.forRoot(), SharedModule.forRoot()];

const COMPONENTS = [DashboardComponent, WidgetContainerComponent];

const DIALOGS = [
	AddWidgetDialogComponent,
	ManageWidgetDialogComponent,
	ManageDashboardDialogComponent,
	ManageDashboardsDialogComponent,
	SettingsMenuDialogComponent,
	SidebarPairComponent,
	ColumnSettingsComponent,
	FilterSettingsComponent
];

@NgModule({
	declarations: [COMPONENTS, DIALOGS],
	imports: [APP_MODULES],
	exports: [COMPONENTS, DIALOGS],
	providers: [],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardModule {}
