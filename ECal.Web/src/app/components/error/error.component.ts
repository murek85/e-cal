import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Error } from 'src/app/shared/models/error.model';
import { DialogService } from 'src/app/core/services/dialog.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

declare var $: any;

@Component({
	selector: 'app-error',
	templateUrl: './error.component.html',
	styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
	private _unsubscribe$ = new Subject<void>();

	_error: Error;
	_returnUrl: string;

	_layout: string;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _dialogService: DialogService,
		private _dashboardsService: DashboardsService
	) {}

	get error(): Error {
		return this._error;
	}

	get layout(): string {
		return this._layout;
	}

	ngOnInit(): void {
		this._dashboardsService.layout
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((layout) => (this._layout = layout))
			)
			.subscribe();

		this._error = {
			type: this._route.snapshot.queryParams['type'],
			message: this._route.snapshot.queryParams['message']
		};
	}
}
