import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { AccountService } from 'src/app/core/services/account.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';

declare var $: any;

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	_error: any;
	_returnUrl: string;

	_userName: string;
	_password: string;

	_layout: string;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _tdLoadingService: TdLoadingService,
		private _translateService: TranslateService,
		private _dashboardsService: DashboardsService,
		private _dialogService: DialogService,
		private _accountService: AccountService,
		private _toastService: ToastService
	) {}

	get userName(): string {
		return this._userName;
	}

	set userName(userName: string) {
		this._userName = userName;
	}

	get password(): string {
		return this._password;
	}

	set password(password: string) {
		this._password = password;
	}

	get error(): any {
		return this._error;
	}

	get loginSocialProviders(): any[] {
		return this._accountService.loginSocialProviders;
	}

	get loginDomainProviders(): any[] {
		return this._accountService.loginDomainProviders;
	}

	get layout(): string {
		return this._layout;
	}

	private initSemanticConfig(): void {
		setTimeout((_) => {
			$('.ui.form-login').form({
				className: {
					label: 'ui basic red prompt label dock-r'
				},
				fields: {
					userName: {
						identifier: 'userName',
						rules: [
							{
								type: 'empty',
								prompt: `Pole nie może być puste.`
							}
						]
					},
					password: {
						identifier: 'password',
						rules: [
							{
								type: 'minLength[6]',
								prompt: `Pole wymaga minimum {ruleValue} znaków.`
							}
						]
					}
				},
				inline: true,
				on: 'blur',
				autoCheckRequired: true
			});
		});
	}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		this._dashboardsService.layout
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((layout) => (this._layout = layout))
			)
			.subscribe();

		if (this._accountService.getLoggedIn()) {
			this._router.navigate(['/dashboards']);
		} else {
			this._returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/dashboards';
		}
	}

	loginPasswordFlow(): void {
		this._error = null;

		if (!$('.ui.form-login').form('is valid')) {
			return;
		}

		this._tdLoadingService.register();
		this._accountService
			.loginPasswordFlow(this._userName, this._password)
			.then((result) => {
				this._router.navigateByUrl(this._returnUrl);
			})
			.catch(
				(err) => (
					(this._error = err),
					this._toastService.open('Błąd logowania', this._translateService.instant(err.message), { class: 'error', showIcon: 'exclamation circle' })
				)
			)
			.finally(() => this._tdLoadingService.resolve());
	}

	loginDomainFlow(): void {
		this._tdLoadingService.register();
		this._accountService
			.loginDomainFlow()
			.then((result) => {
				this._router.navigateByUrl(this._returnUrl);
			})
			.catch(
				(err) => (
					(this._error = err),
					this._toastService.open('Błąd logowania', this._translateService.instant(err.message), { class: 'error', showIcon: 'exclamation circle' })
				)
			)
			.finally(() => this._tdLoadingService.resolve());
	}

	loginAuthorizationCode(providerKey): void {
		this._error = null;
		this._accountService.loginAuthorizationCode(providerKey);
	}
}
