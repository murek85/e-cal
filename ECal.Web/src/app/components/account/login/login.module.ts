import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';

const APP_MODULES: any[] = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [LoginComponent],
	imports: [LoginRoutingModule, APP_MODULES],
	providers: []
})
export class LoginModule {}
