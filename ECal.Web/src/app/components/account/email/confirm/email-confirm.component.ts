import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { takeUntil, tap } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';
import { TranslateService } from '@ngx-translate/core';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { Subject } from 'rxjs';

declare var $: any;

@Component({
	selector: 'app-email-confirm',
	templateUrl: './email-confirm.component.html',
	styleUrls: ['./email-confirm.component.scss']
})
export class EmailConfirmComponent implements OnInit, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	_success: any;
	_error: any;

	_returnUrl: string;
	_system = { version: '1.0.0', date: '01-10-2020', type: 'dev' };

	_layout: string;

	constructor(
		private _route: ActivatedRoute,
		private _tdLoadingService: TdLoadingService,
		private _translateService: TranslateService,
		private _dashboardsService: DashboardsService,
		private _dialogService: DialogService,
		private _accountService: AccountService,
		private _toastService: ToastService
	) {
		console.log(_route);
	}

	get success(): any {
		return this._success;
	}

	get error(): any {
		return this._error;
	}

	get system(): any {
		return this._system;
	}

	get layout(): string {
		return this._layout;
	}

	private initSemanticConfig(): void {}

	ngAfterViewInit(): void {
		setTimeout((_) => this.initSemanticConfig());
	}

	ngOnInit(): void {
		const userId = this._route.snapshot.queryParamMap.get('userId');
		console.log(userId);

		this._dashboardsService.layout
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((layout) => (this._layout = layout))
			)
			.subscribe();
	}

	confirm(): void {
		this._error = null;
		this._success = null;

		if (!$('.ui.form-email').form('is valid')) {
			return;
		}

		this._tdLoadingService.register();
		const userId = this._route.snapshot.queryParams['userId'];
		const token = this._route.snapshot.queryParams['token'];
		this._accountService
			.confirmEmail(userId, token)
			.then((result) => {
				this._success = {
					message: 'Twoje nowe konto użytkownika zostało pomyślnie potwierdzone w aplikacji.'
				};
				this._toastService.open('Konto potwierdzone', this._translateService.instant(this._success.message), {
					class: 'success',
					showIcon: 'check circle'
				});
			})
			.catch(
				(err) => (
					(this._error = err),
					this._toastService.open('Błąd', this._translateService.instant(err.message), { class: 'error', showIcon: 'exclamation circle' })
				)
			)
			.finally(() => this._tdLoadingService.resolve());
	}
}
