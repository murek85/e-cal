import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxMAuthOidcService } from 'src/app/core/services/oidc/oidc.service';
import { AccountService } from 'src/app/core/services/account.service';

@Component({
	selector: 'app-auth',
	template: `<div class="mrk-auth"></div>`
})
export class AuthComponent implements OnInit {
	private _returnUrl: string;

	constructor(private _router: Router, private _route: ActivatedRoute, private _accountService: AccountService, private _oidcService: NgxMAuthOidcService) {}

	public ngOnInit(): void {
		this._returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/dashboards';

		this._oidcService.events.subscribe((e) => {
			switch (e.type) {
				case 'token_received':
				case 'token_refreshed': {
					this._accountService.getUserLogged().then((_) => this._router.navigateByUrl(this._returnUrl));
					break;
				}
			}
		});

		if (this._accountService.getLoggedIn()) {
			this._router.navigate(['/dashboards']);
		}
	}
}
