import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';

declare var $: any;

@Component({
	selector: 'app-code',
	templateUrl: './code.component.html',
	styleUrls: ['./code.component.scss']
})
export class CodeComponent implements OnInit, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	_success;
	_error;

	_code: string;

	_layout: string;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _tdLoadingService: TdLoadingService,
		private _translateService: TranslateService,
		private _dashboardsService: DashboardsService,
		private _dialogService: DialogService,
		private _accountService: AccountService
	) {}

	get code(): string {
		return this._code;
	}

	set code(code: string) {
		this._code = code;
	}

	get success(): any {
		return this._success;
	}

	get error(): any {
		return this._error;
	}

	get layout(): string {
		return this._layout;
	}

	private initSemanticConfig(): void {
		$('.ui.form-code').form({
			fields: {
				email: {
					identifier: 'code',
					rules: [
						{
							type: 'code',
							prompt: `Pole '{name}' wymaga wprowadzenia poprawnego kodu.`
						}
					]
				}
			}
		});
	}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		this._dashboardsService.layout
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((layout) => (this._layout = layout))
			)
			.subscribe();
	}

	activate(): void {
		this._success = this._error = null;

		this._tdLoadingService.register();
		const model = { email: 'marcinmur@gmail.com' };
		this._accountService
			.activateLogin2step(model)
			.then((result) => {
				this._success = {
					message: 'Pomyślnie wysłano wiadomość.'
				};
			})
			.catch((err) => (this._error = err))
			.finally(() => this._tdLoadingService.resolve());
	}

	confirm(): void {
		this._success = this._error = null;

		if (!$('.ui.form-code').form('is valid')) {
			return;
		}

		this._tdLoadingService.register();
		const model = { code: this._code };
		this._accountService
			.confirmLogin2step('e29221ad-7835-4fd9-929f-60982e5c3000', this._code)
			.then((result) => {
				this._success = {
					message: 'Pomyślnie zweryfikowano kod.'
				};
			})
			.catch((err) => (this._error = err))
			.finally(() => this._tdLoadingService.resolve());
	}
}
