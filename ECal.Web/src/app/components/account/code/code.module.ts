import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { CodeComponent } from './code.component';
import { CodeRoutingModule } from './code-routing.module';

const APP_MODULES: any[] = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [CodeComponent],
	imports: [CodeRoutingModule, APP_MODULES],
	providers: []
})
export class CodeModule {}
