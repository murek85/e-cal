import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { AccountService } from 'src/app/core/services/account.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';
import { User } from 'src/app/shared/models/user.model';

declare var $: any;

@Component({
	selector: 'app-lock',
	templateUrl: './lock.component.html',
	styleUrls: ['./lock.component.scss']
})
export class LockComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	private _userLogged: User;

	_error: any;
	_returnUrl: string;

	_userName: string;
	_password: string;

	_layout: string;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _tdLoadingService: TdLoadingService,
		private _translateService: TranslateService,
		private _dashboardsService: DashboardsService,
		private _dialogService: DialogService,
		private _accountService: AccountService,
		private _toastService: ToastService
	) {}

	get userName(): string {
		return this._userName;
	}

	set userName(userName: string) {
		this._userName = userName;
	}

	get password(): string {
		return this._password;
	}

	set password(password: string) {
		this._password = password;
	}

	get error(): any {
		return this._error;
	}

	get getLoggedIn(): boolean {
		return this._accountService.getLoggedIn();
	}

	get userLogged(): User {
		return this._userLogged;
	}

	get layout(): string {
		return this._layout;
	}

	private initSemanticConfig(): void {
		setTimeout((_) => {
			$('.ui.form-login').form({
				className: {
					label: 'ui basic red prompt label dock-r'
				},
				fields: {
					password: {
						identifier: 'password',
						rules: [
							{
								type: 'minLength[6]',
								prompt: `Pole wymaga minimum {ruleValue} znaków.`
							}
						]
					}
				},
				inline: true,
				on: 'blur',
				autoCheckRequired: true
			});
		});
	}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		this._dashboardsService.layout
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((layout) => (this._layout = layout))
			)
			.subscribe();

		this._returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/dashboards';

		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		if (!this._accountService.getLoggedIn()) {
			this._router.navigateByUrl(this._returnUrl);
		}
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	public unlock() {
		this._error = null;

		if (!$('.ui.form-login').form('is valid')) {
			return;
		}

		this._tdLoadingService.register();
		this._accountService
			.loginPasswordFlow(this._userLogged.userName, this._password)
			.then((result) => {
				this._accountService.unlock(this._returnUrl);
			})
			.catch(
				(err) => (
					(this._error = err), this._toastService.open('Błąd', this._translateService.instant(err.message), { class: 'error', showIcon: 'info' })
				)
			)
			.finally(() => this._tdLoadingService.resolve());
	}
}
