import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { AccountService } from 'src/app/core/services/account.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';

declare var $: any;

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	_success;
	_error;
	_returnUrl: string;

	_email: string;
	_newPassword: string;
	_repeatPassword: string;

	_layout: string;

	constructor(
		private _tdLoadingService: TdLoadingService,
		private _translateService: TranslateService,
		private _dashboardsService: DashboardsService,
		private _dialogService: DialogService,
		private _accountService: AccountService,
		private _toastService: ToastService
	) {}

	get email(): string {
		return this._email;
	}

	set email(email: string) {
		this._email = email;
	}

	get newPassword(): string {
		return this._newPassword;
	}

	set newPassword(newPassword: string) {
		this._newPassword = newPassword;
	}

	get repeatPassword(): string {
		return this._repeatPassword;
	}

	set repeatPassword(repeatPassword: string) {
		this._repeatPassword = repeatPassword;
	}

	get success(): any {
		return this._success;
	}

	get error(): any {
		return this._error;
	}

	get layout(): string {
		return this._layout;
	}

	private initSemanticConfig(): void {
		$('.ui.form-register').form({
			className: {
				label: 'ui basic red prompt label dock-r'
			},
			fields: {
				email: {
					identifier: 'email',
					rules: [
						{
							type: 'email',
							prompt: `Pole wymaga adresu e-mail.`
						}
					]
				},
				newPassword: {
					identifier: 'newPassword',
					rules: [
						{
							type: 'minLength[6]',
							prompt: `Pole wymaga minimum {ruleValue} znaków.`
						}
					]
				},
				repeatPassword: {
					identifier: 'repeatPassword',
					rules: [
						{
							type: 'minLength[6]',
							prompt: `Pole wymaga minimum {ruleValue} znaków.`
						}
					]
				},
				agree: {
					identifier: 'agree',
					rules: [
						{
							type: 'checked',
							prompt: `Pole należy zaakceptować.`
						}
					]
				}
			},
			inline: true,
			on: 'blur',
			autoCheckRequired: true
		});
	}

	ngAfterViewInit(): void {
		setTimeout((_) => this.initSemanticConfig());
	}

	ngOnInit(): void {
		this._dashboardsService.layout
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((layout) => (this._layout = layout))
			)
			.subscribe();
	}

	register(): void {
		this._success = this._error = null;

		if (!$('.ui.form-register').form('is valid')) {
			return;
		}

		const model = {
			userName: this._email,
			email: this._email,
			newPassword: this._newPassword,
			repeatPassword: this._repeatPassword
		};

		this._tdLoadingService.register();
		this._accountService
			.registerAccount(model)
			.then((result) => {
				this._success = {
					message:
						'Pomyślnie utworzono Twoje nowe konto użytkownika w aplikacji. Na podany adres e-mail została wysłana wiadomość z linkiem umożliwiającym potwierdzenie utworzenia konta.'
				};
				this._toastService.open('Rejestracja konta', this._translateService.instant(this._success.message), {
					class: 'success',
					showIcon: 'check circle'
				});
			})
			.catch(
				(err) => (
					(this._error = err),
					this._toastService.open('Błąd', this._translateService.instant(err.message), { class: 'error', showIcon: 'exclamation circle' })
				)
			)
			.finally(() => this._tdLoadingService.resolve());
	}
}
