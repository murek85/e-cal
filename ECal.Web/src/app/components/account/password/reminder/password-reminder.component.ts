import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';

declare var $: any;

@Component({
	selector: 'app-password-reminder',
	templateUrl: './password-reminder.component.html',
	styleUrls: ['./password-reminder.component.scss']
})
export class PasswordReminderComponent implements OnInit, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	_success;
	_error;

	_email: string;

	_layout: string;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _tdLoadingService: TdLoadingService,
		private _translateService: TranslateService,
		private _dashboardsService: DashboardsService,
		private _dialogService: DialogService,
		private _accountService: AccountService,
		private _toastService: ToastService
	) {}

	get email(): string {
		return this._email;
	}

	set email(email: string) {
		this._email = email;
	}

	get success(): any {
		return this._success;
	}

	get error(): any {
		return this._error;
	}

	get layout(): string {
		return this._layout;
	}

	private initSemanticConfig(): void {
		$('.ui.form-password-reminder').form({
			className: {
				label: 'ui basic red prompt label dock-r'
			},
			fields: {
				email: {
					identifier: 'email',
					rules: [
						{
							type: 'email',
							prompt: `Pole wymaga adresu e-mail.`
						}
					]
				}
			},
			inline: true,
			on: 'blur',
			autoCheckRequired: true
		});
	}

	ngAfterViewInit(): void {
		setTimeout((_) => this.initSemanticConfig());
	}

	ngOnInit(): void {
		this._dashboardsService.layout
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((layout) => (this._layout = layout))
			)
			.subscribe();
	}

	reminder(): void {
		this._success = this._error = null;

		if (!$('.ui.form-password-reminder').form('is valid')) {
			return;
		}

		this._tdLoadingService.register();
		const model = { email: this._email };
		this._accountService
			.reminderPassword(model)
			.then((result) => {
				this._success = {
					message: 'Pomyślnie wysłano wiadomość z linkiem resetującym hasło do konta użytkownika.'
				};
				this._toastService.open('Przypomnienie hasła', this._translateService.instant(this._success.message), {
					class: 'success',
					showIcon: 'check circle'
				});
			})
			.catch(
				(err) => (
					(this._error = err),
					this._toastService.open('Błąd', this._translateService.instant(err.message), { class: 'error', showIcon: 'exclamation circle' })
				)
			)
			.finally(() => this._tdLoadingService.resolve());
	}
}
