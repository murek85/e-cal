import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { AccountService } from 'src/app/core/services/account.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ToastService } from 'src/app/core/services/toast.service';

declare var $: any;

@Component({
	selector: 'app-password-confirm',
	templateUrl: './password-confirm.component.html',
	styleUrls: ['./password-confirm.component.scss']
})
export class PasswordConfirmComponent implements OnInit, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	_success;
	_error;
	_returnUrl: string;

	_newPassword: string;
	_repeatPassword: string;

	_layout: string;

	constructor(
		private _route: ActivatedRoute,
		private _tdLoadingService: TdLoadingService,
		private _translateService: TranslateService,
		private _dashboardsService: DashboardsService,
		private _dialogService: DialogService,
		private _accountService: AccountService,
		private _toastService: ToastService
	) {}

	get newPassword(): string {
		return this._newPassword;
	}

	set newPassword(newPassword: string) {
		this._newPassword = newPassword;
	}

	get repeatPassword(): string {
		return this._repeatPassword;
	}

	set repeatPassword(repeatPassword: string) {
		this._repeatPassword = repeatPassword;
	}

	get success(): any {
		return this._success;
	}

	get error(): any {
		return this._error;
	}

	get layout(): string {
		return this._layout;
	}

	private initSemanticConfig(): void {
		$('.ui.form-password-confirm').form({
			className: {
				label: 'ui basic red prompt label dock-r'
			},
			fields: {
				newPassword: {
					identifier: 'newPassword',
					rules: [
						{
							type: 'minLength[6]',
							prompt: `Pole wymaga minimum {ruleValue} znaków.`
						}
					]
				},
				repeatPassword: {
					identifier: 'repeatPassword',
					rules: [
						{
							type: 'minLength[6]',
							prompt: `Pole wymaga minimum {ruleValue} znaków.`
						}
					]
				}
			},
			inline: true,
			on: 'blur',
			autoCheckRequired: true
		});
	}

	ngAfterViewInit(): void {
		setTimeout((_) => this.initSemanticConfig());
	}

	ngOnInit(): void {
		this._dashboardsService.layout
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((layout) => (this._layout = layout))
			)
			.subscribe();
	}

	confirm(): void {
		this._success = this._error = null;

		if (!$('.ui.form-password-confirm').form('is valid')) {
			return;
		}

		this._tdLoadingService.register();
		const userId = this._route.snapshot.queryParams['userId'];
		const token = this._route.snapshot.queryParams['token'];
		const model = {
			newPassword: this._newPassword,
			repeatPassword: this._repeatPassword
		};
		this._accountService
			.confirmPassword(model, userId, token)
			.then((result) => {
				this._success = {
					message: 'Twoje nowe hasło do konta użytkownika zostało pomyślnie zmienione.'
				};
				this._toastService.open('Zmieniono hasło', this._translateService.instant(this._success.message), {
					class: 'success',
					showIcon: 'check circle'
				});
			})
			.catch(
				(err) => (
					(this._error = err),
					this._toastService.open('Błąd', this._translateService.instant(err.message), { class: 'error', showIcon: 'exclamation circle' })
				)
			)
			.finally(() => this._tdLoadingService.resolve());
	}
}
