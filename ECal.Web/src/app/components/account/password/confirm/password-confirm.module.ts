import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { PasswordConfirmComponent } from './password-confirm.component';
import { PasswordConfirmRoutingModule } from './password-confirm-routing.module';

const APP_MODULES: any[] = [CoreModule.forRoot(), SharedModule.forRoot()];

@NgModule({
	declarations: [PasswordConfirmComponent],
	imports: [PasswordConfirmRoutingModule, APP_MODULES],
	providers: []
})
export class PasswordConfirmModule {}
