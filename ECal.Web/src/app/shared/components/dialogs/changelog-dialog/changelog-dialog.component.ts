import { Component, OnInit, AfterViewInit, OnDestroy, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';

import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';
import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';

declare var $: any;

@Component({
	selector: ' app-changelog-dialog',
	templateUrl: './changelog-dialog.component.html'
})
export class ChangelogDialogComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;

	constructor(private _dialogRef: DialogOverlayRef, @Inject(DIALOG_MODAL_DATA) public data: {}) {}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get changes() {
		return [
			{
				version: '1.0.6',
				date: '06.10.2021',
				items: [
					`Moduł 'Aplikacje' - zarządzanie aplikacjami w systemie.`,
					`Moduł 'Role i uprawnienia' - zarządzanie uprawnieniami w systemie.`,
					`Moduł 'Grupy zasobów' - zarządzanie zasobami.`
				]
			},
			{
				version: '1.0.5',
				date: '05.10.2021',
				items: [`Dodanie widgetu z grą 'Snake'.`]
			},
			{
				version: '1.0.4',
				date: '18.09.2021',
				items: [
					'Nowe style w aplikacji WIM, możliwość wyboru trybu ciemnego, nowa wersja menu bocznego.',
					'Nowe szablony strony logowania, rejestracji oraz przypomnij hasło.',
					'Rozwój komponentu tabeli - filtrowanie, stronicowanie.',
					'Mechanizm obsługi widgetów - dodawanie, edycja, usuwanie, tryb rozmieszczania.'
				]
			},
			{
				version: '1.0.3',
				date: '02.09.2021',
				items: ['Mechanizm parowania widgetów.', 'Mechanizm obsługi wewnętrznych komunikatów w aplikacji.']
			},
			{
				version: '1.0.2',
				date: '11.08.2021',
				items: ['Nowe widgety monitorowania parametrów CPU, RAM oraz dysków.']
			},
			{
				version: '1.0.1',
				date: '24.05.2021',
				items: [`Moduł 'Użytkownicy' - zarządzanie kontami użytkowników w systemie.`]
			},
			{
				version: '1.0.0',
				date: '05.10.2020',
				items: ['Uruchomienie nowej wersji WIM do zarządzania aplikacjami i użytkownikami.']
			}
		];
	}

	private initSemanticConfig(): void {}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		const dialog = $('.modal-changelog');
		dialog
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initDialog();
				},
				onHidden: () => {
					dialog.remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return true;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	private initDialog(): void {
		this._dialogConfig = {
			title: 'Changelog',
			meta: 'Historia wersji'
		};
	}
}
