import { Component, OnInit, AfterViewInit, OnDestroy, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';

import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';
import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { delay } from 'rxjs/operators';

declare var $: any;

@Component({
	selector: 'app-confirm-action-dialog',
	templateUrl: './confirm-action-dialog.component.html'
})
export class ConfirmActionDialogComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _loading = false;

	constructor(
		private _dialogRef: DialogOverlayRef,
		@Inject(DIALOG_MODAL_DATA)
		public data: {
			title: string;
			meta: string;
			description: string;
			iconClass: string;
			approveButtonLabel: string;
			approveButtonColorClass: string;
			denyButtonLabel: string;
			denyButtonColorClass: string;
		}
	) {
		this._dialogConfig = {
			title: data.title,
			meta: data.meta,
			description: data.description,
			iconClass: data.iconClass,
			approveButtonLabel: data.approveButtonLabel,
			approveButtonColorClass: data.approveButtonColorClass,
			denyButtonLabel: data.denyButtonLabel,
			debyButtonColorClass: data.denyButtonColorClass
		};
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get loading(): boolean {
		return this._loading;
	}

	private initSemanticConfig(): void {}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		const dialog = $('.modal-confirm-action');
		dialog
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initDialog();
				},
				onHidden: () => {
					dialog.remove();
					this._dialogRef.close();
					this._loading = false;
				},
				onDeny: () => {
					this._dialogRef.events.next({ type: 'deny' });
					return true;
				},
				onApprove: () => {
					this._loading = true;
					this._dialogRef.events.next({ type: 'accept' });
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	private initDialog(): void {}
}
