import { Component, OnInit, OnDestroy, Inject, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';

import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';

import { DialogOverlayRef } from 'src/app/core/services/dialog.service';
import { ResourceType } from 'src/app/core/services/websocket.service';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';

import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { Segment, Step } from 'src/app/shared/models/step.model';
import { User } from 'src/app/shared/models/user.model';

declare var $: any;

const MODALNAME = '.modal-contact';

@Component({
	selector: ' app-contact-dialog',
	templateUrl: './contact-dialog.component.html'
})
export class ContactDialogComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;
	private _steps: Step[];
	private _loading = false;
	private _userLogged: User;

	@ViewChild('form') form;

	constructor(private _dialogRef: DialogOverlayRef, @Inject(DIALOG_MODAL_DATA) public data: {}) {}

	get formConfig() {
		return {
			symbol: 'contact',
			broadcast: { type: ResourceType.ACCOUNT }
		};
	}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	get steps(): Step[] {
		return this._steps;
	}

	get loading(): boolean {
		return this._loading;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	ngOnInit(): void {
		const dialog = $('.modal-contact');
		dialog
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initSteps();
					this.initDialog();
				},
				onHidden: () => {
					dialog.remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return true;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	onChangeStep($event): void {
		this.form.changeStep($event);
	}

	onPreviousStep($event): void {
		this.form.previousStep($event);
	}

	onNextStep($event): void {
		this.form.nextStep($event);
	}

	onBackForm(): void {
		$(MODALNAME).modal('hide');
	}

	onSaveForm(): void {
		this.form.saveForm(
			(model) => console.log(model),
			() => $(MODALNAME).modal('hide')
		);
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	private initSteps(): void {
		const steps: Step[] = [];
		steps.push(
			this.createStep('Kontakt', 'Wyślij wiadomość', 'message-data', [
				{
					header: 'Kontakt',
					description: 'Wyślij wiadomość',
					fields: [
						{
							label: 'Temat',
							name: 'title',
							symbol: 'message-title',
							type: FormFieldType.TEXT,
							rules: [
								{
									type: 'empty',
									prompt: `Pole nie może być puste.`
								}
							]
						},
						{
							label: 'Wiadomość',
							name: 'text',
							symbol: 'message-text',
							type: FormFieldType.TEXTAREA
						}
					]
				}
			])
		);
		this._steps = steps;
	}

	private createStep(label: string, description: string, symbol: string, segments: Segment[], completed: boolean = false): Step {
		const menu: Step = { label, description, symbol, segments, completed };
		return menu;
	}

	private initDialog(): void {
		this._dialogConfig = {
			title: 'Kontakt',
			meta: 'Napisz do nas'
		};
	}
}
