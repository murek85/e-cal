import { Component, OnInit, AfterViewInit, OnDestroy, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';

import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AppConfig } from 'src/app/app.config';
import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';

declare var $: any;

@Component({
	selector: 'app-policy-dialog',
	templateUrl: './policy-dialog.component.html'
})
export class PolicyDialogComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	private _dialogConfig: DialogConfig;

	constructor(private _dialogRef: DialogOverlayRef, @Inject(DIALOG_MODAL_DATA) public data: {}) {}

	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	private initSemanticConfig(): void {}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		const dialog = $('.modal-policy');
		dialog
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {
					this.initDialog();
				},
				onHidden: () => {
					dialog.remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return true;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	private initDialog(): void {
		this._dialogConfig = {
			title: 'Polityka prywatności'
		};
	}
}
