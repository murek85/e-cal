import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { NgxGaugeComponent } from './gauge.component';

@NgModule({
	imports: [CommonModule],
	declarations: [NgxGaugeComponent],
	exports: [NgxGaugeComponent],
	bootstrap: [NgxGaugeComponent]
})
export class NgxGaugeModule {}
