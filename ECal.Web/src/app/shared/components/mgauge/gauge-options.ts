import { NgxGaugeType } from './gauge-type';
import { NgxGaugeCap } from './gauge-cap';

export const DEFAULTS = {
	MIN: 0,
	MAX: 100,
	TYPE: 'arch',
	THICK: 10,
	FOREGROUND_COLOR: '#1e88e5',
	BACKGROUND_COLOR: '#e4e4e4',
	FILL_COLOR: '#bbdefb',
	CAP: 'round',
	SIZE: 150,
	DURATION: 2500,
	SHOW_EXTREMUM: true
};

export class NgxGaugeOptions {
	type?: NgxGaugeType = DEFAULTS.TYPE as NgxGaugeType;
	cap?: NgxGaugeCap = DEFAULTS.CAP as NgxGaugeCap;
	size?: number = DEFAULTS.SIZE;
	thick?: number = DEFAULTS.THICK;
	foregroundColor?: string = DEFAULTS.FOREGROUND_COLOR;
	backgroundColor?: string = DEFAULTS.BACKGROUND_COLOR;
	fillColor?: string = DEFAULTS.FILL_COLOR;
	duration?: number = DEFAULTS.DURATION;
	showExtremum?: boolean = DEFAULTS.SHOW_EXTREMUM;
}
