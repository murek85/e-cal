import {
	Component,
	HostBinding,
	Input,
	SimpleChanges,
	ViewEncapsulation,
	Renderer2,
	AfterViewInit,
	ElementRef,
	OnChanges,
	OnDestroy,
	ViewChild
} from '@angular/core';

import { NgxGaugeType } from './gauge-type';
import { NgxGaugeOptions, DEFAULTS } from './gauge-options';

export function clamp(value: number, min: number, max: number): number {
	return Math.max(min, Math.min(max, value));
}
export function coerceBooleanProperty(value: any): boolean {
	return value != null && `${value}` !== 'false';
}
export function coerceNumberProperty(value: any, fallbackValue = 0): number {
	return isNaN(parseFloat(value)) || isNaN(Number(value)) ? fallbackValue : Number(value);
}
export function cssUnit(value: number) {
	return `${value}px`;
}
export function isNumber(value: string) {
	return value !== undefined && !isNaN(parseFloat(value)) && !isNaN(Number(value));
}

@Component({
	selector: 'app-gauge',
	templateUrl: './gauge.component.html',
	styleUrls: ['./gauge.component.scss']
})
export class NgxGaugeComponent implements AfterViewInit, OnChanges, OnDestroy {
	@HostBinding('attr.role') role = 'mgauge';
	@HostBinding('class.mgauge') gaugeMeterClass = true;
	@HostBinding('attr.aria-valuemin') ariaValueminAttr = 'min';
	@HostBinding('attr.aria-valuemax') ariaValuemaxAttr = 'max';
	@HostBinding('attr.aria-valuenow') ariaValueNowAttr = 'value';

	@ViewChild('canvas', { static: true }) _canvas: ElementRef;
	@ViewChild('rLabel', { static: true }) _label: ElementRef;
	@ViewChild('reading', { static: true }) _reading: ElementRef;

	private _initialized: boolean = false;
	private _context: CanvasRenderingContext2D;
	private _animationRequestID: number = 0;

	private _min: number = DEFAULTS.MIN;
	private _max: number = DEFAULTS.MAX;
	private _size: number = DEFAULTS.SIZE;
	private _animate: boolean = true;
	private _value: number = 0;

	@Input()
	get size(): number {
		return this._size;
	}
	set size(value: number) {
		this._size = coerceNumberProperty(value);
	}

	@Input()
	get animate(): boolean {
		return this._animate;
	}
	set animate(value) {
		this._animate = coerceBooleanProperty(value);
	}

	@Input()
	get min(): number {
		return this._min;
	}
	set min(value: number) {
		this._min = coerceNumberProperty(value, DEFAULTS.MIN);
	}

	@Input()
	get max(): number {
		return this._max;
	}
	set max(value: number) {
		this._max = coerceNumberProperty(value, DEFAULTS.MAX);
	}

	@Input() label: string;

	@Input() append: string;

	@Input() prepend: string;

	@Input() thresholds: object;

	@Input() options: NgxGaugeOptions;

	@Input()
	get value(): number {
		return this._value;
	}
	set value(val: number) {
		this._value = coerceNumberProperty(val);
	}

	@Input() duration: number = DEFAULTS.DURATION;

	@Input() showExtremum: boolean = DEFAULTS.SHOW_EXTREMUM;

	constructor(private _elementRef: ElementRef, private _renderer: Renderer2) {}

	ngOnChanges(changes: SimpleChanges): void {
		const isCanvasPropertyChanged = changes['thick'] || changes['type'] || changes['cap'] || changes['size'];
		const isDataChanged = changes['value'] || changes['min'] || changes['max'];

		if (this._initialized) {
			if (isDataChanged) {
				let nv;
				let ov;
				if (changes['value']) {
					nv = changes['value'].currentValue;
					ov = changes['value'].previousValue;
				}
				this._update(nv, ov);
			}

			if (isCanvasPropertyChanged) {
				this._destroy();
				this._init();
			}
		}
	}

	private _updateSize(): void {
		this._renderer.setStyle(this._elementRef.nativeElement, 'width', cssUnit(this._getWidth()));
		this._renderer.setStyle(this._elementRef.nativeElement, 'height', cssUnit(this._getCanvasHeight()));
		// this._canvas.nativeElement.width = this._getWidth();
		// this._canvas.nativeElement.height = this._getCanvasHeight();
		// this._renderer.setStyle(this._label.nativeElement,
		//     'transform', 'translateY(' + (this.options.size / 3 * 2 - this.options.size / 13 / 4) + 'px)');
		// this._renderer.setStyle(this._reading.nativeElement,
		//     'transform', 'translateY(' + (this.options.size / 2 - this.options.size * 0.22 / 2) + 'px)');
	}

	ngAfterViewInit(): void {
		if (this._canvas) {
			this._init();
		}
	}

	ngOnDestroy(): void {
		this._destroy();
	}

	private _getBounds(type: NgxGaugeType): { head; tail } {
		let head: number;
		let tail: number;
		switch (type) {
			case 'semi':
				head = Math.PI;
				tail = 2 * Math.PI;
				break;
			case 'full':
				head = 1.5 * Math.PI;
				tail = 3.5 * Math.PI;
				break;
			case 'arch':
				head = 0.8 * Math.PI;
				tail = 2.2 * Math.PI;
				break;
		}
		return { head, tail };
	}

	private _drawShell(start: number, middle: number, tail: number, color: string): void {
		const center = this._getCenter();
		const radius = this._getRadius();

		middle = Math.max(middle, start); // never below 0%
		middle = Math.min(middle, tail); // never exceed 100%

		if (this._initialized) {
			this._clear();

			this._context.beginPath();
			this._context.strokeStyle = this.options.backgroundColor;
			this._context.arc(center.x, center.y, radius, middle, tail, false);
			this._context.stroke();

			this._context.beginPath();
			this._context.strokeStyle = color;
			this._context.arc(center.x, center.y, radius, start, middle, false);
			this._context.stroke();

			this._context.beginPath();
			this._context.arc(center.x, center.y, radius - this.options.thick - 5, 0, 2 * Math.PI, false);
			this._context.globalAlpha = 0.2;
			this._context.fillStyle = this.options.fillColor ?? color;
			this._context.fill();
			this._context.globalAlpha = 1;
		}
	}

	private _clear(): void {
		if (this._context) {
			this._context.clearRect(0, 0, this._getWidth(), this._getHeight());
		}
	}

	private _getWidth(): number {
		return this.options.size;
	}

	private _getHeight(): number {
		return this.options.size;
	}

	// canvas height will be shorter for type 'semi' and 'arch'
	private _getCanvasHeight(): number {
		return this.options.type === 'arch' || this.options.type === 'semi' ? 0.85 * this._getHeight() : this._getHeight();
	}

	private _getRadius(): number {
		const center = this._getCenter();
		return center.x - this.options.thick;
	}

	private _getCenter(): { x: number; y: number } {
		const x = this._getWidth() / 2;
		const y = this._getHeight() / 2;
		return { x, y };
	}

	private _init(): void {
		this._context = (this._canvas.nativeElement as HTMLCanvasElement).getContext('2d');
		this._initialized = true;
		this._updateSize();
		this._setupStyles();
		this._create();
	}

	private _destroy(): void {
		if (this._animationRequestID) {
			window.cancelAnimationFrame(this._animationRequestID);
			this._animationRequestID = 0;
		}
		this._clear();
		this._context = null;
		this._initialized = false;
	}

	private _setupStyles(): void {
		if (this._context) {
			this._context.canvas.width = this.options.size;
			this._context.canvas.height = this.options.size;
			this._context.lineCap = this.options.cap;
			this._context.lineWidth = this.options.thick;
		}
	}

	private _getForegroundColorByRange(value: number): string {
		if (this.thresholds) {
			const match = Object.keys(this.thresholds)
				.filter((item) => isNumber(item) && Number(item) <= value)
				.sort((a, b) => Number(a) - Number(b))
				.reverse()[0];

			return match !== undefined ? this.thresholds[match].color || this.options.foregroundColor : this.options.foregroundColor;
		} else {
			return this.options.foregroundColor;
		}
	}

	private _create(nv?: number, ov?: number): void {
		const self = this;
		const type = this.options.type;
		const bounds = this._getBounds(type);
		const duration = this.duration;
		const min = this.min;
		const max = this.max;
		const value = clamp(this.value, this.min, this.max);
		const start = bounds.head;
		const unit = (bounds.tail - bounds.head) / (max - min);
		let displacement = unit * (value - min);
		const tail = bounds.tail;
		const color = this._getForegroundColorByRange(value);
		let startTime: any;

		if (self._animationRequestID) {
			window.cancelAnimationFrame(self._animationRequestID);
		}

		const animate = (timestamp: any) => {
			timestamp = timestamp || new Date().getTime();
			const runtime = timestamp - startTime;
			const progress = Math.min(runtime / duration, 1);
			const previousProgress = ov ? (ov - min) * unit : 0;
			const middle = start + previousProgress + displacement * progress;

			self._drawShell(start, middle, tail, color);
			if (self._animationRequestID && runtime < duration) {
				self._animationRequestID = window.requestAnimationFrame((ts) => animate(ts));
			} else {
				window.cancelAnimationFrame(self._animationRequestID);
			}
		};

		if (this._animate) {
			if (nv !== undefined && ov !== undefined) {
				displacement = unit * nv - unit * ov;
			}
			self._animationRequestID = window.requestAnimationFrame((timestamp) => {
				startTime = timestamp || new Date().getTime();
				animate(startTime);
			});
		} else {
			self._drawShell(start, start + displacement, tail, color);
		}
	}

	private _update(nv: number, ov: number): void {
		this._clear();
		this._create(nv, ov);
	}
}
