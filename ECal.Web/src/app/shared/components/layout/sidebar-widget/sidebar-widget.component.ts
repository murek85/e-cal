import { Component, OnInit, AfterViewInit, Input, ChangeDetectorRef, Output, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService, DefaultLangChangeEvent } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { isNullOrUndefined } from '../../../../core/tools';

import { AccountService } from 'src/app/core/services/account.service';
import { AppService } from 'src/app/core/services/app.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';

import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';

import { Dashboard } from 'src/app/shared/models/dashboard.model';
import { User } from 'src/app/shared/models/user.model';
import { Widget } from 'src/app/shared/models/widget.model';

declare var $: any;

@Component({
	selector: 'app-sidebar-widget',
	templateUrl: './sidebar-widget.component.html',
	styleUrls: ['./sidebar-widget.component.scss']
})
export class SidebarWidgetComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	private _dashboard: Dashboard;
	private _gridFull: boolean;
	private _editable = false;

	private _userLogged: User;

	constructor(private _dashboardsService: DashboardsService, private _accountService: AccountService) {}

	get dashboard() {
		return this._dashboard;
	}

	set dashboard(dashboard: Dashboard) {
		this._dashboard = dashboard;
	}

	get gridFull(): boolean {
		return this._gridFull;
	}

	get editable(): boolean {
		return this._editable;
	}

	private initSemanticConfig(): void {}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		this._dashboardsService.selectedDashboard
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((dashboard) => (this._dashboard = dashboard))
			)
			.subscribe();

		this._dashboardsService.gridFull
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((gridFull) => (this._gridFull = gridFull))
			)
			.subscribe();

		this._dashboardsService.editable
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((editable) => (this._editable = editable))
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	private initWidgetsSettings(): void {
		const storage = JSON.parse(localStorage.getItem(`widgets_${this._dashboard?.type}`));
		this._dashboard.widgets = !isNullOrUndefined(storage) ? storage : this._dashboard.widgets;
	}

	private changeWidgetsSettings(): void {
		localStorage.setItem(`widgets_${this._dashboard?.type}`, JSON.stringify(this._dashboard.widgets));
	}

	changeWidgetSettings(widget: Widget): void {
		widget.active = !widget.active;
		this._dashboard.widgets = [...this._dashboard.widgets];
	}

	changeEditable(): void {
		this._editable = !this._editable;
		this._dashboardsService.editable.next(this._editable);
	}
}
