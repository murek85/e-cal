import { Component, OnInit, AfterViewInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService, DefaultLangChangeEvent } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { filter, map, takeUntil, tap } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';

import { DashboardType } from 'src/app/shared/enums/dashboard-type.enum';

import { Dashboard } from 'src/app/shared/models/dashboard.model';
import { User } from 'src/app/shared/models/user.model';

import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';

import { ConfirmActionDialogComponent } from '../../dialogs/confirm-action-dialog/confirm-action-dialog.component';
import { ProfileDialogComponent } from '../../dialogs/profile-dialog/profile-dialog.component';
import { ProblemDialogComponent } from '../../dialogs/problem-dialog/problem-dialog.component';

import { ManageDashboardsDialogComponent } from '../../../../components/dashboards/dashboard/manage-dashboards-dialog/manage-dashboards-dialog.component';
import { SettingsMenuDialogComponent } from '../../../../components/dashboards/dashboard/settings-menu-dialog/settings-menu-dialog.component';
import { ChangelogDialogComponent } from '../../dialogs/changelog-dialog/changelog-dialog.component';

declare var $: any;

@Component({
	selector: 'app-toolbar-menu',
	templateUrl: './toolbar-menu.component.html',
	styleUrls: ['./toolbar-menu.component.scss']
})
export class ToolbarMenuComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	private _dashboards: Array<Dashboard> = [];
	private _selected: Dashboard;

	private _userLogged: User;

	_languages: any[] = AppConfig.settings.i18n.availableLanguages;
	_notifications: any[] = [
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-bell red', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() }
	];

	dashboardType: typeof DashboardType = DashboardType;

	constructor(
		private _router: Router,
		private _tdLoadingService: TdLoadingService,
		private _translateService: TranslateService,
		private _dialogService: DialogService,
		private _dashboardsService: DashboardsService,
		private _accountService: AccountService
	) {}

	get dashboards(): Array<Dashboard> {
		return this._dashboards;
	}

	set dashboards(dashboards: Array<Dashboard>) {
		this._dashboards = dashboards;
	}

	get notifications() {
		return this._notifications;
	}

	get toolbars(): string[] {
		return this._dashboardsService.toolbars;
	}

	get languages() {
		return this._languages;
	}

	get selected(): Dashboard {
		return this._selected;
	}

	@Input()
	set selected(dashboard: Dashboard) {
		this._selected = dashboard;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	get themes(): string[] {
		return this._dashboardsService.themes;
	}

	get defaultTheme(): string {
		return this._dashboardsService.defaultTheme;
	}

	get dark(): boolean {
		return this._dashboardsService.darkTheme;
	}

	set dark(value: boolean) {
		this._dashboardsService.darkTheme = value;
	}

	private initSemanticConfig(): void {
		setTimeout((_) => {
			$('.mrk-toolbar .dropdown-toolbar').dropdown({
				on: 'click',
				onChange: (value, text, $selectedItem) => {
					localStorage.setItem('toolbar', JSON.stringify(value));
					this._dashboardsService.toolbar.next(value);
				}
			});

			$('.mrk-toolbar .dropdown-settings').dropdown({
				on: 'click'
			});

			$('.mrk-toolbar .dropdown-notifications').dropdown({
				on: 'click'
			});

			this._dashboardsService.defaultTheme = JSON.parse(localStorage.getItem('style'));
			$('.mrk-toolbar .dropdown-themes').dropdown({
				on: 'click',
				onChange: (value, text, $selectedItem) => {
					localStorage.setItem('style', JSON.stringify(value));
					this._dashboardsService.theme.next(value);
					this._dashboardsService.defaultTheme = value;
				}
			});

			$('.mrk-toolbar .dropdown-languages').dropdown({
				on: 'click',
				onChange: (value, text, $selectedItem) => {
					this._tdLoadingService.register();
					setTimeout(() => {
						localStorage.setItem('language', JSON.stringify(value));
						this._translateService.setDefaultLang(value);
						this._tdLoadingService.resolve();
					}, 500);
				}
			});

			this._dashboardsService.darkTheme = JSON.parse(localStorage.getItem('dark'));
			$('.mrk-toolbar .checkbox-dark-theme').checkbox({
				onChange: () => {
					localStorage.setItem('dark', JSON.stringify(this._dashboardsService.darkTheme));
					this._dashboardsService.theme.next(this._dashboardsService.defaultTheme);
				}
			});

			const toolbar = JSON.parse(localStorage.getItem('toolbar'));
			$('.mrk-toolbar .dropdown-toolbar').dropdown('set selected', toolbar == null ? AppConfig.settings.toolbar : toolbar);

			const style = JSON.parse(localStorage.getItem('style'));
			$('.mrk-toolbar .dropdown-themes').dropdown('set selected', style == null ? AppConfig.settings.themes.default : style);

			const language = JSON.parse(localStorage.getItem('language'));
			$('.mrk-toolbar .dropdown-languages').dropdown('set selected', language == null ? AppConfig.settings.i18n.defaultLanguage.code : language);

			const dark = JSON.parse(localStorage.getItem('dark'));
			$('.mrk-toolbar .checkbox-dark-theme').checkbox(dark ? 'set checked' : 'set unchecked');
		});
	}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		this._dashboardsService.theme
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((value) => (this._dashboardsService.defaultTheme = value))
			)
			.subscribe();

		this._dashboardsService.dashboards
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((dashboards) => (this._dashboards = dashboards))
			)
			.subscribe();

		this._dashboardsService.selectedDashboard
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((dashboard) => !!dashboard),
				tap((dashboard) => ((this._selected = dashboard), localStorage.setItem('dashboard', dashboard.type)))
			)
			.subscribe();

		this._dashboardsService
			.query()
			.pipe(
				map((_) => <DashboardType>localStorage.getItem('dashboard')),
				tap((type) => this.setSelected(type ?? DashboardType.DASHBOARD))
			)
			.subscribe();

		this._translateService.onDefaultLangChange
			.pipe(tap((params: DefaultLangChangeEvent) => $('.sidenav-menu .languages').dropdown('set selected', params.lang)))
			.subscribe();
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeDark(): void {
		this.dark = !this.dark;
		localStorage.setItem('dark', JSON.stringify(this._dashboardsService.darkTheme));
		this._dashboardsService.theme.next(this._dashboardsService.defaultTheme);
	}

	setSelected(type: DashboardType): void {
		const findDashboard = (dashboards: Dashboard[], type: DashboardType) => {
			for (const dashboard of dashboards) {
				if (dashboard.type === type) {
					return dashboard;
				}
				if (dashboard.subdashboards && dashboard.subdashboards.length > 0) {
					const findSubdashboard = findDashboard(dashboard.subdashboards, type);
					if (findSubdashboard) {
						return findSubdashboard;
					}
				}
			}
			return null;
		};

		this._selected = findDashboard(this._dashboards, type);
		this._dashboardsService.selectedDashboard.next(this._selected);
		this._dashboardsService.header.next({ title: this._selected?.name });
	}

	checkSelected(selected, type): boolean {
		return !!this._dashboards.find((dashboard) => dashboard.type === type).subdashboards.find((sub) => sub.type === selected);
	}

	checkFavourite(selected): boolean {
		const findDashboard = (dashboards: Dashboard[], type: DashboardType) => {
			return dashboards.find((dashboard) => {
				if (dashboard.type === type) {
					return dashboard;
				}
				if (dashboard.subdashboards && dashboard.subdashboards.length > 0) {
					return findDashboard(dashboard.subdashboards, type);
				}
			});
		};

		const dashboard = findDashboard(this._dashboards, selected);
		return dashboard?.favourite === 'hide';
	}

	modalSettingsMenu(): void {
		this._dialogService.open<SettingsMenuDialogComponent>(SettingsMenuDialogComponent);
	}

	modalManageDashboards(): void {
		this._dialogService.open<ManageDashboardsDialogComponent>(ManageDashboardsDialogComponent);
	}

	modalChangelog(): void {
		this._dialogService.open<ChangelogDialogComponent>(ChangelogDialogComponent);
	}

	problem(): void {
		this._dialogService.open<ProblemDialogComponent>(ProblemDialogComponent);
	}

	profile(tabType): void {
		this._dialogService
			.open<ProfileDialogComponent>(ProfileDialogComponent, {
				data: { formModeType: FormModeType.EDIT, userId: this._userLogged.id, tabType }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	lock(): void {
		this._accountService.lock(this._router.url);
	}

	logout(): void {
		this._dialogService
			.open<ConfirmActionDialogComponent>(ConfirmActionDialogComponent, {
				data: {
					title: 'Wylogowanie',
					description: 'Czy chcesz wylogować się z aplikacji?',
					iconClass: 'question circle outline',
					approveButtonLabel: 'Wyloguj',
					approveButtonColorClass: 'primary',
					denyButtonLabel: 'Anuluj'
				}
			})
			.events.pipe(
				filter((event) => !!event),
				filter((event) => event.type === 'accept'),
				tap(() => this._accountService.logout())
			)
			.subscribe();
	}
}
