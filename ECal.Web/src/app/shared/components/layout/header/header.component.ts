import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService, DefaultLangChangeEvent } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { AppService } from 'src/app/core/services/app.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';

import { DashboardType } from 'src/app/shared/enums/dashboard-type.enum';

import { HeaderConfig } from 'src/app/shared/models/header-config.model';
import { User } from 'src/app/shared/models/user.model';
import { Dashboard } from 'src/app/shared/models/dashboard.model';

import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';

import { ConfirmActionDialogComponent } from '../../dialogs/confirm-action-dialog/confirm-action-dialog.component';
import { ProfileDialogComponent } from '../../dialogs/profile-dialog/profile-dialog.component';

import { ProblemDialogComponent } from '../../dialogs/problem-dialog/problem-dialog.component';

declare var $: any;

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	_header: HeaderConfig;
	_dashboards: Array<Dashboard> = [];
	_notifications: any[] = [
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-bell red', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() },
		{ icon: 'mdi mdi-email-receive green', name: 'Zgłoszenie Murka', date: new Date() }
	];

	_userLogged: User;
	_languages: any[] = AppConfig.settings.i18n.availableLanguages;

	dashboardType: typeof DashboardType = DashboardType;

	@ViewChild('mheader') mheader: ElementRef;

	get isSizeHeader() {
		return this.mheader ? (this.mheader.nativeElement as HTMLElement).offsetWidth > 860 : true;
	}

	constructor(
		private _router: Router,
		private _tdLoadingService: TdLoadingService,
		private _translateService: TranslateService,
		private _dashboardsService: DashboardsService,
		private _dialogService: DialogService,
		private _accountService: AccountService
	) {}

	get userLogged() {
		return this._userLogged;
	}

	get header() {
		return this._header;
	}

	get notifications() {
		return this._notifications;
	}

	get toolbars(): string[] {
		return this._dashboardsService.toolbars;
	}

	get languages() {
		return this._languages;
	}

	get dashboards() {
		return this._dashboards;
	}

	set dashboards(dashboards: Array<Dashboard>) {
		this._dashboards = dashboards;
	}

	get themes(): string[] {
		return this._dashboardsService.themes;
	}

	get defaultTheme(): string {
		return this._dashboardsService.defaultTheme;
	}

	get dark(): boolean {
		return this._dashboardsService.darkTheme;
	}

	set dark(value: boolean) {
		this._dashboardsService.darkTheme = value;
	}

	get getToolbar() {
		return JSON.parse(localStorage.getItem('toolbar')) === 'left';
	}

	private initSemanticConfig(): void {
		setTimeout((_) => {
			$('.mrk-header .dropdown-toolbar').dropdown({
				on: 'click',
				onChange: (value, text, $selectedItem) => {
					localStorage.setItem('toolbar', JSON.stringify(value));
					this._dashboardsService.toolbar.next(value);
				}
			});

			$('.mrk-header .dropdown-settings').dropdown({
				on: 'click'
			});

			$('.mrk-header .dropdown-notifications').dropdown({
				on: 'click'
			});

			this._dashboardsService.defaultTheme = JSON.parse(localStorage.getItem('style'));
			$('.mrk-header .dropdown-themes').dropdown({
				on: 'click',
				onChange: (value, text, $selectedItem) => {
					localStorage.setItem('style', JSON.stringify(value));
					this._dashboardsService.theme.next(value);
					this._dashboardsService.defaultTheme = value;
				}
			});

			$('.mrk-header .dropdown-languages').dropdown({
				on: 'click',
				onChange: (value, text, $selectedItem) => {
					this._tdLoadingService.register();
					setTimeout(() => {
						localStorage.setItem('language', JSON.stringify(value));
						this._translateService.setDefaultLang(value);
						this._tdLoadingService.resolve();
					}, 500);
				}
			});

			this._dashboardsService.darkTheme = JSON.parse(localStorage.getItem('dark'));
			$('.mrk-header .checkbox-dark-theme').checkbox({
				onChange: () => {
					localStorage.setItem('dark', JSON.stringify(this._dashboardsService.darkTheme));
					this._dashboardsService.theme.next(this._dashboardsService.defaultTheme);
				}
			});

			const toolbar = JSON.parse(localStorage.getItem('toolbar'));
			$('.mrk-header .dropdown-toolbar').dropdown('set selected', toolbar == null ? AppConfig.settings.toolbar : toolbar);

			const style = JSON.parse(localStorage.getItem('style'));
			$('.mrk-header .dropdown-themes').dropdown('set selected', style == null ? AppConfig.settings.themes.default : style);

			const language = JSON.parse(localStorage.getItem('language'));
			$('.mrk-header .dropdown-languages').dropdown('set selected', language == null ? AppConfig.settings.i18n.defaultLanguage.code : language);

			const dark = JSON.parse(localStorage.getItem('dark'));
			$('.mrk-header .checkbox-dark-theme').checkbox(dark ? 'set checked' : 'set unchecked');
		});
	}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		this._dashboardsService.theme
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((value) => (this._dashboardsService.defaultTheme = value))
			)
			.subscribe();

		this._dashboardsService.header
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((header) => (this._header = header))
			)
			.subscribe();

		this._dashboardsService.dashboards
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((dashboards) => (this._dashboards = dashboards))
			)
			.subscribe();

		this._translateService.onDefaultLangChange
			.pipe(tap((params: DefaultLangChangeEvent) => $('.mrk-header .dropdown-languages').dropdown('set selected', params.lang)))
			.subscribe();
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	problem(): void {
		this._dialogService.open<ProblemDialogComponent>(ProblemDialogComponent);
	}

	profile(tabType): void {
		this._dialogService
			.open<ProfileDialogComponent>(ProfileDialogComponent, {
				data: { formModeType: FormModeType.EDIT, userId: this._userLogged.id, tabType }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	lock(): void {
		this._accountService.lock(this._router.url);
	}

	logout(): void {
		this._dialogService
			.open<ConfirmActionDialogComponent>(ConfirmActionDialogComponent, {
				data: {
					title: 'Wylogowanie',
					description: 'Czy chcesz wylogować się z aplikacji?',
					iconClass: 'question circle outline',
					approveButtonLabel: 'Wyloguj',
					approveButtonColorClass: 'primary',
					denyButtonLabel: 'Anuluj'
				}
			})
			.events.pipe(
				filter((event) => !!event),
				filter((event) => event.type === 'accept'),
				tap(() => this._accountService.logout())
			)
			.subscribe();
	}

	setSelected(type: DashboardType): void {
		const findDashboard = (dashboards: Dashboard[], type: DashboardType) => {
			for (const dashboard of dashboards) {
				if (dashboard.type === type) {
					return dashboard;
				}
				if (dashboard.subdashboards && dashboard.subdashboards.length > 0) {
					const findSubdashboard = findDashboard(dashboard.subdashboards, type);
					if (findSubdashboard) {
						return findSubdashboard;
					}
				}
			}
			return null;
		};

		const selected = findDashboard(this._dashboards, type);
		this._dashboardsService.selectedDashboard.next(selected);
		this._dashboardsService.header.next({ title: selected?.name });
	}
}
