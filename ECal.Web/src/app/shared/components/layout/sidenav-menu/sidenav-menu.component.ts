import { Component, OnInit, AfterViewInit, Input, ChangeDetectorRef, Output, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { TranslateService, DefaultLangChangeEvent } from '@ngx-translate/core';
import { Subject, Subscription } from 'rxjs';
import { filter, finalize, map, mapTo, takeUntil, tap } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';

import { DashboardType } from 'src/app/shared/enums/dashboard-type.enum';

import { Dashboard } from 'src/app/shared/models/dashboard.model';
import { Menu } from 'src/app/shared/models/menu.model';
import { User } from 'src/app/shared/models/user.model';

import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';

import { ManageDashboardsDialogComponent } from '../../../../components/dashboards/dashboard/manage-dashboards-dialog/manage-dashboards-dialog.component';
import { SettingsMenuDialogComponent } from '../../../../components/dashboards/dashboard/settings-menu-dialog/settings-menu-dialog.component';
import { ConfirmActionDialogComponent } from '../../dialogs/confirm-action-dialog/confirm-action-dialog.component';

import { ChangelogDialogComponent } from '../../dialogs/changelog-dialog/changelog-dialog.component';
import { ProblemDialogComponent } from '../../dialogs/problem-dialog/problem-dialog.component';
import { ProfileDialogComponent } from './../../dialogs/profile-dialog/profile-dialog.component';

declare var $: any;

@Component({
	selector: 'app-sidenav-menu',
	templateUrl: './sidenav-menu.component.html',
	styleUrls: ['./sidenav-menu.component.scss']
})
export class SidenavMenuComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	private _dashboards: Array<Dashboard> = [];
	private _selected: Dashboard;
	private _sizeSidenav = true;
	private _unlockSidenav = true;

	private _languages: any[] = AppConfig.settings.i18n.availableLanguages;
	private _userLogged: User;

	dashboardType: typeof DashboardType = DashboardType;

	constructor(
		private _router: Router,
		private _tdLoadingService: TdLoadingService,
		private _translateService: TranslateService,
		private _dialogService: DialogService,
		private _dashboardsService: DashboardsService,
		private _accountService: AccountService
	) {}

	get dashboards(): Array<Dashboard> {
		return this._dashboards;
	}

	set dashboards(dashboards: Array<Dashboard>) {
		this._dashboards = dashboards;
	}

	get unlockSidenav(): boolean {
		return this._unlockSidenav;
	}

	get sizeSidenav(): boolean {
		return this._sizeSidenav;
	}

	get selected(): Dashboard {
		return this._selected;
	}

	@Input()
	set selected(dashboard: Dashboard) {
		this._selected = dashboard;
	}

	get userLogged(): User {
		return this._userLogged;
	}

	get dark(): boolean {
		return this._dashboardsService.darkTheme;
	}

	set dark(value: boolean) {
		this._dashboardsService.darkTheme = value;
	}

	private initSemanticConfig(): void {
		setTimeout((_) => {
			$('.sidenav-menu .dropdown-menus').dropdown({
				on: 'click'
			});

			$('.dropdown-account').dropdown({
				on: 'click'
			});

			this._dashboardsService.defaultTheme = JSON.parse(localStorage.getItem('style'));
			$('.mrk-header .dropdown-themes').dropdown({
				on: 'click',
				onChange: (value, text, $selectedItem) => {
					switch (value) {
						case 'dark':
							localStorage.setItem('dark', JSON.stringify(this._dashboardsService.darkTheme));
							break;

						default:
							localStorage.setItem('style', JSON.stringify(value));
							this._dashboardsService.theme.next(value);
							this._dashboardsService.defaultTheme = value;
							break;
					}
				}
			});

			$('.mrk-header .dropdown-languages').dropdown({
				on: 'click',
				onChange: (value, text, $selectedItem) => {
					this._tdLoadingService.register();
					setTimeout(() => {
						localStorage.setItem('language', JSON.stringify(value));
						this._translateService.setDefaultLang(value);
						this._tdLoadingService.resolve();
					}, 500);
				}
			});

			const language = JSON.parse(localStorage.getItem('language'));
			$('.mrk-header .dropdown-languages').dropdown('set selected', language == null ? AppConfig.settings.i18n.defaultLanguage.code : language);
		});
	}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		this._accountService.userLogged
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((user) => (this._userLogged = user))
			)
			.subscribe();

		this._dashboardsService.unlockSidenav
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((unlockSidenav) => (this._unlockSidenav = unlockSidenav))
			)
			.subscribe();

		this._dashboardsService.sizeSidenav
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((sizeSidenav) => (this._sizeSidenav = sizeSidenav))
			)
			.subscribe();

		this._dashboardsService.dashboards
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((dashboards) => (this._dashboards = dashboards))
			)
			.subscribe();

		this._dashboardsService.selectedDashboard
			.pipe(
				takeUntil(this._unsubscribe$),
				filter((dashboard) => !!dashboard),
				tap((dashboard) => ((this._selected = dashboard), localStorage.setItem('dashboard', dashboard.type)))
			)
			.subscribe();

		this._dashboardsService
			.query()
			.pipe(
				map((_) => <DashboardType>localStorage.getItem('dashboard')),
				tap((type) => this.setSelected(type ?? DashboardType.DASHBOARD))
			)
			.subscribe();

		this._translateService.onDefaultLangChange
			.pipe(tap((params: DefaultLangChangeEvent) => $('.sidenav-menu .languages').dropdown('set selected', params.lang)))
			.subscribe();
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	onChangeDark(): void {
		this.dark = !this.dark;
		localStorage.setItem('dark', JSON.stringify(this._dashboardsService.darkTheme));
		this._dashboardsService.theme.next(this._dashboardsService.defaultTheme);
	}

	onChangeUnlockSidenav(): void {
		this._unlockSidenav = !this._unlockSidenav;
		this._dashboardsService.unlockSidenav.next(this._unlockSidenav);
	}

	onChangeSizeSidenav(): void {
		this._sizeSidenav = !this._sizeSidenav;
		this._dashboardsService.sizeSidenav.next(this._sizeSidenav);
	}

	setSelected(type: DashboardType): void {
		const findDashboard = (dashboards: Dashboard[], type: DashboardType) => {
			for (const dashboard of dashboards) {
				if (dashboard.type === type) {
					return dashboard;
				}
				if (dashboard.subdashboards && dashboard.subdashboards.length > 0) {
					const findSubdashboard = findDashboard(dashboard.subdashboards, type);
					if (findSubdashboard) {
						return findSubdashboard;
					}
				}
			}
			return null;
		};

		this._selected = findDashboard(this._dashboards, type);
		this._dashboardsService.selectedDashboard.next(this._selected);
		this._dashboardsService.header.next({ title: this._selected?.name });
	}

	checkSelected(selected, type): boolean {
		return !!this._dashboards.find((dashboard) => dashboard.type === type).subdashboards.find((sub) => sub.type === selected);
	}

	checkFavourite(selected): boolean {
		const findDashboard = (dashboards: Dashboard[], type: DashboardType) => {
			return dashboards.find((dashboard) => {
				if (dashboard.type === type) {
					return dashboard;
				}
				if (dashboard.subdashboards && dashboard.subdashboards.length > 0) {
					return findDashboard(dashboard.subdashboards, type);
				}
			});
		};

		const dashboard = findDashboard(this._dashboards, selected);
		return dashboard?.favourite === 'hide';
	}

	modalSettingsMenu(): void {
		this._dialogService.open<SettingsMenuDialogComponent>(SettingsMenuDialogComponent);
	}

	modalManageDashboards(): void {
		this._dialogService.open<ManageDashboardsDialogComponent>(ManageDashboardsDialogComponent);
	}

	modalChangelog(): void {
		this._dialogService.open<ChangelogDialogComponent>(ChangelogDialogComponent);
	}

	problem(): void {
		this._dialogService.open<ProblemDialogComponent>(ProblemDialogComponent);
	}

	profile(tabType): void {
		this._dialogService
			.open<ProfileDialogComponent>(ProfileDialogComponent, {
				data: { formModeType: FormModeType.EDIT, userId: this._userLogged.id, tabType }
			})
			.events.pipe(filter((event) => !!event))
			.subscribe();
	}

	lock(): void {
		this._accountService.lock(this._router.url);
	}

	logout(): void {
		this._dialogService
			.open<ConfirmActionDialogComponent>(ConfirmActionDialogComponent, {
				data: {
					title: 'Wylogowanie',
					description: 'Czy chcesz wylogować się z aplikacji?',
					iconClass: 'question circle outline',
					approveButtonLabel: 'Wyloguj',
					approveButtonColorClass: 'primary',
					denyButtonLabel: 'Anuluj'
				}
			})
			.events.pipe(
				filter((event) => !!event),
				filter((event) => event.type === 'accept'),
				tap(() => this._accountService.logout())
			)
			.subscribe();
	}
}
