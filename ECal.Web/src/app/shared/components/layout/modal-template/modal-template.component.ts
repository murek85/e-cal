import { Component, OnInit, OnDestroy, ViewChild, Inject, ElementRef, Input } from '@angular/core';
import { CdkStepper } from '@angular/cdk/stepper';
import { iif, of, Subject } from 'rxjs';
import { catchError, delay, finalize, map, takeUntil, tap } from 'rxjs/operators';

import { DIALOG_MODAL_DATA } from 'src/app/core/services/tokens/form-data.token';
import { DialogConfig } from 'src/app/shared/models/dialog-config.model';
import { DialogOverlayRef } from 'src/app/core/services/dialog.service';

import { AccountService } from 'src/app/core/services/account.service';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { FormModeType } from 'src/app/shared/enums/form-mode-type.enum';

import { Segment, Step } from 'src/app/shared/models/step.model';
import { User } from 'src/app/shared/models/user.model';
import { Application } from 'src/app/shared/models/application.model';
import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';

declare var $: any;

@Component({
	selector: 'app-modal-template',
	templateUrl: './modal-template.component.html'
})
export class ModalTemplateComponent implements OnInit, OnDestroy {
	private _unsubscribe$ = new Subject<void>();

	private _config;
	private _dialogConfig: DialogConfig;
	private _formModeType: FormModeType;

	constructor(private _dialogRef: DialogOverlayRef, @Inject(DIALOG_MODAL_DATA) public data: { formModeType: FormModeType }) {
		this._formModeType = data.formModeType;
	}

	@Input()
	get config() {
		return this._config;
	}

	set config(config) {
		this._config = config;
	}

	@Input()
	get dialogConfig(): DialogConfig {
		return this._dialogConfig;
	}

	set dialogConfig(dialogConfig) {
		this._dialogConfig = dialogConfig;
	}

	ngOnInit(): void {
		$(`.modal-${this._config.symbol}`)
			.modal({
				context: 'td-layout',
				transition: 'fade down',
				closable: false,
				inverted: false,
				blurring: false,
				onShow: () => {},
				onHidden: () => {
					$(`.modal-${this._config.symbol}`).remove();
					this._dialogRef.close();
				},
				onDeny: () => {
					return false;
				},
				onApprove: () => {
					return true;
				}
			})
			.modal('show');
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}
}
