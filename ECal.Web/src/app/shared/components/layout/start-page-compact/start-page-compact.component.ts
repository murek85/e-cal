import { AppConfig } from 'src/app/app.config';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AccountService } from 'src/app/core/services/account.service';
import { DashboardsService } from 'src/app/core/services/dashboards.service';
import { DialogService } from 'src/app/core/services/dialog.service';
import { RegulationsDialogComponent } from 'src/app/shared/components/dialogs/regulations-dialog/regulations-dialog.component';
import { CookieDialogComponent } from 'src/app/shared/components/dialogs/cookie-dialog/cookie-dialog.component';
import { PolicyDialogComponent } from 'src/app/shared/components/dialogs/policy-dialog/policy-dialog.component';
import { ContactDialogComponent } from 'src/app/shared/components/dialogs/contact-dialog/contact-dialog.component';

declare var $: any;

@Component({
	selector: 'app-start-page-compact',
	templateUrl: './start-page-compact.component.html',
	styleUrls: ['./start-page-compact.component.scss']
})
export class StartPageCompactComponent implements OnInit, AfterViewInit {
	_returnUrl: string;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _tdLoadingService: TdLoadingService,
		private _translateService: TranslateService,
		private _dashboardsService: DashboardsService,
		private _dialogService: DialogService,
		private _accountService: AccountService
	) {}

	get system(): any {
		return {
			version: AppConfig.settings.system.version,
			date: AppConfig.settings.system.date,
			type: AppConfig.settings.system.type
		};
	}

	get layouts(): string[] {
		return this._dashboardsService.layouts;
	}

	get languages(): any {
		return AppConfig.settings.i18n.availableLanguages;
	}

	get themes(): string[] {
		return this._dashboardsService.themes;
	}

	get defaultTheme(): string {
		return this._dashboardsService.defaultTheme;
	}

	get dark(): boolean {
		return this._dashboardsService.darkTheme;
	}

	set dark(value: boolean) {
		this._dashboardsService.darkTheme = value;
	}

	get userLoggedIn(): boolean {
		return this._accountService.getLoggedIn();
	}

	get userLock(): boolean {
		return this._accountService.getLock();
	}

	private initSemanticConfig(): void {
		setTimeout((_) => {
			$('.mrk-languages .dropdown-layouts').dropdown({
				on: 'click',
				onChange: (value, text, $selectedItem) => {
					localStorage.setItem('layout', JSON.stringify(value));
					this._dashboardsService.layout.next(value);
				}
			});

			this._dashboardsService.defaultTheme = JSON.parse(localStorage.getItem('style'));
			$('.mrk-languages .dropdown-themes').dropdown({
				on: 'click',
				onChange: (value, text, $selectedItem) => {
					localStorage.setItem('style', JSON.stringify(value));
					this._dashboardsService.theme.next(value);
					this._dashboardsService.defaultTheme = value;
				}
			});

			$('.mrk-languages .dropdown-languages').dropdown({
				on: 'click',
				onChange: (value, text, $selectedItem) => {
					this._tdLoadingService.register();
					setTimeout(() => {
						localStorage.setItem('language', JSON.stringify(value));
						this._translateService.setDefaultLang(value);
						this._tdLoadingService.resolve();
					}, 500);
				}
			});

			this._dashboardsService.darkTheme = JSON.parse(localStorage.getItem('dark'));
			$('.mrk-languages .checkbox-dark-theme').checkbox({
				onChange: () => {
					localStorage.setItem('dark', JSON.stringify(this._dashboardsService.darkTheme));
					this._dashboardsService.theme.next(this._dashboardsService.defaultTheme);
				}
			});

			const layout = JSON.parse(localStorage.getItem('layout'));
			$('.mrk-languages .dropdown-layouts').dropdown('set selected', layout == null ? AppConfig.settings.layout : layout);

			const style = JSON.parse(localStorage.getItem('style'));
			$('.mrk-languages .dropdown-themes').dropdown('set selected', style == null ? AppConfig.settings.themes.default : style);

			const language = JSON.parse(localStorage.getItem('language'));
			$('.mrk-languages .dropdown-languages').dropdown('set selected', language == null ? AppConfig.settings.i18n.defaultLanguage.code : language);

			const dark = JSON.parse(localStorage.getItem('dark'));
			$('.mrk-languages .checkbox-dark-theme').checkbox(dark ? 'set checked' : 'set unchecked');
		});
	}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {}

	logout(): void {
		this._accountService.logout();
	}

	activeLink(name: string): boolean {
		return this._router.url.includes(name);
	}

	modalRegulations(): void {
		this._dialogService.open<RegulationsDialogComponent>(RegulationsDialogComponent);
	}

	modalPolicy(): void {
		this._dialogService.open<PolicyDialogComponent>(PolicyDialogComponent);
	}

	modalCookie(): void {
		this._dialogService.open<CookieDialogComponent>(CookieDialogComponent);
	}

	modalContact(): void {
		this._dialogService.open<ContactDialogComponent>(ContactDialogComponent);
	}
}
