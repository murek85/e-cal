import { CdkStepper, StepperSelectionEvent } from '@angular/cdk/stepper';
import { Component, Input, AfterViewInit, ViewChild } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';

import { from, iif, Observable, of } from 'rxjs';
import { catchError, delay, finalize, map, tap } from 'rxjs/operators';
import { BroadcastService, MessageType } from 'src/app/core/services/broadcast.service';
import { ResourceType } from 'src/app/core/services/websocket.service';
import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';
import { FormField, Step } from 'src/app/shared/models/step.model';

declare var $: any;

const flatten = (arr) => arr.reduce((a, b) => a.concat(Array.isArray(b) ? flatten(b) : b), []);

@Component({
	selector: 'app-tab-form-template',
	templateUrl: './tab-form-template.component.html',
	styleUrls: ['./tab-form-template.component.scss']
})
export class TabFormTemplateComponent implements AfterViewInit {
	formFieldType: typeof FormFieldType = FormFieldType;

	private _formConfig;
	private _steps: Array<Step>;
	private _selectedIndex = 0;
	private _error;
	private _loading = false;

	@ViewChild('stepper') stepper: CdkStepper;

	constructor(private _broadcastService: BroadcastService) {}

	@Input()
	get formConfig() {
		return this._formConfig;
	}

	set formConfig(formConfig) {
		this._formConfig = formConfig;
	}

	@Input()
	get steps(): Array<Step> {
		return this._steps;
	}

	set steps(steps: Array<Step>) {
		this._steps = steps;
	}

	@Input()
	get selectedIndex(): number {
		return this._selectedIndex;
	}

	set selectedIndex(selectedIndex) {
		this._selectedIndex = selectedIndex;
	}

	get error(): any {
		return this._error;
	}

	get loading(): boolean {
		return this._loading;
	}

	private initSemanticConfig(): void {
		this.initConfig(this._steps[this._selectedIndex]);
	}

	private initSemanticForm(): void {
		this.initForm(this._steps[this._selectedIndex]);
	}

	public ngAfterViewInit() {
		this.initSemanticConfig();
		this.initSemanticForm();
	}

	changeTab($event): void {
		const tab = $event as MatTabChangeEvent;
		this._selectedIndex = tab.index;

		this.initForm(this._steps[tab.index]);
		this.initConfig(this._steps[tab.index]);
		this.loadForm(this.prepareForm());
	}

	backForm(): void {}

	saveForm(observable: (model) => Observable<Object>, close: () => void): void {
		this._loading = true;
		this._steps.forEach(
			(step) => (step.completed = step.segments.every((segment) => segment.fields.filter((field) => field.rules).every((field) => field.value)))
		);

		const completed: boolean[] = flatten(this._steps.map((step) => step.completed));
		const valid = completed.every((value) => value);
		const model = this.prepareForm();

		const broadcast = (result) =>
			this._broadcastService.broadcast({
				type: MessageType.RESOURCES_CHANGED,
				payload: {
					type: this._formConfig.broadcast.type,
					title: result.title,
					message: result.message,
					config: result.config
				}
			});

		const success = () => ({
			title: 'Zapisano',
			message: `Dodano.`,
			config: { class: 'success', position: 'top center', showIcon: 'check circle' }
		});

		const error = () =>
			of({
				title: 'Błąd',
				message: `Wystąpił problem.`,
				config: { class: 'error', position: 'top center', showIcon: 'exclamation circle' }
			});

		const invalid = () =>
			of({
				title: 'Problem',
				message: `Nie wypełniono wymaganych pól formularza.`,
				config: { class: 'warning', position: 'top center', showIcon: 'exclamation circle' }
			});

		iif(() => valid, observable(model).pipe(delay(500), map(success), tap(close)), invalid())
			.pipe(
				delay(500),
				catchError(error),
				finalize(() => (this._loading = false))
			)
			.subscribe(broadcast);
	}

	loadForm(model): void {
		this._steps.forEach((step) => {
			step.segments.forEach((segment) => {
				segment.fields.forEach((field) => (field.value = model[field.name]));
			});
		});

		const fields = flatten(this._steps.map((step) => step.segments.map((segment) => segment.fields))).map((field) => ({
			name: field.name,
			value: field.value
		}));

		fields.forEach((field) => $(`.form-${this._formConfig.symbol}`).form('set value', field.name, field.value));
	}

	private initConfig(step: Step): void {
		from(step.segments)
			.pipe(
				delay(0),
				map((segment) => segment.fields.map((field) => this.initField(field)))
			)
			.subscribe();
	}

	private initField(field): void {
		switch (field.type) {
			case FormFieldType.CHECKBOX: {
				$(`.checkbox-${field.symbol}`).checkbox({
					onChecked: () => (field.value = true),
					onUnchecked: () => (field.value = false)
				});
				break;
			}
			case FormFieldType.EMAIL:
			case FormFieldType.NUMBER:
			case FormFieldType.PHONE_NUMBER:
			case FormFieldType.PASSWORD:
			case FormFieldType.TEXT:
			case FormFieldType.TEXTAREA: {
				field.onChange = (model: FormField) => (model.value = $(`.form-${this._formConfig.symbol}`).form('get value', model.name));
				break;
			}
			case FormFieldType.DICTIONARY_MULTI: {
				$(`.dropdown-${field.symbol}`).dropdown({
					values: field.items,
					placeholder: 'Wybierz',
					fields: field.api.fields,
					message: {
						addResult: 'Dodaj <b>{term}</b>',
						count: 'Zaznaczono {count}',
						maxSelections: `Maksymalnie {maxCount}`,
						noResults: 'Nie znaleziono żadnych wpisów.'
					},
					onChange: (value, text, $selectedItem) => (field.value = value)
				});
				$(`.dropdown-${field.symbol}`).dropdown('set selected', field.value ? field.value.split(',') : null);
				break;
			}
			case FormFieldType.DICTIONARY_SINGLE: {
				$(`.dropdown-${field.symbol}`).dropdown({
					values: field.items,
					placeholder: 'Wybierz',
					fields: field.api.fields,
					message: {
						addResult: 'Dodaj <b>{term}</b>',
						count: 'Zaznaczono {count}',
						maxSelections: `Maksymalnie {maxCount}`,
						noResults: 'Nie znaleziono żadnych wpisów.'
					},
					onChange: (value, text, $selectedItem) => (field.value = value)
				});
				$(`.dropdown-${field.symbol}`).dropdown('set selected', field.value);
				break;
			}
		}
	}

	private initForm(step: Step): void {
		setTimeout((_) =>
			$(`.form-${this._formConfig.symbol}`).form({ className: { label: 'ui basic red prompt label dock-a' }, inline: true, autoCheckRequired: true })
		);

		from(step.segments)
			.pipe(
				map((segment) => segment.fields.filter((field) => !!field.rules).map((field) => ({ name: field.name, rules: field.rules }))),
				map((rule) =>
					rule.reduce((map, obj) => {
						map[obj.name] = { identifier: obj.name, rules: obj.rules };
						return map;
					}, {})
				),
				delay(0),
				map((fields) => $(`.form-${this._formConfig.symbol}`).form('add fields', fields))
			)
			.subscribe();
	}

	private prepareForm() {
		const fields = flatten(this._steps.map((step) => step.segments.map((segment) => segment.fields)))
			.filter((field) => !!field.value)
			.map((field) => ({ name: field.name, value: field.value }));

		const model = fields.reduce((map, obj) => {
			map[obj.name] = obj.value;
			return map;
		}, {});

		return model;
	}
}
