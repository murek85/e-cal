import { PagingResults } from './paging.model';

export class Result<T> {
	data: T;
	paging: PagingResults;
}
