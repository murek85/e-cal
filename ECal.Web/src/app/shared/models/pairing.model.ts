export interface Pairing {
	source: any;
	destination: any;
	type: PairingAction;
}

export type PairingAction = 'start' | 'stop' | 'add' | 'remove' | 'highlight';

export interface WidgetPair {
	idDestinationWidget?: number;
	idSourceWidget?: number;
	idWidgetPair?: number;
}
