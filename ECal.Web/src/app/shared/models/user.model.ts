import { Role } from './role.model';
import { PermissionsTypes } from '../enums/permissions-types.enum';

export class User {
	id?: string;
	userName?: string;
	email?: string;
	emailConfirmed?: boolean;
	password?: string;
	phoneNumber?: string;
	phoneNumberConfirmed?: boolean;
	firstName?: string;
	lastName?: string;
	fullName?: string;
	peselNumber?: number;
	lastValidLogin?: string;
	lastIncorrectLogin?: string;
	logins?: string[];
	provider?: any;
	roles?: Role[];
	permissions?: PermissionsTypes[];
	photo?: string;
}
