import { Observable } from 'rxjs';

export interface ParamValue {
	alias?: string;
	logicValue?: boolean;
	textValue?: string;
	numberValue?: number;
	dateValue?: string;
}

export interface Column {
	column: ParamValue;
	filter?: ParamValue;
	sorting?: ParamValue;
	definition: ColumnDefinition;
	display: string;
	filterTooltip: string;
}

export class ColumnDefinition {
	alias: string;
	hidden?: boolean;
	filter?: Filter;
	tooltip?: string;
	icon?: string;
	size?: string;
	formatter?: (data: any, cell?: string, idPropertyName?: string) => string | CellData;
}

export interface CellData {
	text?: string;
	icon?: string;
	iconColor?: string;
}

export class Filter {
	type: FilterType;
	property?: string;
	alternateProperty?: string;
	display?: string;
	dictionary?: Observable<Array<any>>;
}

export enum FilterType {
	TEXT = 'text',
	DATE = 'date',
	NUMBER = 'number',
	DATE_RANGE = 'date_range',
	NUMBER_RANGE = 'number_range',
	INTEGER_RANGE = 'integer_range',
	DATE_HOUR = 'date_hour',
	ONLY_DATE = 'only_date',
	LOGIC = 'logic',
	DICTIONARY_MULTI = 'dictionary_multi',
	DICTIONARY_SINGLE = 'dictionary_single'
}
