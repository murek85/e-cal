export class HeaderConfig {
	title?: string;
	meta?: string;
	icon?: {
		name: string;
		color: string;
	};
}
