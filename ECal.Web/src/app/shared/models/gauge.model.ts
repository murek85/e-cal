export class Gauge {
	symbol: 'processor' | 'memory' | 'disk';
	value: number;
	status?: any;
	name?: string;
	label: string;
	append: string;
	min: number;
	max: number;
	options?: {
		type: string;
		cap: string;
		size: number;
		thick: number;
		foregroundColor?: string;
		backgroundColor?: string;
		fillColor?: string;
		showExtremum?: boolean;
	};
	thresholds?: {};
	data?: any;
}
