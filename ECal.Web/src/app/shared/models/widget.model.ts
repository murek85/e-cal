import { WidgetTypeEnum } from './../enums/widget-type.enum';
import { Column } from './column.model';

export class Widget {
	public id?;
	public position: WidgetPosition;
	public settings: WidgetSettings;
	public active: boolean;
	public type: 'calendar' | 'table' | 'profile' | 'map' | 'guacamole' | 'default' | 'none';
	public workInProgress = false;
	public data: any;

	params?: Array<WidgetParam>;
}

export class WidgetPosition {
	public top: number;
	public left: number;
	public width: number;
	public height: number;
}

export class WidgetSettings {
	public type: WidgetTypeEnum;
	public header?: string;
	public description?: string;
	public icon?: string;
}

export interface WidgetOptions {
	tabType?: 'general' | 'columns' | 'filters' | 'monitor';
	filter?: Column;
	column?: Column;
}

export interface WidgetParam {
	symbol?: string;
	values?: Array<WidgetParamValue>;
}

export interface WidgetParamValue {
	idValue?: number;
	logicValue?: boolean;
	alias?: string;
}
