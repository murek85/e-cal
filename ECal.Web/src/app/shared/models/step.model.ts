import { FormFieldType } from '../enums/form-field-type.enum';

export class Step {
	public label: string;
	public description: string;
	public symbol: string;
	public segments: Segment[];
	public completed = false;
}

export class Segment {
	public header: string;
	public description: string;
	public fields: FormField[];
	public message?: { header; description };
}

export class FormField {
	public label: string;
	public meta?: string;
	public name: string;
	public symbol: string;
	public type: FormFieldType;
	public items?: any[];
	public url?: string;
	public hints?: string[];
	public value? = null;
	public rules?;

	public api?: any;

	public onChange?: any;
}
