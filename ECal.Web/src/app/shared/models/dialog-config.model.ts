export class DialogConfig {
	title?: string;
	meta?: string;
	description?: string;
	iconClass?: string;
	approveButtonLabel?: string;
	approveButtonColorClass?: string;
	denyButtonLabel?: string;
	debyButtonColorClass?: string;
}
