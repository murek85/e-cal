export class Application {
	public id?: string;
	public name?: string;
	public description?: string;
	public symbol?: string;
	public apiUrl?: string;
	public appUrl?: string;
}
