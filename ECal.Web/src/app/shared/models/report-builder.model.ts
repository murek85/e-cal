export interface ReportBuilder {
	title: string;
	format: string;
	labels: string;
	aliases: string;
	locale: string;
}
