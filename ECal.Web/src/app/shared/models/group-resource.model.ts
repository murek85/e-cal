export class GroupResource {
	public id?: number;
	public parentId?: number;
	public name?: string;
	public description?: string;
}
