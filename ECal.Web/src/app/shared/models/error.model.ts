export class Error {
	public type: string;
	public message?: string;
	public desc?: string;
}
