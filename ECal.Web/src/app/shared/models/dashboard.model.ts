import { DashboardType } from '../enums/dashboard-type.enum';
import { Widget } from './widget.model';

export class Dashboard {
	id?: number;
	type?: DashboardType;
	name?: string;
	description?: string;
	toolTip?: string;
	iconClass?: string;
	colorClass?: string;
	routerLink?: string;
	favourite?: string;
	subdashboards?: Dashboard[];
	widgets?: Widget[];
	active?: boolean;
}
