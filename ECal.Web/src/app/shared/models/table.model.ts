import { TableType } from '../enums/table-type.enum';

export class TableSettings {
	public type: TableType = TableType.LIST;
}
