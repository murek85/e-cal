export class Permission {
	public id?: number;
	public resourceId?: number;
	public name?: string;
	public description?: string;
	public symbol?: string;
}
