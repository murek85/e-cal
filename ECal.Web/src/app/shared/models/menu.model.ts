import { DashboardType } from '../enums/dashboard-type.enum';

export class Menu {
	type?: DashboardType;
	name: string;
	description?: string;
	toolTip: string;
	iconClass: string;
	colorClass?: string;
	routerLink?: string;
	selected?: boolean;
	permissionsOnly?: string[];
	submenus?: Menu[];
	count?: number;
	favourite?: string;
}
