export class GroupUser {
	public id?: number;
	public name?: string;
	public description?: string;
	public symbol?: string;
}
