export class PagingSettings {
	pageNumber = 1;
	pageSize = 10;
	sort?: string;
}

export class PagingResults {
	pageNumber = 1;
	pageSize = 10;
	totalRows = 0;
}
