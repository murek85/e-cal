export class Role {
	public id?: number;
	public name?: string;
	public description?: string;
	public symbol?: string;
	public editable?: boolean;
}
