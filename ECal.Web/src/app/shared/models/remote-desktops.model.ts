export class RemoteDesktop {
	public hostname: string;
	public machinename: string;
	public port: number;
	public dpi: number;
	public protocol: 'ssh' | 'rdp' | 'vnc';
	public image: 'image/png';
}
