export enum DashboardType {
	DASHBOARD = 'dashboard',
	SYSTEM_MANAGEMENT = 'system-management',
	APPLICATIONS = 'applications',
	USERS = 'users',
	ROLES = 'roles',
	PERMISSIONS = 'permissions',
	GROUPS_USERS = 'groups-users',
	GROUPS_RESOURCES = 'groups-resources',
	GENERATING_REPORTS = 'generating-reports',
	EVENT_LOG = 'event-log',
	NOTIFICATIONS = 'notifications',
	PROFILE = 'profile',
	GUACAMOLE = 'guacamole',
	REMOTE_DESKTOPS = 'remote-desktops'
}
