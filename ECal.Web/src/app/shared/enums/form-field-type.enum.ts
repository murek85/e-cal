export enum FormFieldType {
	TEXT = 'text',
	TEXTAREA = 'text-area',
	PASSWORD = 'password',
	EMAIL = 'email',
	NUMBER = 'number',
	PHONE_NUMBER = 'phone-number',
	DICTIONARY_SINGLE = 'dictionary-single',
	DICTIONARY_MULTI = 'dictionary-multi',
	DICTIONARY_SEARCH = 'dictionary-search',
	CHECKBOX = 'checkbox'
}
