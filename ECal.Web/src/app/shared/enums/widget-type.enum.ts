export enum WidgetTypeEnum {
	CALENDAR = 'calendar',
	SHORTCUT = 'shortcut',
	HISTORY_OPERATION = 'history-operation',
	APPLICATIONS = 'applications',
	USERS_AUDIT = 'users-audit',
	USERS = 'users',
	ROLES = 'roles',
	PERMISSIONS = 'permissions',
	GROUPS_USERS = 'groups-users',
	GROUPS_RESOURCES = 'groups-resources',
	PROFILE = 'profile',
	MONITOR_SYSTEM = 'monitor-system',
	MONITOR_DISK = 'monitor-disk',
	STATISTICS = 'statistics',
	GUACAMOLE = 'guacamole',
	REMOTE_DESKTOPS = 'remote-desktops',
	SNAKE = 'snake',
	NOTIFICATIONS = 'notifications'
}
