import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './components/account/auth/auth.component';

const routes: Routes = [
	{ path: '', redirectTo: 'login', pathMatch: 'full' },
	{ path: 'login', loadChildren: () => import('./components/account/login/login.module').then((m) => m.LoginModule) },
	{ path: 'register', loadChildren: () => import('./components/account/register/register.module').then((m) => m.RegisterModule) },
	{ path: 'lock', loadChildren: () => import('./components/account/lock/lock.module').then((m) => m.LockModule) },
	{
		path: 'email/confirm',
		loadChildren: () => import('./components/account/email/confirm/email-confirm.module').then((m) => m.EmailConfirmModule)
	},
	{
		path: 'password/confirm',
		loadChildren: () => import('./components/account/password/confirm/password-confirm.module').then((m) => m.PasswordConfirmModule)
	},
	{
		path: 'password/reminder',
		loadChildren: () => import('./components/account/password/reminder/password-reminder.module').then((m) => m.PasswordReminderModule)
	},
	{ path: 'dashboards', loadChildren: () => import('./components/dashboards/dashboards.module').then((m) => m.DashboardsModule) },
	{ path: 'error', loadChildren: () => import('./components/error/error.module').then((m) => m.ErrorModule) },

	{ path: 'auth', component: AuthComponent },
	{ path: '**', redirectTo: 'error' }
];

export const routing = RouterModule.forRoot(routes);
