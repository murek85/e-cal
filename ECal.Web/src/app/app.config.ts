import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';

export interface IAppConfig {
	system: {
		appName: string;
		version: string;
		date: string;
		type: string;
		appUrl: string;
		apiUrl: string;
		guacamoleUrl: string;
	};
	oidc: {
		apiUrl: string;
		clientId: string;
		clientSecret: string;
		responseType: string;
		scope: string;
		requireHttps: boolean;
	};
	i18n: {
		defaultLanguage: any;
		availableLanguages: any[];
	};
	layout: string;
	toolbar: string;
	themes: {
		default: string;
		dark: boolean;
		available: string[];
	};
}

@Injectable()
export class AppConfig {
	static settings: IAppConfig;

	constructor(private http: HttpClient) {}

	load(): Promise<void> {
		const jsonFile = `assets/configs/config.${environment.name}.json`;
		return new Promise<void>((resolve, reject) => {
			this.http
				.get(jsonFile)
				.toPromise()
				.then((response: IAppConfig) => {
					AppConfig.settings = response as IAppConfig;
					resolve();
				})
				.catch((response: any) => {
					reject(`Could not load file '${jsonFile}': ${JSON.stringify(response)}`);
				});
		});
	}
}
