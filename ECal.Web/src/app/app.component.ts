import { Component, OnInit, AfterViewInit, HostBinding, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';

import { NgxMAuthOidcService } from './core/services/oidc/oidc.service';

import { AccountService } from 'src/app/core/services/account.service';
import { DashboardsService } from './core/services/dashboards.service';

import { AppConfig } from './app.config';
import { authConfig } from './auth.config';

const THEME_DARKNESS_SUFFIX = `-dark`;
const THEME_LIGHTNESS_SUFFIX = `-light`;

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {
	private _unsubscribe$ = new Subject<void>();

	// For navigation (sidenav/toolbar). Isn't related to dynamic-theme-switching:
	private _isHandset$: Observable<boolean> = this._breakpointObserver.observe(Breakpoints.Handset).pipe(map((result) => result.matches));

	@HostBinding('class') activeThemeCssClass: string;

	constructor(
		private _router: Router,
		private _breakpointObserver: BreakpointObserver,
		private _overlayContainer: OverlayContainer,
		private _tdLoadingService: TdLoadingService,
		private _dashboardsService: DashboardsService,
		private _oidcService: NgxMAuthOidcService,
		private _translateService: TranslateService,
		public _accountService: AccountService
	) {}

	private initSemanticConfig(): void {}

	ngAfterViewInit(): void {
		this.initSemanticConfig();
	}

	ngOnInit(): void {
		this._router.navigateByUrl(`${location.pathname.substr(1)}${location.search}`);
		window.addEventListener('popstate', () => {
			this._router.navigateByUrl(`${location.pathname.substr(1)}${location.search}`);
		});

		this._dashboardsService.theme
			.pipe(
				takeUntil(this._unsubscribe$),
				tap((value) => this.setActiveTheme(value, this._dashboardsService.darkTheme))
			)
			.subscribe();
		this._translateService.setDefaultLang('pl');

		const theme = JSON.parse(localStorage.getItem('style'));
		const dark = JSON.parse(localStorage.getItem('dark'));
		this.setActiveTheme(theme == null ? 'style-default' : theme, dark);
		localStorage.setItem('style', JSON.stringify(this._dashboardsService.defaultTheme));

		this._oidcService.configure(authConfig(AppConfig.settings.oidc.apiUrl));
		this._oidcService.setStorage(sessionStorage);
		this._oidcService
			.loadDocumentAndTryLogin({ customHashFragment: '?' })
			.then((res) => {
				if (this._accountService.getLoggedIn()) {
					this._accountService.getUserLogged();
				}
			})
			.catch((err) => {
				console.error('loadDocumentAndTryLogin error', err);
			});

		this._oidcService.events.subscribe((e) => {
			switch (e.type) {
				case 'document_loaded': {
					break;
				}
				case 'token_received': {
					this._accountService.getUserLogged();
					break;
				}
				case 'token_expires': {
					this._oidcService.scope = AppConfig.settings.oidc.scope;
					this._oidcService
						.refreshToken()
						.then()
						.catch((err) => {
							console.error(e, err);
						});
					break;
				}
				case 'token_error':
				case 'token_refresh_error': {
					this._accountService.logout();
					this._router.navigate(['/login']);
					break;
				}

				case 'logout': {
					this._router.navigate(['/login']);
					break;
				}
			}
		});
	}

	ngOnDestroy(): void {
		this._unsubscribe$.next();
		this._unsubscribe$.complete();
	}

	private setActiveTheme(theme: string, darkness: boolean = null): void {
		this._tdLoadingService.register();

		if (darkness === null) {
			darkness = this._dashboardsService.darkTheme;
		} else if (this._dashboardsService.darkTheme === darkness) {
			if (this._dashboardsService.defaultTheme === theme) {
			}
		} else {
			this._dashboardsService.darkTheme = darkness;
		}

		this._dashboardsService.defaultTheme = theme;
		const cssClass = darkness === true ? theme + THEME_DARKNESS_SUFFIX : theme + THEME_LIGHTNESS_SUFFIX;
		const classList = this._overlayContainer.getContainerElement().classList;
		if (classList.contains(this.activeThemeCssClass)) {
			classList.replace(this.activeThemeCssClass, cssClass);
		} else {
			classList.add(cssClass);
		}
		this.activeThemeCssClass = cssClass;

		setTimeout((_) => {
			this._tdLoadingService.resolve();
		}, 500);
	}
}
