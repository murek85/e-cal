﻿using ECal.Common;
using ECal.Model;
using System.Collections.Generic;

namespace ECal.Business.Common
{
	public interface IReportManager : IInitiableComponent
	{
		/// <summary>
		/// Generowanie odpowiedniego typu raportu do pliku PDF/CSV/XLSX.
		/// </summary>
		/// <typeparam name="T">Typ obiektu</typeparam>
		/// <param name="reportBuilder"></param>
		/// <param name="data">Pobrane dane wymagane przy generowaniu pliku raportu.</param>
		/// <returns>Dne wygenerowane pliku w odpowiednim formacie.</returns>
		byte[] ExportData<T>(ReportBuilder reportBuilder, List<T> data);
	}
}
