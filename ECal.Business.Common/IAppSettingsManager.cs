﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECal.Business.Common
{
	/// <summary>
	/// Kontrakt menedżera dostępu do ustawień aplikacji
	/// </summary>
	public interface IAppSettingsManager
	{
		/// <summary>
		/// Zwraca wartość ustawienia o podanyum kluczu
		/// </summary>
		/// <param name="appSettingKey">Klucz ustawienia</param>
		string GetAppSettingValue(string appSettingKey);
	}
}
