﻿using ECal.Common;
using System.Collections.Generic;
using DB = ECal.Model.DB;

namespace ECal.Business.Common
{
   public interface IGroupsResourcesManager : IInitiableComponent
   {
      (List<DB.GroupResource>, long) GetGroupsResourcesReport(RsqlData paging);
      (List<DB.GroupResource>, long) GetGroupsResourcesList(RsqlData paging);
      DB.GroupResource GetGroupResource(long id);
      long? CreateGroupResource(DB.GroupResource groupResource);
      bool UpdateGroupResource(DB.GroupResource groupResource);
      bool DeleteGroupResource(long id);
   }
}
