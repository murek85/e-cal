﻿using ECal.Common;
using System.Collections.Generic;
using DB = ECal.Model.DB;

namespace ECal.Business.Common
{
   public interface IGroupsUsersManager : IInitiableComponent
   {
      (List<DB.GroupUser>, long) GetGroupsUsersReport(RsqlData paging);
      (List<DB.GroupUser>, long) GetGroupsUsersList(RsqlData paging);
      DB.GroupUser GetGroupUser(long id);
      long? CreateGroupUser(DB.GroupUser groupUser);
      bool UpdateGroupUser(DB.GroupUser groupUser);
      bool DeleteGroupUser(long id);
   }
}
