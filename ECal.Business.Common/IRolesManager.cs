﻿using ECal.Common;
using System;
using System.Collections.Generic;
using DB = ECal.Model.DB;

namespace ECal.Business.Common
{
   public interface IRolesManager : IInitiableComponent
   {
      (List<DB.Role>, long) GetRolesReport(RsqlData paging);
      (List<DB.Role>, long) GetRolesList(RsqlData paging);
      DB.Role GetRole(long id);
      long? CreateRole(DB.Role role);
      bool UpdateRole(DB.Role role);
      bool DeleteRole(long id);
      bool VerifyAssigningRoleToUser(Guid userId, long roleId);
   }
}
