﻿using ECal.Common;
using ECal.Model.Ldap;
using System.Collections.Generic;

namespace ECal.Business.Common
{
	public interface ILdapManager : IInitiableComponent
	{
		IList<LdapUser> GetUsersByEmailAddress(string emailAddress);
		IList<LdapUser> GetAllUsers();
		LdapUser GetUserByUserPrincipalName(string userPrincipalName);
		LdapUser GetUserBySAMAccountName(string samAccountName);
		LdapUser GetUserByObjectGuid(string objectGuid);
		bool Authenticate(string distinguishedName, string password);
	}
}
