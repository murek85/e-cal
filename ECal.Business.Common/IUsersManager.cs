﻿using ECal.Common;
using System;
using System.Collections.Generic;
using DB = ECal.Model.DB;

namespace ECal.Business.Common
{
   public interface IUsersManager : IInitiableComponent
   {
      (List<DB.User>, long) GetUsersReport(RsqlData paging);
      (List<DB.User>, long) GetUsersList(RsqlData paging);
      DB.User GetUser(Guid id);
      DB.User GetUser(string email);
      long? CreateUser(DB.User user);
      bool UpdateUser(DB.User user);
      bool DeleteUser(Guid id);
      bool UpdateUserLogged(DB.User user);
   }
}
