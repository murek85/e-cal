﻿using ECal.Common;
using System.Collections.Generic;
using DB = ECal.Model.DB;

namespace ECal.Business.Common
{
   public interface IApplicationsManager : IInitiableComponent
   {
      (List<DB.Application>, long) GetApplicationsReport(RsqlData paging);
      (List<DB.Application>, long) GetApplicationsList(RsqlData paging);
      DB.Application GetApplication(long id);
      long? CreateApplication(DB.Application application);
      bool UpdateApplication(DB.Application application);
      bool DeleteApplication(long id);
   }
}
