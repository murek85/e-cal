﻿using ECal.Common;
using System.Collections.Generic;
using DB = ECal.Model.DB;

namespace ECal.Business.Common
{
   public interface IPermissionsManager : IInitiableComponent
   {
      (List<DB.Permission>, long) GetPermissionsReport(RsqlData paging);
      (List<DB.Permission>, long) GetPermissionsList(RsqlData paging);
      DB.Permission GetPermission(long id);
      long? CreatePermission(DB.Permission permission);
      bool UpdatePermission(DB.Permission permission);
      bool DeletePermission(long id);

   }
}
