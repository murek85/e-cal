﻿using ECal.Common;
using ECal.Common.Mapping;
using ECal.Data.Common;
using ECal.Data.Common.DB.Applications;
using ECal.Data.Database;
using ECal.Data.PostgresqlDB;
using ECal.Model.DB;
using System.Collections.Generic;
using System.Data;

namespace ECal.Data.DB.Applications
{
   public partial class ApplicationsDAO : PostgresqlDBDatabaseDAO, IApplicationsDAO
   {
      private readonly IRsqlMapper _rsqlMapper;

      public ApplicationsDAO(DBDatabaseContainer databaseContainer, IRsqlMapper rsqlMapper) : base(databaseContainer)
      {
         _rsqlMapper = rsqlMapper;
      }

      private Application ReadSignleApplication(IDataReader reader)
      {
         Application application = new Application
         {
            Id = reader.GetNullableInt32("ApplicationId"),
            Name = reader.GetString("Name"),
            Description = reader.GetString("Description"),
            Symbol = reader.GetString("Symbol"),
            ApiUrl = reader.GetString("ApiUrl"),
            AppUrl = reader.GetString("AppUrl"),
         };

         return application;
      }

      public (List<Application>, long) ReadApplicationsReport(IDatabaseTransaction transaction, RsqlData rsql)
      {
         long totalRows = 0;
         string sqlQuery = _rsqlMapper.BuildQuery("ReadApplications", rsql, DbCommands.ReadApplications, "id", noPaging: true);
         return (Read(sqlQuery, transaction,
            (reader) =>
            {
               if (rsql != null)
               {
                  totalRows = ReadPagingTotalRows(reader);
               }
               return ReadSignleApplication(reader);
            }), totalRows);
      }

      public (List<Application>, long) ReadApplicationsList(IDatabaseTransaction transaction, RsqlData rsql)
      {
         long totalRows = 0;
         string sqlQuery = _rsqlMapper.BuildQuery("ReadApplications", rsql, DbCommands.ReadApplications, "id");
         return (Read(sqlQuery, transaction,
            (reader) =>
            {
               if (rsql != null)
               {
                  totalRows = ReadPagingTotalRows(reader);
               }
               return ReadSignleApplication(reader);
            }), totalRows);
      }

      public Application ReadApplicationByRole(IDatabaseTransaction transaction, long id)
      {
         throw new System.NotImplementedException();
      }

      public Application ReadApplicationByPermission(IDatabaseTransaction transaction, long id)
      {
         throw new System.NotImplementedException();
      }

      public Application ReadApplicationByClient(IDatabaseTransaction transaction, string id)
      {
         throw new System.NotImplementedException();
      }

      public Application ReadApplication(IDatabaseTransaction transaction, long id)
      {
         return ReadFirst(DbCommands.ReadApplication, transaction,
            (command) =>
            {
               SetParameterTypeAndValue(command, ":applicationId", DbType.Int64, id);
            },
            (reader) =>
            {
               return ReadSignleApplication(reader);
            });
      }

      public long? CreateApplication(IDatabaseTransaction transaction, Application application)
      {
         return InsertOneWithLongOutputId(DbCommands.CreateApplication, transaction,
           (command) =>
           {
              SetParameterTypeAndValue(command, ":name", DbType.String, application.Name);
              SetParameterTypeAndValue(command, ":description", DbType.String, application.Description);
              SetParameterTypeAndValue(command, ":symbol", DbType.String, application.Symbol);
              SetParameterTypeAndValue(command, ":apiurl", DbType.String, application.ApiUrl);
              SetParameterTypeAndValue(command, ":appurl", DbType.String, application.AppUrl);
           }
        );
      }

      public bool UpdateApplication(IDatabaseTransaction transaction, Application application)
      {
         return Update(DbCommands.UpdateApplication, transaction,
           (command) =>
           {
              SetParameterTypeAndValue(command, ":name", DbType.String, application.Name);
              SetParameterTypeAndValue(command, ":description", DbType.String, application.Description);
              SetParameterTypeAndValue(command, ":symbol", DbType.String, application.Symbol);
              SetParameterTypeAndValue(command, ":apiurl", DbType.String, application.ApiUrl);
              SetParameterTypeAndValue(command, ":appurl", DbType.String, application.AppUrl);

              SetParameterTypeAndValue(command, ":applicationId", DbType.Int64, application.Id);
           }
        ) > 0;
      }

      public bool DeleteApplication(IDatabaseTransaction transaction, long id)
      {
         return Update(DbCommands.DeleteApplication, transaction,
            (command) =>
            {
               SetParameterTypeAndValue(command, ":applicationId", DbType.Int64, id);
            }
         ) > 0;
      }
   }
}
