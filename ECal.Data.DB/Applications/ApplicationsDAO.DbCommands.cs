﻿namespace ECal.Data.DB.Applications
{
   public partial class ApplicationsDAO
   {
      private static class DbCommands
      {
         internal static string ReadApplications = @"
			SELECT
				a.""ApplicationId"", a.""Name"", a.""Description"", a.""Symbol"", a.""ApiUrl"", a.""AppUrl""
			FROM ""WisApplications"" a";

         internal static string ReadApplication = @"
			SELECT
				a.""ApplicationId"", a.""Name"", a.""Description"", a.""Symbol"", a.""ApiUrl"", a.""AppUrl""
			FROM ""WisApplications"" a
			WHERE a.""ApplicationId"" = :applicationId";

         internal static string CreateApplication = @"
         INSERT INTO ""WisApplications"" (""Name"", ""Description"", ""Symbol"", ""ApiUrl"", ""AppUrl"")
         VALUES (:name, :description, :symbol, :apiurl, :appurl)
         RETURNING ""ApplicationId"" AS ""Id""
         ";

         internal static string UpdateApplication = @"
         UPDATE ""WisApplications""
            SET ""Name"" = :name, ""Description"" = :description, ""Symbol"" = :symbol, ""ApiUrl"" = :apiurl, ""AppUrl"" = :appurl
         WHERE ""ApplicationId"" = :applicationId
         ";

         internal static string DeleteApplication = @"
         DELETE FROM ""WisApplications"" a
         WHERE a.""ApplicationId"" = :applicationId
         ";
      }
   }
}
