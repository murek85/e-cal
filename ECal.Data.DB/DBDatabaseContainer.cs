﻿using ECal.Data.PostgresqlDB;

namespace ECal.Data.DB
{
	/// <summary>
	/// Wrapper kontenera bazy Postgresql
	/// </summary>
	public class DBDatabaseContainer : PostgresqlDBDatabaseContainer
	{
		public DBDatabaseContainer(string connectionString) : base(connectionString)
		{
		}
	}
}