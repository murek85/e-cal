﻿using ECal.Common;
using ECal.Common.Mapping;
using ECal.Data.Common;
using ECal.Data.Common.DB.Permissions;
using ECal.Data.Database;
using ECal.Data.PostgresqlDB;
using ECal.Model.DB;
using System;
using System.Collections.Generic;
using System.Data;

namespace ECal.Data.DB.Permissions
{
   public partial class PermissionsDAO : PostgresqlDBDatabaseDAO, IPermissionsDAO
   {
      private readonly IRsqlMapper _rsqlMapper;

      public PermissionsDAO(DBDatabaseContainer databaseContainer, IRsqlMapper rsqlMapper) : base(databaseContainer)
      {
         _rsqlMapper = rsqlMapper;
      }

      private Permission ReadSignlePermission(IDataReader reader)
      {
         Permission permission = new Permission
         {
            Id = reader.GetNullableInt32("PermissionId"),
            Name = reader.GetString("Name"),
            Description = reader.GetString("Description"),
            Symbol = reader.GetString("Symbol")
         };

         return permission;
      }

      public (List<Permission>, long) ReadPermissionsReport(IDatabaseTransaction transaction, RsqlData rsql)
      {
         long totalRows = 0;
         string sqlQuery = _rsqlMapper.BuildQuery("ReadPermissions", rsql, DbCommands.ReadPermissions, "id", noPaging: true);
         return (Read(sqlQuery, transaction,
            (reader) =>
            {
               if (rsql != null)
               {
                  totalRows = ReadPagingTotalRows(reader);
               }
               return ReadSignlePermission(reader);
            }), totalRows);
      }

      public (List<Permission>, long) ReadPermissionsList(IDatabaseTransaction transaction, RsqlData rsql)
      {
         long totalRows = 0;
         string sqlQuery = _rsqlMapper.BuildQuery("ReadPermissions", rsql, DbCommands.ReadPermissions, "id");
         return (Read(sqlQuery, transaction,
            (reader) =>
            {
               if (rsql != null)
               {
                  totalRows = ReadPagingTotalRows(reader);
               }
               return ReadSignlePermission(reader);
            }), totalRows);
      }

      public List<Permission> ReadPermissionsByRole(IDatabaseTransaction transaction, long id)
      {
         throw new NotImplementedException();
      }

      public List<Permission> ReadPermissionsByUser(IDatabaseTransaction transaction, Guid id)
      {
         throw new NotImplementedException();
      }

      public Permission ReadPermission(IDatabaseTransaction transaction, long id)
      {
         return ReadFirst(DbCommands.ReadPermission, transaction,
            (command) =>
            {
               SetParameterTypeAndValue(command, ":permissionId", DbType.Int64, id);
            },
            (reader) =>
            {
               return ReadSignlePermission(reader);
            });
      }

      public long? CreatePermission(IDatabaseTransaction transaction, Permission permission)
      {
         return InsertOneWithLongOutputId(DbCommands.CreatePermission, transaction,
           (command) =>
           {
              SetParameterTypeAndValue(command, ":permissionId", DbType.Int64, permission.Id);
              SetParameterTypeAndValue(command, ":name", DbType.String, permission.Name);
              SetParameterTypeAndValue(command, ":description", DbType.String, permission.Description);
              SetParameterTypeAndValue(command, ":symbol", DbType.String, permission.Symbol);
           }
        );
      }

      public bool UpdatePermission(IDatabaseTransaction transaction, Permission permission)
      {
         return Update(DbCommands.UpdatePermission, transaction,
           (command) =>
           {
              SetParameterTypeAndValue(command, ":name", DbType.String, permission.Name);
              SetParameterTypeAndValue(command, ":description", DbType.String, permission.Description);
              SetParameterTypeAndValue(command, ":symbol", DbType.String, permission.Symbol);

              SetParameterTypeAndValue(command, ":permissionId", DbType.Int64, permission.Id);
           }
        ) > 0;
      }

      public bool DeletePermission(IDatabaseTransaction transaction, long id)
      {
         return Update(DbCommands.DeletePermission, transaction,
            (command) =>
            {
               SetParameterTypeAndValue(command, ":permissionId", DbType.Int64, id);
            }
         ) > 0;
      }
   }
}
