﻿namespace ECal.Data.DB.Permissions
{
   public partial class PermissionsDAO
   {
      private static class DbCommands
      {
         internal static string ReadPermissions = @"
			SELECT
				p.""PermissionId"", p.""Name"", p.""Description"", p.""Symbol""
			FROM ""WisPermissions"" p";

         internal static string ReadPermission = @"
			SELECT
				p.""PermissionId"", p.""Name"", p.""Description"", p.""Symbol""
			FROM ""WisPermissions"" p
			WHERE p.""PermissionId"" = :permissionId";

         internal static string CreatePermission = @"
         INSERT INTO ""WisPermissions"" (""PermissionId"", ""Name"", ""Description"", ""Symbol"")
         VALUES (:permissionId, :name, :description, :symbol)
         RETURNING ""PermissionId"" AS ""Id""
         ";

         internal static string UpdatePermission = @"
         UPDATE ""WisPermissions""
            SET ""Name"" = :name, ""Description"" = :description, ""Symbol"" = :symbol
         WHERE ""PermissionId"" = :permissionId
         ";

         internal static string DeletePermission = @"
         DELETE FROM ""WisPermissions"" p
         WHERE p.""PermissionId"" = :permissionId
         ";
      }
   }
}
