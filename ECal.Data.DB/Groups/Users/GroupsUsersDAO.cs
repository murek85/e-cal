﻿using ECal.Common;
using ECal.Common.Mapping;
using ECal.Data.Common;
using ECal.Data.Common.DB.Groups.Users;
using ECal.Data.Database;
using ECal.Data.PostgresqlDB;
using ECal.Model.DB;
using System.Collections.Generic;
using System.Data;

namespace ECal.Data.DB.Groups.Users
{
   public partial class GroupsUsersDAO : PostgresqlDBDatabaseDAO, IGroupsUsersDAO
   {
      private readonly IRsqlMapper _rsqlMapper;

      public GroupsUsersDAO(DBDatabaseContainer databaseContainer, IRsqlMapper rsqlMapper) : base(databaseContainer)
      {
         _rsqlMapper = rsqlMapper;
      }

      private GroupUser ReadSignleGroupUser(IDataReader reader)
      {
         GroupUser group = new GroupUser
         {
            Id = reader.GetNullableInt32("GroupUserId"),
            Name = reader.GetString("Name"),
            Description = reader.GetString("Description"),
            Symbol = reader.GetString("Symbol"),
         };

         return group;
      }

      public (List<GroupUser>, long) ReadGroupsUsersReport(IDatabaseTransaction transaction, RsqlData rsql)
      {
         long totalRows = 0;
         string sqlQuery = _rsqlMapper.BuildQuery("ReadGroupsUsers", rsql, DbCommands.ReadGroupsUsers, "id", noPaging: true);
         return (Read(sqlQuery, transaction,
            (reader) =>
            {
               if (rsql != null)
               {
                  totalRows = ReadPagingTotalRows(reader);
               }
               return ReadSignleGroupUser(reader);
            }), totalRows);
      }

      public (List<GroupUser>, long) ReadGroupsUsersList(IDatabaseTransaction transaction, RsqlData rsql)
      {
         long totalRows = 0;
         string sqlQuery = _rsqlMapper.BuildQuery("ReadGroupsUsers", rsql, DbCommands.ReadGroupsUsers, "id");
         return (Read(sqlQuery, transaction,
            (reader) =>
            {
               if (rsql != null)
               {
                  totalRows = ReadPagingTotalRows(reader);
               }
               return ReadSignleGroupUser(reader);
            }), totalRows);
      }

      public GroupUser ReadGroupUser(IDatabaseTransaction transaction, long id)
      {
         return ReadFirst(DbCommands.ReadGroupUser, transaction,
            (command) =>
            {
               SetParameterTypeAndValue(command, ":groupUserId", DbType.Int64, id);
            },
            (reader) =>
            {
               return ReadSignleGroupUser(reader);
            });
      }

      public long? CreateGroupUser(IDatabaseTransaction transaction, GroupUser groupUser)
      {
         return InsertOneWithLongOutputId(DbCommands.CreateGroupUser, transaction,
           (command) =>
           {
              SetParameterTypeAndValue(command, ":name", DbType.String, groupUser.Name);
              SetParameterTypeAndValue(command, ":description", DbType.String, groupUser.Description);
              SetParameterTypeAndValue(command, ":symbol", DbType.String, groupUser.Symbol);
           }
        );
      }

      public bool UpdateGroupUser(IDatabaseTransaction transaction, GroupUser groupUser)
      {
         return Update(DbCommands.UpdateGroupUser, transaction,
           (command) =>
           {
              SetParameterTypeAndValue(command, ":name", DbType.String, groupUser.Name);
              SetParameterTypeAndValue(command, ":description", DbType.String, groupUser.Description);
              SetParameterTypeAndValue(command, ":symbol", DbType.String, groupUser.Symbol);

              SetParameterTypeAndValue(command, ":groupUserId", DbType.Int64, groupUser.Id);
           }
        ) > 0;
      }

      public bool DeleteGroupUser(IDatabaseTransaction transaction, long id)
      {
         return Update(DbCommands.DeleteGroupUser, transaction,
            (command) =>
            {
               SetParameterTypeAndValue(command, ":groupUserId", DbType.Int64, id);
            }
         ) > 0;
      }
   }
}
