﻿namespace ECal.Data.DB.Groups.Users
{
   public partial class GroupsUsersDAO
   {
      private static class DbCommands
      {
         internal static string ReadGroupsUsers = @"
			SELECT
				g.""GroupUserId"", g.""Name"", g.""Description"", g.""Symbol""
			FROM ""WisGroupsUsers"" g";

         internal static string ReadGroupUser = @"
			SELECT
				g.""GroupUserId"", g.""Name"", g.""Description"", g.""Symbol""
			FROM ""WisGroupsUsers"" g
			WHERE g.""GroupUserId"" = :groupUserId";

         internal static string CreateGroupUser = @"
         INSERT INTO ""WisGroupsUsers"" (""Name"", ""Description"", ""Symbol"")
         VALUES (:name, :description, :symbol)
         RETURNING ""GroupUserId"" AS ""Id""
         ";

         internal static string UpdateGroupUser = @"
         UPDATE ""WisGroupsUsers""
            SET ""Name"" = :name, ""Description"" = :description, ""Symbol"" = :symbol
         WHERE ""GroupUserId"" = :groupUserId
         ";

         internal static string DeleteGroupUser = @"
         DELETE FROM ""WisGroupsUsers"" g
         WHERE g.""GroupUserId"" = :groupUserId
         ";
      }
   }
}
