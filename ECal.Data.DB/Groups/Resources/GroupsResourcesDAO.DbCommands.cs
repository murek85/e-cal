﻿namespace ECal.Data.DB.Groups.Resources
{
   public partial class GroupsResourcesDAO
   {
      private static class DbCommands
      {
         internal static string ReadGroupsResources = @"
			SELECT
				g.""GroupResourceId"", g.""Name"", g.""Description"", g.""Symbol""
			FROM ""WisGroupsResources"" g";

         internal static string ReadGroupResource = @"
			SELECT
				g.""GroupResourceId"", g.""Name"", g.""Description"", g.""Symbol""
			FROM ""WisGroupsResources"" g
			WHERE g.""GroupResourceId"" = :groupResourceId";

         internal static string CreateGroupResource = @"
         INSERT INTO ""WisGroupsResources"" (""Name"", ""Description"", ""Symbol"")
         VALUES (:name, :description, :symbol)
         RETURNING ""GroupResourceId"" AS ""Id""
         ";

         internal static string UpdateGroupResource = @"
         UPDATE ""WisGroupsResources""
            SET ""Name"" = :name, ""Description"" = :description, ""Symbol"" = :symbol
         WHERE ""GroupResourceId"" = :groupResourceId
         ";

         internal static string DeleteGroupResource = @"
         DELETE FROM ""WisGroupsResources"" g
         WHERE g.""GroupResourceId"" = :groupResourceId
         ";
      }
   }
}
