﻿using ECal.Common;
using ECal.Common.Mapping;
using ECal.Data.Common;
using ECal.Data.Common.DB.Groups.Resources;
using ECal.Data.Database;
using ECal.Data.PostgresqlDB;
using ECal.Model.DB;
using System.Collections.Generic;
using System.Data;

namespace ECal.Data.DB.Groups.Resources
{
   public partial class GroupsResourcesDAO : PostgresqlDBDatabaseDAO, IGroupsResourcesDAO
   {
      private readonly IRsqlMapper _rsqlMapper;

      public GroupsResourcesDAO(DBDatabaseContainer databaseContainer, IRsqlMapper rsqlMapper) : base(databaseContainer)
      {
         _rsqlMapper = rsqlMapper;
      }

      private GroupResource ReadSignleGroupResource(IDataReader reader)
      {
         GroupResource group = new GroupResource
         {
            Id = reader.GetNullableInt32("GroupResourceId"),
            Name = reader.GetString("Name"),
            Description = reader.GetString("Description"),
            Symbol = reader.GetString("Symbol"),
         };

         return group;
      }

      public (List<GroupResource>, long) ReadGroupsResourcesReport(IDatabaseTransaction transaction, RsqlData rsql)
      {
         long totalRows = 0;
         string sqlQuery = _rsqlMapper.BuildQuery("ReadGroupsResources", rsql, DbCommands.ReadGroupsResources, "id", noPaging: true);
         return (Read(sqlQuery, transaction,
            (reader) =>
            {
               if (rsql != null)
               {
                  totalRows = ReadPagingTotalRows(reader);
               }
               return ReadSignleGroupResource(reader);
            }), totalRows);
      }

      public (List<GroupResource>, long) ReadGroupsResourcesList(IDatabaseTransaction transaction, RsqlData rsql)
      {
         long totalRows = 0;
         string sqlQuery = _rsqlMapper.BuildQuery("ReadGroupsResources", rsql, DbCommands.ReadGroupsResources, "id");
         return (Read(sqlQuery, transaction,
            (reader) =>
            {
               if (rsql != null)
               {
                  totalRows = ReadPagingTotalRows(reader);
               }
               return ReadSignleGroupResource(reader);
            }), totalRows);
      }

      public GroupResource ReadGroupResource(IDatabaseTransaction transaction, long id)
      {
         return ReadFirst(DbCommands.ReadGroupResource, transaction,
            (command) =>
            {
               SetParameterTypeAndValue(command, ":groupResourceId", DbType.Int64, id);
            },
            (reader) =>
            {
               return ReadSignleGroupResource(reader);
            });
      }

      public long? CreateGroupResource(IDatabaseTransaction transaction, GroupResource groupResource)
      {
         return InsertOneWithLongOutputId(DbCommands.CreateGroupResource, transaction,
           (command) =>
           {
              SetParameterTypeAndValue(command, ":name", DbType.String, groupResource.Name);
              SetParameterTypeAndValue(command, ":description", DbType.String, groupResource.Description);
              SetParameterTypeAndValue(command, ":symbol", DbType.String, groupResource.Symbol);
           }
        );
      }

      public bool UpdateGroupResource(IDatabaseTransaction transaction, GroupResource groupResource)
      {
         return Update(DbCommands.UpdateGroupResource, transaction,
           (command) =>
           {
              SetParameterTypeAndValue(command, ":name", DbType.String, groupResource.Name);
              SetParameterTypeAndValue(command, ":description", DbType.String, groupResource.Description);
              SetParameterTypeAndValue(command, ":symbol", DbType.String, groupResource.Symbol);

              SetParameterTypeAndValue(command, ":groupResourceId", DbType.Int64, groupResource.Id);
           }
        ) > 0;
      }

      public bool DeleteGroupResource(IDatabaseTransaction transaction, long id)
      {
         return Update(DbCommands.DeleteGroupResource, transaction,
            (command) =>
            {
               SetParameterTypeAndValue(command, ":groupResourceId", DbType.Int64, id);
            }
         ) > 0;
      }
   }
}
