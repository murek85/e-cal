﻿using ECal.Common;
using ECal.Common.Mapping;
using ECal.Data.Common;
using ECal.Data.Common.DB.Users;
using ECal.Data.Database;
using ECal.Data.PostgresqlDB;
using ECal.Model.DB;
using System;
using System.Collections.Generic;
using System.Data;

namespace ECal.Data.DB.Users
{
   public partial class UsersDAO : PostgresqlDBDatabaseDAO, IUsersDAO
   {
      private readonly IRsqlMapper _rsqlMapper;

      public UsersDAO(DBDatabaseContainer databaseContainer, IRsqlMapper rsqlMapper) : base(databaseContainer)
      {
         _rsqlMapper = rsqlMapper;
      }

      private User ReadSignleUser(IDataReader reader)
      {
         User user = new User
         {
            Id = reader.GetNullableGuid("Id"),
            UserName = reader.GetString("UserName"),
            //EmployeeNumber = reader.GetString("UserName"),
            Email = reader.GetString("Email"),
            EmailConfirmed = reader.GetNullableBoolean("EmailConfirmed"),
            FirstName = reader.GetString("FirstName"),
            LastName = reader.GetString("LastName"),
            PhoneNumber = reader.GetString("PhoneNumber"),
            //IsActive = reader.GetNullableBoolean("IsActive"),
            //IsDomain = reader.GetNullableBoolean("IsDomain"),
            //LockoutEnd = reader.GetNullableDateTime("LockoutEnd"),
            //LastValidLogin = reader.GetNullableDateTime("LastValidLogin"),
            //LastIncorrectLogin = reader.GetNullableDateTime("LastIncorrectLogin")
         };

         return user;
      }

      public (List<User>, long) ReadUsersReport(IDatabaseTransaction transaction, RsqlData rsql)
      {
         long totalRows = 0;
         string sqlQuery = _rsqlMapper.BuildQuery("ReadUsers", rsql, DbCommands.ReadUsers, "id", noPaging: true);
         return (Read(sqlQuery, transaction,
            (reader) =>
            {
               if (rsql != null)
               {
                  totalRows = ReadPagingTotalRows(reader);
               }
               return ReadSignleUser(reader);
            }), totalRows);
      }

      public (List<User>, long) ReadUsersList(IDatabaseTransaction transaction, RsqlData rsql)
      {
         long totalRows = 0;
         string sqlQuery = _rsqlMapper.BuildQuery("ReadUsers", rsql, DbCommands.ReadUsers, "id");
         return (Read(sqlQuery, transaction,
            (reader) =>
            {
               if (rsql != null)
               {
                  totalRows = ReadPagingTotalRows(reader);
               }
               return ReadSignleUser(reader);
            }), totalRows);
      }

      public User ReadUser(IDatabaseTransaction transaction, Guid aspId)
      {
         return ReadFirst(DbCommands.ReadUser, transaction,
            (command) =>
            {
               SetParameterTypeAndValue(command, ":aspId", DbType.Guid, aspId);
            },
            (reader) =>
            {
               return ReadSignleUser(reader);
            });
      }

      public User ReadUser(IDatabaseTransaction transaction, string email)
      {
         return ReadFirst(DbCommands.ReadUser, transaction,
            (command) =>
            {
               SetParameterTypeAndValue(command, ":email", DbType.String, email);
            },
            (reader) =>
            {
               return ReadSignleUser(reader);
            });
      }

      public long? CreateUser(IDatabaseTransaction transaction, User user)
      {
         return InsertOneWithLongOutputId(DbCommands.CreateUser, transaction,
           (command) =>
           {
              //SetParameterTypeAndValue(command, ":name", DbType.String, application.Name);
           }
        );
      }

      public bool UpdateUser(IDatabaseTransaction transaction, User user)
      {
         return Update(DbCommands.UpdateUser, transaction,
           (command) =>
           {

              SetParameterTypeAndValue(command, ":applicationId", DbType.Int64, user.Id);
           }
        ) > 0;
      }

      public bool DeleteUser(IDatabaseTransaction transaction, Guid id)
      {
         return Update(DbCommands.DeleteUser, transaction,
            (command) =>
            {
               SetParameterTypeAndValue(command, ":applicationId", DbType.Int64, id);
            }
         ) > 0;
      }

      public bool UpdateUserLogged(IDatabaseTransaction transaction, User user)
      {
         throw new NotImplementedException();
      }
   }
}
