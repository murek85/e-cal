﻿namespace ECal.Data.DB.Users
{
   public partial class UsersDAO
   {
      private static class DbCommands
      {
         internal static string ReadUsers = @"
			SELECT
				au.""Id"", au.""UserName"", au.""Email"", au.""EmailConfirmed"", wu.""FirstName"", wu.""LastName"", wu.""PhoneNumber""
			FROM ""AspNetUsers"" au
         LEFT JOIN ""WisUsers"" wu ON wu.""UserId"" = au.""Id""";

         internal static string ReadUser = @"
			SELECT
				au.""Id"", au.""UserName"", au.""Email"", au.""EmailConfirmed"", wu.""FirstName"", wu.""LastName"", wu.""PhoneNumber""
			FROM ""AspNetUsers"" au
         LEFT JOIN ""WisUsers"" wu ON wu.""UserId"" = au.""Id""
			WHERE au.""Id"" = :aspId";

         internal static string CreateUser = @"
         INSERT INTO ""WisApplications"" (""Name"", ""Description"", ""Symbol"", ""ApiUrl"", ""AppUrl"")
         VALUES (:name, :description, :symbol, :apiurl, :appurl)
         RETURNING ""ApplicationId"" AS ""Id""
         ";

         internal static string UpdateUser = @"
         UPDATE ""WisApplications""
            SET ""Name"" = :name, ""Description"" = :description, ""Symbol"" = :symbol, ""ApiUrl"" = :apiurl, ""AppUrl"" = :appurl
         WHERE ""ApplicationId"" = :applicationId
         ";

         internal static string DeleteUser = @"
         DELETE FROM ""WisApplications"" wa
         WHERE wa.""ApplicationId"" = :applicationId
         ";
      }
   }
}
