﻿using ECal.Common;
using ECal.Common.Mapping;
using ECal.Data.Common;
using ECal.Data.Common.DB.Roles;
using ECal.Data.Database;
using ECal.Data.PostgresqlDB;
using ECal.Model.DB;
using System;
using System.Collections.Generic;
using System.Data;

namespace ECal.Data.DB.Roles
{
   public partial class RolesDAO : PostgresqlDBDatabaseDAO, IRolesDAO
   {
      private readonly IRsqlMapper _rsqlMapper;

      public RolesDAO(DBDatabaseContainer databaseContainer, IRsqlMapper rsqlMapper) : base(databaseContainer)
      {
         _rsqlMapper = rsqlMapper;
      }

      private Role ReadSignleRole(IDataReader reader)
      {
         Role role = new Role
         {
            Id = reader.GetNullableInt32("RoleId"),
            Name = reader.GetString("Name"),
            Description = reader.GetString("Description"),
            Symbol = reader.GetString("Symbol"),
         };

         return role;
      }

      public (List<Role>, long) ReadRolesReport(IDatabaseTransaction transaction, RsqlData rsql)
      {
         long totalRows = 0;
         string sqlQuery = _rsqlMapper.BuildQuery("ReadRoles", rsql, DbCommands.ReadRoles, "id", noPaging: true);
         return (Read(sqlQuery, transaction,
            (reader) =>
            {
               if (rsql != null)
               {
                  totalRows = ReadPagingTotalRows(reader);
               }
               return ReadSignleRole(reader);
            }), totalRows);
      }

      public (List<Role>, long) ReadRolesList(IDatabaseTransaction transaction, RsqlData rsql)
      {
         long totalRows = 0;
         string sqlQuery = _rsqlMapper.BuildQuery("ReadRoles", rsql, DbCommands.ReadRoles, "id");
         return (Read(sqlQuery, transaction,
            (reader) =>
            {
               if (rsql != null)
               {
                  totalRows = ReadPagingTotalRows(reader);
               }
               return ReadSignleRole(reader);
            }), totalRows);
      }

      public List<Role> ReadRolesByUser(IDatabaseTransaction transaction, Guid id)
      {
         throw new NotImplementedException();
      }

      public Role ReadRole(IDatabaseTransaction transaction, long id)
      {
         return ReadFirst(DbCommands.ReadRole, transaction,
            (command) =>
            {
               SetParameterTypeAndValue(command, ":roleId", DbType.Int64, id);
            },
            (reader) =>
            {
               return ReadSignleRole(reader);
            });
      }

      public long? CreateRole(IDatabaseTransaction transaction, Role role)
      {
         return InsertOneWithLongOutputId(DbCommands.CreateRole, transaction,
           (command) =>
           {
              SetParameterTypeAndValue(command, ":name", DbType.String, role.Name);
              SetParameterTypeAndValue(command, ":description", DbType.String, role.Description);
              SetParameterTypeAndValue(command, ":symbol", DbType.String, role.Symbol);
           }
        );
      }

      public bool UpdateRole(IDatabaseTransaction transaction, Role role)
      {
         return Update(DbCommands.UpdateRole, transaction,
           (command) =>
           {
              SetParameterTypeAndValue(command, ":name", DbType.String, role.Name);
              SetParameterTypeAndValue(command, ":description", DbType.String, role.Description);
              SetParameterTypeAndValue(command, ":symbol", DbType.String, role.Symbol);

              SetParameterTypeAndValue(command, ":roleId", DbType.Int64, role.Id);
           }
        ) > 0;
      }

      public bool DeleteRole(IDatabaseTransaction transaction, long id)
      {
         return Update(DbCommands.DeleteRole, transaction,
            (command) =>
            {
               SetParameterTypeAndValue(command, ":roleId", DbType.Int64, id);
            }
         ) > 0;
      }

      public bool VerifyAssigningRoleToUser(IDatabaseTransaction transaction, Guid userId, long roleId)
      {
         throw new NotImplementedException();
      }
   }
}
