﻿namespace ECal.Data.DB.Roles
{
   public partial class RolesDAO
   {
      private static class DbCommands
      {
         internal static string ReadRoles = @"
			SELECT
				r.""RoleId"", r.""Name"", r.""Description"", r.""Symbol""
			FROM ""WisRoles"" r";

         internal static string ReadRole = @"
			SELECT
				r.""RoleId"", r.""Name"", r.""Description"", r.""Symbol""
			FROM ""WisRoles"" r
			WHERE r.""RoleId"" = :roleId";

         internal static string CreateRole = @"
         INSERT INTO ""WisRoles"" (""Name"", ""Description"", ""Symbol"")
         VALUES (:name, :description, :symbol)
         RETURNING ""RoleId"" AS ""Id""
         ";

         internal static string UpdateRole = @"
         UPDATE ""WisRoles""
            SET ""Name"" = :name, ""Description"" = :description, ""Symbol"" = :symbol
         WHERE ""RoleId"" = :roleId
         ";

         internal static string DeleteRole = @"
         DELETE FROM ""WisRoles"" r
         WHERE r.""RoleId"" = :roleId
         ";
      }
   }
}
