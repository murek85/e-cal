﻿using ECal.Business.Common;
using ECal.Common;
using ECal.Common.Logging;
using ECal.Model;
using iTextSharp.text;
using iTextSharp.text.pdf;
using NPOI.OpenXmlFormats.Wordprocessing;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.XWPF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ECal.Business.Managers
{
   public class ReportManager : IReportManager
   {
      public IAppSettingsManager AppSettingsManager { get; set; }

      public ILogger Logger { get; set; }

      public void Initialize(Dictionary<string, object> parameters = null)
      {

      }

      public void Uninitialize()
      {

      }

      /// <summary>
      /// Generowanie raportu w postaci pliku pdf.
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="reportBuilder"></param>
      /// <param name="data">Pobrane dane do zapisania w pliku.</param>
      /// <returns>Wygenerowane dane w postaci pliku w odpowiednim formacie.</returns>
      private static byte[] ExportPdf<T>(ReportBuilder reportBuilder, List<T> data)
      {
         MemoryStream ms = new();

         iTextSharp.text.Document document = new();
         PdfWriter writer = PdfWriter.GetInstance(document, ms);

         document.SetPageSize(PageSize.A4.Rotate());
         document.SetMargins(25, 25, 25, 25);
         document.Open();

         BaseFont comic = BaseFont.CreateFont(BaseFont.HELVETICA, "ISO-8859-2", BaseFont.NOT_EMBEDDED);
         Font font = new(comic);

         Paragraph header = new()
         {
            Alignment = Element.ALIGN_CENTER,
            Font = font
         };
         header.Font.Size = 20;
         header.Add(String.Format("\n{0}\n\n", reportBuilder.title));
         document.Add(header);

         Paragraph additional = new()
         {
            Alignment = Element.ALIGN_RIGHT,
            Font = font
         };
         additional.Font.Size = 12;
         additional.Add(String.Format("Data wygenerowania: {0}", DateTime.UtcNow.ConvertDateTimeToString(reportBuilder.locale)));
         document.Add(additional);

         Paragraph space = new()
         {
            Alignment = Element.ALIGN_RIGHT,
            Font = font
         };
         space.Font.Size = 12;
         space.Add(Chunk.Newline);
         document.Add(space);

         PdfPTable table = new(reportBuilder.labels.Split(';').Length)
         {
            WidthPercentage = 100
         };

         void CreateHeader(string name)
         {
            PdfPCell cell = new()
            {
               HorizontalAlignment = Element.ALIGN_LEFT,
               VerticalAlignment = Element.ALIGN_CENTER,
               Phrase = new Phrase(name, font)
            };
            cell.Phrase.Font.Size = 12;
            table.AddCell(cell);
         }

         int colIdx = 0;
         foreach (var column in reportBuilder.labels.Split(';'))
         {
            CreateHeader(column);
            colIdx++;
         }

         void CreateCell(string value)
         {
            PdfPCell cell = new()
            {
               HorizontalAlignment = Element.ALIGN_LEFT,
               VerticalAlignment = Element.ALIGN_CENTER,
               Phrase = new Phrase(value, font)
            };
            cell.Phrase.Font.Size = 12;
            table.AddCell(cell);
         }

         int rowIndex = 0;
         foreach (var item in data.Nullable())
         {
            foreach (var column in reportBuilder.aliases.Split(';'))
            {
               CreateCell(PrepareData<T>(item, column, reportBuilder.locale));
            }
            rowIndex++;
         }

         document.Add(table);
         document.Close();
         writer.Close();
         ms.Close();

         return ms.ToArray();
      }

      /// <summary>
      /// Generowanie raportu w postaci pliku csv.
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="reportBuilder"></param>
      /// <param name="data">Pobrane dane do zapisania w pliku.</param>
      /// <returns>Wygenerowane dane w postaci pliku w odpowiednim formacie.</returns>
      private static byte[] ExportCsv<T>(ReportBuilder reportBuilder, List<T> data)
      {
         MemoryStream ms = new();
         StreamWriter sw = new(ms, Encoding.UTF8);

         int colIdx = 0;
         foreach (var column in reportBuilder.labels.Split(';'))
         {
            sw.Write($"{column};");
            colIdx++;
         }
         sw.WriteLine();

         int rowIdx = 0;
         foreach (var item in data.Nullable())
         {
            foreach (var column in reportBuilder.aliases.Split(';'))
            {
               sw.Write($"{PrepareData<T>(item, column, reportBuilder.locale)};");
            }
            sw.WriteLine();
            rowIdx++;
         }
         sw.Flush();

         return ms.ToArray();
      }

      /// <summary>
      /// Generowanie raportu w postaci pliku xlsx.
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="reportBuilder"></param>
      /// <param name="data">Pobrane dane do zapisania w pliku.</param>
      /// <returns>Wygenerowane dane w postaci pliku w odpowiednim formacie.</returns>
      private static byte[] ExportXlsx<T>(ReportBuilder reportBuilder, List<T> data)
      {
         IWorkbook workbook = new XSSFWorkbook();
         ISheet sheet = workbook.CreateSheet(reportBuilder.title);

         int colIdx = 0;
         IRow parameterHeaderRow = sheet.CreateRow(0);
         foreach (var column in reportBuilder.labels.Split(';'))
         {
            parameterHeaderRow.CreateCell(colIdx).SetCellValue(column);
            colIdx++;
         }

         int rowIdx = 0;
         foreach (var item in data.Nullable())
         {
            IRow row = sheet.CreateRow(rowIdx + 1);
            colIdx = 0;
            foreach (var column in reportBuilder.aliases.Split(';'))
            {
               row.CreateCell(colIdx).SetCellValue(PrepareData<T>(item, column, reportBuilder.locale));
               colIdx++;
            }
            sheet.SetColumnWidth(rowIdx, 20 * 256);
            rowIdx++;
         }

         sheet.SetColumnWidth(0, 20 * 256);

         MemoryStream ms = new();
         workbook.Write(ms);

         return ms.ToArray();
      }

      /// <summary>
      /// Generowanie raportu w postaci pliku docx.
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="reportBuilder"></param>
      /// <param name="data">Pobrane dane do zapisania w pliku.</param>
      /// <returns>Wygenerowane dane w postaci pliku w odpowiednim formacie.</returns>
      private static byte[] ExportDocx<T>(ReportBuilder reportBuilder, List<T> data)
      {
         XWPFDocument document = new();
         document.Document.body.sectPr = new CT_SectPr
         {
            pgSz = new CT_PageSz()
         };
         document.Document.body.sectPr.pgSz.orient = ST_PageOrientation.landscape;
         // rozmiar A4 poziomy
         document.Document.body.sectPr.pgSz.h = 11900;
         document.Document.body.sectPr.pgSz.w = 16840;

         XWPFParagraph paragHeader = document.CreateParagraph();
         paragHeader.Alignment = ParagraphAlignment.CENTER;
         paragHeader.VerticalAlignment = TextAlignment.CENTER;
         XWPFRun header = paragHeader.CreateRun();
         header.IsBold = true;
         header.SetText(reportBuilder.title);
         header.FontFamily = "Arial";
         header.FontSize = 18;

         XWPFParagraph paragDateGen = document.CreateParagraph();
         paragDateGen.Alignment = ParagraphAlignment.RIGHT;
         paragDateGen.VerticalAlignment = TextAlignment.CENTER;
         XWPFRun dateGen = paragDateGen.CreateRun();
         header.IsBold = true;
         dateGen.SetText(String.Format("Data wygenerowania: {0}", DateTime.UtcNow.ConvertDateTimeToString(reportBuilder.locale)));
         dateGen.FontFamily = "Arial";
         dateGen.FontSize = 14;

         XWPFTable table = document.CreateTable();
         table.Width = 5000;
         XWPFParagraph paragRow = document.CreateParagraph();
         paragRow.Alignment = ParagraphAlignment.CENTER;
         paragRow.VerticalAlignment = TextAlignment.CENTER;

         int colIdx = 0;
         XWPFTableRow row = table.GetRow(0);
         CT_Tc tc = row.GetCell(colIdx).GetCTTc();
         tc.tcPr = new CT_TcPr
         {
            tcW = new CT_TblWidth
            {
                type = ST_TblWidth.pct,
                w = "100"
            }
         };

         foreach (var column in reportBuilder.labels.Split(';'))
         {
            if (colIdx < 1)
            {
               row.GetCell(0).SetText(column);
            }
            else
            {
               row.AddNewTableCell().SetText(column);
            }
            colIdx++;
         }

         int rowIdx = 0;
         foreach (var item in data.Nullable())
         {
            row = table.CreateRow();
            colIdx = 0;
            foreach (var column in reportBuilder.aliases.Split(';'))
            {
                row.GetCell(colIdx).SetText(PrepareData<T>(item, column, reportBuilder.locale));

                tc = row.GetCell(colIdx).GetCTTc();
                tc.tcPr = new CT_TcPr
                {
                    tcW = new CT_TblWidth
                    {
                        type = ST_TblWidth.pct,
                        w = "100"
                    }
                };

                colIdx++;
            }

            rowIdx++;
         }

         MemoryStream ms = new();
         document.Write(ms);

         return ms.ToArray();
      }

      private static string FormatDateTime<T>(T item)
      {
         return item switch
         {
            _ => "dd-MM-yyyy HH:mm:ss"
         };
      }

      private static string PrepareData<T>(T item, string column, string locale)
        {
            var value = item.GetPropertyValue(column);
            return value switch
            {
                DateTime => Convert.ToDateTime(value).ConvertDateTimeToString(locale, FormatDateTime(item)),
                Boolean => Convert.ToBoolean(value) ? "TAK" : "NIE",
                _ => Convert.ToString(value)
            };
        }

      /// <summary>
      /// Generowanie odpowiedniego typu raportu do pliku PDF/CSV/XLSX.
      /// </summary>
      /// <typeparam name="T">Typ obiektu</typeparam>
      /// <param name="reportBuilder"></param>
      /// <param name="data">Pobrane dane wymagane przy generowaniu pliku raportu.</param>
      /// <returns>Dne wygenerowane pliku w odpowiednim formacie.</returns>
      public byte[] ExportData<T>(ReportBuilder reportBuilder, List<T> data) => reportBuilder.format switch
      {
          "pdf" => ExportPdf(reportBuilder, data),
          "csv" => ExportCsv(reportBuilder, data),
          "xlsx" => ExportXlsx(reportBuilder, data),
          "docx" => ExportDocx(reportBuilder, data),
          _ => null,
      };
   }
}
