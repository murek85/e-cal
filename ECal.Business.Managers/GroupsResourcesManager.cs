﻿using ECal.Business.Common;
using ECal.Common;
using ECal.Data.Common.DB.Groups.Resources;
using ECal.Model.DB;
using System.Collections.Generic;
using DB = ECal.Model.DB;


namespace ECal.Business.Managers
{
   public class GroupsResourcesManager : IGroupsResourcesManager
   {
      private readonly IGroupsResourcesDAO _groupsResourcesDAO;

      public GroupsResourcesManager(IGroupsResourcesDAO groupsResourcesDAO)
      {
         _groupsResourcesDAO = groupsResourcesDAO;
      }

      public void Initialize(Dictionary<string, object> parameters = null)
      {
      }

      public void Uninitialize()
      {
      }

      public (List<DB.GroupResource>, long) GetGroupsResourcesReport(RsqlData paging)
      {
         return _groupsResourcesDAO.ReadGroupsResourcesReport(null, paging);
      }

      public (List<DB.GroupResource>, long) GetGroupsResourcesList(RsqlData paging)
      {
         return _groupsResourcesDAO.ReadGroupsResourcesList(null, paging);
      }

      public GroupResource GetGroupResource(long id)
      {
         return _groupsResourcesDAO.ReadGroupResource(null, id);
      }

      public long? CreateGroupResource(GroupResource groupResource)
      {
         return _groupsResourcesDAO.CreateGroupResource(null, groupResource);
      }

      public bool UpdateGroupResource(GroupResource groupResource)
      {
         return _groupsResourcesDAO.UpdateGroupResource(null, groupResource);
      }

      public bool DeleteGroupResource(long id)
      {
         return _groupsResourcesDAO.DeleteGroupResource(null, id);
      }
   }
}
