﻿using ECal.Business.Common;
using ECal.Data.Common.Ldap;
using ECal.Model.Ldap;
using System.Collections.Generic;
using System.Linq;

namespace ECal.Business.Managers
{
	public class LdapManager : ILdapManager
	{
		private ILdapDAO _ldapUsersDAO;

		public LdapManager(
			ILdapDAO ldapUsersDAO)
		{
			_ldapUsersDAO = ldapUsersDAO;
		}

		public void Initialize(Dictionary<string, object> parameters = null)
		{

		}

		public void Uninitialize()
		{
		}

		public IList<LdapUser> GetUsersByEmailAddress(string emailAddress)
		{
			List<LdapUser> ldapUsers = _ldapUsersDAO.GetUsersByEmailAddress(emailAddress).ToList();
			return ldapUsers;
		}

		public IList<LdapUser> GetAllUsers()
		{
			List<LdapUser> ldapUsers = _ldapUsersDAO.GetAllUsers().ToList();
			return ldapUsers;
		}

		public LdapUser GetUserByUserPrincipalName(string userPrincipalName)
		{
			LdapUser ldapUser = _ldapUsersDAO.GetUserByUserPrincipalName(userPrincipalName);
			return ldapUser;
		}

		public LdapUser GetUserBySAMAccountName(string samAccountName)
		{
			LdapUser ldapUser = _ldapUsersDAO.GetUserBySAMAccountName(samAccountName);
			return ldapUser;
		}

		public LdapUser GetUserByObjectGuid(string objectGuid)
		{
			LdapUser ldapUser = _ldapUsersDAO.GetUserByObjectGuid(objectGuid);
			return ldapUser;
		}

		public bool Authenticate(string distinguishedName, string password)
		{
			return _ldapUsersDAO.Authenticate(distinguishedName, password);
		}
	}
}
