﻿using ECal.Business.Common;
using ECal.Common;
using ECal.Data.Common.DB.Applications;
using ECal.Model.DB;
using System.Collections.Generic;
using DB = ECal.Model.DB;


namespace ECal.Business.Managers
{
   public class ApplicationsManager : IApplicationsManager
   {
      private readonly IApplicationsDAO _applicationsDAO;

      public ApplicationsManager(IApplicationsDAO applicationsDAO)
      {
         _applicationsDAO = applicationsDAO;
      }

      public void Initialize(Dictionary<string, object> parameters = null)
      {
      }

      public void Uninitialize()
      {
      }

      public (List<DB.Application>, long) GetApplicationsReport(RsqlData paging)
      {
         return _applicationsDAO.ReadApplicationsReport(null, paging);
      }

      public (List<DB.Application>, long) GetApplicationsList(RsqlData paging)
      {
         return _applicationsDAO.ReadApplicationsList(null, paging);
      }

      public Application GetApplication(long id)
      {
         return _applicationsDAO.ReadApplication(null, id);
      }

      public long? CreateApplication(Application application)
      {
         return _applicationsDAO.CreateApplication(null, application);
      }

      public bool UpdateApplication(Application application)
      {
         return _applicationsDAO.UpdateApplication(null, application);
      }

      public bool DeleteApplication(long id)
      {
         return _applicationsDAO.DeleteApplication(null, id);
      }
   }
}
