﻿using ECal.Business.Common;
using ECal.Common;
using ECal.Data.Common.DB.Permissions;
using ECal.Model.DB;
using System.Collections.Generic;
using DB = ECal.Model.DB;

namespace ECal.Business.Managers
{
   public class PermissionsManager : IPermissionsManager
   {
      private readonly IPermissionsDAO _permissionsDAO;

      public PermissionsManager(IPermissionsDAO permissionsDAO)
      {
         _permissionsDAO = permissionsDAO;
      }

      public void Initialize(Dictionary<string, object> parameters = null)
      {
      }

      public void Uninitialize()
      {
      }

      public (List<DB.Permission>, long) GetPermissionsReport(RsqlData paging)
      {
         return _permissionsDAO.ReadPermissionsReport(null, paging);
      }

      public (List<DB.Permission>, long) GetPermissionsList(RsqlData paging)
      {
         return _permissionsDAO.ReadPermissionsList(null, paging);
      }

      public Permission GetPermission(long id)
      {
         return _permissionsDAO.ReadPermission(null, id);
      }

      public long? CreatePermission(Permission permission)
      {
         return _permissionsDAO.CreatePermission(null, permission);
      }

      public bool UpdatePermission(Permission permission)
      {
         return _permissionsDAO.UpdatePermission(null, permission);
      }

      public bool DeletePermission(long id)
      {
         return _permissionsDAO.DeletePermission(null, id);
      }
   }
}
