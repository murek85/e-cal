﻿using Microsoft.Extensions.Configuration;
using ECal.Business.Common;

namespace ECal.Business.Managers
{
	/// <summary>
	/// Implementacja menedżera dostępu do ustawień aplikacji
	/// </summary>
	public class AppSettingsManager : IAppSettingsManager
	{
		public IConfiguration Configuration { get; set; }

		public AppSettingsManager(IConfiguration _configuration)
		{
			Configuration = _configuration;
		}

		/// <summary>
		/// Zwraca wartość ustawienia o podanyum kluczu
		/// </summary>
		/// <param name="appSettingKey">Klucz ustawienia</param>
		public string GetAppSettingValue(string appSettingKey)
		{
			return (Configuration == null) ? null : Configuration[appSettingKey];
		}

	}
}
