﻿using ECal.Business.Common;
using ECal.Common;
using ECal.Data.Common.DB.Groups.Users;
using ECal.Model.DB;
using System.Collections.Generic;
using DB = ECal.Model.DB;


namespace ECal.Business.Managers
{
   public class GroupsUsersManager : IGroupsUsersManager
   {
      private readonly IGroupsUsersDAO _groupsUsersDAO;

      public GroupsUsersManager(IGroupsUsersDAO groupsUsersDAO)
      {
         _groupsUsersDAO = groupsUsersDAO;
      }

      public void Initialize(Dictionary<string, object> parameters = null)
      {
      }

      public void Uninitialize()
      {
      }

      public (List<DB.GroupUser>, long) GetGroupsUsersReport(RsqlData paging)
      {
         return _groupsUsersDAO.ReadGroupsUsersReport(null, paging);
      }

      public (List<DB.GroupUser>, long) GetGroupsUsersList(RsqlData paging)
      {
         return _groupsUsersDAO.ReadGroupsUsersList(null, paging);
      }

      public GroupUser GetGroupUser(long id)
      {
         return _groupsUsersDAO.ReadGroupUser(null, id);
      }

      public long? CreateGroupUser(GroupUser groupUser)
      {
         return _groupsUsersDAO.CreateGroupUser(null, groupUser);
      }

      public bool UpdateGroupUser(GroupUser groupUser)
      {
         return _groupsUsersDAO.UpdateGroupUser(null, groupUser);
      }

      public bool DeleteGroupUser(long id)
      {
         return _groupsUsersDAO.DeleteGroupUser(null, id);
      }
   }
}
