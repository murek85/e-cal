﻿using ECal.Business.Common;
using ECal.Common;
using ECal.Data.Common.DB.Users;
using ECal.Model.DB;
using System;
using System.Collections.Generic;
using DB = ECal.Model.DB;

namespace ECal.Business.Managers
{
   public class UsersManager : IUsersManager
   {
      private readonly IUsersDAO _usersDAO;

      public UsersManager(IUsersDAO usersDAO)
      {
         _usersDAO = usersDAO;
      }

      public void Initialize(Dictionary<string, object> parameters = null)
      {
      }

      public void Uninitialize()
      {
      }

      public (List<DB.User>, long) GetUsersReport(RsqlData paging)
      {
         return _usersDAO.ReadUsersReport(null, paging);
      }

      public (List<DB.User>, long) GetUsersList(RsqlData paging)
      {
         return _usersDAO.ReadUsersList(null, paging);
      }

      public DB.User GetUser(Guid id)
      {
         return _usersDAO.ReadUser(null, id);
      }

      public User GetUser(string email)
      {
         return _usersDAO.ReadUser(null, email);
      }

      public long? CreateUser(User user)
      {
         return _usersDAO.CreateUser(null, user);
      }

      public bool UpdateUser(User user)
      {
         return _usersDAO.UpdateUser(null, user);
      }

      public bool DeleteUser(Guid id)
      {
         return _usersDAO.DeleteUser(null, id);
      }

      public bool UpdateUserLogged(User user)
      {
         return _usersDAO.UpdateUserLogged(null, user);
      }
   }
}
