﻿using ECal.Business.Common;
using ECal.Common;
using ECal.Data.Common.DB.Roles;
using ECal.Model.DB;
using System;
using System.Collections.Generic;
using DB = ECal.Model.DB;

namespace ECal.Business.Managers
{
   public class RolesManager : IRolesManager
   {
      private readonly IRolesDAO _rolesDAO;

      public RolesManager(IRolesDAO rolesDAO)
      {
         _rolesDAO = rolesDAO;
      }

      public void Initialize(Dictionary<string, object> parameters = null)
      {
      }

      public void Uninitialize()
      {
      }

      public (List<DB.Role>, long) GetRolesReport(RsqlData paging)
      {
         return _rolesDAO.ReadRolesReport(null, paging);
      }

      public (List<DB.Role>, long) GetRolesList(RsqlData paging)
      {
         return _rolesDAO.ReadRolesList(null, paging);
      }

      public Role GetRole(long id)
      {
         return _rolesDAO.ReadRole(null, id);
      }

      public long? CreateRole(Role role)
      {
         return _rolesDAO.CreateRole(null, role);
      }

      public bool UpdateRole(Role role)
      {
         return _rolesDAO.UpdateRole(null, role);
      }

      public bool DeleteRole(long id)
      {
         return _rolesDAO.DeleteRole(null, id);
      }

      public bool VerifyAssigningRoleToUser(Guid userId, long roleId)
      {
         return _rolesDAO.VerifyAssigningRoleToUser(null, userId, roleId);
      }
   }
}
