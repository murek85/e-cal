using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;

namespace ECal.Agent
{
	public class Startup
	{
		public Startup(IConfiguration configuration, IWebHostEnvironment environment)
		{
			Configuration = configuration;
			Environment = environment;
		}

		public IConfiguration Configuration { get; }

		public IWebHostEnvironment Environment { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services
				.AddControllers()
				.AddNewtonsoftJson(options =>
				{
					options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
					options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
				})
				.ConfigureApiBehaviorOptions(options =>
				{
					options.SuppressModelStateInvalidFilter = true;
				});

			services
				.AddRouting(options =>
				{
					options.LowercaseUrls = true;
				});

			services
				.Configure<ForwardedHeadersOptions>(options =>
				{
					options.ForwardedHeaders =
						ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
				});

			services.AddCors(options =>
			{
				options.AddPolicy("CorsPolicy",
					builder => builder
						.WithOrigins(Configuration.GetSection("Cors:Origins").Get<string[]>())
						.AllowAnyMethod()
						.AllowAnyHeader()
						.AllowCredentials()
					);
			});

			services.AddApiVersioning(c =>
			{
				c.ReportApiVersions = true;
				c.AssumeDefaultVersionWhenUnspecified = true;
				c.DefaultApiVersion = new ApiVersion(1, 0);
			});
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			app.UseCors("CorsPolicy");

			app.UseForwardedHeaders(new ForwardedHeadersOptions
			{
				ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
			});

			if (env.IsDevelopment())
			{
			}

			app.UseForwardedHeaders();
			app.UseRouting();

			app.Use((context, next) =>
			{
				context.Request.EnableBuffering();
				return next();
			});

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
				endpoints.MapDefaultControllerRoute();
			});
		}
	}
}
