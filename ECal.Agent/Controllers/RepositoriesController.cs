﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Syroot.Windows.IO;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace ECal.Agent.Controllers
{
	[ApiController]
	[Route("api/repositories")]
	public class RepositoriesController : ControllerBase
	{
		private readonly ILogger _logger;
		public RepositoriesController(
			ILoggerFactory loggerFactory)
		{
			_logger = loggerFactory.CreateLogger<RepositoriesController>();
		}

		[HttpPost]
		[RequestFormLimits(MultipartBodyLengthLimit = 52428800000)]
		[RequestSizeLimit(52428800000)]
		public async Task<IActionResult> Upload(string type)
		{
			string methodName = nameof(Upload);

			string filePath = String.Empty;
			string outputDir = $@"{Environment.GetFolderPath(Environment.SpecialFolder.Desktop)}";
			switch (type)
			{
				case "desktop":
					outputDir = $@"{Environment.GetFolderPath(Environment.SpecialFolder.Desktop)}";
					break;
				case "mydocuments":
					outputDir = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}";
					break;
			}

			_logger.LogDebug($"[{methodName}] Katalog zapisu: {outputDir}.");

			string directoryPath = Path.Combine(outputDir);
			IFormFileCollection files = Request.Form.Files;
			foreach (IFormFile file in files)
			{
				_logger.LogDebug($"[{methodName}] Plik: {file.FileName} [{file.Length / 1024} KB].");
				filePath = Path.Combine(directoryPath, file.FileName);
				using var fileStream = new FileStream(filePath, FileMode.Create);
				file.CopyTo(fileStream);
			}

			return Ok(new { progress = 100 });
		}

		[HttpGet]
		[Route("~/api/status")]
		public IActionResult Status() => Ok(true);

		[HttpGet]
		[Route("~/api/machine")]
		public ActionResult<string> GetMachineName() => Environment.MachineName;

		[HttpGet]
		[Route("~/api/version")]
		public ActionResult<string> GetVersion() => Assembly.GetEntryAssembly().GetName().Version.ToString();
	}
}
