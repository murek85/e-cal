using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.EventLog;

namespace ECal.Agent
{
	public class Program
	{
		public static void Main(string[] args)
		{
			CreateHostBuilder(args).Build().Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureServices(services =>
				{
					services.Configure<EventLogSettings>(config =>
					{
						config.LogName = "ECalAgent API Service";
						config.SourceName = "ECalAgent API Service Source";
					});
				})
				.ConfigureLogging((ctx, builder) =>
				{
					builder.AddFilter("Microsoft", LogLevel.Debug);
					builder.AddFilter("System", LogLevel.Debug);
					builder.ClearProviders();
					builder.AddConsole();
					builder.AddFile(options => options.RootPath = ctx.HostingEnvironment.ContentRootPath);
				})
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseStartup<Startup>();
				})
				.UseWindowsService();
	}
}
