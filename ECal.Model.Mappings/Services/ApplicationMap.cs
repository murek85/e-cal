﻿using AutoMapper;
using ECal.Model.DB;
using ECal.Model.Services;

namespace ECal.Model.Mappings.Services
{
	public class ApplicationMap : Profile
	{
		public ApplicationMap()
		{
			CreateMap<Application, ApplicationViewModel>();
			CreateMap<ApplicationViewModel, Application>();
		}
	}
}
