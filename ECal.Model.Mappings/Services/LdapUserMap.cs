﻿using AutoMapper;
using ECal.Model.Ldap;
using ECal.Model.Services;

namespace ECal.Model.Mappings.Services
{
	public class LdapUserMap : Profile
	{
		public LdapUserMap()
		{
			CreateMap<LdapUser, LdapUserViewModel>();
			CreateMap<LdapUserViewModel, LdapUser>();
		}
	}
}
