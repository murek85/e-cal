﻿using AutoMapper;
using ECal.Model.DB;
using ECal.Model.Services;

namespace ECal.Model.Mappings.Services
{
	public class RoleMap : Profile
	{
		public RoleMap()
		{
			CreateMap<Role, RoleViewModel>();
			CreateMap<RoleViewModel, Role>();
		}
	}
}
