﻿using AutoMapper;
using ECal.Model.DB;
using ECal.Model.Services;

namespace ECal.Model.Mappings.Services
{
	public class UserMap : Profile
	{
		public UserMap()
		{
			CreateMap<User, UserViewModel>();
			CreateMap<UserViewModel, User>();
		}
	}
}
