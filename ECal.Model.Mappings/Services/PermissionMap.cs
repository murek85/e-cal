﻿using AutoMapper;
using ECal.Model.DB;
using ECal.Model.Services;

namespace ECal.Model.Mappings.Services
{
	public class PermissionMap : Profile
	{
		public PermissionMap()
		{
			CreateMap<Permission, PermissionViewModel>();
			CreateMap<PermissionViewModel, Permission>();
		}
	}
}
