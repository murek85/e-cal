﻿using AutoMapper;
using ECal.Model.DB;
using ECal.Model.Services;

namespace ECal.Model.Mappings.Services
{
	public class GroupResourceMap : Profile
	{
		public GroupResourceMap()
		{
			CreateMap<GroupResource, GroupResourceViewModel>();
			CreateMap<GroupResourceViewModel, GroupResource>();
		}
	}
}
