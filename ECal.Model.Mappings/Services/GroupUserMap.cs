﻿using AutoMapper;
using ECal.Model.DB;
using ECal.Model.Services;

namespace ECal.Model.Mappings.Services
{
	public class GroupUserMap : Profile
	{
		public GroupUserMap()
		{
			CreateMap<GroupUser, GroupUserViewModel>();
			CreateMap<GroupUserViewModel, GroupUser>();
		}
	}
}
