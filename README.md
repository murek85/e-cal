# ** # NET5 **

1. NET.Core SDK
2. VSCODE
3. OpenIddict
4. Npgsql
5. Mssql

> dotnet tool update --global dotnet-ef

> dotnet ef migrations add InitialCreate -c ECal.App.Contexts.KeysDbContext

> dotnet ef database update InitialCreate -c ECal.App.Contexts.KeysDbContext

> dotnet ef migrations add InitialCreate -c ECal.App.Contexts.ApplicationDbContext

> dotnet ef database update InitialCreate -c ECal.App.Contexts.ApplicationDbContext

> sudo nano /etc/systemd/system/kestrel-ecal.service

```
[Unit]
Description=ECal Server

[Service]
WorkingDirectory=/opt/ecalserver/api/publish
ExecStart=dotnet /opt/ecalserver/api/publish/ECal.App.dll
Restart=always
# Restart service after 10 seconds if the dotnet service crashes:
RestartSec=10
KillSignal=SIGINT
SyslogIdentifier=dotnet-ecal
User=root
Environment=ASPNETCORE_ENVIRONMENT=Production

[Install]
WantedBy=multi-user.target
```

> sudo systemctl enable kestrel-ecal.service

> systemctl daemon-reload

> sudo journalctl -fu kestrel-ecal.service

### Screenshots

![Login](./ECal.Web/images/2021_11_10_15_13_39_ecal.png)
![Dashboard](./ECal.Web/images/2021_11_10_15_15_43_ecal.png)
![Table](./ECal.Web/images/2021_11_10_15_15_58_ecal.png)
![Settings menu](./ECal.Web/images/2021_11_10_15_33_04_ecal.png)
![Dashboard dark 1](./ECal.Web/images/2021_11_10_15_24_33_ecal.png)
![Dashboard dark 2](./ECal.Web/images/2021_11_10_15_17_21_ecal.png)

### Components

-   https://material.angular.io/components
-   https://teradata.github.io/covalent/
-   https://material.angular.io/components/
-   https://materialdesignicons.com/
-   https://www.materialui.co/colors/
-   https://fomantic-ui.com/
