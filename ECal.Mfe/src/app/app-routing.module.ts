import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RouteFactory } from 'ng-micro-frontend';

const routes: Routes = [
  RouteFactory.createRoute('mfe-dashboard', 'http://localhost:4501'),
  RouteFactory.createRoute('mfe-management', 'http://localhost:4502'),
  RouteFactory.createRoute('mfe-games', 'http://localhost:4200'),
  RouteFactory.createRoute('mfe-reports', 'http://localhost:4200'),
  RouteFactory.createRoute('mfe-remote-desktop', 'http://localhost:4200'),
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
