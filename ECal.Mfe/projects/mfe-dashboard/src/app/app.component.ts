import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { environment } from '../environments/environment';

import { GlobalStore } from 'redux-micro-frontend';

import { createAction, createReducer, on } from '@ngrx/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  globalStore;
  public constructor(
    private readonly location: Location,
    private readonly router: Router
  ) {
    this.globalStore = GlobalStore.Get(false);
  }

  public ngOnInit(): void {
    if (environment.production) {
      this.router.initialNavigation();
      this.router.navigate([this.location.path()]);
    }
  }

  title = 'mfe-dashboard';

  add() {}
}
