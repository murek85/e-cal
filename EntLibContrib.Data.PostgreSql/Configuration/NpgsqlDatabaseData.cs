﻿using System;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Configuration;

namespace EntLibContrib.Data.PostgreSql.Configuration
{
	/// <summary>
	/// Describes a <see cref="NpgsqlDatabaseData"/> instance, aggregating information from a
	/// <see cref="ConnectionStringSettings"/> and any Postgresql-specific database information.
	/// </summary>
	public class NpgsqlDatabaseData : DatabaseData
	{
		///<summary>
		/// Initializes a new instance of the <see cref="NpgsqlDatabaseData"/> class with a connection string and a configuration
		/// source.
		///</summary>
		///<param name="connectionStringSettings">The <see cref="ConnectionStringSettings"/> for the represented database.</param>
		///<param name="configurationSource">The <see cref="IConfigurationSource"/> from which Postgresql-specific information
		/// should be retrieved.</param>
		public NpgsqlDatabaseData(ConnectionStringSettings connectionStringSettings, Func<string, ConfigurationSection> configurationSource)
				: base(connectionStringSettings, configurationSource)
		{
		}

		/// <summary>
		/// Builds the <see cref="Database" /> represented by this configuration object.
		/// </summary>
		/// <returns>
		/// A database.
		/// </returns>
		public override Database BuildDatabase()
		{
			return new NpgsqlDatabase(ConnectionString);
		}
	}
}