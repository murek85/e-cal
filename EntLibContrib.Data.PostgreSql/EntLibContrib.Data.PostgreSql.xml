<?xml version="1.0"?>
<doc>
    <assembly>
        <name>EntLibContrib.Data.PostgreSql</name>
    </assembly>
    <members>
        <member name="T:EntLibContrib.Data.PostgreSql.Configuration.NpgsqlDatabaseData">
            <summary>
            Describes a <see cref="T:EntLibContrib.Data.PostgreSql.Configuration.NpgsqlDatabaseData"/> instance, aggregating information from a
            <see cref="T:System.Configuration.ConnectionStringSettings"/> and any Postgresql-specific database information.
            </summary>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.Configuration.NpgsqlDatabaseData.#ctor(System.Configuration.ConnectionStringSettings,System.Func{System.String,System.Configuration.ConfigurationSection})">
            <summary>
             Initializes a new instance of the <see cref="T:EntLibContrib.Data.PostgreSql.Configuration.NpgsqlDatabaseData"/> class with a connection string and a configuration
             source.
            </summary>
            <param name="connectionStringSettings">The <see cref="T:System.Configuration.ConnectionStringSettings"/> for the represented database.</param>
            <param name="configurationSource">The <see cref="!:IConfigurationSource"/> from which Postgresql-specific information
             should be retrieved.</param>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.Configuration.NpgsqlDatabaseData.BuildDatabase">
            <summary>
            Builds the <see cref="T:Microsoft.Practices.EnterpriseLibrary.Data.Database" /> represented by this configuration object.
            </summary>
            <returns>
            A database.
            </returns>
        </member>
        <member name="T:EntLibContrib.Data.PostgreSql.NpgsqlDatabase">
            <summary>
            Represents a PostgreSQL database.
            </summary>
            <remarks>
            	<para>Internally uses Npgsql .NET Provider to connect to the database.</para>
            	<para>Revision 1: Steve Phillips  Date: 24 Sept 2009 - Updated to use EntLib 4.1 core and Npgsql Driver 2.0.6</para>
            </remarks>
            <author>Oleg Ufaev</author>
            <version>4.1</version>
        </member>
        <member name="F:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.ParameterToken">
            <summary>
            <para>Gets the parameter token used to delimit parameters for the PostgreSQL Server database.</para>
            </summary>
            <value>
            <para>The ':' symbol.</para>
            </value>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.#ctor(System.String)">
            <summary>
            Initializes a new instance of the <see cref="T:EntLibContrib.Data.PostgreSql.NpgsqlDatabase"/> class
            with a connection string.
            </summary>
            <param name="connectionString">The connection string.</param>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.AddInParameter(System.Data.Common.DbCommand,System.String,NpgsqlTypes.NpgsqlDbType)">
            <summary>
            Adds a new In <see cref="T:System.Data.Common.DbParameter"/> object to the given <paramref name="command"/>.
            </summary>
            <param name="command">The command to add the in parameter.</param>
            <param name="name">The name of the parameter.</param>
            <param name="dbType">One of the <see cref="T:NpgsqlTypes.NpgsqlDbType"/> values.</param>
            <remarks>
            This version of the method is used when you can have the same parameter object multiple times with different values.
            </remarks>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.AddInParameter(System.Data.Common.DbCommand,System.String,NpgsqlTypes.NpgsqlDbType,System.Object)">
            <summary>
            Adds a new In <see cref="T:System.Data.Common.DbParameter"/> object to the given <paramref name="command"/>.
            </summary>
            <param name="command">The commmand to add the parameter.</param>
            <param name="name">The name of the parameter.</param>
            <param name="dbType">One of the <see cref="T:NpgsqlTypes.NpgsqlDbType"/> values.</param>
            <param name="value">The value of the parameter.</param>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.AddInParameter(System.Data.Common.DbCommand,System.String,NpgsqlTypes.NpgsqlDbType,System.String,System.Data.DataRowVersion)">
            <summary>
            Adds a new In <see cref="T:System.Data.Common.DbParameter"/> object to the given <paramref name="command"/>.
            </summary>
            <param name="command">The command to add the parameter.</param>
            <param name="name">The name of the parameter.</param>
            <param name="dbType">One of the <see cref="T:NpgsqlTypes.NpgsqlDbType"/> values.</param>
            <param name="sourceColumn">The name of the source column mapped to the DataSet and used for loading or returning the value.</param>
            <param name="sourceVersion">One of the <see cref="T:System.Data.DataRowVersion"/> values.</param>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.AddOutParameter(System.Data.Common.DbCommand,System.String,NpgsqlTypes.NpgsqlDbType,System.Int32)">
            <summary>
            Adds a new Out <see cref="T:System.Data.Common.DbParameter"/> object to the given <paramref name="command"/>.
            </summary>
            <param name="command">The command to add the out parameter.</param>
            <param name="name">The name of the parameter.</param>
            <param name="dbType">One of the <see cref="T:NpgsqlTypes.NpgsqlDbType"/> values.</param>
            <param name="size">The maximum size of the data within the column.</param>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.AddParameter(System.Data.Common.DbCommand,System.String,NpgsqlTypes.NpgsqlDbType,System.Int32,System.Data.ParameterDirection,System.Boolean,System.Byte,System.Byte,System.String,System.Data.DataRowVersion,System.Object)">
            <summary>
            Adds a new instance of a <see cref="T:System.Data.Common.DbParameter"/> object to the command.
            </summary>
            <param name="command">The command to add the parameter.</param>
            <param name="name">The name of the parameter.</param>
            <param name="dbType">One of the <see cref="T:System.Data.DbType"/> values.</param>
            <param name="size">The maximum size of the data within the column.</param>
            <param name="direction">One of the <see cref="T:System.Data.ParameterDirection"/> values.</param>
            <param name="nullable">A value indicating whether the parameter accepts <see langword="null"/> (<b>Nothing</b> in Visual Basic) values.</param>
            <param name="precision">The maximum number of digits used to represent the <paramref name="value"/>.</param>
            <param name="scale">The number of decimal places to which <paramref name="value"/> is resolved.</param>
            <param name="sourceColumn">The name of the source column mapped to the DataSet and used for loading or returning the <paramref name="value"/>.</param>
            <param name="sourceVersion">One of the <see cref="T:System.Data.DataRowVersion"/> values.</param>
            <param name="value">The value of the parameter.</param>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.AddParameter(System.Data.Common.DbCommand,System.String,NpgsqlTypes.NpgsqlDbType,System.Data.ParameterDirection,System.String,System.Data.DataRowVersion,System.Object)">
            <summary>
            Adds a new instance of a <see cref="T:System.Data.Common.DbParameter"/> object to the command.
            </summary>
            <param name="command">The command to add the parameter.</param>
            <param name="name">The name of the parameter.</param>
            <param name="dbType">One of the <see cref="T:NpgsqlTypes.NpgsqlDbType"/> values.</param>
            <param name="direction">One of the <see cref="T:System.Data.ParameterDirection"/> values.</param>
            <param name="sourceColumn">The name of the source column mapped to the DataSet and used for loading or returning the <paramref name="value"/>.</param>
            <param name="sourceVersion">One of the <see cref="T:System.Data.DataRowVersion"/> values.</param>
            <param name="value">The value of the parameter.</param>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.BuildParameterName(System.String)">
            <summary>
            Builds a value parameter name for the current database.
            </summary>
            <param name="name">The name of the parameter.</param>
            <returns>A correctly formated parameter name.</returns>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.ConfigureParameter(Npgsql.NpgsqlParameter,System.String,NpgsqlTypes.NpgsqlDbType,System.Int32,System.Data.ParameterDirection,System.Boolean,System.Byte,System.Byte,System.String,System.Data.DataRowVersion,System.Object)">
            <summary>
            Configures a given <see cref="T:Npgsql.NpgsqlParameter"/>.
            </summary>
            <param name="parameter">The parameter.</param>
            <param name="name">The name of the parameter.</param>
            <param name="dbType">One of the <see cref="T:NpgsqlTypes.NpgsqlDbType"/> values.</param>
            <param name="size">The maximum size of the data within the column.</param>
            <param name="direction">One of the <see cref="T:System.Data.ParameterDirection"/> values.</param>
            <param name="nullable">A value indicating whether the parameter accepts <see langword="null"/> (<b>Nothing</b> in Visual Basic) values.</param>
            <param name="precision">The maximum number of digits used to represent the <paramref name="value"/>.</param>
            <param name="scale">The number of decimal places to which <paramref name="value"/> is resolved.</param>
            <param name="sourceColumn">The name of the source column mapped to the DataSet and used for loading or returning the <paramref name="value"/>.</param>
            <param name="sourceVersion">One of the <see cref="T:System.Data.DataRowVersion"/> values.</param>
            <param name="value">The value of the parameter.</param>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.ConfigureParameter(System.Data.Common.DbParameter,System.String,System.Data.DbType,System.Int32,System.Data.ParameterDirection,System.Boolean,System.Byte,System.Byte,System.String,System.Data.DataRowVersion,System.Object)">
            <summary>
            Configures a given <see cref="T:System.Data.Common.DbParameter"/>.
            </summary>
            <param name="param">The <see cref="T:System.Data.Common.DbParameter"/> to configure.</param>
            <param name="name">The name of the parameter.</param>
            <param name="dbType">One of the <see cref="T:System.Data.DbType"/> values.</param>
            <param name="size">The maximum size of the data within the column.</param>
            <param name="direction">One of the <see cref="T:System.Data.ParameterDirection"/> values.</param>
            <param name="nullable">A value indicating whether the parameter accepts <see langword="null"/> (<b>Nothing</b> in Visual Basic) values.</param>
            <param name="precision">The maximum number of digits used to represent the <paramref name="value"/>.</param>
            <param name="scale">The number of decimal places to which <paramref name="value"/> is resolved.</param>
            <param name="sourceColumn">The name of the source column mapped to the DataSet and used for loading or returning the <paramref name="value"/>.</param>
            <param name="sourceVersion">One of the <see cref="T:System.Data.DataRowVersion"/> values.</param>
            <param name="value">The value of the parameter.</param>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.CreateParameter(System.String,NpgsqlTypes.NpgsqlDbType,System.Int32,System.Data.ParameterDirection,System.Boolean,System.Byte,System.Byte,System.String,System.Data.DataRowVersion,System.Object)">
            <summary>
            Adds a new instance of a <see cref="T:System.Data.Common.DbParameter"/> object.
            </summary>
            <param name="name">The name of the parameter.</param>
            <param name="dbType">One of the <see cref="T:NpgsqlTypes.NpgsqlDbType"/> values.</param>
            <param name="size">The maximum size of the data within the column.</param>
            <param name="direction">One of the <see cref="T:System.Data.ParameterDirection"/> values.</param>
            <param name="nullable">A value indicating whether the parameter accepts <see langword="null"/> (<b>Nothing</b> in Visual Basic) values.</param>
            <param name="precision">The maximum number of digits used to represent the <paramref name="value"/>.</param>
            <param name="scale">The number of decimal places to which <paramref name="value"/> is resolved.</param>
            <param name="sourceColumn">The name of the source column mapped to the DataSet and used for loading or returning the <paramref name="value"/>.</param>
            <param name="sourceVersion">One of the <see cref="T:System.Data.DataRowVersion"/> values.</param>
            <param name="value">The value of the parameter.</param>
            <returns></returns>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.DeriveParameters(System.Data.Common.DbCommand)">
            <summary>
            Retrieves parameter information from the stored procedure specified in the <see cref="T:System.Data.Common.DbCommand"/> and populates the Parameters collection of the specified <see cref="T:System.Data.Common.DbCommand"/> object.
            </summary>
            <param name="discoveryCommand">The <see cref="T:System.Data.Common.DbCommand"/> to do the discovery.</param>
            <remarks>The <see cref="T:System.Data.Common.DbCommand"/> must be a <see cref="T:Npgsql.NpgsqlCommand"/> instance.</remarks>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.ExecuteDataSet(System.String,System.Object[])">
            <summary>
            Executes the <paramref name="storedProcedureName"/> with <paramref name="parameterValues"/> and returns the results in a new <see cref="T:System.Data.DataSet"/>.
            </summary>
            <param name="storedProcedureName">The stored procedure to execute.</param>
            <param name="parameterValues">An array of paramters to pass to the stored procedure. The parameter values must be in call order as they appear in the stored procedure.</param>
            <returns>
            A <see cref="T:System.Data.DataSet"/> with the results of the <paramref name="storedProcedureName"/>.
            </returns>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.ExecuteDataSet(System.Data.Common.DbTransaction,System.String,System.Object[])">
            <summary>
            Executes the <paramref name="storedProcedureName"/> with <paramref name="parameterValues"/> as part of the <paramref name="transaction"/> and returns the results in a new <see cref="T:System.Data.DataSet"/> within a transaction.
            </summary>
            <param name="transaction">The <see cref="T:System.Data.IDbTransaction"/> to execute the command within.</param>
            <param name="storedProcedureName">The stored procedure to execute.</param>
            <param name="parameterValues">An array of paramters to pass to the stored procedure. The parameter values must be in call order as they appear in the stored procedure.</param>
            <returns>
            A <see cref="T:System.Data.DataSet"/> with the results of the <paramref name="storedProcedureName"/>.
            </returns>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.ExecuteNonQuery(System.String,System.Object[])">
            <summary>
            Executes the <paramref name="storedProcedureName"/> using the given <paramref name="parameterValues"/> and returns the number of rows affected.
            </summary>
            <param name="storedProcedureName">The name of the stored procedure to execute.</param>
            <param name="parameterValues">An array of paramters to pass to the stored procedure. The parameter values must be in call order as they appear in the stored procedure.</param>
            <returns>The number of rows affected</returns>
            <seealso cref="M:System.Data.IDbCommand.ExecuteScalar"/>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.ExecuteNonQuery(System.Data.Common.DbTransaction,System.String,System.Object[])">
            <summary>
            Executes the <paramref name="storedProcedureName"/> using the given <paramref name="parameterValues"/> within a transaction and returns the number of rows affected.
            </summary>
            <param name="transaction">The <see cref="T:System.Data.IDbTransaction"/> to execute the command within.</param>
            <param name="storedProcedureName">The name of the stored procedure to execute.</param>
            <param name="parameterValues">An array of parameters to pass to the stored procedure. The parameter values must be in call order as they appear in the stored procedure.</param>
            <returns>The number of rows affected.</returns>
            <seealso cref="M:System.Data.IDbCommand.ExecuteScalar"/>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.ExecuteReader(System.String,System.Object[])">
            <summary>
            Executes the <paramref name="storedProcedureName"/> with the given <paramref name="parameterValues"/> and returns an <see cref="T:System.Data.IDataReader"></see> through which the result can be read.
            It is the responsibility of the caller to close the connection and reader when finished.
            </summary>
            <param name="storedProcedureName">The command that contains the query to execute.</param>
            <param name="parameterValues">An array of parameters to pass to the stored procedure. The parameter values must be in call order as they appear in the stored procedure.</param>
            <returns>
            An <see cref="T:System.Data.IDataReader"/> object.
            </returns>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.ExecuteReader(System.Data.Common.DbTransaction,System.String,System.Object[])">
            <summary>
            Executes the <paramref name="storedProcedureName"/> with the given <paramref name="parameterValues"/> within the given <paramref name="transaction"/> and returns an <see cref="T:System.Data.IDataReader"></see> through which the result can be read.
            It is the responsibility of the caller to close the connection and reader when finished.
            </summary>
            <param name="transaction">The <see cref="T:System.Data.IDbTransaction"/> to execute the command within.</param>
            <param name="storedProcedureName">The command that contains the query to execute.</param>
            <param name="parameterValues">An array of parameters to pass to the stored procedure. The parameter values must be in call order as they appear in the stored procedure.</param>
            <returns>
            An <see cref="T:System.Data.IDataReader"/> object.
            </returns>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.ExecuteScalar(System.String,System.Object[])">
            <summary>
            Executes the <paramref name="storedProcedureName"/> with the given <paramref name="parameterValues"/> and returns the first column of the first row in the result set returned by the query. Extra columns or rows are ignored.
            </summary>
            <param name="storedProcedureName">The stored procedure to execute.</param>
            <param name="parameterValues">An array of paramters to pass to the stored procedure. The parameter values must be in call order as they appear in the stored procedure.</param>
            <returns>
            The first column of the first row in the result set.
            </returns>
            <seealso cref="M:System.Data.IDbCommand.ExecuteScalar"/>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.ExecuteScalar(System.Data.Common.DbTransaction,System.String,System.Object[])">
            <summary>
            Executes the <paramref name="storedProcedureName"/> with the given <paramref name="parameterValues"/> within a
            <paramref name="transaction"/> and returns the first column of the first row in the result set returned by the query. Extra columns or rows are ignored.
            </summary>
            <param name="transaction">The <see cref="T:System.Data.IDbTransaction"/> to execute the command within.</param>
            <param name="storedProcedureName">The stored procedure to execute.</param>
            <param name="parameterValues">An array of paramters to pass to the stored procedure. The parameter values must be in call order as they appear in the stored procedure.</param>
            <returns>
            The first column of the first row in the result set.
            </returns>
            <seealso cref="M:System.Data.IDbCommand.ExecuteScalar"/>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.SameNumberOfParametersAndValues(System.Data.Common.DbCommand,System.Object[])">
            <summary>
            Determines if the number of parameters in the command matches the array of parameter values.
            </summary>
            <param name="command">The <see cref="T:System.Data.Common.DbCommand"/> containing the parameters.</param>
            <param name="values">The array of parameter values.</param>
            <returns>
            	<see langword="true"/> if the number of parameters and values match; otherwise, <see langword="false"/>.
            </returns>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.SetUpRowUpdatedEvent(System.Data.Common.DbDataAdapter)">
            <summary>
            Sets the RowUpdated event for the data adapter.
            </summary>
            <param name="adapter">The <see cref="T:System.Data.Common.DbDataAdapter"/> to set the event.</param>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.UserParametersStartIndex">
            <summary>
            Returns the starting index for parameters in a command.
            </summary>
            <returns>
            The starting index for parameters in a command.
            </returns>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.CheckIfNpgsqlCommand(System.Data.Common.DbCommand)">
            <summary>
            Checks if a database command is a Npgsql command and converts.
            </summary>
            <param name="command">The command.</param>
            <returns>converted NpgsqlCommand</returns>
        </member>
        <member name="M:EntLibContrib.Data.PostgreSql.NpgsqlDatabase.OnNpgsqlRowUpdated(System.Object,Npgsql.NpgsqlRowUpdatedEventArgs)">
            <summary>
            Called when [NPGSQL row updated].
            </summary>
            <param name="sender">The sender.</param>
            <param name="rowThatCouldNotBeWritten">The <see cref="T:Npgsql.NpgsqlRowUpdatedEventArgs"/> instance containing the event data.</param>
            <devdoc>
            Listens for the RowUpdate event on a dataadapter to support
            UpdateBehavior.Continue
            </devdoc>
        </member>
        <member name="T:EntLibContrib.Data.PostgreSql.Properties.Resources">
            <summary>
              A strongly-typed resource class, for looking up localized strings, etc.
            </summary>
        </member>
        <member name="P:EntLibContrib.Data.PostgreSql.Properties.Resources.ResourceManager">
            <summary>
              Returns the cached ResourceManager instance used by this class.
            </summary>
        </member>
        <member name="P:EntLibContrib.Data.PostgreSql.Properties.Resources.Culture">
            <summary>
              Overrides the current thread's CurrentUICulture property for all
              resource lookups using this strongly typed resource class.
            </summary>
        </member>
        <member name="P:EntLibContrib.Data.PostgreSql.Properties.Resources.ExceptionCommandNotNpgsqlCommand">
            <summary>
              Looks up a localized string similar to The command must be a NpgsqlCommand..
            </summary>
        </member>
        <member name="P:EntLibContrib.Data.PostgreSql.Properties.Resources.ExceptionMessageUpdateDataSetRowFailure">
            <summary>
              Looks up a localized string similar to Failed to update row..
            </summary>
        </member>
    </members>
</doc>
