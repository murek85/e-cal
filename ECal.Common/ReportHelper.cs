﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ECal.Common
{
	public static class ReportHelper
	{
		public static IEnumerable<T> Nullable<T>(this IEnumerable<T> obj)
		{
			if (obj == null)
				return new List<T>();
			else
				return obj;
		}

		public static object GetPropertyValue(this object src, string name)
		{
			return src.GetType().GetProperty(name, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance).GetValue(src, null);
		}

		public static string ConvertDateTimeToString(this DateTime src, string locale, string format = "yyyy-MM-dd HH:mm:ss")
		{
			// TODO: kiedyś trzeba zrobić jakoś przesuwanie czasu na podstawie parametru locale = +02:00
			return src.ToLocalTime().ToString(format);
		}
	}
}
