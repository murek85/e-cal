﻿using AutoMapper;

namespace ECal.Common.Mapping
{
	/// <summary>
	/// Interfejs wyprowadzony w celu ułatwienia wywoływania metod CreateMap z poziomu klasy AutoMapperConfig
	/// </summary>
	public interface IMapped
	{
		/// <summary>
		/// Metoda, w której definiuje się mapowania dotyczące typu implementującego interfejs (mapowania "na" typ i "z" typu)
		/// </summary>
		void CreateMap(IMapperConfigurationExpression mc);
	}

	public abstract class MappedEntity<U> : IMapped
	{
		public IMappingExpression<T, U> MapFrom<T>(IMapperConfigurationExpression mc)
		{
			return mc.CreateMap<T, U>();
		}

		public IMappingExpression<U, T> MapTo<T>(IMapperConfigurationExpression mc)
		{
			return mc.CreateMap<U, T>();
		}

		public abstract void CreateMap(IMapperConfigurationExpression mc);
	}
}
