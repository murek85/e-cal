﻿namespace ECal.Common.Mapping
{
	public interface IRsqlMapper
	{
		IPostgresqlDBRsqlMapperField ForQuery(string queryKey);

		string BuildQuery(string queryKey, RsqlData rsql, string sourceQuery, string defaultSort = "id", bool containsWhereExpression = false, bool noPaging = false);
	}

	public interface IPostgresqlDBRsqlMapperField
	{
		string Build(RsqlData rsql, string sourceQuery, string defaultSort = "id", bool containsWhereExpression = false, bool noPaging = false);

		IPostgresqlDBRsqlMapperField ForField<ValueType>(string rsqlField, string dbField);
	}
}
