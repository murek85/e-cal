﻿using NLog;
using NLog.Fluent;
using System;

namespace ECal.Common.Logging
{
	public class NLogLogger : ILogger
	{
		private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

		void log(LogLevel logLevel, LogCategories logCategory, string message, Exception exception = null)
		{
			logger.Log(logLevel)
			.Message(message)
			.Exception(exception)
			.Property("logCategory", logCategory.ToString())
			.Write();
		}

		public void Debug(LogCategories logCategory, string message, Exception exception = null)
		{
			log(LogLevel.Debug, logCategory, message, exception);
		}

		public void Error(LogCategories logCategory, string message, Exception exception = null)
		{
			log(LogLevel.Error, logCategory, message, exception);
		}

		public void Fatal(LogCategories logCategory, string message, Exception exception = null)
		{
			log(LogLevel.Fatal, logCategory, message, exception);
		}

		public void Info(LogCategories logCategory, string message, Exception exception = null)
		{
			log(LogLevel.Info, logCategory, message, exception);
		}

		public void Trace(LogCategories logCategory, string message, Exception exception = null)
		{
			log(LogLevel.Trace, logCategory, message, exception);
		}

		public void Warn(LogCategories logCategory, string message, Exception exception = null)
		{
			log(LogLevel.Warn, logCategory, message, exception);
		}
	}
}
