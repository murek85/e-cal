﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

namespace ECal.Common.Logging
{
	/// <summary>
	/// Klasa pomocnicza do określania w jakiej klasie i metodzie znajduje się aktualnie działający wątek
	/// </summary>
	public static class WhoAmI
	{
		/// <summary>
		/// Nazwa metody razem z zawierajacą ją klasą
		/// </summary>
		public static string Name
		{
			[MethodImpl(MethodImplOptions.NoInlining)]
			get
			{
				StackFrame stackFrame = new StackFrame(1);
				Type declaringType = stackFrame.GetMethod().DeclaringType;

				string declaringTypeName = String.Empty;
				//Klasy dziedziczące po MethodBase mają prawo zwracać tu nulla, więc na wszelki wypadek zabezpieczenie
				if (declaringType != null)
					declaringTypeName = declaringType.Name;

				return new StringBuilder(declaringTypeName).Append("::").Append(stackFrame.GetMethod().Name).Append("()").ToString();
			}
		}

		/// <summary>
		/// Nazwa metody
		/// </summary>
		public static string MethodName
		{
			[MethodImpl(MethodImplOptions.NoInlining)]
			get
			{
				return new StackFrame(1).GetMethod().Name;
			}
		}
	}
}