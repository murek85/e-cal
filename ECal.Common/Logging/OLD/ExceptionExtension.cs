﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ECal.Common.Logging
{
	/// <summary>
	/// Klasa narzędziowa do obsługi wyjątków
	/// </summary>
	public static class ExceptionExtension
	{
		/// <summary>
		/// Przygotowuje rozbudowaną treść wyjątku 
		/// </summary>
		/// <param name="e">Obiekt wyjątku</param>
		/// <returns>Treść wyjątku</returns>
		public static string GetMessage(this Exception e)
		{
			StringBuilder message = new StringBuilder();
			message.AppendLine(e.Message);
			message.AppendLine(e.StackTrace);
			message.Append(GetCallStack());

			return message.ToString();
		}

		/// <summary>
		/// Przygotowuje stos wyjątku 
		/// </summary>
		/// <returns>Stos wyjątku</returns>
		private static string GetCallStack()
		{
			StringBuilder callStack = new StringBuilder();
			StackTrace stackTrace = new StackTrace(true);
			IEnumerable<StackFrame> stackFrames = stackTrace.GetFrames();

			foreach (StackFrame stackFrame in stackFrames)
			{
				var method = stackFrame.GetMethod();
				StringBuilder methodName = new StringBuilder();
				if (method.DeclaringType != null)
				{
					methodName.Append(method.DeclaringType.FullName);
					methodName.Append(".");
					methodName.Append(method.Name);
					methodName.Append(" at Line: ");
					methodName.Append(stackFrame.GetFileLineNumber().ToString());
					callStack.AppendLine(methodName.ToString());
				}
				else
				{
					break;
				}
			}

			return callStack.ToString();
		}

		/// <summary>
		/// Przygotowuje rozbudowaną treść wyjątku z dołączonym nagłówkiem
		/// </summary>
		/// <param name="e">Obiekt wyjątku</param>
		/// <param name="headerMessage">Nagłówek</param>
		/// <returns>Treść wyjątku</returns>
		public static string GetMessage(this Exception e, string headerMessage)
		{
			StringBuilder message = new StringBuilder();
			message.AppendLine(headerMessage);
			message.Append(e.GetMessage());

			return message.ToString();
		}
	}
}