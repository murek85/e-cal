﻿using System;

namespace ECal.Common.Logging
{
	public interface ILogger
	{
		void Trace(LogCategories logCategory, string message, Exception exception = null);

		void Debug(LogCategories logCategory, string message, Exception exception = null);

		void Info(LogCategories logCategory, string message, Exception exception = null);

		void Warn(LogCategories logCategory, string message, Exception exception = null);

		void Error(LogCategories logCategory, string message, Exception exception = null);

		void Fatal(LogCategories logCategory, string message, Exception exception = null);
	}
}
