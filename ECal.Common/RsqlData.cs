﻿using ECal.Common.Database;

namespace ECal.Common
{
	public class RsqlData : Paging
	{
		public string qData { set; get; }

		public string Sort { set; get; }

		public RsqlData(string q, string sort)
		{
			qData = q;
			Sort = sort;
		}

		public RsqlData(long pageSize, long pageNumber, string q, string sort): base(pageSize, pageNumber)
		{
			qData = q;
			Sort = sort;
		}

		public RsqlData(PageSettings pageSettings) : base(pageSettings.PageSize, pageSettings.PageNumber)
		{
			qData = pageSettings.Query;
			Sort = pageSettings.Sort;
		}
	}
}
