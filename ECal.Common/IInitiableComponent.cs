﻿using System.Collections.Generic;

namespace ECal.Common
{
	/// <summary>
	/// Specyfikuje kontrakt komponentu, który na początku pracy serwisu trzeba zainicjować, a po zakończeniu pracy wyłączyć
	/// </summary>
	public interface IInitiableComponent
	{
		/// <summary>
		/// Inicjuje komponent i przygotowuje go do pracy
		/// </summary>
		/// <param name="parameters">Opcjonalny słownik parametrów w postaci klucz-wartość</param>
		void Initialize(Dictionary<string, object> parameters = null);

		/// <summary>
		/// Wyłącza komponent po zakończonej pracy
		/// </summary>
		void Uninitialize();
	}
}