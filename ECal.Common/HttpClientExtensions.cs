﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ECal.Common
{
	public static class HttpClientExtensions
	{
		public static Task<HttpResponseMessage> PostAsJsonAsync<T>(this HttpClient httpClient, string url, T data)
		{
			var dataAsString = JsonConvert.SerializeObject(data);
			var content = new StringContent(dataAsString);
			content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
			return httpClient.PostAsync(url, content);
		}

		public static async Task<T> ReadAsJsonAsync<T>(this HttpContent content)
		{
			var dataAsString = await content.ReadAsStringAsync();
			return JsonConvert.DeserializeObject<T>(dataAsString);
		}

		public static async Task<U> CallRESTAPIJson2Json<T, U>(this HttpClient client, string relativeUrl, T request)
		{
			HttpResponseMessage response = await client.PostAsJsonAsync(relativeUrl, request).ConfigureAwait(false);

			return await response.Content.ReadAsJsonAsync<U>();
		}

		public static async Task<T> CallRESTAPIFormUrlEncoded2Json<T>(this HttpClient client, string relativeUrl, StringContent content)
		{
			HttpResponseMessage response = await client.PostAsync(relativeUrl, content).ConfigureAwait(false);
			response.EnsureSuccessStatusCode();

			return await response.Content.ReadAsJsonAsync<T>();
		}
	}
}
