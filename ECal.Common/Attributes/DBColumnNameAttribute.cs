﻿using System;

namespace ECal.Common.Attributes
{
	/// <summary>
	/// Atrybut określający nazwę kolumny w bazie danych
	/// </summary>
	public class DBColumnNameAttribute : Attribute
	{
		/// <summary>
		/// Nazwa kolumny w bazie danych
		/// </summary>
		public string ColumnName { get; set; }

		/// <summary>
		/// Konstruktor domyślny
		/// </summary>
		/// <param name="columnName">Nazwa kolumny w bazie danych</param>
		public DBColumnNameAttribute(string columnName)
		{
			ColumnName = columnName;
		}
	}
}