﻿namespace ECal.Common.Attributes
{
	/// <summary>
	/// Atrybut określający nazwę kolumny-klucza w bazie danych
	/// </summary>
	public class DBKeyColumnNameAttribute : DBColumnNameAttribute
	{
		/// <summary>
		/// Konstruktor domyślny
		/// </summary>
		/// <param name="columnName">Nazwa kolumny-klucza w bazie danych</param>
		public DBKeyColumnNameAttribute(string columnName)
			: base(columnName)
		{
		}
	}
}