﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ECal.Common.Database
{
	/// <summary>
	/// Obiekt pomocniczy do częściowej aktualizacji encji
	/// </summary>
	/// <typeparam name="T">Typ encji</typeparam>
	public class PatchUpdate<T> where T : DatabaseEntityWithLongId
	{
		/// <summary>
		/// Pomocnicza lista właściwości do zaktualizowania
		/// </summary>
		private HashSet<string> properties = new HashSet<string>();

		/// <summary>
		/// Encja z wartościami
		/// </summary>
		public T Entity { get; private set; }

		/// <summary>
		/// Lista właściwości do zaktualizowania
		/// </summary>
		public List<string> Properties { get { return properties.ToList(); } }

		/// <summary>
		/// Konstruktor
		/// </summary>
		/// <param name="entity">Model encji z nowymi wartościami</param>
		/// <param name="properties">Aktualizowane właściwości - nameof(Type.PropertyName)</param>
		/// <exception cref="ArgumentNullException">Nullowy model encji</exception>
		/// <exception cref="ArgumentException">Niezgodna nazwa właściwości</exception>
		public PatchUpdate(T entity, params string[] properties)
		{
			if (entity == null)
				throw new ArgumentNullException("entity");

			Entity = entity;

			foreach (string property in properties)
				AddProperty(property);
		}

		/// <summary>
		/// Do listy aktualizowanych właściwości dodaje właściwośc o podanej nazwie
		/// </summary>
		/// <param name="property">Nazwa właściwości - nameof(Type.PropertyName)</param>
		/// <exception cref="ArgumentException">Niezgodna nazwa właściwości</exception>
		public void AddProperty(string property)
		{
			if (!properties.Contains(property))
			{
				CheckEntityPropertyName(property);
				properties.Add(property);
			}
		}

		/// <summary>
		/// Dla typu T sprawdza czy istnieje publiczna właściowść o podanej nazwie
		/// </summary>
		/// <param name="property">Nazwa właściowości</param>
		/// <exception cref="ArgumentException">Niezgodna nazwa właściwości</exception>
		private void CheckEntityPropertyName(string property)
		{
			if (typeof(T).GetProperty(property, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.GetProperty) == null)
				throw new ArgumentException($"Typ \"{typeof(T).Name}\" nie posiada publicznej właściwości \"{property}\" zwracającej wartość (get)");
		}

		/// <summary>
		/// Tworzy pomocniczy obiekt paramtru
		/// </summary>
		/// <param name="columnName">Nazwa kolumny</param>
		/// <param name="columnType">Typ kolumny</param>
		/// <param name="columnValue">Wartość kolumny</param>
		/// <exception cref="ArgumentException">Jeśli pusta nazwa kolumny</exception>
		public ParamInfo CreateParamInfo(string columnName, DbType columnType, object columnValue)
		{
			return new ParamInfo(columnName, columnType, columnValue);
		}

		/// <summary>
		/// Tworzy pustą listę pomocniczych obiektów parametrów
		/// </summary>
		/// <returns></returns>
		public List<ParamInfo> CreateParamInfoList()
		{
			return new List<ParamInfo>();
		}

		/// <summary>
		/// Pomocniczy obiekt parametru
		/// </summary>
		public class ParamInfo
		{
			/// <summary>
			/// Nazwa kolumny
			/// </summary>
			public string ColumnName { get; private set; }

			/// <summary>
			/// Typ kolumny
			/// </summary>
			public DbType ColumnType { get; private set; }

			/// <summary>
			/// Wartość kolumny
			/// </summary>
			public object ColumnValue { get; private set; }

			/// <summary>
			/// Konstruktor
			/// </summary>
			/// <param name="columnName">Nazwa kolumny</param>
			/// <param name="columnType">Typ kolumny</param>
			/// <param name="columnValue">Wartość kolumny</param>
			/// <exception cref="ArgumentException">Jeśli pusta nazwa kolumny</exception>
			internal ParamInfo(string columnName, DbType columnType, object columnValue)
			{
				if (string.IsNullOrWhiteSpace(columnName))
					throw new ArgumentException("Nazwa kolumna nie może być pusta", "columnName");

				ColumnName = columnName.Trim();
				ColumnType = columnType;
				ColumnValue = columnValue;
			}
		}
	}
}