﻿using System;

namespace ECal.Common.Database
{
	/// <summary>
	/// Klasa bazowa encji przechowywanych w źródle danych - klucz typu Guid
	/// </summary>
	public class DatabaseEntityWithGuidId : DatabaseEntity<Guid?>
	{
		/// <summary>
		/// Konstruktor domyślny
		/// </summary>
		public DatabaseEntityWithGuidId() : base()
		{
		}

		/// <summary>
		/// Konstruktor do tworzenia encji z wypełnionym Id - do wczytywania ze źródła danych referencji do obiektów
		/// </summary>
		/// <param name="id">Identyfikator encji w źródle danych</param>
		public DatabaseEntityWithGuidId(Guid? id) : base(id)
		{
		}
	}
}
