﻿namespace ECal.Common.Database
{
	/// <summary>
	/// Klasa bazowa encji przechowywanych w źródle danych - klucz typu long
	/// </summary>
	public class DatabaseEntityWithLongId : DatabaseEntity<long?>
	{
		/// <summary>
		/// Konstruktor domyślny
		/// </summary>
		public DatabaseEntityWithLongId() : base()
		{
		}

		/// <summary>
		/// Konstruktor do tworzenia encji z wypełnionym Id - do wczytywania ze źródła danych referencji do obiektów
		/// </summary>
		/// <param name="id">Identyfikator encji w źródle danych</param>
		public DatabaseEntityWithLongId(long? id) : base(id)
		{
		}
	}
}