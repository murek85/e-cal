﻿using System;

namespace ECal.Common.Database
{
	/// <summary>
	/// Model stronnicowania
	/// </summary>
	public class Paging
	{
		/// <summary>
		/// Ilość wszystkich rekordów z zapytania
		/// </summary>
		public long TotalRows { get; set; }

		/// <summary>
		/// Rozmiar strony (>= 1)
		/// </summary>
		public long PageSize { get; set; }

		/// <summary>
		/// Numer strony (>= 1)
		/// </summary>
		public long PageNumber { get; set; }

		/// <summary>
		/// Konstruktor
		/// </summary>
		public Paging()
		{
			TotalRows = 0;
		}

		/// <summary>
		/// Konstruktor
		/// </summary>
		/// <param name="pageSize">Rozmiar strony (>= 1)</param>
		/// <param name="pageNumber">Numer strony (>= 1)</param>
		public Paging(long pageSize, long pageNumber)
		{
			if (pageSize < 1)
				throw new ArgumentException("Rozmiar strony musi być większy lub równy 1", nameof(pageSize));

			if (pageNumber < 1)
				throw new ArgumentException("Numer strony musi być większy lub równy 1", nameof(pageNumber));

			PageSize = pageSize;
			PageNumber = pageNumber;
			TotalRows = 0;
		}
	}
}
