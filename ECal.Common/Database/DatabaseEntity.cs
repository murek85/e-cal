﻿using ECal.Common.Attributes;

namespace ECal.Common.Database
{
	/// <summary>
	/// Klasa bazowa encji przechowywanych w źródle danych
	/// </summary>
	public class DatabaseEntity<T>
	{
		/// <summary>
		/// Konstruktor domyślny
		/// </summary>
		public DatabaseEntity()
		{
		}

		/// <summary>
		/// Konstruktor do tworzenia encji z wypełnionym Id - do wczytywania ze źródła danych referencji do obiektów
		/// </summary>
		/// <param name="id">Identyfikator encji w źródle danych</param>
		public DatabaseEntity(T id)
		{
			Id = id;
		}

		/// <summary>
		/// Identyfikator encji w źródle danych
		/// </summary>
		[DBKeyColumnName("id")]
		public T Id { get; set; }
	}
}
