﻿namespace ECal.Common
{
	public class PageSettings
	{
		public int PageNumber { get; set; }
		public int PageSize { get; set; }
		public string Query { get; set; }
		public string Sort { get; set; }
	}
}
