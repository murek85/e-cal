﻿using System;

namespace ECal.Data.Common
{
	/// <summary>
	/// Interface transakcji źródła danych
	/// </summary>
	public interface IDatabaseTransaction : IDisposable
	{
		/// <summary>
		/// Zatwierdza transakcję
		/// </summary>
		void Commit();

		/// <summary>
		/// Wycofuje transakcję
		/// </summary>
		void Rollback();
	}
}