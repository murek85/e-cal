﻿namespace ECal.Data.Common
{
	/// <summary>
	/// Interfejs opisujący obiekt umożliwiający tworzenie transackji bazodanowej.
	/// </summary>
	public interface IDatabaseDAO
	{
		/// <summary>
		/// Tworzy i rozpoczyna nową transakcję bazodanową.
		/// </summary>
		/// <returns>Utworzona transakcja bazodanowa.</returns>
		IDatabaseTransaction BeginTransaction();
	}
}