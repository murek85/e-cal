﻿using ECal.Model.Ldap;
using System.Collections.Generic;

namespace ECal.Data.Common.Ldap
{
	/// <summary>
	/// Interfejs DAO użytkowników Ldap
	/// </summary>
	public interface ILdapDAO
	{
		/// <summary>
		///
		/// </summary>
		/// <param name="groupName"></param>
		/// <param name="getChildGroups"></param>
		/// <returns></returns>
		IList<LdapEntry> GetGroups(string groupName, bool getChildGroups = false);
		/// <summary>
		///
		/// </summary>
		/// <param name="groupName"></param>
		/// <returns></returns>
		IList<LdapUser> GetUsersInGroup(string groupName);
		/// <summary>
		///
		/// </summary>
		/// <param name="groups"></param>
		/// <returns></returns>
		IList<LdapUser> GetUsersInGroups(IList<LdapEntry> groups = null);
		/// <summary>
		/// Pobranie użytkowników na podstawie filtrowania po adresie e-mail
		/// </summary>
		/// <param name="emailAddress">Adres e-mail</param>
		/// <returns></returns>
		IList<LdapUser> GetUsersByEmailAddress(string emailAddress);
		/// <summary>
		/// Pobranie użytkowników z katalogu Ldap na podstawie odpowiednich filtrów
		/// </summary>
		/// <param name="filterName">Filtry rozdzielone przecinkami: SAMAccountName, DisplayName</param>
		/// <param name="query">Szukana fraza</param>
		/// <returns></returns>
		IList<LdapUser> GetUsersByFilterName(string[] filterName, string query);
		/// <summary>
		/// Pobranie wszystkich użytkowników z katalogu Ldap'a
		/// </summary>
		/// <returns></returns>
		IList<LdapUser> GetAllUsers();
		/// <summary>
		/// Pobranie administratora Ldap
		/// </summary>
		/// <returns></returns>
		LdapUser GetLdapAdministrator();
		/// <summary>
		///
		/// </summary>
		/// <param name="userPrincipalName"></param>
		/// <returns></returns>
		LdapUser GetUserByUserPrincipalName(string userPrincipalName);
		/// <summary>
		/// Pobranie użytkownika z katalogu Ldap filtrowane po nazwie
		/// </summary>
		/// <param name="samAccountName">Nazwa użytkownika</param>
		/// <returns></returns>
		LdapUser GetUserBySAMAccountName(string samAccountName);
		/// <summary>
		/// Pobranie użytkownika z katalogu Ldap filtrowane po identyfikatorze z Ldap'a
		/// </summary>
		/// <param name="objectGuid">Identyfikator z Ldap</param>
		/// <returns></returns>
		LdapUser GetUserByObjectGuid(string objectGuid);
		/// <summary>
		/// Uwierzytelnienie użytkownika z katalogu Ldap
		/// </summary>
		/// <param name="distinguishedName">Nazwa konta/użytkownika</param>
		/// <param name="password"></param>
		/// <returns></returns>
		bool Authenticate(string distinguishedName, string password);
		/// <summary>
		///
		/// </summary>
		/// <param name="distinguishedName"></param>
		/// <returns></returns>
		bool AddUserToGroup(string distinguishedName);
		/// <summary>
		///
		/// </summary>
		/// <param name="distinguishedName"></param>
		/// <returns></returns>
		bool RemoveUserFromGroup(string distinguishedName);
	}
}
