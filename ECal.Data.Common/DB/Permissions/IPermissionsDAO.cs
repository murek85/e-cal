﻿using ECal.Common;
using ECal.Model.DB;
using System;
using System.Collections.Generic;

namespace ECal.Data.Common.DB.Permissions
{
   public interface IPermissionsDAO : IDatabaseDAO
   {
      (List<Permission>, long) ReadPermissionsReport(IDatabaseTransaction transaction, RsqlData rsql);
      (List<Permission>, long) ReadPermissionsList(IDatabaseTransaction transaction, RsqlData rsql);
      List<Permission> ReadPermissionsByRole(IDatabaseTransaction transaction, long id);
      List<Permission> ReadPermissionsByUser(IDatabaseTransaction transaction, Guid id);
      Permission ReadPermission(IDatabaseTransaction transaction, long id);
      long? CreatePermission(IDatabaseTransaction transaction, Permission permission);
      bool UpdatePermission(IDatabaseTransaction transaction, Permission permission);
      bool DeletePermission(IDatabaseTransaction transaction, long id);
   }
}
