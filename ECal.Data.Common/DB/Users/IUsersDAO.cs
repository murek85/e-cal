﻿using ECal.Common;
using ECal.Model.DB;
using System;
using System.Collections.Generic;

namespace ECal.Data.Common.DB.Users
{
   public interface IUsersDAO : IDatabaseDAO
   {
      (List<User>, long) ReadUsersReport(IDatabaseTransaction transaction, RsqlData rsql);
      (List<User>, long) ReadUsersList(IDatabaseTransaction transaction, RsqlData rsql);
      User ReadUser(IDatabaseTransaction transaction, Guid id);
      User ReadUser(IDatabaseTransaction transaction, string email);
      long? CreateUser(IDatabaseTransaction transaction, User user);
      bool UpdateUser(IDatabaseTransaction transaction, User user);
      bool DeleteUser(IDatabaseTransaction transaction, Guid id);
      bool UpdateUserLogged(IDatabaseTransaction transaction, User user);
   }
}
