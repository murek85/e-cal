﻿using ECal.Common;
using ECal.Model.DB;
using System.Collections.Generic;

namespace ECal.Data.Common.DB.Applications
{
   public interface IApplicationsDAO : IDatabaseDAO
   {
      (List<Application>, long) ReadApplicationsReport(IDatabaseTransaction transaction, RsqlData rsql);
      (List<Application>, long) ReadApplicationsList(IDatabaseTransaction transaction, RsqlData rsql);
      Application ReadApplicationByRole(IDatabaseTransaction transaction, long id);
      Application ReadApplicationByPermission(IDatabaseTransaction transaction, long id);
      Application ReadApplicationByClient(IDatabaseTransaction transaction, string id);
      Application ReadApplication(IDatabaseTransaction transaction, long id);
      long? CreateApplication(IDatabaseTransaction transaction, Application application);
      bool UpdateApplication(IDatabaseTransaction transaction, Application application);
      bool DeleteApplication(IDatabaseTransaction transaction, long id);
   }
}
