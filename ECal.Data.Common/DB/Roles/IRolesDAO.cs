﻿using ECal.Common;
using ECal.Model.DB;
using System;
using System.Collections.Generic;

namespace ECal.Data.Common.DB.Roles
{
   public interface IRolesDAO : IDatabaseDAO
   {
      (List<Role>, long) ReadRolesReport(IDatabaseTransaction transaction, RsqlData rsql);
      (List<Role>, long) ReadRolesList(IDatabaseTransaction transaction, RsqlData rsql);
      List<Role> ReadRolesByUser(IDatabaseTransaction transaction, Guid id);
      Role ReadRole(IDatabaseTransaction transaction, long id);
      long? CreateRole(IDatabaseTransaction transaction, Role role);
      bool UpdateRole(IDatabaseTransaction transaction, Role role);
      bool DeleteRole(IDatabaseTransaction transaction, long id);
      bool VerifyAssigningRoleToUser(IDatabaseTransaction transaction, Guid userId, long roleId);
   }
}
