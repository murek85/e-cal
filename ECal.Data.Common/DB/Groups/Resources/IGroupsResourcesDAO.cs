﻿using ECal.Common;
using ECal.Model.DB;
using System.Collections.Generic;

namespace ECal.Data.Common.DB.Groups.Resources
{
   public interface IGroupsResourcesDAO : IDatabaseDAO
   {
      (List<GroupResource>, long) ReadGroupsResourcesReport(IDatabaseTransaction transaction, RsqlData rsql);
      (List<GroupResource>, long) ReadGroupsResourcesList(IDatabaseTransaction transaction, RsqlData rsql);
      GroupResource ReadGroupResource(IDatabaseTransaction transaction, long id);
      long? CreateGroupResource(IDatabaseTransaction transaction, GroupResource groupResource);
      bool UpdateGroupResource(IDatabaseTransaction transaction, GroupResource groupResource);
      bool DeleteGroupResource(IDatabaseTransaction transaction, long id);
   }
}
