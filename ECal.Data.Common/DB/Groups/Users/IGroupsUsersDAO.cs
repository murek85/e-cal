﻿using ECal.Common;
using ECal.Model.DB;
using System.Collections.Generic;

namespace ECal.Data.Common.DB.Groups.Users
{
   public interface IGroupsUsersDAO : IDatabaseDAO
   {
      (List<GroupUser>, long) ReadGroupsUsersReport(IDatabaseTransaction transaction, RsqlData rsql);
      (List<GroupUser>, long) ReadGroupsUsersList(IDatabaseTransaction transaction, RsqlData rsql);
      GroupUser ReadGroupUser(IDatabaseTransaction transaction, long id);
      long? CreateGroupUser(IDatabaseTransaction transaction, GroupUser groupUser);
      bool UpdateGroupUser(IDatabaseTransaction transaction, GroupUser groupUser);
      bool DeleteGroupUser(IDatabaseTransaction transaction, long id);
   }
}
