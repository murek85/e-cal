﻿using EntLibContrib.Data.PostgreSql;
using ECal.Data.Database;

namespace ECal.Data.PostgresqlDB
{
	public class PostgresqlDBDatabaseContainer : DatabaseContainer
	{
		/// <summary>
		/// Klasa kontenera dla obiektu bazy Npgsql. Zawiera również niezbędny kod inicjalizacyjny.
		/// </summary>
		/// <param name="connectionStringName">Zawartość ciągu połączenia</param>
		public PostgresqlDBDatabaseContainer(string connectionString)
		{
			Database = new NpgsqlDatabase(connectionString);
		}
	}
}