﻿using ECal.Common.Database;
using ECal.Data.Common;
using ECal.Data.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Text;

namespace ECal.Data.PostgresqlDB
{
   /// <summary>
   /// Implementuje metody DatabaseDAO specyficzne dla MSSQL
   /// </summary>
   public class PostgresqlDBDatabaseDAO : DatabaseDAO
   {
      /// <summary>
      /// Inicjalizuje DAO bazy
      /// </summary>
      /// <param name="database">Obiekt bazy</param>
      public PostgresqlDBDatabaseDAO(PostgresqlDBDatabaseContainer databaseContainer) : base(databaseContainer) { }

      /// <summary>
      /// Dodaje obsługę stronicowania do zapytania
      /// </summary>
      /// <param name="dbQuery">Główne zapytanie (z ORDER BY)</param>
      /// <param name="paging">Stropnicowanie danych</param>
      /// <returns>Zapytanie opakowane w stronicowanie</returns>
      protected override string AddPagingWithOrderBy(string dbQuery, Paging paging, string sortAdditional = "Id")
      {
         StringBuilder dbQueryPaged = new StringBuilder();

         string query = $"SELECT *, COUNT (*) OVER() FROM ({dbQuery}) AS x";
         dbQueryPaged.AppendLine(query);
         dbQueryPaged.AppendLine("ORDER BY " + sortAdditional);
         dbQueryPaged.AppendFormat("OFFSET {0} LIMIT {1}", (paging.PageNumber - 1) * paging.PageSize, paging.PageSize);

         return dbQueryPaged.ToString();
      }

      /// <summary>
      /// Odczytuje wartość kolumny, zawierającą informację o ilości wszystkich wierszy - dla zapytań utworzonych z pomocą metody AddPaging
      /// </summary>
      /// <param name="reader">Obiekt odczytu</param>
      /// <returns>Ilość wszystkich wierszy</returns>
      protected override long ReadPagingTotalRows(IDataReader reader)
      {
         return reader.GetInt64("COUNT");
      }

      /// <summary>
      /// Pobiera listę elementów
      /// </summary>
      /// <typeparam name="T">Typ elementu</typeparam>
      /// <param name="selectQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="readItem">Funkcja wczytująca pojedynczy element</param>
      /// <param name="userId"></param>
      /// <returns>Lista elementów</returns>
      protected List<T> Read<T>(string selectQuery, IDatabaseTransaction transaction, Func<IDataReader, T> readItem)
      {
         return Read<T>(selectQuery, transaction, null, readItem);
      }

      /// <summary>
      /// Pobiera listę elementów
      /// </summary>
      /// <typeparam name="T">Typ elementu</typeparam>
      /// <param name="selectQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <param name="readItem">Funkcja wczytująca pojedynczy element</param>
      /// <param name="userId"></param>
      /// <returns>Lista elementów</returns>
      protected List<T> Read<T>(string selectQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters, Func<IDataReader, T> readItem)
      {
         using (DbCommand command = GetSqlStringCommand(selectQuery))
         {
            var stopWatch = new Stopwatch();
            addParameters?.Invoke(command);
            stopWatch.Start();
            try
            {
               using (var reader = ExecuteReader(transaction, command))
               {
                  return ReadAll<T>(reader, readItem);
               }
            }
            finally
            {
               stopWatch.Stop();
               Log(command, stopWatch.ElapsedMilliseconds);
            }
         }
      }

      /// <summary>
      /// Pobiera pierwszy element
      /// </summary>
      /// <typeparam name="T">Typ elementu</typeparam>
      /// <param name="selectQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="readItem">Funkcja wczytująca pojedynczy element</param>
      /// <param name="userId"></param>
      /// <returns>Pobrany element</returns>
      protected T ReadFirst<T>(string selectQuery, IDatabaseTransaction transaction, Func<IDataReader, T> readItem)
      {
         return ReadFirst<T>(selectQuery, transaction, null, readItem);
      }

      /// <summary>
      /// Pobiera pierwszy element
      /// </summary>
      /// <typeparam name="T">Typ elementu</typeparam>
      /// <param name="selectQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <param name="readItem">Funkcja wczytująca pojedynczy element</param>
      /// <param name="userId">Identyfikator użytkownika biznesowego.</param>
      /// <returns>Pobrany element</returns>
      protected T ReadFirst<T>(string selectQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters, Func<IDataReader, T> readItem)
      {
         using (DbCommand command = GetSqlStringCommand(selectQuery))
         {
            var stopWatch = new Stopwatch();
            addParameters?.Invoke(command);
            stopWatch.Start();
            try
            {
               using (var reader = ExecuteReader(transaction, command))
               {
                  return ReadFirst<T>(reader, readItem);
               }
            }
            finally
            {
               stopWatch.Stop();
               Log(command, stopWatch.ElapsedMilliseconds);
            }
         }
      }

      /// <summary>
      /// Pobiera wartość z zapytania zwracającego tylko jedną wartość
      /// </summary>
      /// <typeparam name="T">Zwracana wartość</typeparam>
      /// <param name="selectQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <returns>Pobrana wartość</returns>
      protected T ReadScalar<T>(string selectQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters)
      {
         using (DbCommand command = GetSqlStringCommand(selectQuery))
         {
            var stopWatch = new Stopwatch();
            addParameters?.Invoke(command);
            stopWatch.Start();
            try
            {
               object o = ExecuteScalar(transaction, command);
               if (o == null || o == DBNull.Value)
                  return default(T);

               return (T)o;
            }
            finally
            {
               stopWatch.Stop();
               Log(command, stopWatch.ElapsedMilliseconds);
            }
         }
      }

      #region INSERT Long

      /// <summary>
      /// Obsługa zapytań INSERT (dodającej jeden rekord) zawierających frazę OUTPUT (tylko z INSERTED.Id), np.:
      /// INSERT INTO TestTable (Name, Enabled)
      /// OUTPUT INSERTED.Id
      /// VALUES ('A2', 1)
      /// </summary>
      /// <param name="insertQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <returns>Identyfikator nadany rekordowi przy dodawaniu</returns>
      protected long? InsertOneWithLongOutputId(string insertQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters)
      {
         List<long?> outputValues = Insert(insertQuery, transaction, addParameters, (IDataReader reader) => { return reader.GetNullableInt64("Id"); });
         return (outputValues != null && outputValues.Count > 0) ? outputValues[0] : 0;
      }


      /// <summary>
      /// Obsługa zapytań INSERT zawierających frazę OUTPUT (tylko z INSERTED.Id), np.:
      /// INSERT INTO TestTable (Name, Enabled)
      /// OUTPUT INSERTED.Id
      /// VALUES ('A2', 1), ('A3', 1)
      /// </summary>
      /// <param name="insertQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <returns>Identyfikator nadany rekordowi przy dodawaniu</returns>
      protected List<long?> InsertWithLongOutputId(string insertQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters)
      {
         return Insert(insertQuery, transaction, addParameters, (IDataReader reader) => { return reader.GetNullableInt64("Id"); });
      }

      #endregion

      #region INSERT Guid

      /// <summary>
      /// Obsługa zapytań INSERT (dodającej jeden rekord) zawierających frazę OUTPUT (tylko z INSERTED.Id), np.:
      /// INSERT INTO TestTable (Name, Enabled)
      /// OUTPUT INSERTED.Id
      /// VALUES ('A2', 1)
      /// </summary>
      /// <param name="insertQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <returns>Identyfikator nadany rekordowi przy dodawaniu</returns>
      protected Guid? InsertOneWithGuidOutputId(string insertQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters)
      {
         List<Guid?> outputValues = Insert(insertQuery, transaction, addParameters, (IDataReader reader) => { return reader.GetNullableGuid("id"); });
         return (outputValues != null && outputValues.Count > 0) ? outputValues[0] : null;
      }

      /// <summary>
      /// Obsługa zapytań INSERT zawierających frazę OUTPUT (tylko z INSERTED.Id), np.:
      /// INSERT INTO TestTable (Name, Enabled)
      /// OUTPUT INSERTED.Id
      /// VALUES ('A2', 1), ('A3', 1)
      /// </summary>
      /// <param name="insertQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <returns>Identyfikator nadany rekordowi przy dodawaniu</returns>
      protected List<Guid?> InsertWithGuidOutputId(string insertQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters)
      {
         return Insert(insertQuery, transaction, addParameters, (IDataReader reader) => { return reader.GetNullableGuid("Id"); });
      }

      #endregion

      /// <summary>
      /// Obsługa zapytań INSERT zawierających frazę OUTPUT, np.:
      /// INSERT INTO TestTable (Name, Enabled)
      /// OUTPUT INSERTED.Id, INSERTED.Name
      /// VALUES ('A2', 1), ('A3', 1)
      /// </summary>
      /// <param name="insertQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <param name="readItem">Funkcja wczytująca pojedynczy element opisany w frazie OUTPUT</param>
      /// <returns>Lista elementów opisanych w frazie OUTPUT</returns>
      protected List<T> Insert<T>(string insertQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters, Func<IDataReader, T> readItem)
      {
         return ExecuteNonQuery<T>(insertQuery, transaction, addParameters, readItem);
      }

      /// <summary>
      /// Obsługa zapytań INSERT (bez części OUTPUT), np.:
      /// INSERT INTO TestTable (Name, Enabled)
      /// VALUES ('A2', 1), ('A3', 1)
      /// </summary>
      /// <param name="insertQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <returns>Ilość dodanych rekordów</returns>
      protected int Insert(string insertQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters)
      {
         return ExecuteNonQuery(insertQuery, transaction, addParameters);
      }

      /// <summary>
      /// Obsługa zapytań UPDATE zawierających frazę OUTPUT (tylko z INSERTED.Id), np.:
      /// UPDATE TestTable
      /// SET Name = 'NewVal'
      /// OUTPUT INSERTED.Id
      /// WHERE Id IN (2, 3)
      /// </summary>
      /// <param name="updateQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <returns>Identyfikatory zaktualizowanych rekordów</returns>
      protected List<long?> UpdateWithOutputId(string updateQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters)
      {
         return Update(updateQuery, transaction, addParameters, (IDataReader reader) => { return reader.GetNullableInt64("Id"); });
      }

      /// <summary>
      /// Obsługa zapytań UPDATE zawierających frazę OUTPUT, np.:
      /// UPDATE TestTable
      /// SET Name = 'NewVal'
      /// OUTPUT INSERTED.Id, INSERTED.Name
      /// WHERE Id IN (2, 3)
      /// </summary>
      /// <param name="updateQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <param name="readItem">Funkcja wczytująca pojedynczy element opisany w frazie OUTPUT</param>
      /// <returns>Lista elementów opisanych w frazie OUTPUT</returns>
      protected List<T> Update<T>(string updateQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters, Func<IDataReader, T> readItem)
      {
         return ExecuteNonQuery<T>(updateQuery, transaction, addParameters, readItem);
      }

      /// <summary>
      /// Obsługa zapytań UPDATE (bez części OUTPUT), np.:
      /// UPDATE TestTable
      /// SET Name = 'NewVal'
      /// WHERE Id IN (2, 3)
      /// </summary>
      /// <param name="updateQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <returns>Ilość dodanych rekordów</returns>
      protected int Update(string updateQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters)
      {
         return ExecuteNonQuery(updateQuery, transaction, addParameters);
      }

      /// <summary>
      /// Obsługa zapytań DELETE zawierających frazę OUTPUT (tylko z DELETED.Id), np.:
      /// DELETE FROM TestTable
      /// OUTPUT DELETED.Id
      /// WHERE Id IN (2, 3)
      /// </summary>
      /// <param name="deleteQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <returns>Identyfikatory usuniętych rekordów</returns>
      protected List<long?> DeleteWithOutputId(string deleteQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters)
      {
         return Delete(deleteQuery, transaction, addParameters, (IDataReader reader) => { return reader.GetNullableInt64("Id"); });
      }

      /// <summary>
      /// Obsługa zapytań DELETE zawierających frazę OUTPUT, np.:
      /// DELETE FROM TestTable
      /// OUTPUT DELETED.Id, DELETED.Name
      /// WHERE Id IN (2, 3)
      /// </summary>
      /// <param name="deleteQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <param name="readItem">Funkcja wczytująca pojedynczy element opisany w frazie OUTPUT</param>
      /// <returns>Lista elementów opisanych w frazie OUTPUT</returns>
      protected List<T> Delete<T>(string deleteQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters, Func<IDataReader, T> readItem)
      {
         return ExecuteNonQuery<T>(deleteQuery, transaction, addParameters, readItem);
      }

      /// <summary>
      /// Obsługa zapytań DELETE (bez części OUTPUT), np.:
      /// DELETE FROM TestTable
      /// WHERE Id IN (2, 3)
      /// </summary>
      /// <param name="deleteQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <returns>Ilość dodanych rekordów</returns>
      protected int Delete(string deleteQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters)
      {
         return ExecuteNonQuery(deleteQuery, transaction, addParameters);
      }

      /// <summary>
      /// Obsługa zapytań INSERT, UPDATE, DELETE zawierających frazę OUTPUT
      /// </summary>
      /// <param name="insertUpdateDeleteQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <param name="readItem">Funkcja wczytująca pojedynczy element opisany w frazie OUTPUT</param>
      /// <returns>Lista elementów opisanych w frazie OUTPUT</returns>
      private List<T> ExecuteNonQuery<T>(string insertUpdateDeleteQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters, Func<IDataReader, T> readItem)
      {
         using (DbCommand command = GetSqlStringCommand(insertUpdateDeleteQuery))
         {
            var stopWatch = new Stopwatch();
            addParameters?.Invoke(command);
            stopWatch.Start();
            try
            {
               using (var reader = ExecuteReader(transaction, command))
               {
                  return ReadAll<T>(reader, readItem);
               }
            }
            finally
            {
               stopWatch.Stop();
               Log(command, stopWatch.ElapsedMilliseconds);
            }
         }
      }

      /// <summary>
      /// Obsługa zapytań INSERT, UPDATE, DELETE (bez części OUTPUT)
      /// </summary>
      /// <param name="insertUpdateDeleteQuery">Zapytanie bazodanowe</param>
      /// <param name="transaction">Obiekt transakcji bazodanowej</param>
      /// <param name="addParameters">Parametry do zapytania bazodanowego</param>
      /// <returns>Ilość rekordów dodanych/zmodyfikowanych/usuniętych przez zapytanie</returns>
      private int ExecuteNonQuery(string insertUpdateDeleteQuery, IDatabaseTransaction transaction, Action<DbCommand> addParameters)
      {
         using (DbCommand command = GetSqlStringCommand(insertUpdateDeleteQuery))
         {
            var stopWatch = new Stopwatch();
            addParameters?.Invoke(command);
            stopWatch.Start();
            try
            {
               return ExecuteNonQuery(transaction, command);
            }
            finally
            {
               stopWatch.Stop();
               Log(command, stopWatch.ElapsedMilliseconds);
            }
         }
      }

      /// <summary>
      /// Odczytuje wszystkie elementy z readera z uwzględnieniem ilości wierszy
      /// </summary>
      /// <typeparam name="T">Typ encji</typeparam>
      /// <param name="reader">Obiekt odczytu</param>
      /// <param name="readItem">Metoda tworząca obiekt encji</param>
      /// <param name="count">Referencja do zmiennej określającej ilość pobranych rekordów</param>
      /// <returns>Odczytane elementy lub null</returns>
      private List<T> ReadAllWithTotalRowCount<T>(IDataReader reader, Func<IDataReader, T> readItem, ref long count)
      {
         List<T> items = new List<T>();
         while (reader.Read())
         {
            if (count == 0)
               count = reader.GetInt32("DAORowsCount");

            items.Add(readItem(reader));
         }

         return (items.Count > 0) ? items : null;
      }

      /// <summary>
      /// Odczytuje wszystkie elementy z readera
      /// </summary>
      /// <typeparam name="T">Typ encji</typeparam>
      /// <param name="reader">Obiekt odczytu</param>
      /// <param name="readItem">Metoda tworząca obiekt encji</param>
      /// <returns>Odczytane elementy lub null</returns>
      private List<T> ReadAll<T>(IDataReader reader, Func<IDataReader, T> readItem)
      {
         List<T> items = new List<T>();
         while (reader.Read())
            items.Add(readItem(reader));

         return (items.Count > 0) ? items : null;
      }

      /// <summary>
      /// Odczytuje pierwszy element z readera
      /// </summary>
      /// <typeparam name="T">Typ encji</typeparam>
      /// <param name="reader">Obiekt odczytu</param>
      /// <param name="readItem">Metoda tworząca obiekt encji</param>
      /// <returns>Odczytany element lub null</returns>
      private T ReadFirst<T>(IDataReader reader, Func<IDataReader, T> readItem)
      {
         if (reader.Read())
            return readItem(reader);

         return default(T);
      }

      #region Execution Time Logging

      /// <summary>
      /// Flaga określa czy należy załączyć logowanie czasów wykonania metod Read, ReadListItems, ReadFirst
      /// </summary>
      private bool ExecutionTimeLogging
      {
         get
         {
#if DEBUG
            return true;
#else
				return false;
#endif
         }
      }

      /// <summary>
      /// Minimalny czas wykonania zapytania określający czy zapytanie należy zalogować
      /// </summary>
      private double ExecutionTimeSecondsMinLevel
      {
         get
         {
            return 1.0D;
         }
      }

      private void Log(DbCommand command, long executionTime)
      {
         if (ExecutionTimeLogging)
         {
            double executionTimeSeconds = Math.Round(executionTime / 1000D, 2);
            if (executionTimeSeconds >= ExecutionTimeSecondsMinLevel)
            {
               try
               {
                  StringBuilder sb = new StringBuilder();
                  sb.AppendLine();
                  sb.AppendLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                  sb.AppendLine("CommandText:");
                  sb.AppendLine(command.CommandText);
                  if (command.Parameters != null && command.Parameters.Count > 0)
                  {
                     sb.AppendLine("Parameters:");
                     foreach (DbParameter parameter in command.Parameters)
                     {
                        sb.AppendLine($"[{parameter.DbType}] {parameter.ParameterName} = {parameter.Value ?? "NULL"}");
                     }
                  }
                  sb.AppendLine($"Execution Time {executionTimeSeconds}s");
                  sb.AppendLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

                  //LoggerMock.WriteLog(DataLogCategories.Data_General, "ExecutionTimeLogging", sb.ToString(), TraceEventType.Verbose);
               }
               catch //(Exception ex)
               {
                  //LoggerMock.WriteLog(DataLogCategories.Data_General, "ExecutionTimeLogging", "Błąd generowania informacji o wykonaniu zapytania", ex, TraceEventType.Error);
               }
            }
         }
      }

      #endregion Execution Time Logging
   }
}
