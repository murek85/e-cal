﻿using ECal.Common;
using ECal.Common.Database;
using ECal.Common.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECal.Data.PostgreSqlDB
{
	public class PostgresqlDBRsqlMapper : IRsqlMapper
	{
		static Dictionary<string, Dictionary<string, KeyValuePair<string, Type>>> fieldMaping = new Dictionary<string, Dictionary<string, KeyValuePair<string, Type>>>();

		public PostgresqlDBRsqlMapper()
		{

		}

		public IPostgresqlDBRsqlMapperField ForQuery(string queryKey)
		{
			Dictionary<string, KeyValuePair<string, Type>> mapping = null;
			if (fieldMaping.ContainsKey(queryKey))
			{
				mapping = fieldMaping[queryKey];
			}
			else
			{
				mapping = new Dictionary<string, KeyValuePair<string, Type>>();
				fieldMaping.Add(queryKey, mapping);
			}
			return new PostgresqlDBRsqlMapperField(mapping);
		}

		public string BuildQuery(string queryKey, RsqlData rsql, string sourceQuery, string defaultSort = "id", bool containsWhereExpression = false, bool noPaging = false)
		{
			return ForQuery(queryKey).Build(rsql, sourceQuery, defaultSort, containsWhereExpression, noPaging);
		}

		public class PostgresqlDBRsqlMapperField : IPostgresqlDBRsqlMapperField
		{
			Dictionary<string, KeyValuePair<string, Type>> mapping = null;

			public PostgresqlDBRsqlMapperField(Dictionary<string, KeyValuePair<string, Type>> fieldDict)
			{
				mapping = fieldDict;
			}

			public IPostgresqlDBRsqlMapperField ForField<ValueType>(string rsqlMember, string dbMember)
			{
				mapping.Add(rsqlMember, new KeyValuePair<string, Type>(dbMember, typeof(ValueType)));
				return this;
			}

			public string Build(RsqlData rsql, string sourceQuery, string defaultSort, bool containsWhereExpression, bool noPaging = false)
			{
				string sortDir = "";
				string sortColumn = defaultSort;
				if (!string.IsNullOrEmpty(rsql.Sort))
				{
					if (rsql.Sort[0] == '-')
					{
						sortDir = "DESC";
						sortColumn = rsql.Sort[1..];
					}
					else
					{
						sortDir = "ASC";
						sortColumn = rsql.Sort[1..];
					}
				}

				if (mapping.ContainsKey(sortColumn))
				{
					sortColumn = mapping[sortColumn].Key;
					int index = sortColumn.LastIndexOf(".");
					if (index > 0)
					{
						sortColumn = sortColumn[(index + 1)..];
					}
				}

				String q = rsql.qData;
				StringBuilder sb = new();
				Type valueType = null;
				if (q != null)
				{
					String[] listParams = q.Split(";");
					foreach (String field in listParams)
					{
						if (field.Contains("=="))
						{
							//valueType = EqualityOption(sb, field);
						}
						else if (field.Contains("=eq="))
						{
							valueType = EqualityOption(sb, field);
						}
						else if (field.Contains("=like="))
						{
							valueType = LikeOption(sb, field);
						}
						else if (field.Contains("=ge="))
						{
							valueType = GeOption(sb, field);
						}
						else if (field.Contains("=le="))
						{
							valueType = LeOption(sb, field);
						}
					}
				}
				if (sb.Length > 0)
				{
					if (containsWhereExpression)
					{
						sourceQuery += " AND ";
					}
					else
					{
						sourceQuery += " WHERE ";
					}

					sourceQuery += sb.ToString();
				}
				sourceQuery = AddPagingWithOrderBy(sourceQuery, rsql, $"{sortColumn} {sortDir}", noPaging);
				return sourceQuery;
			}

			private Type EqualityOption(StringBuilder sb, string field)
			{
				var v = Parse(mapping, field, "=eq=", out Type valueType);
				if (sb.Length > 0)
				{
					sb.AppendLine(" AND ");
				}

				if (valueType == typeof(string))
				{
					if (v.Value == "NULL")
					{
						sb.Append($"{v.Key} IS NULL");
					}
					else
					{
						sb.Append($"{v.Key} LIKE ('%{v.Value}%')");
					}
				}
				else
				{
					sb.Append($"{v.Key} = {v.Value}");
				}
				return valueType;
			}

			private Type LikeOption(StringBuilder sb, string field)
			{
				var v = Parse(mapping, field, "=ilike=", out Type valueType);
				if (sb.Length > 0)
				{
					sb.AppendLine(" AND ");
				}

				sb.Append($"{v.Key} LIKE ('%{v.Value}%')");
				return valueType;
			}

			private Type LeOption(StringBuilder sb, string field)
			{
				var v = Parse(mapping, field, "=le=", out Type valueType);
				if (sb.Length > 0)
				{
					sb.AppendLine(" AND ");
				}

				if (valueType == typeof(DateTime))
				{
					var dateStr = DateTime.Parse(v.Value).ToString("yyyy-MM-dd HH:mm:ss.fff");
					sb.Append($"{v.Key} <= '{dateStr}'");
				}
				else if (valueType == typeof(DateTimeOffset))
				{
					var dateStr = DateTimeOffset.Parse(v.Value).ToString("yyyy-MM-dd HH:mm:ss.fff");
					sb.Append($"{v.Key} <= '{dateStr}'");
				}
				else if (valueType == typeof(string))
				{
					sb.Append($"{v.Key} <= '{v.Value}'");
				}
				else
				{
					sb.Append($"{v.Key} <= {v.Value}");
				}
				return valueType;
			}

			private Type GeOption(StringBuilder sb, string field)
			{
				var v = Parse(mapping, field, "=ge=", out Type valueType);
				if (sb.Length > 0)
				{
					sb.AppendLine(" AND ");
				}

				if (valueType == typeof(DateTime))
				{
					var dateStr = DateTime.Parse(v.Value).ToString("yyyy-MM-dd HH:mm:ss.fff");
					sb.Append($"{v.Key} >= '{dateStr}'");
				}
				else if (valueType == typeof(DateTimeOffset))
				{
					var dateStr = DateTimeOffset.Parse(v.Value).ToString("yyyy-MM-dd HH:mm:ss.fff");
					sb.Append($"{v.Key} >= '{dateStr}'");
				}
				else if (valueType == typeof(string))
				{
					sb.Append($"{v.Key} >= '{v.Value}'");
				}
				else
				{
					sb.Append($"{v.Key} >= {v.Value}");
				}
				return valueType;
			}

			KeyValuePair<string, string> Parse(Dictionary<string, KeyValuePair<string, Type>> map, String field, String symbol, out Type valueType)
			{
				String[] pair = field.Split(symbol);
				var key = pair[0].Trim();
				var value = pair[1]?.Trim().Replace("\"", "");

				valueType = typeof(string);
				if (map.ContainsKey(key))
				{
					valueType = map[key].Value;
					key = map[key].Key;
				}

				return new KeyValuePair<string, string>(key, value);
			}

			static string AddPagingWithOrderBy(string dbQuery, Paging paging, string sortAdditional, bool noPaging)
			{
				StringBuilder dbQueryPaged = new StringBuilder();

				string query = $"SELECT *, COUNT (*) OVER() AS COUNT FROM ({dbQuery}) AS x";
				dbQueryPaged.AppendLine(query);
				dbQueryPaged.AppendLine("ORDER BY " + sortAdditional);

				if (!noPaging)
				{
					dbQueryPaged.AppendFormat("OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY", (paging.PageNumber - 1) * paging.PageSize, paging.PageSize);
				}

				return dbQueryPaged.ToString();
			}

		}
	}
}
