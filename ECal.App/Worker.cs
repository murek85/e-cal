﻿using ECal.App.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OpenIddict.Abstractions;
using System;
using System.Threading;
using System.Threading.Tasks;
using static OpenIddict.Abstractions.OpenIddictConstants;

namespace ECal.App
{
   public class Worker : IHostedService
   {
      private readonly IServiceProvider _serviceProvider;

      public Worker(IServiceProvider serviceProvider)
         => _serviceProvider = serviceProvider;

      public async Task StartAsync(CancellationToken cancellationToken)
      {
         using var scope = _serviceProvider.CreateScope();

         var keysContext = scope.ServiceProvider.GetRequiredService<KeysDbContext>();
         await keysContext.Database.MigrateAsync(cancellationToken);

         var applicationContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
         await applicationContext.Database.MigrateAsync(cancellationToken);

         await RegisterApplicationsAsync(scope.ServiceProvider);
         await RegisterScopesAsync(scope.ServiceProvider);

         static async Task RegisterApplicationsAsync(IServiceProvider provider)
         {
            var manager = provider.GetRequiredService<IOpenIddictApplicationManager>();
            if (await manager.FindByClientIdAsync("resource-server-admin") is null)
            {
               var descriptor = new OpenIddictApplicationDescriptor
               {
                  ClientId = "resource-server-admin",
                  ClientSecret = "45317829-e0ec-490e-8b68-4bbe2d54aa6e",
                  DisplayName = "Resource server Admin Api",
                  Permissions =
                  {
                     Permissions.Endpoints.Introspection
                  }
               };
               await manager.CreateAsync(descriptor);
            }

            if (await manager.FindByClientIdAsync("admin") is null)
            {
               var descriptor = new OpenIddictApplicationDescriptor
               {
                  ClientId = "admin",
                  ClientSecret = "5fb7d832-04e6-4566-b860-24126fb28097",
                  ConsentType = ConsentTypes.Explicit,
                  DisplayName = "Admin client application",
                  RedirectUris =
                  {
                     new Uri("https://localhost:4202/auth"),
                     new Uri("http://localhost:4202/auth")
                  },
                  Permissions = {
                     Permissions.Endpoints.Revocation,
                     Permissions.Endpoints.Authorization,
                     Permissions.Endpoints.Token,
                     Permissions.Endpoints.Logout,
                     Permissions.GrantTypes.AuthorizationCode,
                     Permissions.GrantTypes.Implicit,
                     Permissions.GrantTypes.Password,
                     Permissions.GrantTypes.RefreshToken,
                     Permissions.ResponseTypes.Code,
                     Permissions.Scopes.Email,
                     Permissions.Scopes.Profile,
                     Permissions.Scopes.Roles,
                     Permissions.Scopes.Phone,
                     Permissions.Prefixes.Scope + "api-resource-server-admin"
                  }
               };
               await manager.CreateAsync(descriptor);
            }

            if (await manager.FindByClientIdAsync("murek") is null)
            {
               var descriptor = new OpenIddictApplicationDescriptor
               {
                  ClientId = "murek",
                  ClientSecret = "5fb7d832-04e6-4566-b860-24126fb28097",
                  ConsentType = ConsentTypes.Explicit,
                  DisplayName = "Murek client application",
                  RedirectUris =
                  {
                     new Uri("https://localhost:4202/auth"),
                     new Uri("http://localhost:4202/auth")
                  },
                  Permissions = {
                     Permissions.Endpoints.Revocation,
                     Permissions.Endpoints.Authorization,
                     Permissions.Endpoints.Token,
                     Permissions.Endpoints.Logout,
                     Permissions.GrantTypes.AuthorizationCode,
                     Permissions.GrantTypes.Implicit,
                     Permissions.GrantTypes.Password,
                     Permissions.GrantTypes.RefreshToken,
                     Permissions.ResponseTypes.Code,
                     Permissions.Scopes.Email,
                     Permissions.Scopes.Profile,
                     Permissions.Scopes.Roles,
                     Permissions.Scopes.Phone,
                     Permissions.Prefixes.Scope + "api-resource-server-admin"
                  }
               };
               await manager.CreateAsync(descriptor);
            }
         }
         static async Task RegisterScopesAsync(IServiceProvider provider)
         {
            var manager = provider.GetRequiredService<IOpenIddictScopeManager>();
            if (await manager.FindByNameAsync("api-resource-server-admin") is null)
            {
               var descriptor = new OpenIddictScopeDescriptor
               {
                  DisplayName = "Resource server Admin Api access",
                  Name = "api-resource-server-admin",
                  Resources = { "resource-server-admin" }
               };

               await manager.CreateAsync(descriptor);
            }
         }
      }

      public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
   }
}
