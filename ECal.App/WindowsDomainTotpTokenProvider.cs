﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace ECal.App
{
	public class WindowsDomainTotpTokenProvider<TUser> : TotpSecurityStampBasedTokenProvider<TUser>
        where TUser : class
    {
        public override Task<bool> CanGenerateTwoFactorTokenAsync(UserManager<TUser> manager, TUser user)
        {
            return Task.FromResult(false);
        }

        public override async Task<string> GetUserModifierAsync(string purpose, UserManager<TUser> manager, TUser user)
        {
            var userName = await manager.GetUserNameAsync(user);
            return $"WindowsDomain:{purpose}:{userName}";
        }
    }
}
