﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace ECal.App.Hubs
{
	public class BroadcastHub : Hub
	{
		public async Task Notifications(string message)
		{
			await Clients.All.SendAsync("notifications", message);
		}

		public async Task Dashboards(string message)
		{
			await Clients.All.SendAsync("dashboards", message);
		}

		public async Task Users(string message)
		{
			await Clients.All.SendAsync("users", message);
		}

		public async Task Roles(string message)
		{
			await Clients.All.SendAsync("roles", message);
		}

		public async Task Workstations(string message)
		{
			await Clients.All.SendAsync("workstations", message);
		}
	}
}
