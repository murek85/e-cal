﻿using ECal.Common.Mapping;
using System;

namespace ECal.App
{
   public class RsqlMapperInitializer
   {
      private IRsqlMapper _rsqlMapper;

      public RsqlMapperInitializer(IRsqlMapper rsqlMapper)
      {
         _rsqlMapper = rsqlMapper;
      }

      public void Initialize()
      {
         _rsqlMapper.ForQuery("ReadApplications")
            .ForField<int?>("id", @"a.""ApplicationId""")
            .ForField<string>("name", @"a.""Name""")
            .ForField<string>("description", @"a.""Description""")
            .ForField<string>("apiUrl", @"a.""ApiUrl""")
            .ForField<string>("appUrl", @"a.""AppUrl""");

         _rsqlMapper.ForQuery("ReadGroupsResources")
            .ForField<int?>("id", @"g.""GroupResourceId""")
            .ForField<string>("name", @"g.""Name""")
            .ForField<string>("description", @"g.""Description""");

         _rsqlMapper.ForQuery("ReadGroupsUsers")
            .ForField<int?>("id", @"g.""GroupUserId""")
            .ForField<string>("name", @"g.""Name""")
            .ForField<string>("description", @"g.""Description""");

         _rsqlMapper.ForQuery("ReadRoles")
            .ForField<int?>("id", @"r.""RoleId""")
            .ForField<string>("name", @"r.""Name""")
            .ForField<string>("description", @"r.""Description""");

         _rsqlMapper.ForQuery("ReadPermissions")
            .ForField<int?>("id", @"p.""PermissionId""")
            .ForField<string>("name", @"p.""Name""");

         _rsqlMapper.ForQuery("ReadUsers")
            .ForField<Guid?>("id", @"u.""Id""")
            .ForField<string>("userName", @"u.""UserName""")
            .ForField<string>("email", @"u.""Email""")
            .ForField<bool?>("emailConfirmed", @"u.""EmailConfirmed""")
            .ForField<string>("phoneNumber", @"u.""PhoneNumber""");
      }
   }
}
