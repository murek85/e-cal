﻿using Microsoft.AspNetCore.Identity;

namespace ECal.App.Extensions
{
	public static class CustomIdentityBuilderExtensions
	{
        public static IdentityBuilder AddWindowsDomainTotpTokenProvider(this IdentityBuilder builder)
        {
            var userType = builder.UserType;
            var totpProvider = typeof(WindowsDomainTotpTokenProvider<>).MakeGenericType(userType);
            return builder.AddTokenProvider("WindowsDomainTotpTokenProvider", totpProvider);
        }
    }
}
