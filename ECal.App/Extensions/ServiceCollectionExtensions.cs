﻿using ECal.Model.Ldap;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECal.App.Extensions
{
	public static class ServiceCollectionExtensions
    {
        public static void AddLdapAuthentication<TUser>(this IServiceCollection collection, Action<LdapConfig> setupAction = null)
            where TUser : class
        {
            if (setupAction != null)
            {
                collection.Configure(setupAction);
            }
        }

        public static IEnumerable<(T item, int index)> WithIndex<T>(this IEnumerable<T> self) => self.Select((item, index) => (item, index));
    }
}
