﻿using AutoMapper;
using ECal.App.Filters;
using ECal.App.Services.Email;
using ECal.App.Services.Identities;
using ECal.Business.Common;
using ECal.Common;
using ECal.Model;
using ECal.Model.DB;
using ECal.Model.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenIddict.Validation.AspNetCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECal.App.Controllers
{
   [ServiceFilter(typeof(ApiExceptionFilter))]
   [ApiController]
   [Route("api/[controller]")]
   [Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
   public class ApplicationsController : ControllerBase
   {
      private readonly ApplicationSignInManager _signInManager;
      private readonly ApplicationUserManager _userManager;
      private readonly IOptions<IdentityOptions> _identityOptions;
      private readonly IApplicationsManager _applicationsManager;
      private readonly IReportManager _reportManager;
      private readonly IEmailSender _emailSender;
      private readonly IMapper _mapper;
      private readonly ILogger _logger;
      private readonly IDataProtector _protector;

      public ApplicationsController(
         ApplicationSignInManager signInManager,
         ApplicationUserManager userManager,
         IOptions<IdentityOptions> identityOptions,
         IApplicationsManager applicationsManager,
         IReportManager reportManager,
         IEmailSender emailSender,
         IMapper mapper,
         ILoggerFactory loggerFactory,
         IDataProtectionProvider provider)
      {
         _userManager = userManager;
         _applicationsManager = applicationsManager;
         _reportManager = reportManager;
         _emailSender = emailSender;
         _identityOptions = identityOptions;
         _mapper = mapper;
         _logger = loggerFactory.CreateLogger<ApplicationsController>();
         _protector = provider.CreateProtector("protect_my_query_string");
      }

      [HttpGet("report")]
      public ActionResult GetReport([FromQuery] PageSettings pageSettings, [FromQuery] ReportBuilder reportBuilder)
      {
         var paging = new RsqlData(pageSettings.Query, pageSettings.Sort);
         (List<Application>, long) applications = _applicationsManager.GetApplicationsReport(paging);

         var bytes = _reportManager.ExportData<ApplicationViewModel>(reportBuilder, _mapper.Map<List<ApplicationViewModel>>(applications.Item1));
         return File(bytes, "application/octet-stream");
      }

      [HttpGet("list")]
      public ActionResult<IList<ApplicationViewModel>> GetList([FromQuery] PageSettings pageSettings)
      {
         var paging = new RsqlData(pageSettings);
         (List<Application>, long) applications = _applicationsManager.GetApplicationsList(paging);
         paging.TotalRows = applications.Item2;

         return Ok(new
         {
            data = _mapper.Map<List<ApplicationViewModel>>(applications.Item1),
            paging
         });
      }

      [HttpGet("{id}")]
      public async Task<ActionResult<ApplicationViewModel>> Get(long id)
      {
         Application application = _applicationsManager.GetApplication(id);
         return Ok(_mapper.Map<ApplicationViewModel>(application));
      }

      [HttpPost]
      public async Task<IActionResult> Create([FromBody] ApplicationViewModel model)
      {
         Application application = _mapper.Map<Application>(model);

         long? id = _applicationsManager.CreateApplication(application);
         if (id.HasValue)
         {
            return Ok(new { id });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpPut]
      public async Task<IActionResult> Update([FromBody] ApplicationViewModel model)
      {
         Application application = _mapper.Map<Application>(model);

         bool result = _applicationsManager.UpdateApplication(application);
         if (result)
         {
            return Ok(new { result });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpDelete("{id}")]
      public async Task<IActionResult> Delete(int id)
      {
         bool result = _applicationsManager.DeleteApplication(id);
         if (result)
         {
            return Ok(new { result });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }
   }
}
