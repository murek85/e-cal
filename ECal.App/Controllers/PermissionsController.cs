﻿using AutoMapper;
using ECal.App.Filters;
using ECal.App.Services.Email;
using ECal.App.Services.Identities;
using ECal.Business.Common;
using ECal.Common;
using ECal.Model;
using ECal.Model.DB;
using ECal.Model.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenIddict.Validation.AspNetCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECal.App.Controllers
{
   [ServiceFilter(typeof(ApiExceptionFilter))]
   [ApiController]
   [Route("api/[controller]")]
   [Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
   public class PermissionsController : ControllerBase
   {
      private readonly ApplicationSignInManager _signInManager;
      private readonly ApplicationUserManager _userManager;
      private readonly IOptions<IdentityOptions> _identityOptions;
      private readonly IPermissionsManager _permissionsManager;
      private readonly IReportManager _reportManager;
      private readonly IEmailSender _emailSender;
      private readonly IMapper _mapper;
      private readonly ILogger _logger;
      private readonly IDataProtector _protector;

      public PermissionsController(
         ApplicationSignInManager signInManager,
         ApplicationUserManager userManager,
         IOptions<IdentityOptions> identityOptions,
         IPermissionsManager permissionsManager,
         IReportManager reportManager,
         IEmailSender emailSender,
         IMapper mapper,
         ILoggerFactory loggerFactory,
         IDataProtectionProvider provider)
      {
         _userManager = userManager;
         _permissionsManager = permissionsManager;
         _reportManager = reportManager;
         _emailSender = emailSender;
         _identityOptions = identityOptions;
         _mapper = mapper;
         _logger = loggerFactory.CreateLogger<PermissionsController>();
         _protector = provider.CreateProtector("protect_my_query_string");
      }

      [HttpGet("~/api/workstations/list")]
      [AllowAnonymous]
      public ActionResult<IList<PermissionViewModel>> GetWorkstations([FromQuery] PageSettings pageSettings)
      {
         var paging = new RsqlData(pageSettings);
         (List<Permission>, long) permissions = _permissionsManager.GetPermissionsList(paging);
         paging.TotalRows = permissions.Item2;

         return Ok(new
         {
            data = _mapper.Map<List<PermissionViewModel>>(permissions.Item1),
            paging
         });
      }

      [HttpGet("list")]
      public ActionResult<IList<PermissionViewModel>> GetList([FromQuery] PageSettings pageSettings)
      {
         var paging = new RsqlData(pageSettings);
         (List<Permission>, long) permissions = _permissionsManager.GetPermissionsList(paging);
         paging.TotalRows = permissions.Item2;

         return Ok(new
         {
            data = _mapper.Map<List<PermissionViewModel>>(permissions.Item1),
            paging
         });
      }

      [HttpGet("{id}")]
      public async Task<ActionResult<PermissionViewModel>> Get(long id)
      {
         Permission permission = _permissionsManager.GetPermission(id);
         return Ok(_mapper.Map<PermissionViewModel>(permission));
      }

      [HttpPost]
      public async Task<IActionResult> Create([FromBody] PermissionViewModel model)
      {
         Permission permission = _mapper.Map<Permission>(model);

         long? id = _permissionsManager.CreatePermission(permission);
         if (id.HasValue)
         {
            return Ok(new { id });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpPut]
      public async Task<IActionResult> Update([FromBody] PermissionViewModel model)
      {
         Permission permission = _mapper.Map<Permission>(model);

         bool result = _permissionsManager.UpdatePermission(permission);
         if (result)
         {
            return Ok(new { result });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpDelete("{id}")]
      public async Task<IActionResult> Delete(int id)
      {
         bool result = _permissionsManager.DeletePermission(id);
         if (result)
         {
            return Ok(new { result });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }
   }
}
