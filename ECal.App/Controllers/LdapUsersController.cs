﻿using ECal.App.Filters;
using ECal.App.Services.Identities;
using ECal.App.Services.Ldap;
using ECal.Model.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenIddict.Abstractions;
using OpenIddict.Server.AspNetCore;
using OpenIddict.Validation.AspNetCore;
using System.Collections.Generic;

namespace ECal.App.Controllers
{
	[ServiceFilter(typeof(ApiExceptionFilter))]
	[ApiController]
	[Route("api/[controller]")]
	[Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
	public class LdapUsersController : ControllerBase
	{
		private readonly ApplicationSignInManager _signInManager;
		private readonly ApplicationUserManager _userManager;
		private readonly LdapUserManager _ldapUserManager;
		private readonly LdapSignInManager _ldapSignInManager;
		private readonly IOpenIddictScopeManager _scopeManager;
		private readonly IOptions<IdentityOptions> _identityOptions;
		private readonly ILogger _logger;
		private readonly IDataProtector _protector;

		public LdapUsersController(
			ApplicationSignInManager signInManager,
			ApplicationUserManager userManager,
			LdapUserManager ldapUserManager,
			LdapSignInManager ldapSignInManager,
			IOpenIddictScopeManager scopeManager,
			IOptions<IdentityOptions> identityOptions,
			ILoggerFactory loggerFactory,
			IDataProtectionProvider provider)
		{
			_signInManager = signInManager;
			_userManager = userManager;
			_ldapSignInManager = ldapSignInManager;
			_ldapUserManager = ldapUserManager;
			_scopeManager = scopeManager;
			_identityOptions = identityOptions;
			_logger = loggerFactory.CreateLogger<LdapUsersController>();
			_protector = provider.CreateProtector("protect_my_query_string");
		}

		[HttpGet("filter")]
		public ActionResult<IList<LdapUserViewModel>> GetFilterLdapUsers(
			string query)
		{
			return Ok();
		}
	}
}