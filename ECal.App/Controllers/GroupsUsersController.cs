﻿using AutoMapper;
using ECal.App.Filters;
using ECal.App.Services.Email;
using ECal.App.Services.Identities;
using ECal.Business.Common;
using ECal.Common;
using ECal.Model.DB;
using ECal.Model.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenIddict.Validation.AspNetCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECal.App.Controllers
{
   [ServiceFilter(typeof(ApiExceptionFilter))]
   [ApiController]
   [Route("api/groups/users")]
   [Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
   public class GroupsUsersController : ControllerBase
   {
      private readonly ApplicationSignInManager _signInManager;
      private readonly ApplicationUserManager _userManager;
      private readonly IOptions<IdentityOptions> _identityOptions;
      private readonly IGroupsUsersManager _groupsUsersManager;
      private readonly IReportManager _reportManager;
      private readonly IEmailSender _emailSender;
      private readonly IMapper _mapper;
      private readonly ILogger _logger;
      private readonly IDataProtector _protector;

      public GroupsUsersController(
         ApplicationSignInManager signInManager,
         ApplicationUserManager userManager,
         IOptions<IdentityOptions> identityOptions,
         IGroupsUsersManager groupsUsersManager,
         IReportManager reportManager,
         IEmailSender emailSender,
         IMapper mapper,
         ILoggerFactory loggerFactory,
         IDataProtectionProvider provider)
      {
         _userManager = userManager;
         _reportManager = reportManager;
         _emailSender = emailSender;
         _identityOptions = identityOptions;
         _groupsUsersManager = groupsUsersManager;
         _mapper = mapper;
         _logger = loggerFactory.CreateLogger<RolesController>();
         _protector = provider.CreateProtector("protect_my_query_string");
      }

      [HttpGet("list")]
      public ActionResult<IList<GroupUserViewModel>> GetList([FromQuery] PageSettings pageSettings)
      {
         var paging = new RsqlData(pageSettings);
         (List<GroupUser>, long) groups = _groupsUsersManager.GetGroupsUsersList(paging);
         paging.TotalRows = groups.Item2;

         return Ok(new
         {
            data = _mapper.Map<List<GroupUserViewModel>>(groups.Item1),
            paging
         });
      }

      [HttpGet("{id}")]
      public async Task<ActionResult<GroupUserViewModel>> Get(long id)
      {
         GroupUser groupUser = _groupsUsersManager.GetGroupUser(id);
         return Ok(_mapper.Map<GroupUserViewModel>(groupUser));
      }

      [HttpPost]
      public async Task<IActionResult> Create([FromBody] GroupUserViewModel model)
      {
         GroupUser groupUser = _mapper.Map<GroupUser>(model);

         long? id = _groupsUsersManager.CreateGroupUser(groupUser);
         if (id.HasValue)
         {
            return Ok(new { id });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpPut]
      public async Task<IActionResult> Update([FromBody] GroupUserViewModel model)
      {
         GroupUser groupUser = _mapper.Map<GroupUser>(model);

         bool result = _groupsUsersManager.UpdateGroupUser(groupUser);
         if (result)
         {
            return Ok(new { result });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpDelete("{id}")]
      public async Task<IActionResult> Delete(int id)
      {
         bool result = _groupsUsersManager.DeleteGroupUser(id);
         if (result)
         {
            return Ok(new { result });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }
   }
}
