﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OpenIddict.Abstractions;
using System;

namespace ECal.App.Controllers
{
	[ApiController]
    [Route("[controller]")]
    public class ErrorController : Controller
    {
        private RedirectResult Redirect(
            string type, string message)
        {
            string referer = Request.Headers["Referer"].ToString();
            Uri address = new Uri(referer);

            return Redirect($"{address.Scheme}://{address.Authority}/error?type={type}&message={message}");
        }

        [HttpGet, HttpPost, Route("~/error")]
        public IActionResult Error()
        {
            var exceptionHandler = HttpContext.Features.Get<IExceptionHandlerFeature>();
            if (exceptionHandler != null)
            {
                return Redirect("invalid_request", exceptionHandler.Error.Message);
            }

            return Redirect("invalid_request", "error_unknown");
        }

        [HttpGet, HttpPost, Route("~/error-status-code")]
        public IActionResult Error(OpenIddictResponse response)
        {
            if (response != null)
            {
                return Redirect(response.Error, response.ErrorDescription);
            }

            return Redirect("invalid_request", "error_oidc");
        }

        [HttpGet, HttpPost, Route("~/error-signin")]
        public IActionResult Error(
            string type, string message)
        {
            return Redirect(type, message);
        }
    }
}