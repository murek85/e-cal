﻿using AutoMapper;
using ECal.App.Filters;
using ECal.App.Services.Email;
using ECal.App.Services.Identities;
using ECal.Business.Common;
using ECal.Common;
using ECal.Model;
using ECal.Model.DB;
using ECal.Model.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenIddict.Validation.AspNetCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECal.App.Controllers
{
   [ServiceFilter(typeof(ApiExceptionFilter))]
   [ApiController]
   [Route("api/groups/resources")]
   [Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
   public class GroupsResourcesController : ControllerBase
   {
      private readonly ApplicationSignInManager _signInManager;
      private readonly ApplicationUserManager _userManager;
      private readonly IOptions<IdentityOptions> _identityOptions;
      private readonly IGroupsResourcesManager _groupsResourcesManager;
      private readonly IReportManager _reportManager;
      private readonly IEmailSender _emailSender;
      private readonly IMapper _mapper;
      private readonly ILogger _logger;
      private readonly IDataProtector _protector;

      public GroupsResourcesController(
         ApplicationSignInManager signInManager,
         ApplicationUserManager userManager,
         IOptions<IdentityOptions> identityOptions,
         IGroupsResourcesManager groupsResourcesManager,
         IReportManager reportManager,
         IEmailSender emailSender,
         IMapper mapper,
         ILoggerFactory loggerFactory,
         IDataProtectionProvider provider)
      {
         _userManager = userManager;
         _reportManager = reportManager;
         _emailSender = emailSender;
         _identityOptions = identityOptions;
         _groupsResourcesManager = groupsResourcesManager;
         _mapper = mapper;
         _logger = loggerFactory.CreateLogger<RolesController>();
         _protector = provider.CreateProtector("protect_my_query_string");
      }

      [HttpGet("list")]
      public ActionResult<IList<GroupResourceViewModel>> GetList([FromQuery] PageSettings pageSettings)
      {
         var paging = new RsqlData(pageSettings);
         (List<GroupResource>, long) groups = _groupsResourcesManager.GetGroupsResourcesList(paging);
         paging.TotalRows = groups.Item2;

         return Ok(new
         {
            data = _mapper.Map<List<GroupResourceViewModel>>(groups.Item1),
            paging
         });
      }

      [HttpGet("{id}")]
      public async Task<ActionResult<GroupResourceViewModel>> Get(long id)
      {
         GroupResource groupResource = _groupsResourcesManager.GetGroupResource(id);
         return Ok(_mapper.Map<GroupResourceViewModel>(groupResource));
      }

      [HttpPost]
      public async Task<IActionResult> Create([FromBody] GroupResourceViewModel model)
      {
         GroupResource groupResource = _mapper.Map<GroupResource>(model);

         long? id = _groupsResourcesManager.CreateGroupResource(groupResource);
         if (id.HasValue)
         {
            return Ok(new { id });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpPut]
      public async Task<IActionResult> Update([FromBody] GroupResourceViewModel model)
      {
         GroupResource groupResource = _mapper.Map<GroupResource>(model);

         bool result = _groupsResourcesManager.UpdateGroupResource(groupResource);
         if (result)
         {
            return Ok(new { result });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpDelete("{id}")]
      public async Task<IActionResult> Delete(int id)
      {
         bool result = _groupsResourcesManager.DeleteGroupResource(id);
         if (result)
         {
            return Ok(new { result });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }
   }
}
