﻿using ECal.App.Filters;
using ECal.App.Services.Email;
using ECal.App.Services.Identities;
using ECal.App.Services.Ldap;
using ECal.Common.Helpers;
using ECal.Model;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Negotiate;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json.Linq;
using OpenIddict.Abstractions;
using OpenIddict.Server.AspNetCore;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using static OpenIddict.Abstractions.OpenIddictConstants;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace ECal.App.Controllers
{
   [ServiceFilter(typeof(ApiExceptionFilter))]
   [ApiController]
   [Route("[controller]")]
   public class AuthorizationController : ControllerBase
   {
      private readonly ApplicationSignInManager _signInManager;
      private readonly ApplicationUserManager _userManager;
      private readonly LdapUserManager _ldapUserManager;
      private readonly LdapSignInManager _ldapSignInManager;
      private readonly IOpenIddictScopeManager _scopeManager;
      private readonly IOpenIddictApplicationManager _applicationManager;
      private readonly IOptions<IdentityOptions> _identityOptions;
      private readonly IEmailSender _emailSender;
      private readonly IConfiguration _configuration;
      private readonly ILogger _logger;
      private readonly IDataProtector _protector;

      public AuthorizationController(
         ApplicationSignInManager signInManager,
         ApplicationUserManager userManager,
         LdapUserManager ldapUserManager,
         LdapSignInManager ldapSignInManager,
         IOpenIddictScopeManager scopeManager,
         IOpenIddictApplicationManager applicationManager,
         IOptions<IdentityOptions> identityOptions,
         IEmailSender emailSender,
         IConfiguration configuration,
         ILoggerFactory loggerFactory,
         IDataProtectionProvider provider)
      {
         _signInManager = signInManager;
         _userManager = userManager;
         _ldapSignInManager = ldapSignInManager;
         _ldapUserManager = ldapUserManager;
         _identityOptions = identityOptions;
         _scopeManager = scopeManager;
         _applicationManager = applicationManager;
         _emailSender = emailSender;
         _configuration = configuration;
         _logger = loggerFactory.CreateLogger<AuthorizationController>();
         _protector = provider.CreateProtector("protect_my_query_string");
      }

      private RedirectResult Redirect(
         string type, string message)
      {
         string referer = Request.Headers["Referer"].ToString();
         Uri address = new Uri(referer);

         return Redirect($"{address.Scheme}://{address.Authority}/error?type={type}&message={message}");
      }

      [Authorize(AuthenticationSchemes = NegotiateDefaults.AuthenticationScheme)]
      [HttpGet("~/windows/user")]
      public async Task<IActionResult> GetUser()
      {
         string userName = GetUserName();
         return Ok(new { identityName = User.Identity.Name, userName });
      }

      [Authorize(AuthenticationSchemes = NegotiateDefaults.AuthenticationScheme)]
      [HttpPost("~/windows/token")]
      public async Task<IActionResult> Windows()
      {
         var request = HttpContext.GetOpenIddictServerRequest() ??
            throw new InvalidOperationException("The OpenID Connect request cannot be retrieved.");

         // grant_type = password
         if (request.IsPasswordGrantType())
         {
            // wyodrębnij nazwę użytkownika z nazwy domenowej
            // w zależności od parametru UserIdentityFullName utworzy się pełna nazwa domenowa lub login konta użytkownika
            string userName = GetUserName();
            ApplicationUser user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
               return BadRequest(new ApiError(Errors.InvalidGrant,
                  "invalid_grant.username_or_password_invalid",
                  $"authorization.token.invalid_grant.username_or_password_invalid"
               ));
            }

            var principal = await CreatePrincipalAsync(request, user);
            return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
         }

         // grant_type = refresh token
         if (request.IsRefreshTokenGrantType())
         {
            AuthenticateResult authenticateResult = await HttpContext.AuthenticateAsync(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
            if (!authenticateResult.Succeeded)
            {
            }

            ApplicationUser user = await _userManager.GetUserAsync(authenticateResult.Principal);
            if (user == null)
            {
               return BadRequest(new ApiError(Errors.InvalidGrant,
                  "invalid_grant.refresh_token_no_longer_valid",
                  $"authorization.token.invalid_grant.refresh_token_no_longer_valid"
               ));
            }

            var principal = await CreatePrincipalAsync(request, user);
            return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
         }

         return BadRequest(new ApiError(Errors.UnsupportedGrantType,
            "invalid_grant.specified_grant_type_not_support",
            $"authorization.token.invalid_grant.specified_grant_type_not_support"
         ));
      }

      [AllowAnonymous]
      [HttpPost("~/connect/token")]
      public async Task<IActionResult> Exchange()
      {
         var request = HttpContext.GetOpenIddictServerRequest() ??
            throw new InvalidOperationException("The OpenID Connect request cannot be retrieved.");

         // grant_type = password
         if (request.IsPasswordGrantType())
         {
            ApplicationUser user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
               return BadRequest(new ApiError(Errors.InvalidGrant,
                  "invalid_grant.username_or_password_invalid",
                  $"authorization.token.invalid_grant.username_or_password_invalid"
               ));
            }

            if (_userManager.SupportsUserEmail && !await _userManager.IsEmailConfirmedAsync(user))
            {
               await _userManager.UpdateAsync(user);

               return BadRequest(new ApiError(Errors.InvalidGrant,
                  "invalid_grant.user_email_not_confirmed",
                  $"authorization.token.invalid_grant.user_email_not_confirmed"
               ));
            }

            if (!await _signInManager.CanSignInAsync(user))
            {
               await _userManager.UpdateAsync(user);

               return BadRequest(new ApiError(Errors.InvalidGrant,
                  "invalid_grant.username_cant_login",
                  $"authorization.token.invalid_grant.username_cant_login"
               ));
            }

            if (_userManager.SupportsUserLockout && await _userManager.IsLockedOutAsync(user))
            {
               await _userManager.UpdateAsync(user);

               return BadRequest(new ApiError(Errors.InvalidGrant,
                  "access_denied.user_account_block",
                  $"authorization.token.access_denied.user_account_block"
               ));
            }

            if (!await _userManager.CheckPasswordAsync(user, request.Password))
            {
               // czy wspiera blokowanie bo błędnym logowaniu
               if (_userManager.SupportsUserLockout)
               {
                  // zapisz licznik błędnego logowania do systemu
                  await _userManager.AccessFailedAsync(user);
               }

               await _userManager.UpdateAsync(user);

               return BadRequest(new ApiError(Errors.InvalidGrant,
                  "invalid_grant.username_or_password_invalid",
                  $"authorization.token.invalid_grant.username_or_password_invalid"
               ));
            }

            var result = await _signInManager.CheckPasswordSignInAsync(user, request.Password, lockoutOnFailure: true);
            if (!result.Succeeded)
            {
               // czy wspiera blokowanie bo błędnym logowaniu
               if (_userManager.SupportsUserLockout)
               {
                  // zapisz licznik błędnego logowania do systemu
                  await _userManager.AccessFailedAsync(user);
               }

               await _userManager.UpdateAsync(user);

               return BadRequest(new ApiError(Errors.InvalidGrant,
                  "invalid_grant.username_or_password_invalid",
                  $"authorization.token.invalid_grant.username_or_password_invalid"
               ));
            }

            if (_userManager.SupportsUserTwoFactor && result.RequiresTwoFactor)
            {

            }

            if (_userManager.SupportsUserLockout)
            {
               await _userManager.ResetAccessFailedCountAsync(user);
            }

            await _userManager.UpdateAsync(user);

            var principal = await CreatePrincipalAsync(request, user);
            return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
         }
         // grant_type = client credentials
         else if (request.IsClientCredentialsGrantType())
         {
            var application = await _applicationManager.FindByClientIdAsync(request.ClientId);
            if (application == null)
            {
               return BadRequest(new ApiError(Errors.InvalidClient,
                  "invalid_client.client_invalid",
                  $"authorization.token.invalid_client.client_invalid"
               ));
            }
            var identity = new ClaimsIdentity(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
            identity.AddClaim(Claims.Subject, await _applicationManager.GetClientIdAsync(application), Destinations.AccessToken, Destinations.IdentityToken);
            identity.AddClaim(Claims.Name, await _applicationManager.GetDisplayNameAsync(application), Destinations.AccessToken, Destinations.IdentityToken);

            // wydawanie tokenu na dla zalogowanego użytkownika
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);
            return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
         }
         // grant_type = refresh token
         else if (request.IsRefreshTokenGrantType() || request.IsAuthorizationCodeGrantType())
         {
            // retrieve the claims principal stored in the refresh token.
            AuthenticateResult authenticateResult = await HttpContext.AuthenticateAsync(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
            if (!authenticateResult.Succeeded)
            {

            }

            ApplicationUser user = await _userManager.GetUserAsync(authenticateResult.Principal);
            if (user == null)
            {
               return BadRequest(new ApiError(Errors.InvalidGrant,
                  "invalid_grant.refresh_token_no_longer_valid",
                  $"authorization.token.invalid_grant.refresh_token_no_longer_valid"
               ));
            }

            // ensure the user is still allowed to sign in.
            if (!await _signInManager.CanSignInAsync(user))
            {
               await _userManager.UpdateAsync(user);

               return BadRequest(new ApiError(Errors.InvalidGrant,
                  "invalid_grant.user_no_longer_allowed_sign_in",
                  $"authorization.token.invalid_grant.user_no_longer_allowed_sign_in"
               ));
            }

            await _userManager.UpdateAsync(user);

            var principal = await CreatePrincipalAsync(request, user);
            return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
         }

         return BadRequest(new ApiError(Errors.UnsupportedGrantType,
            "invalid_grant.specified_grant_type_not_support",
            $"authorization.token.invalid_grant.specified_grant_type_not_support"
         ));
      }

      [AllowAnonymous]
      [HttpGet("~/connect/authorize")]
      public async Task<IActionResult> Authorize()
      {
         var request = HttpContext.GetOpenIddictServerRequest() ??
            throw new InvalidOperationException("The OpenID Connect request cannot be retrieved.");

         // pobierz zewnętrznych dostawców logowania
         ExternalLoginInfo externalLoginInfo = await _signInManager.GetExternalLoginInfoAsync();
         if (externalLoginInfo == null)
         {
            OpenIddictParameter? provider = request.GetParameter("provider");
            if (provider.HasValue)
            {
               // request a redirect to the external login provider.
               string returnUrl = Request.PathBase + Request.Path + Request.QueryString;
               AuthenticationProperties properties = _signInManager.ConfigureExternalAuthenticationProperties((string)provider, returnUrl);
               return Challenge(properties, (string)provider);
            }

            // błąd providera
            return Redirect("invalid_grant.provider_invalid",
               $"authorization.token.invalid_grant.provider_invalid");
         }

         // sign in the user with this external login provider if the user already has a login.
         SignInResult signInResult = await _signInManager.ExternalLoginSignInAsync(externalLoginInfo.LoginProvider, externalLoginInfo.ProviderKey, isPersistent: false);
         if (signInResult == SignInResult.Success)
         {
            // weryfikacja istnienia konta w systemie
            ApplicationUser user = await _userManager.FindByLoginAsync(externalLoginInfo.LoginProvider, externalLoginInfo.ProviderKey);
            if (user == null)
            {
               return Redirect("invalid_grant.username_or_password_invalid",
                  $"authorization.token.invalid_grant.username_or_password_invalid");
            }

            // ensure the user is still allowed to sign in.
            if (!await _signInManager.CanSignInAsync(user))
            {
               await _userManager.UpdateAsync(user);

               return Redirect("invalid_grant.user_no_longer_allowed_sign_in",
                  $"authorization.token.invalid_grant.user_no_longer_allowed_sign_in");
            }

            await _userManager.UpdateAsync(user);

            var principal = await CreatePrincipalAsync(request, user);

            // returning a SignInResult will ask OpenIddict to issue the appropriate access/identity tokens.
            return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
         }

         if (signInResult == SignInResult.LockedOut)
         {
            // weryfikacja istnienia konta w systemie
            ApplicationUser user = await _userManager.FindByLoginAsync(externalLoginInfo.LoginProvider, externalLoginInfo.ProviderKey);
            if (user == null)
            {
               return Redirect("invalid_grant.username_or_password_invalid",
                  $"authorization.token.invalid_grant.username_or_password_invalid");
            }

            await _userManager.UpdateAsync(user);

            return Redirect("access_denied.user_account_block",
               $"authorization.token.access_denied.user_account_block");
         }

         if (signInResult == SignInResult.NotAllowed)
         {
            // weryfikacja istnienia konta w systemie
            ApplicationUser user = await _userManager.FindByLoginAsync(externalLoginInfo.LoginProvider, externalLoginInfo.ProviderKey);
            if (user == null)
            {
               return Redirect("invalid_grant.username_or_password_invalid",
                  $"authorization.token.invalid_grant.username_or_password_invalid");
            }

            await _userManager.UpdateAsync(user);

            return Redirect("access_denied.user_not_allowed",
               $"authorization.token.access_denied.user_not_allowed");
         }

         if (signInResult == SignInResult.Failed)
         {
            // odczytanie adresu e-mail z odpowiedniego dostawcy usługi/platformy Oidc
            string emailClaim = externalLoginInfo.Principal.GetClaim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress");
            if (!string.IsNullOrEmpty(emailClaim))
            {
               string firstNameClaim = string.Empty, lastNameClaim = string.Empty;
               switch (externalLoginInfo.LoginProvider)
               {
                  case "Google":
                     firstNameClaim = externalLoginInfo.Principal.GetClaim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname");
                     lastNameClaim = externalLoginInfo.Principal.GetClaim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname");
                     break;

                  default:
                     break;
               }

               // utworzenie konta użytkownika we systemie
               ApplicationUser user = new ApplicationUser
               {
                  UserName = emailClaim,
                  Email = emailClaim,
                  EmailConfirmed = !string.IsNullOrEmpty(emailClaim),
                  //FirstName = firstNameClaim,
                  //LastName = lastNameClaim,
                  //IsActive = true
               };
               IdentityResult userCreateResult = await _userManager.CreateAsync(user);
               if (userCreateResult == IdentityResult.Success)
               {
                  // dodawanie informacji o providerze Oidc
                  IdentityResult userAddLoginResult = await _userManager.AddLoginAsync(user, externalLoginInfo);
                  if (userAddLoginResult == IdentityResult.Success)
                  {
                     // pobranie użytkownika
                     ApplicationUser appUser = await _userManager.FindByEmailAsync(user.Email);
                     if (appUser == null)
                     {
                        return Redirect("invalid_grant.user_not_exists",
                           $"authorization.token.invalid_grant.user_not_exists");
                     }

                     // ensure the user is still allowed to sign in.
                     if (!await _signInManager.CanSignInAsync(user))
                     {
                        await _userManager.UpdateAsync(user);

                        return Redirect("invalid_grant.user_no_longer_allowed_sign_in",
                           $"authorization.token.invalid_grant.user_no_longer_allowed_sign_in");
                     }

                     await _userManager.UpdateAsync(user);

                     var principal = await CreatePrincipalAsync(request, user);

                     // returning a SignInResult will ask OpenIddict to issue the appropriate access/identity tokens.
                     return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
                  }

                  if (userAddLoginResult != IdentityResult.Success)
                  {
                     return Redirect("invalid_grant.invalid_add_user_login",
                        $"authorization.token.invalid_grant.invalid_add_user_login");
                  }
               }

               if (userCreateResult != IdentityResult.Success)
               {
                  return Redirect("invalid_grant.email_already_exists",
                     $"authorization.token.invalid_grant.email_already_exists");
               }
            }
            else
            {
               // błąd zewnętrznego dostawcy Oidc
               return Redirect("invalid_grant.external_account_not_local_account",
                  $"authorization.token.invalid_grant.external_account_not_local_account");
            }
         }

         // błąd serwera autoryzacji Oidc
         return Redirect("server_error.error_authorize",
            $"authorization.token.server_error.error_authorize");
      }

      [AllowAnonymous]
      [HttpPost("~/connect/logout")]
      public async Task<IActionResult> Logout()
      {
         // Ask ASP.NET Core Identity to delete the local and external cookies created
         // when the user agent is redirected from the external identity provider
         // after a successful authentication flow (e.g Google or Facebook).
         await _signInManager.SignOutAsync();

         // Returning a SignOutResult will ask OpenIddict to redirect the user agent
         // to the post_logout_redirect_uri specified by the client application.
         return SignOut(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
      }

      [Authorize(AuthenticationSchemes = OpenIddictServerAspNetCoreDefaults.AuthenticationScheme)]
      [HttpGet("~/connect/userinfo"), Produces("application/json")]
      public async Task<IActionResult> UserInfo()
      {
         var user = await GetCurrentUserAsync();
         if (user is null)
         {
            return BadRequest(new OpenIddictResponse
            {
               Error = Errors.InvalidGrant,
               ErrorDescription = "authorization.token.invalid_grant.user_profile_no_longer_available"
            });
         }

         var claims = new JObject();

         // Note: the "sub" claim is a mandatory claim and must be included in the JSON response.
         claims[Claims.Subject] = await _userManager.GetUserIdAsync(user);
         claims[Claims.Username] = await _userManager.GetUserNameAsync(user);

         if (User.HasScope(Scopes.Email))
         {
            claims[Claims.Email] = await _userManager.GetEmailAsync(user);
            claims[Claims.EmailVerified] = await _userManager.IsEmailConfirmedAsync(user);
         }

         if (User.HasScope(Scopes.Phone))
         {
            claims[Claims.PhoneNumber] = await _userManager.GetPhoneNumberAsync(user);
            claims[Claims.PhoneNumberVerified] = await _userManager.IsPhoneNumberConfirmedAsync(user);
         }

         if (User.HasScope(Scopes.Roles))
         {
            claims[Scopes.Roles] = JArray.FromObject(await _userManager.GetRolesAsync(user));
         }

         // Note: the complete list of standard claims supported by the OpenID Connect specification
         // can be found here: http://openid.net/specs/openid-connect-core-1_0.html#StandardClaims

         return Ok(claims);
      }

      private async Task<ClaimsPrincipal> CreatePrincipalAsync(
         OpenIddictRequest request,
         ApplicationUser user)
      {
         var principal = await _signInManager.CreateUserPrincipalAsync(user);

         principal.SetScopes(request.GetScopes());
         principal.SetResources(await _scopeManager.ListResourcesAsync(principal.GetScopes()).ToListAsync());

         foreach (var claim in principal.Claims)
         {
            claim.SetDestinations(GetDestinations(claim, principal));
         }

         return principal;
      }

      private IEnumerable<string> GetDestinations(Claim claim, ClaimsPrincipal principal)
      {
         // Note: by default, claims are NOT automatically included in the access and identity tokens.
         // To allow OpenIddict to serialize them, you must attach them a destination, that specifies
         // whether they should be included in access tokens, in identity tokens or in both.

         switch (claim.Type)
         {
            case Claims.Name:
               yield return Destinations.AccessToken;

               if (principal.HasScope(Scopes.Profile))
                  yield return Destinations.IdentityToken;

               yield break;

            case Claims.Email:
               yield return Destinations.AccessToken;

               if (principal.HasScope(Scopes.Email))
                  yield return Destinations.IdentityToken;

               yield break;

            case Claims.Role:
               yield return Destinations.AccessToken;

               if (principal.HasScope(Scopes.Roles))
                  yield return Destinations.IdentityToken;

               yield break;

            // Never include the security stamp in the access and identity tokens, as it's a secret value.
            case "AspNet.Identity.SecurityStamp": yield break;

            default:
               yield return Destinations.AccessToken;
               yield break;
         }
      }

      private void AddErrors(
         IdentityResult result)
      {
         foreach (var error in result.Errors)
         {
            ModelState.AddModelError(error.Code, error.Description);
         }
      }

      private async Task<ApplicationUser> GetCurrentUserAsync()
      {
         return await _userManager.GetUserAsync(User);
      }

      private string GetClientAddress()
      {
         if (HttpContext.Request.Headers.TryGetValue("X-Forwarded-For", out StringValues value))
         {
            return value.ToString();
         }

         return HttpContext.Connection.RemoteIpAddress.ToString();
      }

      private string GetUserName()
      {
         string name = User.Identity.Name;

         // weryfikacja czy używać pełnej nazwy domenowej użytkownika
         bool isUserIdentityFullName = _configuration.GetValue<bool>("UserIdentityFullName");
         if (!isUserIdentityFullName)
         {
            if (name.Contains('\\'))
            {
               name = name[(name.IndexOf('\\') + 1)..];
            }

            if (name.Contains('@'))
            {
               name = name.Substring(0, name.IndexOf('@'));
            }
         }

         return name.ToLower();
      }
   }
}
