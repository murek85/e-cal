﻿using AutoMapper;
using ECal.App.Filters;
using ECal.App.Services.Email;
using ECal.App.Services.Identities;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ECal.App.Controllers
{
	[ServiceFilter(typeof(ApiExceptionFilter))]
	[ApiController]
	[Route("api/[controller]")]
	public class TestController : ControllerBase
	{
		private readonly ApplicationSignInManager _signInManager;
		private readonly ApplicationUserManager _userManager;
		private readonly IOptions<IdentityOptions> _identityOptions;
		private readonly IEmailSender _emailSender;
		private readonly IMapper _mapper;
		private readonly ILogger _logger;
		private readonly IDataProtector _protector;

		public TestController(
			ApplicationSignInManager signInManager,
			ApplicationUserManager userManager,
			IOptions<IdentityOptions> identityOptions,
			IEmailSender emailSender,
			IMapper mapper,
			ILoggerFactory loggerFactory,
			IDataProtectionProvider provider)
		{
			_userManager = userManager;
			_emailSender = emailSender;
			_identityOptions = identityOptions;
			_mapper = mapper;
			_logger = loggerFactory.CreateLogger<ApplicationsController>();
			_protector = provider.CreateProtector("protect_my_query_string");
		}

		[HttpGet("names")]
		public ActionResult Get()
		{
			return Ok();
		}
	}
}
