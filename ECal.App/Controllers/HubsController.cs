﻿using ECal.App.Hubs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace ECal.App.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class HubsController : ControllerBase
	{
		private IHubContext<BroadcastHub> _broadcastHub;

		public HubsController(
			IHubContext<Hubs.BroadcastHub> broadcastHub)
		{
			_broadcastHub = broadcastHub;
		}

		[HttpGet("notifications")]
		public async Task Nnotifications(string message)
		{
			await _broadcastHub.Clients.All.SendAsync("notifications", message);
		}

		[HttpGet("dashboards")]
		public async Task Dashboards(string message)
		{
			await _broadcastHub.Clients.All.SendAsync("dashboards", message);
		}

		[HttpGet("users")]
		public async Task Users(string message)
		{
			await _broadcastHub.Clients.All.SendAsync("users", message);
		}

		[HttpGet("roles")]
		public async Task Roles(string message)
		{
			await _broadcastHub.Clients.All.SendAsync("roles", message);
		}

		[HttpGet("workstations")]
		public async Task Workstations(string message)
		{
			await _broadcastHub.Clients.All.SendAsync("workstations", message);
		}
	}
}
