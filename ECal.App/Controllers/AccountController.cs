﻿using AutoMapper;
using ECal.App.Filters;
using ECal.App.Services.Email;
using ECal.App.Services.Identities;
using ECal.Business.Common;
using ECal.Model;
using ECal.Model.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenIddict.Validation.AspNetCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using DB = ECal.Model.DB;

namespace ECal.App.Controllers
{
   [ServiceFilter(typeof(ApiExceptionFilter))]
   [ApiController]
   [Route("api/[controller]")]
   public class AccountController : ControllerBase
   {
      private readonly ApplicationSignInManager _signInManager;
      private readonly ApplicationUserManager _userManager;
      private readonly IOptions<IdentityOptions> _identityOptions;
      private readonly IUsersManager _usersManager;
      private readonly IEmailSender _emailSender;
      private readonly IMapper _mapper;
      private readonly ILogger _logger;
      private readonly IDataProtector _protector;

      public AccountController(
         ApplicationSignInManager signInManager,
         ApplicationUserManager userManager,
         IOptions<IdentityOptions> identityOptions,
         IUsersManager usersManager,
         IEmailSender emailSender,
         IMapper mapper,
         ILoggerFactory loggerFactory,
         IDataProtectionProvider provider)
      {
         _signInManager = signInManager;
         _userManager = userManager;
         _emailSender = emailSender;
         _identityOptions = identityOptions;
         _usersManager = usersManager;
         _mapper = mapper;
         _logger = loggerFactory.CreateLogger<AccountController>();
         _protector = provider.CreateProtector("protect_my_query_string");
      }

      [AllowAnonymous]
      [HttpPost("register")]
      public async Task<IActionResult> Register(
         [FromBody] RegisterViewModel model)
      {
         ApplicationUser user = new ApplicationUser
         {
            UserName = model.UserName,
            Email = model.Email
         };
         IdentityResult result = await _userManager.CreateAsync(user, model.NewPassword);
         if (result.Succeeded)
         {
            ApplicationUser appUser = await _userManager.FindByNameAsync(user.UserName);
            if (appUser == null)
            {
               return BadRequest(new ApiError(
                  $"register.error.user_invalid"
               ));
            }

            var token = await _userManager.GenerateEmailConfirmationTokenAsync(appUser);
            if (String.IsNullOrEmpty(token))
            {
               return BadRequest(new ApiError(
                  $"register.error.verify_reset_token_invalid"
               ));
            }

            string referer = Request.Headers["Referer"].ToString();
            Uri address = new Uri(referer);
            string originUrl = $"{address.Scheme}://{address.Authority}";
            EmailSenderModel emailSenderModel = new EmailSenderModel
            {
               MailType = MailType.RegisterAccount,
               To = appUser.Email,
               UserId = appUser.Id,
               Token = WebUtility.UrlEncode(token),
               OriginUrl = originUrl
            };

            if (!await _emailSender.SendEmailAsync(emailSenderModel))
            {
               return BadRequest(new ApiError(
                  $"register.error.problem_send_email"
               ));
            }

            return Ok(new { appUser.Id });
         }

         AddErrors(result);
         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpPost("password/reminder")]
      [AllowAnonymous]
      public async Task<IActionResult> ReminderPassword(
         [FromBody] ReminderPasswordViewModel model)
      {
         ApplicationUser appUser = await _userManager.FindByNameAsync(model.Email);
         if (appUser == null || !await _userManager.IsEmailConfirmedAsync(appUser))
         {
            return NoContent();
         }

         IList<UserLoginInfo> userLogins = await _userManager.GetLoginsAsync(appUser);
         if (userLogins.Count > 0)
         {
            return NoContent();
         }

         string token = await _userManager.GeneratePasswordResetTokenAsync(appUser);
         if (String.IsNullOrEmpty(token))
         {
            return BadRequest(new ApiError(
               $"password.reminder.error.generate_reset_token_invalid"
            ));
         }

         string referer = Request.Headers["Referer"].ToString();
         Uri address = new Uri(referer);
         string originUrl = $"{address.Scheme}://{address.Authority}";
         EmailSenderModel emailSenderModel = new EmailSenderModel
         {
            MailType = MailType.ReminderPassword,
            To = appUser.Email,
            UserId = appUser.Id,
            Token = WebUtility.UrlEncode(token),
            OriginUrl = originUrl
         };

         if (!await _emailSender.SendEmailAsync(emailSenderModel))
         {
            return BadRequest(new ApiError(
               $"password.reminder.error.problem_send_email"
            ));
         }

         return NoContent();
      }

      [HttpPost("password/confirm")]
      [AllowAnonymous]
      public async Task<IActionResult> ConfirmPassword(
         [FromBody] ConfirmPasswordViewModel model,
         [FromHeader] string userId,
         [FromHeader] string token)
      {
         if (String.IsNullOrEmpty(userId) || String.IsNullOrEmpty(token))
         {
            return BadRequest(new ApiError(
               $"password.confirm.error.parameters_invalid"
            ));
         }

         ApplicationUser appUser = await _userManager.FindByIdAsync(userId);
         if (appUser == null)
         {
            return Ok();
         }

         if (token.Contains('%'))
         {
            token = WebUtility.UrlDecode(token);
         }

         if (!await _userManager.VerifyResetPasswordTokenAsync(appUser, token))
         {
            return BadRequest(new ApiError(
               $"password.confirm.error.verify_token_invalid"
            ));
         }

         IdentityResult result = await _userManager.ResetPasswordAsync(appUser, token, model.NewPassword);
         if (result.Succeeded)
         {

            string referer = Request.Headers["Referer"].ToString();
            Uri address = new Uri(referer);
            string originUrl = $"{address.Scheme}://{address.Authority}";
            EmailSenderModel emailSenderModel = new EmailSenderModel
            {
               MailType = MailType.ConfirmPassword,
               To = appUser.Email,
               UserId = appUser.Id,
               OriginUrl = originUrl
            };

            if (!await _emailSender.SendEmailAsync(emailSenderModel))
            {
               return BadRequest(new ApiError(
                  $"password.confirm.error.problem_send_email"
               ));
            }

            return Ok(result.Succeeded);
         }

         AddErrors(result);
         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpPost("email/confirm")]
      [AllowAnonymous]
      public async Task<IActionResult> ConfirmEmail(
         [FromHeader] string userId,
         [FromHeader] string token)
      {
         if (String.IsNullOrEmpty(userId) || String.IsNullOrEmpty(token))
         {
            return BadRequest(new ApiError(
               $"email.confirm.error.parameters_invalid"
            ));
         }

         ApplicationUser appUser = await _userManager.FindByIdAsync(userId.ToString());
         if (appUser == null)
         {
            return NoContent();
         }

         if (token.Contains('%'))
         {
            token = WebUtility.UrlDecode(token);
         }

         if (!await _userManager.VerifyEmailConfirmationTokenAsync(appUser, token))
         {
            return BadRequest(new ApiError(
               $"email.confirm.error.verify_token_invalid"
            ));
         }

         if (await _userManager.IsEmailConfirmedAsync(appUser))
         {
            return BadRequest(new ApiError(
               $"email.confirm.error.account_email_verified"
            ));
         }

         IdentityResult result = await _userManager.ConfirmEmailAsync(appUser, token);
         if (result.Succeeded)
         {
            return Ok(result.Succeeded);
         }

         AddErrors(result);
         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpPost("login2step/activate")]
      [AllowAnonymous]
      public async Task<IActionResult> Login2Step(string email)
      {
         var appUser = await _userManager.FindByEmailAsync(email);
         if (appUser == null || !await _userManager.IsEmailConfirmedAsync(appUser))
         {
            return NoContent();
         }

         var providers = await _userManager.GetValidTwoFactorProvidersAsync(appUser);


         var token = await _userManager.GenerateTwoFactorTokenAsync(appUser, "Email");
         if (String.IsNullOrEmpty(token))
         {
            return BadRequest(new ApiError(
               $"login2step.activate.error.generate_twofactor_token_invalid"
            ));
         }

         string referer = Request.Headers["Referer"].ToString();
         Uri address = new Uri(referer);
         string originUrl = $"{address.Scheme}://{address.Authority}";
         EmailSenderModel emailSenderModel = new EmailSenderModel
         {
            MailType = MailType.TwoFactorToken,
            To = appUser.Email,
            UserId = appUser.Id,
            Token = WebUtility.UrlEncode(token),
            OriginUrl = originUrl
         };

         if (!await _emailSender.SendEmailAsync(emailSenderModel))
         {
            return BadRequest(new ApiError(
               $"login2step.activate.error.problem_send_email"
            ));
         }

         return NoContent();
      }

      [HttpPost("login2step/confirm")]
      [AllowAnonymous]
      public async Task<IActionResult> Login2Step([FromHeader] string userId, [FromHeader] string token)
      {
         if (String.IsNullOrEmpty(token))
         {
            return BadRequest(new ApiError(
               $"login2step.confirm.error.parameters_invalid"
            ));
         }

         ApplicationUser appUser = await _userManager.FindByIdAsync(userId);
         if (appUser == null)
         {
            return NoContent();
         }

         if (!await _userManager.VerifyTwoFactorTokenAsync(appUser, "Email", token))
         {
            return BadRequest(new ApiError(
               $"login2step.confirm.error.verify_token_invalid"
            ));
         }

         var result = await _signInManager.TwoFactorSignInAsync("Email", token, false, false);
         if (result.Succeeded)
         {
            return Ok(result.Succeeded);
         }

         return BadRequest(new ApiError(
            $"login2step.confirm.error.problem_verify_token"
         ));
      }

      [HttpPost("logout")]
      public async Task<IActionResult> Logout()
      {
         await _signInManager.SignOutAsync();
         return NoContent();
      }

      [Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
      [HttpGet("~/api/user/current")]
      [HttpGet("user")]
      public async Task<ActionResult<UserViewModel>> GetUserLogged()
      {
         ApplicationUser currentUser = await GetCurrentUserAsync();
         DB.User user = _usersManager.GetUser(currentUser.Id);

         IList<UserLoginInfo> userLogins = await _userManager.GetLoginsAsync(currentUser);
         user.Logins = userLogins.Select(login => login.LoginProvider).ToList();

         return Ok(_mapper.Map<UserViewModel>(user));
      }

      [Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
      [HttpPut("~/api/user/current")]
      [HttpPut("user")]
      public async Task<ActionResult<UserViewModel>> UpdateUserLogged(UserViewModel model)
      {
         return Ok();
      }

      [Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
      [HttpGet("~/api/user/permissions")]
      [HttpGet("user/permissions")]
      public async Task<ActionResult> GetPermissionsUserLogged()
      {
		 IList<string> permissions = new List<string>();
	     return Ok(permissions);
	  }

      [HttpGet]
      [Route("~/api/status")]
      public IActionResult Status() => Ok(true);

      [HttpGet]
      [Route("~/api/machine")]
      public ActionResult<string> GetMachineName() => Environment.MachineName;

      [HttpGet]
      [Route("~/api/version")]
      public ActionResult<string> GetVersion() => Assembly.GetEntryAssembly().GetName().Version.ToString();

      private void AddErrors(
         IdentityResult result)
      {
         foreach (var error in result.Errors)
         {
            ModelState.AddModelError(error.Code, error.Description);
         }
      }

      private async Task<ApplicationUser> GetCurrentUserAsync()
      {
         return await _userManager.GetUserAsync(User);
      }
   }
}
