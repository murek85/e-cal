﻿using AutoMapper;
using ECal.App.Filters;
using ECal.App.Services.Email;
using ECal.App.Services.Identities;
using ECal.Business.Common;
using ECal.Common;
using ECal.Model;
using ECal.Model.DB;
using ECal.Model.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using OpenIddict.Validation.AspNetCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECal.App.Controllers
{
   [ServiceFilter(typeof(ApiExceptionFilter))]
   [ApiController]
   [Route("api/[controller]")]
   [Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
   public class RolesController : ControllerBase
   {
      private readonly ApplicationSignInManager _signInManager;
      private readonly ApplicationUserManager _userManager;
      private readonly IOptions<IdentityOptions> _identityOptions;
      private readonly IRolesManager _rolesManager;
      private readonly IReportManager _reportManager;
      private readonly IEmailSender _emailSender;
      private readonly IMapper _mapper;
      private readonly ILogger _logger;
      private readonly IDataProtector _protector;

      public RolesController(
         ApplicationSignInManager signInManager,
         ApplicationUserManager userManager,
         IOptions<IdentityOptions> identityOptions,
         IRolesManager rolesManager,
         IReportManager reportManager,
         IEmailSender emailSender,
         IMapper mapper,
         ILoggerFactory loggerFactory,
         IDataProtectionProvider provider)
      {
         _userManager = userManager;
         _reportManager = reportManager;
         _emailSender = emailSender;
         _identityOptions = identityOptions;
         _rolesManager = rolesManager;
         _mapper = mapper;
         _logger = loggerFactory.CreateLogger<RolesController>();
         _protector = provider.CreateProtector("protect_my_query_string");
      }

      [HttpGet("list")]
      public ActionResult<IList<RoleViewModel>> GetList([FromQuery] PageSettings pageSettings)
      {
         var paging = new RsqlData(pageSettings);
         (List<Role>, long) roles = _rolesManager.GetRolesList(paging);
         paging.TotalRows = roles.Item2;

         return Ok(new
         {
            data = _mapper.Map<List<RoleViewModel>>(roles.Item1),
            paging
         });
      }

      [HttpGet("{id}")]
      public async Task<ActionResult<RoleViewModel>> Get(long id)
      {
         Role role = _rolesManager.GetRole(id);
         return Ok(_mapper.Map<RoleViewModel>(role));
      }

      [HttpPost]
      public async Task<IActionResult> Create([FromBody] RoleViewModel model)
      {
         Role role = _mapper.Map<Role>(model);

         long? id = _rolesManager.CreateRole(role);
         if (id.HasValue)
         {
            return Ok(new { id });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpPut]
      public async Task<IActionResult> Update([FromBody] RoleViewModel model)
      {
         Role role = _mapper.Map<Role>(model);

         bool result = _rolesManager.UpdateRole(role);
         if (result)
         {
            return Ok(new { result });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpDelete("{id}")]
      public async Task<IActionResult> Delete(int id)
      {
         bool result = _rolesManager.DeleteRole(id);
         if (result)
         {
            return Ok(new { result });
         }

         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpGet("{id}/assigning")]
      public async Task<ActionResult<bool>> VerifyAssigningRoleToUser(long id)
      {
         var currentUser = await GetCurrentUserAsync();
         return Ok(_rolesManager.VerifyAssigningRoleToUser(currentUser.Id, id));
      }

      private async Task<ApplicationUser> GetCurrentUserAsync()
      {
         return await _userManager.GetUserAsync(User);
      }
   }
}
