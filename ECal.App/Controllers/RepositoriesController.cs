﻿using AutoMapper;
using ECal.App.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OpenIddict.Validation.AspNetCore;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ECal.App.Controllers
{
	[ServiceFilter(typeof(ApiExceptionFilter))]
	[ApiController]
	[Route("api/repositories")]
	public class RepositoriesController : ControllerBase
	{
		private readonly IMapper _mapper;
		private readonly ILogger _logger;
		private readonly IDataProtector _protector;
		public RepositoriesController(
			IMapper mapper,
			ILoggerFactory loggerFactory,
			IDataProtectionProvider provider)
		{
			_mapper = mapper;
			_logger = loggerFactory.CreateLogger<RepositoriesController>();
			_protector = provider.CreateProtector("protect_my_query_string");
		}

		[HttpPost]
		[RequestFormLimits(MultipartBodyLengthLimit = 52428800000)]
		[RequestSizeLimit(52428800000)]
		public async Task<IActionResult> Upload()
		{
			string methodName = nameof(Upload);

			string filePath = String.Empty;
			string outputDir = @$"\\{Environment.MachineName}\Instalki";

			DateTime dateProcess = DateTime.Now;

			//string guidDir = Guid.NewGuid().ToString();

			//string directoryPath = Path.Combine(outputDir,
			//	dateProcess.Year.ToString(), dateProcess.Month.ToString(), dateProcess.Day.ToString(),
			//	guidDir);

			string directoryPath = Path.Combine(outputDir);

			if (!Directory.Exists(directoryPath))
				Directory.CreateDirectory(directoryPath);

			IFormFileCollection files = Request.Form.Files;
			foreach (IFormFile file in files)
			{
				_logger.LogDebug($"[{methodName}] Plik: {file.FileName} [{file.Length / 1024} KB].");
				filePath = Path.Combine(directoryPath, file.FileName);
				using var fileStream = new FileStream(filePath, FileMode.Create);
				file.CopyTo(fileStream);
			}

			return Ok(new { progress = 100 });
		}
	}
}
