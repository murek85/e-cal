﻿using AutoMapper;
using ECal.App.Filters;
using ECal.App.Services.Email;
using ECal.App.Services.Identities;
using ECal.Business.Common;
using ECal.Common;
using ECal.Model;
using ECal.Model.DB;
using ECal.Model.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenIddict.Validation.AspNetCore;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace ECal.App.Controllers
{
   [ServiceFilter(typeof(ApiExceptionFilter))]
   [ApiController]
   [Route("api/[controller]")]
   [Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
   public class UsersController : ControllerBase
   {
      private readonly ApplicationSignInManager _signInManager;
      private readonly ApplicationUserManager _userManager;
      private readonly IOptions<IdentityOptions> _identityOptions;
      private readonly IUsersManager _usersManager;
      private readonly IReportManager _reportManager;
      private readonly IEmailSender _emailSender;
      private readonly IMapper _mapper;
      private readonly ILogger _logger;
      private readonly IDataProtector _protector;

      public UsersController(
         ApplicationSignInManager signInManager,
         ApplicationUserManager userManager,
         IOptions<IdentityOptions> identityOptions,
         IUsersManager usersManager,
         IReportManager reportManager,
         IEmailSender emailSender,
         IMapper mapper,
         ILoggerFactory loggerFactory,
         IDataProtectionProvider provider)
      {
         _userManager = userManager;
         _reportManager = reportManager;
         _emailSender = emailSender;
         _identityOptions = identityOptions;
         _usersManager = usersManager;
         _mapper = mapper;
         _logger = loggerFactory.CreateLogger<UsersController>();
         _protector = provider.CreateProtector("protect_my_query_string");
      }

      [HttpGet("report")]
      public ActionResult GetReport([FromQuery] PageSettings pageSettings, [FromQuery] ReportBuilder reportBuilder)
      {
         var paging = new RsqlData(pageSettings.Query, pageSettings.Sort);
         (List<User>, long) users = _usersManager.GetUsersReport(paging);

         var bytes = _reportManager.ExportData<UserViewModel>(reportBuilder, _mapper.Map<List<UserViewModel>>(users.Item1));
         return File(bytes, "application/octet-stream");
      }

      [HttpGet("list")]
      public ActionResult<IList<UserViewModel>> GetList([FromQuery] PageSettings pageSettings)
      {
         var paging = new RsqlData(pageSettings);
         (List<User>, long) users = _usersManager.GetUsersList(paging);
         paging.TotalRows = users.Item2;

         return Ok(new
         {
            data = _mapper.Map<List<UserViewModel>>(users.Item1),
            paging
         });
      }

      [HttpGet("{id}")]
      public async Task<ActionResult<UserViewModel>> Get(Guid id)
      {
         User user = _usersManager.GetUser(id);
         return Ok(_mapper.Map<UserViewModel>(user));
      }

      [HttpPost]
      public async Task<IActionResult> Create([FromBody] UserViewModel model)
      {
         ApplicationUser user = new ApplicationUser
         {
            UserName = model.UserName,
            Email = model.Email
         };

         IdentityResult result = await _userManager.CreateAsync(user);
         if (result.Succeeded)
         {
            string token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            if (String.IsNullOrEmpty(token))
            {
               return BadRequest(new ApiError(
                  $"account.register.verify_reset_token_invalid"
               ));
            }

            string originUrl = $"{Request.Scheme}://{Request.Host.Host}:{Request.Host.Port}";
            EmailSenderModel emailSenderModel = new EmailSenderModel
            {
               MailType = MailType.RegisterAccount,
               To = user.Email,
               UserId = user.Id,
               Token = WebUtility.UrlEncode(token),
               OriginUrl = originUrl
            };

            if (!await _emailSender.SendEmailAsync(emailSenderModel))
            {
               return BadRequest(new ApiError(
                  $"account.register.problem_send_email"
               ));
            }

            return Ok(new { user.Id });
         }

         AddErrors(result);
         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpPut]
      public async Task<IActionResult> Update([FromBody] UserViewModel model)
      {
         ApplicationUser user = await _userManager.FindByNameAsync(model.Email);
         if (user == null)
         {
            return BadRequest(new ApiError(
               $"account.manage.user_invalid"
            ));
         }

         user.PhoneNumber = model.PhoneNumber;

         IdentityResult result = await _userManager.UpdateAsync(user);
         if (result.Succeeded)
         {
            return Ok(new { user.Id });
         }

         AddErrors(result);
         return BadRequest(
            new ApiError(ModelState)
         );
      }

      [HttpDelete("{id}")]
      public async Task<IActionResult> Delete(Guid aspId)
      {
         return NoContent();
      }

      private void AddErrors(
         IdentityResult result)
      {
         foreach (var error in result.Errors)
         {
            ModelState.AddModelError(error.Code, error.Description);
         }
      }

      private async Task<ApplicationUser> GetCurrentUserAsync()
      {
         return await _userManager.GetUserAsync(User);
      }
   }
}
