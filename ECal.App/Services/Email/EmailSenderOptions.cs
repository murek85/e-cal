﻿namespace ECal.App.Services.Email
{
	public class EmailSenderOptions
	{
		public string Server { get; set; } = string.Empty;
		public int SmtpPort { get; set; }
		public int ImapPort { get; set; }
		public string User { get; set; } = string.Empty;
		public string Password { get; set; } = string.Empty;
		public bool UseSsl { get; set; } = false;
		public bool RequiresAuthentication { get; set; } = false;
		public string PreferredEncoding { get; set; } = string.Empty;
		public string FromName { get; set; }
		public string FromAddress { get; set; }
		public string ReplyToAddress { get; set; }
	}
}
