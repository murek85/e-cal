﻿using System;

namespace ECal.App.Services.Email
{
	public class EmailSenderModel
	{
		public MailType MailType { get; set; }
		public string From { get; set; }
		public string To { get; set; }
		public string Subject { get; set; }
		public string HtmlBody { get; set; }
		public string TextBody { get; set; }
		public Guid UserId { get; set; }
		public string Token { get; set; }
		public string OriginUrl { get; set; }
	}
}
