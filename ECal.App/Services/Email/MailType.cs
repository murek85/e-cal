﻿namespace ECal.App.Services.Email
{
   public enum MailType
   {
      RegisterAccount,
      ReminderPassword,
      ConfirmPassword,
      ReportBug,
      TwoFactorToken
   }
}
