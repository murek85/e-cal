﻿using System.Threading.Tasks;

namespace ECal.App.Services.Email
{
	public interface IEmailSender
	{
		Task<bool> SendEmailAsync(EmailSenderModel model);
	}
}
