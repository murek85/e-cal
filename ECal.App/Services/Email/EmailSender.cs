﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Utils;
using System;
using System.Threading.Tasks;

namespace ECal.App.Services.Email
{
   public class EmailSender : IEmailSender
   {
      private readonly IOptions<EmailSenderOptions> _options;

      public EmailSender(
         IOptions<EmailSenderOptions> options)
      {
         _options = options;
      }

      public async Task<bool> SendEmailAsync(
         EmailSenderModel model)
      {
         try
         {
            PrepareEmailSenderModel(ref model);

            var mailMessage = new MimeMessage
            {
               MessageId = MimeUtils.GenerateMessageId()
            };
            mailMessage.From.Add(new MailboxAddress(_options.Value.FromName, _options.Value.FromAddress));

            if (!string.IsNullOrEmpty(_options.Value.ReplyToAddress))
            {
               mailMessage.ReplyTo.Add(new MailboxAddress(_options.Value.FromName, _options.Value.ReplyToAddress));
            }

            mailMessage.To.Add(new MailboxAddress(model.To, model.To));
            mailMessage.Subject = model.Subject;

            var builder = new BodyBuilder()
            {
               HtmlBody = model.HtmlBody
            };
            mailMessage.Body = builder.ToMessageBody();

            using (var client = new SmtpClient())
            {
               // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
               client.ServerCertificateValidationCallback = (s, c, h, e) => true;

               await client
                  .ConnectAsync(_options.Value.Server, _options.Value.SmtpPort, _options.Value.UseSsl)
                  .ConfigureAwait(false);

               if (_options.Value.RequiresAuthentication)
               {
                  await client.AuthenticateAsync(_options.Value.User, _options.Value.Password);
               }

               await client.SendAsync(mailMessage);
               await client.DisconnectAsync(true)
                  .ConfigureAwait(false);

               return true;
            }
         }
         catch (Exception ex)
         {
            return false;
         }
      }

      private void PrepareEmailSenderModel(ref EmailSenderModel model)
      {
         string appName = "Identity Server";
         switch (model.MailType)
         {
            case MailType.RegisterAccount:
               {
                  model.Subject = "Potwierdzenie rejestracji konta";
                  model.TextBody = $@"<p>Witaj,</p>
                                 <p>Potwierdzenie adresu e-mail {model.To} w aplikacji pozwoli na aktywowanie i
                                 zalogowanie na konto użytkownika.
                                 Aby kontynuować proces potwierdzenia, proszę kliknij w link poniżej:</p>
                                 <p><a href=""{model.OriginUrl}/email/confirm?userId={model.UserId}&token={model.Token}"">Kliknij tu, aby potwierdzić adres e-mail</a></p>
                                 <p>Jeśli masz pytanie i/lub potrzebujesz pomocy, skontaktuj się z działem obsługi pomocy.</p>";
                  model.HtmlBody = FormatMessage(model.Subject, model.TextBody, model.OriginUrl, appName, model.OriginUrl);
                  break;
               }
            case MailType.ReminderPassword:
               {
                  model.Subject = "Przypomnienie hasła";
                  model.TextBody = $@"<p>Witaj, czy chcesz zresetować swoje hasło?</p>
                                 <p>Ktoś zażądał zresetowania hasła Twojego konta w aplikacji.
                                 Jeśli to nie byłeś Ty, proszę zignoruj tą wiadomość - nie zostaną wtedy dokonane żadne zmiany na Twoim koncie.
                                 Jeśli jednak zażądałeś zresetowania hasła, proszę kliknij w link poniżej:</p>
                                 <p><a href=""{model.OriginUrl}/password/confirm?userId={model.UserId}&token={model.Token}"">Kliknij tu, aby zresetować hasło</a></p>
                                 <p>Zostaniesz przekierowany do formularza resetowania hasła na stronie.</p>
                                 <p>Jeśli masz pytanie i/lub potrzebujesz pomocy w zresetowaniu hasła, skontaktuj się z działem obsługi pomocy.</p>";
                  model.HtmlBody = FormatMessage(model.Subject, model.TextBody, model.OriginUrl, appName, model.OriginUrl);
                  break;
               }
            case MailType.ConfirmPassword:
               {
                  model.Subject = "Potwierdzenie zmiany hasła";
                  model.TextBody = $@"<p>Witaj!</p>
                                 <p>Hasło do Twojego konta zostało pomyślnie zmienione.
                                 Jeśli to nie byłeś Ty, proszę skontaktuj się z działem obsługi pomocy.</p>
                                 <p>Jeśli masz pytanie i/lub potrzebujesz pomocy w zresetowaniu hasła, skontaktuj się z działem obsługi pomocy.</p>";
                  model.HtmlBody = FormatMessage(model.Subject, model.TextBody, model.OriginUrl, appName, model.OriginUrl);
                  break;
               }
            case MailType.ReportBug:
               {
                  model.Subject = "Zgłoszenie problemu";
                  model.HtmlBody = FormatMessage(model.Subject, model.TextBody, model.OriginUrl, appName, model.OriginUrl);
                  break;
               }

            case MailType.TwoFactorToken:
               {
                  model.Subject = "Weryfikacja konta";
                  model.TextBody = $@"<p>Witaj,</p>
                                 <p>Kod:</p>
                                 <p>{model.Token}</p>
                                 <p>Aby kontynuować proces weryfikacji konta, proszę kliknij w link poniżej:</p>
                                 <p><a href=""{model.OriginUrl}/code?userId={model.UserId}"">Kliknij tu, aby zweryfikować konto</a></p>
                                 <p>Jeśli masz pytanie i/lub potrzebujesz pomocy, skontaktuj się z działem obsługi pomocy.</p>";
                  model.HtmlBody = FormatMessage(model.Subject, model.TextBody, model.OriginUrl, appName, model.OriginUrl);
                  break;
               }
         }
      }

      private string FormatMessage(string title, string text, string hostName, string appName,
         string hostLink = "#", string regulationsLink = "#", string policyLink = "#", string cookieLink = "#") =>
         $@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd"">
         <html>
         <head>
            <meta http-equiv=""content-type"" content=""text/html; charset=utf-8"">
         </head>
         <body style=""margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0;"">
         <!-- WRAPPER-->
         <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""640"" style=""border:none; display:block; width:640px;"">
            <!-- TOP-->
            <tr>
               <td height=""10"" style=""height:10px;""></td>
            </tr>
            <tr>
               <td bgcolor=""#ffffff"" width=""640"" style=""width:640px;"">
                  <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""640"" style=""border:none; display:block; width:640px;"">
                     <tr>
                        <td height=""18"" style=""height:18px;""></td>
                     </tr>
                  </table>
               </td>
            </tr>
            <tr>
               <td bgcolor=""#ffffff"" width=""640"" style=""width:640px;border-bottom: 1px solid #E0E0E0"">
                  <table align=""center"" bgcolor=""#ffffff"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""640"" style=""border:none; display:block; width:640px;"">
                     <tr>
                        <td bgcolor=""#ffffff"" width=""640"" style=""width:640px;"">
                           <table align=""center"" bgcolor=""#ffffff"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""640"" style=""border:none; width:640px;"">
                              <tr>
                                 <td width=""340"" height=""60"" style=""color:#acd037; font-family:Arial, Helvetica, sans-serif; font-size:18px; text-align:left; width:340px"">
                                    <a href=""{hostLink}"" style=""color:#707177; text-decoration:none;"" target=""_blank"" title=""{appName}"">
                                       {appName}
                                    </a>
                                 </td>
                                 <td width=""300""></td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
            <tr>
               <td height=""20"" style=""height:20px;""></td>
            </tr>
            <!-- END OF TOP-->
            <!-- CONTENT-->
            <tr>
               <td bgcolor=""#ffffff"" width=""640"" style=""width:640px;"">
                  <table bgcolor=""#ffffff"" align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""640"" style=""display:block; border:none; width:640px;"">
                     <tr>
                        <td bgcolor=""#ffffff"" width=""640"" style=""width:640px;"">
                           <table align=""center"" bgcolor=""#ffffff"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""640"" style=""border:none; width:640px;"">
                              <tr>
                                 <td height=""14"" style=""height:14px;""></td>
                              </tr>
                              <tr>
                                 <td align=""left"" width=""640"" style=""color:#707177; font-family:Arial, Helvetica, sans-serif; font-size:18px; text-align:left; width:640px;"">
                                    <span style=""color:#707177; font-size:18px; font-family:Arial, Helvetica, sans-serif; text-align:left;"">
                                       {title}
                                    </span>
                                 </td>
                              </tr>
                              <tr>
                                 <td height=""22"" style=""height:22px;""></td>
                              </tr>
                              <tr>
                                 <td align=""left"" width=""640"" style=""color:#707177; font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:left; width:640px;"">
                                    <span style=""color:#707177; font-size:14px; font-family:Arial, Helvetica, sans-serif; text-align:left;"">
                                       {text}
                                    </span>
                                 </td>
                              </tr>
                              <tr>
                                 <td height=""46"" style=""height:46px;""></td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
            <!-- END OF CONTENT-->
            <!-- FOOTER -->
            <tr>
               <td width=""640"" style=""width:640px; border-top: 1px solid #B0BEC5;"">
                  <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""border:none; display:block; width:640px;"" width=""640"">
                     <tr>
                        <td width=""640"" style=""width:640px;"">
                           <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""640"" style=""border:none; width:640px;"">
                              <tr>
                                 <td height=""20"" style=""height:20px;""></td>
                              </tr>
                              <tr>
                                 <td align=""left"" width=""640"" style=""color:#707177; font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:left; width:640px;"">
                                    <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""border:none; width:640px;"" width=""640"">
                                       <tr>
                                          <td align=""left"" width=""640"" style=""font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:left; width:640px;"">
                                             <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""border:none;width:640px"" width=""640"">
                                                <tr>
                                                   <td width=""640"" style=""color:#707177;font-family:Arial,Helvetica,sans-serif;font-size:12px;text-align:left;width:640px"">
                                                      {appName}
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td height=""10"" style=""height:10px""></td>
                                                </tr>
                                                <tr>
                                                   <td width=""640"" style=""color:#707177;font-family:Arial,Helvetica,sans-serif;font-size:12px;text-align:left;width:640px"">
                                                      <a href=""{hostLink}"" target=""_blank"" title=""{appName}"" style=""color:#000000; text-decoration:underline;"">{hostName}</a>
                                                      <span style=""padding: 5px;"">|</span>
                                                      <a href=""{regulationsLink}"" target=""_blank"" style=""color:#000000; text-decoration:underline;"">Regulamin</a>
                                                      <span style=""padding: 5px;"">|</span>
                                                      <a href=""{policyLink}"" target=""_blank"" style=""color:#000000; text-decoration:underline;"">Polityka prywatności</a>
                                                      <span style=""padding: 5px;"">|</span>
                                                      <a href=""{cookieLink}"" target=""_blank"" style=""color:#000000; text-decoration:underline;"">Ciasteczka</a>
                                                   </td>
                                                </tr>
                                             </table>
                                          </td>
                                       </tr>
                                    </table>
                                 </td>
                              </tr>
                              <tr>
                                 <td height=""24"" style=""height:24px;""></td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
            <!-- END OF FOOTER 1-->
            <!-- FOOTER 2 -->
            <tr>
               <td width=""640"" style=""width:640px;"">
                  <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""border:none; display:block; width:640px;"" width=""640"">
                     <tr>
                        <td height=""10"" style=""height:10px;""></td>
                     </tr>
                     <tr>
                        <td width=""640"" style=""color:#707177; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:left; width:640px;"">
                           <span style=""color:#707177; font-size:12px; font-family:Arial, Helvetica, sans-serif; text-align:left;"">
                              Ta wiadomość e-mail została wygenerowana automatycznie. Prosimy na nią nie odpowiadać. <br />Wasko S.A. © 2019. Wszelkie prawa zastrzeżone.
                           </span>
                        </td>
                     </tr>
                     <tr>
                        <td height=""10"" style=""height:10px;""></td>
                     </tr>
                  </table>
               </td>
            </tr>
            <!-- END OF FOOTER 2-->
         </table>
         <!-- END OF WRAPPER-->
         </body>
         </html>";
   }
}
