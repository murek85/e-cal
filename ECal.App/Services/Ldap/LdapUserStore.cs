﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using ECal.App.Contexts;
using ECal.Model.Ldap;

namespace ECal.App.Services.Ldap
{
	public class LdapUserStore : UserStore<LdapUser>
	{
		public LdapUserStore(ApplicationDbContext context, IdentityErrorDescriber describer = null) : base(context, describer)
		{
		}
	}
}
