﻿using ECal.Model.Ldap;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace ECal.App.Services.Ldap
{
	public class LdapSignInManager : SignInManager<LdapUser>
	{
		public LdapSignInManager(LdapUserManager ldapUserManager,
			IHttpContextAccessor contextAccessor,
			IUserClaimsPrincipalFactory<LdapUser> claimsFactory,
			IOptions<IdentityOptions> optionsAccessor,
			ILogger<LdapSignInManager> logger,
			IAuthenticationSchemeProvider schemes,
			IUserConfirmation<LdapUser> confirmation) : base(ldapUserManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes, confirmation)
		{

		}

		public override async Task<SignInResult> PasswordSignInAsync(string userName, string password, bool rememberMe, bool lockoutOnFailure)
		{
			LdapUser user = await UserManager.FindByNameAsync(userName);
			if (user == null)
			{
				return SignInResult.Failed;
			}

			return await PasswordSignInAsync(user, password, rememberMe, lockoutOnFailure);
		}
	}
}
