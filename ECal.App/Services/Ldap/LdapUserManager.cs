﻿using ECal.Business.Common;
using ECal.Model.Ldap;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ECal.App.Services.Ldap
{
	public class LdapUserManager : UserManager<LdapUser>
	{
		private readonly ILdapManager _ldapManager;

		public LdapUserManager(ILdapManager ldapManager,
			IUserStore<LdapUser> store,
			IOptions<IdentityOptions> optionsAccessor,
			IPasswordHasher<LdapUser> passwordHasher,
			IEnumerable<IUserValidator<LdapUser>> userValidators,
			IEnumerable<IPasswordValidator<LdapUser>> passwordValidators,
			ILookupNormalizer keyNormalizer,
			IdentityErrorDescriber errors,
			IServiceProvider services,
			ILogger<LdapUserManager> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
		{
			_ldapManager = ldapManager;
		}

		public Task<LdapUser> FindByObjectGuidAsync(string objectGuid)
		{
			return Task.FromResult(_ldapManager.GetUserByObjectGuid(objectGuid));
		}

		public Task<LdapUser> FindBySAMAccountNameAsync(string samAccountName)
		{
			return Task.FromResult(_ldapManager.GetUserBySAMAccountName(samAccountName));
		}

		public Task<LdapUser> FindByUserPrincipalNameAsync(string userPrincipalName)
		{
			return Task.FromResult(_ldapManager.GetUserByUserPrincipalName(userPrincipalName));
		}

		public override async Task<bool> CheckPasswordAsync(LdapUser user, string password)
		{
			return _ldapManager.Authenticate(user.DistinguishedName, password);
		}

		public override Task<LdapUser> FindByIdAsync(string userId)
		{
			return Task.FromResult(_ldapManager.GetUserByObjectGuid(userId));
		}

		public override Task<LdapUser> FindByNameAsync(string userName)
		{
			return Task.FromResult(_ldapManager.GetUserBySAMAccountName(userName));
		}

		public override Task<string> GetEmailAsync(LdapUser user)
		{
			return base.GetEmailAsync(user);
		}

		public override Task<string> GetUserIdAsync(LdapUser user)
		{
			return base.GetUserIdAsync(user);
		}

		public override Task<string> GetUserNameAsync(LdapUser user)
		{
			return base.GetUserNameAsync(user);
		}

		public override Task<LdapUser> GetUserAsync(ClaimsPrincipal claimsPrincipal)
		{
			return base.GetUserAsync(claimsPrincipal);
		}

		public override IQueryable<LdapUser> Users => _ldapManager.GetAllUsers().AsQueryable();
	}
}
