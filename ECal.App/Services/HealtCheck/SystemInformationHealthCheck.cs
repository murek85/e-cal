﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace ECal.App.Services.HealtCheck
{
	public class SystemInformationMetrics
	{
		public string NameProcessor;
	}

	public class SystemInformationMetricsClient
	{
		public SystemInformationMetrics GetMetrics()
		{
			SystemInformationMetrics metrics;

			if (!IsUnix())
			{
				metrics = GetWindowsMetrics();
			}
			else
			{
				metrics = GetLinuxMetrics();
			}

			return metrics;
		}

		private bool IsUnix()
		{
			var isUnix = RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

			return isUnix;
		}

		private SystemInformationMetrics GetWindowsMetrics()
		{
			var output = string.Empty;
			var info = new ProcessStartInfo
			{
				FileName = "wmic",
				Arguments = "CPU get Name /Value",
				RedirectStandardOutput = true
			};

			using (var process = Process.Start(info))
			{
				output = process.StandardOutput.ReadToEnd();
			}

			var lines = output.Trim().Split("\n");
			var nameProcessor = lines[0].Split("=", System.StringSplitOptions.RemoveEmptyEntries);

			var metrics = new SystemInformationMetrics
			{
				NameProcessor = nameProcessor[1]
			};
			return metrics;
		}

		private SystemInformationMetrics GetLinuxMetrics()
		{
			var metrics = new SystemInformationMetrics();
			return metrics;
		}
	}
	public class SystemInformationHealthCheck : IHealthCheck
	{
		public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
		{
			var data = new Dictionary<string, object>();
			var client = new SystemInformationMetricsClient();
			var metrics = client.GetMetrics();
			var status = HealthStatus.Healthy;

			var dataProcessor = new Dictionary<string, object>
			{

				{ "name", metrics.NameProcessor },
			};

			data.Add("cpu", dataProcessor);

			var result = new HealthCheckResult(status, null, null, data);
			return await Task.FromResult(result);
		}
	}
}
