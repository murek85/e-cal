﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace ECal.App.Services.HealtCheck
{
	public class ProcessorMetrics
	{
		public string Name;
		public double Used;
		public double PercentUsed;
	}

	public class ProcessorMetricsClient
	{
		public ProcessorMetrics GetMetrics()
		{
			ProcessorMetrics metrics;

			if (!IsUnix())
			{
				metrics = GetWindowsMetrics();
			}
			else
			{
				metrics = GetLinuxMetrics();
			}

			return metrics;
		}

		private bool IsUnix()
		{
			var isUnix = RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

			return isUnix;
		}

		private ProcessorMetrics GetWindowsMetrics()
		{
			var output = string.Empty;
			var info = new ProcessStartInfo
			{
				FileName = "wmic",
				Arguments = "CPU get Name,LoadPercentage /Value",
				RedirectStandardOutput = true
			};

			using (var process = Process.Start(info))
			{
				output = process.StandardOutput.ReadToEnd();
			}

			var lines = output.Trim().Split("\n");
			var loadPercentage = lines[0].Split("=", System.StringSplitOptions.RemoveEmptyEntries);
			var nameProcessor = lines[1].Split("=", System.StringSplitOptions.RemoveEmptyEntries);

			var metrics = new ProcessorMetrics
			{
				Name = nameProcessor[1],
				Used = Math.Round(double.Parse(loadPercentage[1]), 0),
				PercentUsed = Math.Round(double.Parse(loadPercentage[1]), 0)
			};
			return metrics;
		}

		private ProcessorMetrics GetLinuxMetrics()
		{
			var metrics = new ProcessorMetrics();
			return metrics;
		}
	}

	public class SystemProcessorHealthCheck : IHealthCheck
	{
		public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
		{
			var data = new Dictionary<string, object>();
			var client = new ProcessorMetricsClient();
			var metrics = client.GetMetrics();
			var status = HealthStatus.Healthy;

			var percentUsed = metrics.PercentUsed;
			if (percentUsed > 75)
			{
				status = HealthStatus.Degraded;
			}

			if (percentUsed > 90)
			{
				status = HealthStatus.Unhealthy;
			}

			var dataProcessor = new Dictionary<string, object>
			{
				{ "name", metrics.Name },
				{ "used", metrics.Used },
				{ "percentUsed", metrics.PercentUsed },
				{ "status", status }
			};

			data.Add($"cpu", dataProcessor);

			var result = new HealthCheckResult(status, null, null, data);
			return await Task.FromResult(result);
		}
	}
}
