﻿using ECal.App.Extensions;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace ECal.App.Services.HealtCheck
{
	public class DiskMetrics
	{
		public string Name;
		public double Total;
		public double Used;
		public double Free;
		public double PercentUsed;
	}

	public class DiskMetricsClient
	{
		public DiskMetrics GetMetrics(string driveName)
		{
			DiskMetrics metrics;

			if (!IsUnix())
			{
				metrics = GetWindowsMetrics(driveName);
			}
			else
			{
				metrics = GetLinuxMetrics();
			}

			return metrics;
		}

		private bool IsUnix()
		{
			var isUnix = RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

			return isUnix;
		}

		private DiskMetrics GetWindowsMetrics(string driveName)
		{
			DriveInfo drive = new DriveInfo(driveName);

			var metrics = new DiskMetrics
			{
				Name = drive.Name,
				Total = drive.TotalSize / (1024 * 1024),
				Free = drive.TotalFreeSpace / (1024 * 1024),
				Used = (drive.TotalSize - drive.TotalFreeSpace) / (1024 * 1024)
			};
			metrics.PercentUsed = Math.Round((100 * metrics.Used / metrics.Total), 0);
			return metrics;
		}

		private DiskMetrics GetLinuxMetrics()
		{
			var metrics = new DiskMetrics();
			return metrics;
		}
	}
	public class SystemDiskHealthCheck : IHealthCheck
	{
		public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
		{
			var data = new Dictionary<string, object>();
			var client = new DiskMetricsClient();
			var status = HealthStatus.Healthy;

			List<DriveInfo> allDrives = DriveInfo.GetDrives().Where(drive => drive.DriveType == DriveType.Fixed).ToList();
			foreach (var (driveInfo, index) in allDrives.WithIndex())
			{
				var metrics = client.GetMetrics(driveInfo.Name);

				var percentUsed = metrics.PercentUsed;
				if (percentUsed > 75)
				{
					status = HealthStatus.Degraded;
				}

				if (percentUsed > 90)
				{
					status = HealthStatus.Unhealthy;
				}

				var dataDrive = new Dictionary<string, object>
				{
					{ "name", metrics.Name },
					{ "total", metrics.Total },
					{ "free", metrics.Free },
					{ "used", metrics.Used },
					{ "percentUsed", metrics.PercentUsed },
					{ "status", status }
				};

				data.Add($"disk{index}", dataDrive);
			}

			var result = new HealthCheckResult(status, null, null, data);
			return await Task.FromResult(result);
		}
	}
}
