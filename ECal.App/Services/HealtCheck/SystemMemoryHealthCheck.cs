﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace ECal.App.Services.HealtCheck
{
	public class MemoryMetrics
	{
		public double Total;
		public double Used;
		public double Free;
		public double PercentUsed;
	}

	public class MemoryMetricsClient
	{
		public MemoryMetrics GetMetrics()
		{
			MemoryMetrics metrics;

			if (!IsUnix())
			{
				metrics = GetWindowsMetrics();
			}
			else
			{
				metrics = GetLinuxMetrics();
			}

			return metrics;
		}

		private bool IsUnix()
		{
			var isUnix = RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

			return isUnix;
		}

		private MemoryMetrics GetWindowsMetrics()
		{
			var output = string.Empty;
			var info = new ProcessStartInfo
			{
				FileName = "wmic",
				Arguments = "OS get FreePhysicalMemory,TotalVisibleMemorySize /Value",
				RedirectStandardOutput = true
			};

			using (var process = Process.Start(info))
			{
				output = process.StandardOutput.ReadToEnd();
			}

			var lines = output.Trim().Split("\n");
			var freeMemomryParts = lines[0].Split("=", System.StringSplitOptions.RemoveEmptyEntries);
			var totalMemomryParts = lines[1].Split("=", System.StringSplitOptions.RemoveEmptyEntries);

			var metrics = new MemoryMetrics
			{
				Total = Math.Round(double.Parse(totalMemomryParts[1]) / 1024, 0),
				Free = Math.Round(double.Parse(freeMemomryParts[1]) / 1024, 0)
			};
			metrics.Used = Math.Round(metrics.Total - metrics.Free, 0);
			metrics.PercentUsed = Math.Round((100 * metrics.Used / metrics.Total), 0);
			return metrics;
		}

		private MemoryMetrics GetLinuxMetrics()
		{
			var metrics = new MemoryMetrics();
			return metrics;
		}
	}

	public class SystemMemoryHealthCheck : IHealthCheck
	{
		public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
		{
			var data = new Dictionary<string, object>();
			var client = new MemoryMetricsClient();
			var metrics = client.GetMetrics();
			var status = HealthStatus.Healthy;

			var percentUsed = metrics.PercentUsed;
			if (percentUsed > 75)
			{
				status = HealthStatus.Degraded;
			}

			if (percentUsed > 90)
			{
				status = HealthStatus.Unhealthy;
			}

			var dataMemory = new Dictionary<string, object>
			{
				{ "total", metrics.Total },
				{ "free", metrics.Free },
				{ "used", metrics.Used },
				{ "percentUsed", metrics.PercentUsed },
				{ "status", status }
			};

			data.Add($"memory", dataMemory);

			var result = new HealthCheckResult(status, null, null, data);
			return await Task.FromResult(result);
		}
	}
}
