﻿using ECal.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECal.App.Services.Identities
{
	public class ApplicationUserManager : UserManager<ApplicationUser>
	{
		public ApplicationUserManager(IUserStore<ApplicationUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<ApplicationUser> passwordHasher, IEnumerable<IUserValidator<ApplicationUser>> userValidators, IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<ApplicationUser>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
		{
		}

		public async virtual Task<bool> VerifyResetPasswordTokenAsync(ApplicationUser appUser, string token)
		{
			var tokenProvider = Options.Tokens.PasswordResetTokenProvider;
			return await VerifyUserTokenAsync(appUser, tokenProvider, "ResetPassword", token);
		}

		public async virtual Task<bool> VerifyEmailConfirmationTokenAsync(ApplicationUser appUser, string token)
		{
			var tokenProvider = Options.Tokens.EmailConfirmationTokenProvider;
			return await VerifyUserTokenAsync(appUser, tokenProvider, "EmailConfirmation", token);
		}
	}
}
