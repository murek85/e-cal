﻿using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ECal.App.Contexts
{
	public class KeysDbContext : DbContext, IDataProtectionKeyContext
	{
		// A recommended constructor overload when using EF Core 
		// with dependency injection.
		public KeysDbContext(DbContextOptions<KeysDbContext> options)
			: base(options) { }

		// This maps to the table that stores keys.
		public DbSet<DataProtectionKey> DataProtectionKeys { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);

			// Customize the ASP.NET Identity model and override the defaults if needed.
			// For example, you can rename the ASP.NET Identity table names and more.
			// Add your customizations after calling base.OnModelCreating(builder);
		}
	}
}
