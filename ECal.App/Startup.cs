﻿using ECal.App.Contexts;
using ECal.App.Filters;
using ECal.App.Hubs;
using ECal.App.Services.Email;
using ECal.App.Services.HealtCheck;
using ECal.App.Services.Identities;
using ECal.App.Services.Ldap;
using ECal.Business.Common;
using ECal.Business.Managers;
using ECal.Common.Mapping;
using ECal.Data.Common.DB.Applications;
using ECal.Data.Common.DB.Groups.Resources;
using ECal.Data.Common.DB.Groups.Users;
using ECal.Data.Common.DB.Permissions;
using ECal.Data.Common.DB.Roles;
using ECal.Data.Common.DB.Users;
using ECal.Data.Common.Ldap;
using ECal.Data.DB;
using ECal.Data.Ldap.LdapUsers;
using ECal.Data.PostgreSqlDB;
using ECal.Model;
using ECal.Model.Ldap;
using ECal.Model.Mappings.Services;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using static OpenIddict.Abstractions.OpenIddictConstants;

namespace ECal.App
{
	public class Startup
	{
		public Startup(IConfiguration configuration, IWebHostEnvironment environment)
		{
			Configuration = configuration;
			Environment = environment;
		}

		public IConfiguration Configuration { get; }
		public IWebHostEnvironment Environment { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services
			   .AddControllers(options =>
			   {
				   options.Filters.Add(typeof(ModelValidationFilter));
			   })
			   .AddNewtonsoftJson(options =>
			   {
				   options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
				   options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
			   })
			   .ConfigureApiBehaviorOptions(options =>
			   {
				   options.SuppressModelStateInvalidFilter = true;
			   });

			services
			   .AddRouting(options =>
			   {
				   options.LowercaseUrls = true;
			   });

			// DB
			string providerAUTHDB = Configuration.GetValue<string>("ProviderAUTHDB");
			services
			   .AddDbContext<ApplicationDbContext>(options => _ = providerAUTHDB switch
			   {
				   "Npgsql" => (options.UseNpgsql(Configuration.GetConnectionString("AUTHDB")), options.UseOpenIddict()),
				   "Sqlite" => (options.UseSqlite(Configuration.GetConnectionString("AUTHDB")), options.UseOpenIddict()),
				   _ => throw new Exception($"Unsupported provider for APPLICATION: {providerAUTHDB}")
			   });

			services
			   .AddDbContext<KeysDbContext>(options => _ = providerAUTHDB switch
			   {
				   "Npgsql" => options.UseNpgsql(Configuration.GetConnectionString("AUTHDB")),
				   "Sqlite" => options.UseSqlite(Configuration.GetConnectionString("AUTHDB")),
				   _ => throw new Exception($"Unsupported provider for KEYS: {providerAUTHDB}")
			   });

			services
			   .AddDataProtection()
			   .PersistKeysToDbContext<KeysDbContext>();

			services
			   .Configure<DataProtectionTokenProviderOptions>(options =>
			   {
				   options.TokenLifespan = TimeSpan.FromHours(Configuration.GetValue<int>("DataProtection:TokenLifespan"));
			   });

			services
			   .Configure<IdentityOptions>(options =>
			   {
				   options.ClaimsIdentity.UserNameClaimType = Claims.Name;
				   options.ClaimsIdentity.UserIdClaimType = Claims.Subject;
				   options.ClaimsIdentity.RoleClaimType = Claims.Role;
			   });

			services
			   .AddIdentity<ApplicationUser, IdentityRole<Guid>>(options =>
			   {
				   options.Password.RequireDigit = true;
				   options.Password.RequiredLength = 6;
				   options.Password.RequireNonAlphanumeric = true;
				   options.Password.RequireUppercase = false;
				   options.Password.RequireLowercase = true;
				   options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(Configuration.GetValue<int>("Identity:Lockout:DefaultLockoutTimeSpan"));
				   options.Lockout.MaxFailedAccessAttempts = Configuration.GetValue<int>("Identity:Lockout:MaxFailedAccessAttempts");
				   options.User.RequireUniqueEmail = false;
			   })
			   .AddEntityFrameworkStores<ApplicationDbContext>()
			   .AddUserManager<ApplicationUserManager>()
			   .AddSignInManager<ApplicationSignInManager>()
			   .AddDefaultTokenProviders();

			services.AddIdentityCore<LdapUser>()
			   .AddUserManager<LdapUserManager>()
			   .AddSignInManager<LdapSignInManager>()
			   .AddUserStore<LdapUserStore>();

			services
			   .AddOpenIddict()
			   .AddCore(options =>
			   {
				   options.UseEntityFrameworkCore()
					  .UseDbContext<ApplicationDbContext>();
			   })
			   .AddServer(options =>
			   {
				   options.Configure(configuration =>
				{
					configuration.DisableAuthorizationStorage = Configuration.GetValue<bool>("OpendIddict:DisableAuthorizationStorage");
					configuration.DisableTokenStorage = Configuration.GetValue<bool>("OpendIddict:DisableTokenStorage");
				});

				   options.AddEphemeralSigningKey()
				   .AddEphemeralEncryptionKey()
				   .DisableAccessTokenEncryption();

				   options.RegisterScopes(Scopes.OpenId, Scopes.Email, Scopes.Profile, Scopes.Roles, Scopes.OfflineAccess);

				   options
				   .AllowAuthorizationCodeFlow()
				   .AllowImplicitFlow()
				   .AllowPasswordFlow()
				   .AllowRefreshTokenFlow();

				   options
				   .SetRevocationEndpointUris(Configuration.GetValue<string>("OpendIddict:RevokeTokenEndpoint"))
				   .SetAuthorizationEndpointUris(Configuration.GetValue<string>("OpendIddict:AuthorizationEndpoint"))
				   .SetIntrospectionEndpointUris(Configuration.GetValue<string>("OpendIddict:IntrospectionEndpoint"))
				   .SetLogoutEndpointUris(Configuration.GetValue<string>("OpendIddict:LogoutEndpoint"))
				   .SetTokenEndpointUris(Configuration.GetValue<string>("OpendIddict:TokenEndpoint"), Configuration.GetValue<string>("OpendIddict:WindowsEndpoint"))
				   .SetUserinfoEndpointUris(Configuration.GetValue<string>("OpendIddict:UserinfoEndpoint"));

				   options.SetAuthorizationCodeLifetime(TimeSpan.FromMinutes(Configuration.GetValue<int>("OpendIddict:AuthorizationCodeLifetime")))
				   .SetAccessTokenLifetime(TimeSpan.FromMinutes(Configuration.GetValue<int>("OpendIddict:AccessTokenLifetime")))
				   .SetIdentityTokenLifetime(TimeSpan.FromMinutes(Configuration.GetValue<int>("OpendIddict:IdentityTokenLifetime")))
				   .SetRefreshTokenLifetime(TimeSpan.FromDays(Configuration.GetValue<int>("OpendIddict:RefreshTokenLifetime")));

				   options.UseAspNetCore()
				   .EnableAuthorizationEndpointPassthrough()
				   .EnableLogoutEndpointPassthrough()
				   .EnableTokenEndpointPassthrough()
				   .EnableUserinfoEndpointPassthrough();

				   if (!Configuration.GetValue<bool>("OpendIddict:DisableHttpsRequirement"))
				   {
					   options.UseAspNetCore()
					   .DisableTransportSecurityRequirement();
				   }

				   options.UseDataProtection();
			   })
			   .AddValidation(options =>
			   {
				   options.SetIssuer(Configuration.GetValue<string>("Introspecion:Issuer"));
				   options.AddAudiences(Configuration.GetValue<string>("Introspecion:AudienceName"));

				   options.UseIntrospection()
				   .SetClientId(Configuration.GetValue<string>("Introspecion:ClientId"))
				   .SetClientSecret(Configuration.GetValue<string>("Introspecion:ClientSecret"));

				   options.UseSystemNetHttp();

				   options.UseAspNetCore();
			   });

			services
			   .AddAuthentication()
			   .AddNegotiate()
			   .AddGoogle(options =>
			   {
				   options.ClientId = Configuration["Authentication:Google:ClientId"];
				   options.ClientSecret = Configuration["Authentication:Google:ClientSecret"];

				   //options.Events.OnRemoteFailure = context =>
				   //{
				   //   if (!String.IsNullOrEmpty(context.Failure.Message))
				   //   {
				   //      context.Response.Redirect(
				   //         $"/error-signin?type={context.Options.SignInScheme}&message={context.Failure.Message}");
				   //      context.HandleResponse();
				   //   }

				   //   return Task.CompletedTask;
				   //};
			   });

			services
			   .Configure<EmailSenderOptions>(Configuration.GetSection("EmailSenderOptions"));

			services
			   .Configure<LdapConfig>(Configuration.GetSection("Ldap"));

			services
			   .Configure<ForwardedHeadersOptions>(options =>
			   {
				   options.ForwardedHeaders =
				   ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
			   });

			//services.Configure<CookiePolicyOptions>(options =>
			//{
			//   options.CheckConsentNeeded = context => true;
			//   options.MinimumSameSitePolicy = SameSiteMode.None;
			//});

			// CORS
			services.AddCors(options =>
			{
				options.AddPolicy("CorsPolicy",
				builder => builder
				   .WithOrigins(Configuration.GetSection("Cors:Origins").Get<string[]>())
				   .AllowAnyMethod()
				   .AllowAnyHeader()
				   .AllowCredentials()
				);
			});

			// Mappers
			services.AddAutoMapper(typeof(ApplicationMap));
			services.AddAutoMapper(typeof(LdapUserMap));
			services.AddAutoMapper(typeof(PermissionMap));
			services.AddAutoMapper(typeof(RoleMap));
			services.AddAutoMapper(typeof(UserMap));
			services.AddAutoMapper(typeof(GroupUserMap));
			services.AddAutoMapper(typeof(GroupResourceMap));

			// Managers
			services.AddScoped<IApplicationsManager, ApplicationsManager>();
			services.AddScoped<ILdapManager, LdapManager>();
			services.AddScoped<IPermissionsManager, PermissionsManager>();
			services.AddScoped<IRolesManager, RolesManager>();
			services.AddScoped<IUsersManager, UsersManager>();
			services.AddScoped<IGroupsUsersManager, GroupsUsersManager>();
			services.AddScoped<IGroupsResourcesManager, GroupsResourcesManager>();
			services.AddScoped<IReportManager, ReportManager>();

			// DAO
			string providerDB = Configuration.GetValue<string>("ProviderDB");

			// RSQL
			_ = providerDB switch
			{
				"Npgsql" => services.AddScoped<IRsqlMapper, PostgresqlDBRsqlMapper>(),
				_ => throw new Exception($"Unsupported provider for DAO: {providerDB}")
			};

			// DAO
			_ = providerDB switch
			{
				"Npgsql" => services.AddScoped<IUsersDAO, Data.DB.Users.UsersDAO>(),
				_ => throw new Exception($"Unsupported provider for DAO: {providerDB}")
			};
			_ = providerDB switch
			{
				"Npgsql" => services.AddScoped<IApplicationsDAO, Data.DB.Applications.ApplicationsDAO>(),
				_ => throw new Exception($"Unsupported provider for DAO: {providerDB}")
			};
			_ = providerDB switch
			{
				"Npgsql" => services.AddScoped<IPermissionsDAO, Data.DB.Permissions.PermissionsDAO>(),
				_ => throw new Exception($"Unsupported provider for DAO: {providerDB}")
			};
			_ = providerDB switch
			{
				"Npgsql" => services.AddScoped<IRolesDAO, Data.DB.Roles.RolesDAO>(),
				_ => throw new Exception($"Unsupported provider for DAO: {providerDB}")
			};
			_ = providerDB switch
			{
				"Npgsql" => services.AddScoped<IGroupsUsersDAO, Data.DB.Groups.Users.GroupsUsersDAO>(),
				_ => throw new Exception($"Unsupported provider for DAO: {providerDB}")
			};
			_ = providerDB switch
			{
				"Npgsql" => services.AddScoped<IGroupsResourcesDAO, Data.DB.Groups.Resources.GroupsResourcesDAO>(),
				_ => throw new Exception($"Unsupported provider for DAO: {providerDB}")
			};

			// LDAP
			services.AddScoped<ILdapDAO, LdapDAO>();

			// Services
			services.AddTransient<IEmailSender, EmailSender>();

			_ = providerDB switch
			{
				"Npgsql" => services.AddScoped<DBDatabaseContainer>(s => new DBDatabaseContainer(Configuration.GetConnectionString("DB"))),
				_ => throw new Exception($"Unsupported provider for SERVICES: {providerDB}")
			};

			// Exception
			services.AddScoped<ApiExceptionFilter>();

			// RSQL
			services.AddScoped<RsqlMapperInitializer>();

			// SignalR
			services.AddSignalR(options =>
			{
				options.EnableDetailedErrors = true;
			});

			// Swagger
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo
				{
					Title = "AUTH",
					Version = "v1",
					Contact = new OpenApiContact
					{
						Name = "Marcin Murawski",
						Email = "marcinmur@gmail.com"
					}
				});

				// XML Documentation
				var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
				var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
				c.IncludeXmlComments(xmlPath);

				c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
				{
					Name = "oauth2",
					Type = SecuritySchemeType.OAuth2,
					Flows = new OpenApiOAuthFlows
					{
						Password = new OpenApiOAuthFlow
						{
							TokenUrl = new Uri(Configuration.GetValue<string>("Swagger:TokenUrl")),
							Scopes = Configuration.GetSection("Swagger:Scopes").Get<string[]>().ToDictionary(value => value, value => value)
						}
					},
					Description = "Authorization header using OAUTH2"
				});
				c.AddSecurityRequirement(new OpenApiSecurityRequirement
			   {
			   {
				  new OpenApiSecurityScheme
				  {
					 Reference = new OpenApiReference
					 {
						Type = ReferenceType.SecurityScheme,
						Id = "oauth2"
					 }
				  },
				  Configuration.GetSection("Swagger:Scopes").Get<string[]>()
			   }
			   });

				c.AddSecurityDefinition("bearer", new OpenApiSecurityScheme
				{
					In = ParameterLocation.Header,
					Name = "Authorization",
					Description = "Authorization header using BEARER",
					Type = SecuritySchemeType.ApiKey,
					Scheme = "Bearer",
				});
				c.AddSecurityRequirement(new OpenApiSecurityRequirement()
			   {
			   {
				  new OpenApiSecurityScheme {
					 Reference = new OpenApiReference
					 {
						Type = ReferenceType.SecurityScheme,
						Id = "bearer"
					 },
					 Scheme = "bearer",
					 Name = "Bearer",
					 In = ParameterLocation.Header,
					 Type = SecuritySchemeType.ApiKey
				  },
				  new List<string>()
			   }
			   });
			});

			// Api Version
			services.AddApiVersioning(c =>
			{
				c.ReportApiVersions = true;
				c.AssumeDefaultVersionWhenUnspecified = true;
				c.DefaultApiVersion = new ApiVersion(1, 0);
			});

			// Health Checks
			services.AddHealthChecks()
				.AddNpgSql(Configuration.GetConnectionString("DB"))
				.AddCheck<SystemMemoryHealthCheck>("memory")
				.AddCheck<SystemProcessorHealthCheck>("processor")
				.AddCheck<SystemDiskHealthCheck>("disk")
				.AddCheck<SystemInformationHealthCheck>("information");

			services.AddHealthChecksUI(settings =>
			   {
				   settings.UseApiEndpointHttpMessageHandler(sp =>
				   {
					   return new HttpClientHandler
					   {
						   ClientCertificateOptions = ClientCertificateOption.Manual,
						   ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, cetChain, policyErrors) => { return true; }
					   };
				   });
			   }).AddInMemoryStorage();

			services.AddHostedService<Worker>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime, RsqlMapperInitializer rsqlMapperInitializer)
		{
			app.UseCors("CorsPolicy");

			app.UseForwardedHeaders(new ForwardedHeadersOptions
			{
				ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
			});

			if (env.IsDevelopment())
			{
				app.UseExceptionHandler("/error");
			}

			if (Configuration.GetValue<bool>("Swagger:Enabled"))
			{
				app.UseSwagger();
				app.UseSwaggerUI(c =>
				{
					c.SwaggerEndpoint("/swagger/v1/swagger.json", "Wasko.IdentityServer v1");
				});
			}

			if (Configuration.GetValue<bool>("OpendIddict:HttpsRequired"))
			{
				app.UseHttpsRedirection();
			}

			app.UseForwardedHeaders();
			app.UseRouting();
			app.UseAuthentication();
			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
				endpoints.MapDefaultControllerRoute();
				endpoints.MapHub<BroadcastHub>("/api/hubs/broadcast");
				endpoints.MapHealthChecksUI(options =>
				{
					options.AddCustomStylesheet("health-metrics.css");
					options.UIPath = "/healthz-ui";
				});
				endpoints.MapHealthChecks("/healthz", new HealthCheckOptions()
				{
					Predicate = _ => true,
					ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
				});
				endpoints.MapHealthChecks("/health-metrics", new HealthCheckOptions()
				{
					Predicate = _ => true,
					ResponseWriter = CustomResponseWriter,
					ResultStatusCodes =
			   {
				  [HealthStatus.Healthy] = StatusCodes.Status200OK,
				  [HealthStatus.Degraded] = StatusCodes.Status200OK,
				  [HealthStatus.Unhealthy] = StatusCodes.Status200OK
			   }
				});
			});

			rsqlMapperInitializer.Initialize();
		}

		private static Task CustomResponseWriter(HttpContext context, HealthReport healthReport)
		{
			context.Response.ContentType = "application/json";

			var result = JsonConvert.SerializeObject(new
			{
				entries = healthReport.Entries.Select(e => new
				{
					status = e.Value.Status,
					key = e.Key,
					value = e.Value.Data.Select(d => new { key = d.Key, value = d.Value })
				})
			});
			return context.Response.WriteAsync(result);

		}
	}
}
