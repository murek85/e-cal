﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ECal.App.Filters
{
	public class ModelValidationFilter : IActionFilter
	{
		public void OnActionExecuted(ActionExecutedContext context)
		{

		}

		public void OnActionExecuting(ActionExecutingContext context)
		{
			if (!context.ModelState.IsValid)
			{
				var result = new ContentResult();
				string content = JsonConvert.SerializeObject(new ApiError(context.ModelState), new JsonSerializerSettings
				{
					ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
					ContractResolver = new CamelCasePropertyNamesContractResolver()
				});
				result.Content = content;
				result.ContentType = "application/json";

				context.HttpContext.Response.StatusCode = 400;
				context.Result = result;
			}
		}
	}
}
