﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace ECal.App.Filters
{
	public class ApiExceptionFilter : ExceptionFilterAttribute
	{
		public override void OnException(ExceptionContext context)
		{
			ApiError error = null;
			if (context.Exception is ApiException)
			{
				var ex = context.Exception as ApiException;
				error = new ApiError
				{
					Message = ex.Message,
					Errors = ex.Errors
				};
				context.HttpContext.Response.StatusCode = ex.StatusCode;
			}
			else if (context.Exception is UnauthorizedAccessException)
			{
				error = new ApiError("Unauthorized Access");
				context.HttpContext.Response.StatusCode = 401;
			}
			else
			{
				error = new ApiError
				{
					Message = context.Exception.GetBaseException().Message
				};
				context.HttpContext.Response.StatusCode = 500;
			}

			context.Result = new JsonResult(error);

			base.OnException(context);
		}
	}
}
