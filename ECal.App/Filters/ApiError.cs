﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace ECal.App.Filters
{
	public class ApiError
	{
		public string Type { get; set; }
		public string Code { get; set; }
		public string Message { get; set; }
		public object Data { get; set; }
		public IList<ValidationError> Errors { get; set; }

		public ApiError() { }

		public ApiError(string type, string code, string message)
		{
			Type = type;
			Code = code;
			Message = message;
		}

		public ApiError(string type, string code, string message, object data)
		{
			Type = type;
			Code = code;
			Message = message;
			Data = data;
		}

		public ApiError(string message)
		{
			Message = message;
		}

		public ApiError(ModelStateDictionary modelState)
		{
			Errors = modelState.Keys
				.SelectMany(key => modelState[key].Errors
					.Select(err => new ValidationError(key, err.ErrorMessage)))
				.ToList();
		}
	}
}
