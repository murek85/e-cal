using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ECal.App
{
	public class Program
	{
		public static void Main(string[] args)
		{
			CreateHostBuilder(args).Build().Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureLogging((ctx, builder) =>
				{
					builder.AddFilter("Microsoft", LogLevel.Debug);
					builder.AddFilter("System", LogLevel.Debug);
					builder.AddFilter("Console", LogLevel.Debug);
					builder.AddFile(options => options.RootPath = ctx.HostingEnvironment.ContentRootPath);
				})
				.ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>());
	}
}
